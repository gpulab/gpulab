// ==========================================================================
// GPUlab main file
// ==========================================================================
// (C)opyright: 2010
//
// DTU Compute - http://compute.dtu.dk
// GPULab      - http://gpulab.imm.dtu.dk/
//
// Author: Stefan Lemvig Glimberg
// Email:  slgl@imm.dtu.dk
//
// Author: Allan P. Engsig-Karup
// Email:  apek@imm.dtu.dk
//
// Date: October, 2012
// ==========================================================================

#define COPYRIGHT_STATEMENT "\n==========================================================================\n\
The GPUlab Library\n\
==========================================================================\n\
(C)opyright: 2010\n\
\n\
Author: Stefan L. Glimberg\n\
Email:  slgl@imm.dtu.dk\n\
\n\
Author: Allan P. Engsig-Karup\n\
Email:  apek@imm.dtu.dk\n\
\n\
DTU Compute - http://compute.dtu.dk \n\
GPULab      - http://gpulab.imm.dtu.dk \n\
==========================================================================\n\n\n"

#ifndef GPULAB_GPULAB_H
#define GPULAB_GPULAB_H

#include <gpulab/mpi.h>
#include <gpulab/config.h>
#include <gpulab/grid.h>
#include <gpulab/io/log.h>
#include <gpulab/io/config_file_reader.h>
#include <gpulab/type_traits.h>

namespace gpulab
{

	bool init(int &argc, char *argv[])
	{
		static bool gpulab_is_initialized = false;

		if(gpulab_is_initialized)
			return true;

		try
		{
			
			// Mpi rank and size
			int mpi_rank, mpi_size = 0;
			int flag;

			// Test to see if we are running openmpi or mvapich2
			// If none, then it is probably not a MPI program and rank+size remains 0
			if(getenv( "OMPI_COMM_WORLD_RANK" ) != NULL)
			{
				mpi_rank = atoi( getenv( "OMPI_COMM_WORLD_RANK" ) );
			}
			else if( getenv( "MV2_COMM_WORLD_LOCAL_RANK" ) != NULL)
			{
				// http://mvapich.cse.ohio-state.edu/support/user_guide_mvapich2-1.9.html#x1-840006.20
				mpi_rank = atoi( getenv( "MV2_COMM_WORLD_LOCAL_RANK" ) );
				printf("MV2_USE_CUDA");
			}
			else
			{
				// Init MPI now to get rank and size
				MPI_Initialized(&flag);
				if(!flag)
				{
					MPI_Init(&argc, &argv);
				}
				MPI_Comm_dup(MPI_COMM_WORLD,&gpulab::GPULAB_COMM_WORLD);
				MPI_Comm_size(gpulab::GPULAB_COMM_WORLD,&mpi_size);
				MPI_Comm_rank(gpulab::GPULAB_COMM_WORLD,&mpi_rank);
			}
						
			// Scatter MPI processes to available GPUs
			// Assign GPUs to ranks
			int  *gpuidx = 0;
			int  ngpus = 0;
			char line[1024];
		
			// Test to see if we are running on a Tourge GPU system with MPI nodes
			if(mpi_size > 1)
			{
				FILE *f_ptr;
				char *gpufile;
				char *gpunumstr, *part;

				gpuidx = (int *)calloc(mpi_size, sizeof(int));  // assumes 1 GPU/rank!

				gpufile = getenv("PBS_GPUFILE"); // This is only for Tourque systems
				if(gpufile)
				{
					f_ptr = fopen(gpufile, "r");
					if(f_ptr)
					{
						while ( fgets ( line, sizeof line, f_ptr ) != NULL ) /* read a line */
						{
							/* Loop over the line, using '-' as delimiter, until we
							 * get the last part - that's gpuNN 
							 */
							part = strtok(line, "-");
							while ( part != NULL )
							{
								gpunumstr = part;
								part = strtok(NULL, "-");
							}
							gpunumstr += 3; /* move three chars ("gpu") */
							gpuidx[ngpus] = atoi(gpunumstr);
							ngpus++;
						}
						fclose(f_ptr);
					}
				}
			}

			
			// If GPU info was found, set the device based on gpuidx[rank]
			int cuda_device = 0;
			if(ngpus>0)
			{
				cuda_device = gpuidx[mpi_rank];
			}
			// If there was no GPUs found in Tourge systen, just set device based on rank % No. local GPUs
			else
			{
				cudaGetDeviceCount(&ngpus);
				if(ngpus>0)
					cuda_device = mpi_rank % ngpus;
				else
				{
					printf("There are no CUDA enabled devices, it will cause you trouble!\n");
					throw std::exception();
				}
			}
			// Set device
			cudaSetDevice(cuda_device);

			// In case MPI was not initialized before, do it now
			MPI_Initialized(&flag);
			if(!flag)
			{
				MPI_Init(&argc, &argv);
				MPI_Comm_dup(MPI_COMM_WORLD,&gpulab::GPULAB_COMM_WORLD);
				MPI_Comm_size(gpulab::GPULAB_COMM_WORLD,&mpi_size);
				MPI_Comm_rank(gpulab::GPULAB_COMM_WORLD,&mpi_rank);
			}

			// Create Logging
			gpulab::io::log_manager::instance()->register_logger<gpulab::io::log_default>();
			if(mpi_rank==0) GPULAB_LOG_INF(COPYRIGHT_STATEMENT);

			// Initialize configuration and set log level
			std::string config_filename = (argc>1) ? argv[1] : "config.ini";
			gpulab::io::config_manager::instance(config_filename);
			GPULAB_CONFIG_SET("vGPULAB",1.0);
			std::string lvl;
			GPULAB_CONFIG_GET_3_ARGS("LOG_LVL",&lvl,"INFO");
			gpulab::io::log_manager::instance()->set_level(lvl);

			// Print info
			gethostname(line, 1024);
			cudaGetDeviceCount(&ngpus);
			GPULAB_LOG_INF("MPI process %d of %d, using GPU %d of %d on %s\n",mpi_rank,mpi_size,cuda_device,ngpus,line);

			if (gpuidx) free(gpuidx);

			gpulab_is_initialized = true;
		}
		catch(...)
		{
			printf("ERROR During gpulab initialization\n");
		}
		return gpulab_is_initialized;
	}

	void init()
	{
		char* tmp[1];
		int args = 0;
		init(args,tmp);
	}

	void finalize( void )
	{
		// Make sure all node are ready to finalize
		MPI_Barrier(gpulab::GPULAB_COMM_WORLD);

		// Clean up
		MPI_Comm_free(&gpulab::GPULAB_COMM_WORLD);

		// Finalize MPI
		MPI_Finalize();

		// Delete CUDA context
		cudaDeviceReset();
	}


} // namespace gpulab


#endif // GPULAB_GPULAB_H
