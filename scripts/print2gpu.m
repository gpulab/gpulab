function [ flag ] = print2gpu( filename, p, varargin )

args = struct(varargin{:});

if isfield(args,'precision')
    precision = args.precision;
else
    precision = 'single';
end

if isfield(args,'format')
    binary = strcmp('binary',args.format);
else
    binary = 1;
end


[Ns Nx] = size(p);

% Add extensions
if(~binary)
    if(length(filename)<3 ||~strcmp(filename(end-1:end),'.m'))
        filename = strcat(filename, '.m');
    end
else
    if(length(filename)<5 || ~strcmp(filename(end-3:end),'.bin'))
        filename = strcat(filename, '.bin');
    end
end
% Write to disk
f = fopen(filename,'w');
if(f < 0)
    flag = 0;
    return;
end

p = p'; % We want row order
p = p(:);
N = Nx*Ns;
if(~binary)
    if(N>1024)
        disp('It is not adviseable to print such large vectors to text files.');
    end
    % csvwrite or dlmwrite could do the same.. but leaves a blank line in the end!
    for i=1:N
        fprintf(f,'%0.16f',p(i));
        if(i<N)
            fprintf(f,'\n');
        end
    end
else
    try
        fwrite(f,p,precision);
    catch e
        fprintf('Error: %s\n',e.message);
        fclose(f);
        flag = 0;
        return;
    end
end
fclose(f);

flag = 1;
end

