function [ file, info ] = readgpu( filename, varargin )
%READGPU read a file printed by gpulab
% varargin can be:
%   precision: usually 'single' or 'double'
%   Nx: if you want output reshaped
%   Ny: if you want output reshaped
%   Nz: if you want output reshaped
%   interioronly: if info file contains ghost point info

args = struct(varargin{:});

info = [];
file = [];
dist = 0;

fp = fopen(filename,'r'); % Test if file is there
if(fp==-1)
    % Maybe there is a distributed file
    fp = fopen([filename '.000000'],'r'); % Test if file is there
    dist = 1;
end

if(fp==-1)
    return;
end

% There is a file ready to read, but we can close fp for now
fclose(fp);

% Test if there are multiple files to read
if(dist)
    % Multiple files
    info0 = readinfo([filename '.000000']);
    
    if(info0.R>1)
        warning('Vertical decomposition reads are not supported!');
        return;
    end
    
    for q=0:info0.Q-1
        tmp = [];
        for p=0:info0.P-1
            
            subname = sprintf('%s.%02i%02i%02i',filename,p,q,0);
            [U info] = readfile(subname,args);

            if(~info.found)
                warning(['No info file found for ' subname '! Returns']);
                return;
            end
            % Begin i index
            i = info.ghost0(1)+2;
            if(p==0)
                i = 1;
            end
            % End i index
            I = info.N(1)-info.ghost1(1);
            if(p==info0.P-1)
                I = info.N(1);
            end
            % Interioronly case
            if(info.interioronly)
                i = 1 + (p~=0);
                [tmp, I, tmp2] = size(U);
            end
            
            tmp = [tmp, U(:,i:I,:)];
        end
        
        % Begin j index
        j = info.ghost0(2)+2;
        if(q==0)
            j = 1;
        end
        % End j index
        J = info.N(2)-info.ghost1(2);
        if(q==info0.Q-1)
            J = info.N(2);
        end
        % Interioronly case
        if(info.interioronly)
            j = 1 + (q~=0);
            [J, tmp, tmp] = size(tmp);
        end

        file = [file; tmp(j:J,:,:)];
        %file = [tmp(:,j:J,:), file];
    end
else
    % Just one file
    [file info] = readfile(filename,args);
end

end


function [ file, info ] = readfile( filename, args )

% Read info file
if isfield(args,'info')
    info = args.info;
else
    info = readinfo(filename);
end
file = [];

if isfield(args,'precision')
    info.precision = args.precision;
end

if isfield(args,'Nx')
    info.N(1) = args.Nx;
end

if isfield(args,'Ny')
    info.N(2) = args.Ny;
end

if isfield(args,'Nz')
    info.N(3) = args.Nz;
end

if isfield(args,'interioronly')
    info.interioronly = ~strcmp(num2str(args.interioronly), '0');
else
    info.interioronly = 0;
end

% Open file
fp = fopen(filename,'r');
if(fp<0)
    return;
end

% Read file based on info
N = info.N(1)*info.N(2)*info.N(3);
if(~info.found)
    if(N==1)
        file = fread(fp,info.precision);
    else
        file = fread(fp,N,info.precision);
        file = reshape(file,info.N(1),info.N(2),info.N(3));
    end
else
    file = fread(fp,N,info.precision);
    if(info.N(3)>1)

        file = reshape(file,info.N(1),info.N(2),info.N(3));
%        file = permute(file,[2 1 3]);
    else
        file = reshape(file,info.N(1),info.N(2))';
    end
    
    
    
    
    %file = flipdim(file,2);
    if(info.interioronly)
 %       file = file(1+info.ghost0(2):end-info.ghost1(2) ...
  %                 ,1+info.ghost0(1):end-info.ghost1(1) ...
   %                ,1+info.ghost0(3):end-info.ghost1(3));
        file = file(1+info.ghost0(1):end-info.ghost1(1) ...
                    ,1+info.ghost0(2):end-info.ghost1(2) ...
                    ,1+info.ghost0(3):end-info.ghost1(3));
               
    end
end

fclose(fp);

end


function info = readinfo(filename)

% Defaults
info.found     = 0;
info.precision = 'double';
info.N         = [1 1 1];
info.ghost0    = [0 0 0];
info.ghost1    = [0 0 0];
info.delta     = [0 0 0];
info.P         = 1;
info.Q         = 1;
info.R         = 1;
info.p         = 0;
info.q         = 0;
info.r         = 0;

fpinfo = fopen(strcat(filename,'.info'),'r');
if(fpinfo~=-1)
    info.found = 1;

    try
        % The scan order is important !
        M = textscan(fpinfo,'%s %s','commentStyle', '//');
        M = cell2struct(M{2},M{1});
        
        if(isfield(M,'precision'))
            info.precision = M.precision;
        end
        if(isfield(M,'Nx'))
            info.N(1) = str2double(M.Nx);
        end
        if(isfield(M,'Ny'))
            info.N(2) = str2double(M.Ny);
        end
        if(isfield(M,'Nz'))
            info.N(3) = str2double(M.Nz);
        end
        if(isfield(M,'dx'))
            info.delta(1) = str2double(M.dx);
        end
        if(isfield(M,'dy'))
            info.delta(2) = str2double(M.dy);
        end
        if(isfield(M,'dz'))
            info.delta(3) = str2double(M.dz);
        end
        if(isfield(M,'gx0'))
            info.ghost0(1) = str2double(M.gx0);
        end
        if(isfield(M,'gy0'))
            info.ghost0(2) = str2double(M.gy0);
        end
        if(isfield(M,'gz0'))
            info.ghost0(3) = str2double(M.gz0);
        end
        if(isfield(M,'gx1'))
            info.ghost1(1) = str2double(M.gx1);
        end
        if(isfield(M,'gy1'))
            info.ghost1(2) = str2double(M.gy1);
        end
        if(isfield(M,'gz1'))
            info.ghost1(3) = str2double(M.gz1);
        end
        if(isfield(M,'P'))
            info.P = str2double(M.P);
        end
        if(isfield(M,'Q'))
            info.Q = str2double(M.Q);
        end
        if(isfield(M,'R'))
            info.R = str2double(M.R);
        end
        if(isfield(M,'p'))
            info.p = str2double(M.p);
        end
        if(isfield(M,'q'))
            info.q = str2double(M.q);
        end
        if(isfield(M,'r'))
            info.r = str2double(M.r);
        end
    catch err
        printf('Error reading info file: %s', err.message);
    end
    fclose(fpinfo);
end
end
