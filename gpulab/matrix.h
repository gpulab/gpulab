// ==========================================================================
// Matrix header
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_MATRIX_H
#define GPULAB_MATRIX_H

#include <gpulab/config.h>
#include <gpulab/type_traits.h>
#include <gpulab/vector.h>
#include <gpulab/dispatch/matrix.h>

namespace gpulab
{

	/**
	* Base matrix class, shared between both device and host matrices.
	* Dense format representation.
	* @param T value type
	* @param Major column or row major
	* @param MemorySpace host or device memory
	*/
	template <typename T, typename MemorySpace, typename Major = column_major> 
	class matrix : public gpulab::vector<T, MemorySpace>
	{
	private: 
		typedef gpulab::vector<T,MemorySpace>		super;
		typedef gpulab::matrix<T,MemorySpace,Major>	my_type;
		typedef super								vector_type;
		
	public:
		typedef Major								major_type;
		typedef typename super::iterator			iterator;
		typedef typename super::pointer				pointer;
		typedef typename super::size_type			size_type;
		typedef typename super::value_type			value_type;
		
	protected:
		/**
		* Hide default constructor
		*/
		matrix() : super() {};

	protected:
		size_type m_I;					///< Number of rows
		size_type m_J;					///< Number of columns

	public:

		/**
		* Construct a matrix of size I*I with default T value
		*/
		__host__ __device__
		matrix(size_type I)
			: super(I*I)
			, m_I(I)
			, m_J(I)
		{ };


		/**
		* Construct a matrix of size I*J with value val
		*/
		__host__ __device__
		matrix(size_type I, size_type J, const T& val)
			: super(I*J, val) 
			, m_I(I)
			, m_J(J)
		{};

		/**
		* Construct a matrix of size I*J with default T value
		*/
		__host__ __device__
		matrix(size_type I, size_type J)
			: super(I*J)
			, m_I(I)
			, m_J(J)
		{ };
		
		/**
		* Copy constructor
		*/
		__host__ __device__
		matrix(my_type const& m)
			: super(m)
			, m_I(m.m_I)
			, m_J(m.m_J)
		{ };

		/**
		* Copy constructor from other T 
		*/
		template<typename OtherT, typename OtherMemorySpace>
		__host__ __device__
		matrix(const matrix<OtherT,OtherMemorySpace,Major> &m)
		  : super(m)
		  , m_I(m.rows())
		  , m_J(m.cols())
		{}

		//
		//template <template <typename,typename,typename> class other_matrix, typename other_alloc>
		//matrix_base(other_matrix<T, major, other_alloc> const& m)
		//	: super(m)
		//	, m_I(m.rows())
		//	, m_J(m.cols())
		//{
		//}

		///**
		//* Copy constructor from other allocation and major
		//*/
		//template <template <typename,typename,typename> class other_matrix, typename other_major, typename other_alloc>
		//matrix_base(other_matrix<T, other_major, other_alloc> const& m)
		//	: super(m)
		//	, m_I(m.rows())
		//	, m_J(m.cols())
		//{
		//	ASSERT(m.size() == this->size());

		//	T* from = this->m_first;
		//	if(buy(m_count))
		//	{
		//		change_major(this->m_first, from, m_I, m_J, major(), other_major(), get_allocator());
		//		this->m_allocator.deallocate(from, m_count); // Deallocate the old location
		//	}
		//	else
		//	{
		//		throw ("Bad allocation");
		//	}
		//}

		///**
		//* Deconstructor
		//*/
		//~matrix()
		//{ };

		/**
		* Return number of rows
		*/
		__host__ __device__
		size_type rows() const
		{
			return m_I;
		}

		/**
		* Return number of columns
		*/
		__host__ __device__
		size_type cols() const
		{
			return m_J;
		}

		__host__ __device__
		major_type major() const
		{
			return major_type();
		}

		__host__
		void mult(vector_type const& x, vector_type& b) const
		{
			gpulab::dispatch::matrix::mult(*this, x, b, typename super::memory_space());
		}

		__host__
		bool equals(my_type const& m) const
		{
			return (m.rows() == m_I && m.cols() == m_J && super::equals(m));
		}

		/**
		* Index set operator
		*/
		__host__
		void operator()(size_type i, size_type j, T val)
		{
			ASSERT(i < this->m_I && j < this->m_J);
			(*this)[major_type::index(i,j,this->m_I,this->m_J)] = val;
		}

		__host__
		T & operator()(size_type i, size_type j)
		{
			return (*this)[major_type::index(i,j,this->m_I,this->m_J)];
		}
		
		__host__
		T const& operator()(size_type i, size_type j) const
		{
			return (*this)[major_type::index(i,j,this->m_I,this->m_J)];
		}

		/**
		* Set all values in the matrix
		* @param value to be set
		*/
		__host__
		void set(T value)
		{
			ASSERT(!"Not Implemented");
		}

		/**
		* Set all values in the matrix
		* @param value to be set
		*/
		__host__
		void set_diag(T value)
		{
			ASSERT(!"Not Implemented");
		}

		/**
		* Set all diagonal values in the matrix
		* @param v vector of values to be set
		*/
		__host__
		void set_diag(vector_type const& v)
		{
			ASSERT(v.size() == min(this->m_J, this->m_I));
			ASSERT(!"Not Implemented");
		}

		/**
		* Set all diagonal values in the matrix
		* @param m matrix of diagonal values to be set
		*/
		__host__
		void set_diag(my_type const& m)
		{
			ASSERT(m.rows() == this->m_I && m.cols() == this->m_J);
			ASSERT(!"Not Implemented");
		}

		/**
		* Set first n columns to the values in the list of vectors v
		* @param vs vectors to be set
		* @param n number of vectors in vs
		*/
		__host__
		void set_cols(vector_type* vs, size_type n)
		{
			ASSERT(n <= this->m_J);
			for(unsigned int j=0; j<n; ++j, vs++)
			{
				set_col(*vs, j);
			}
		}

		/**
		* Set column number n to vector v
		* @param v vector to be set
		* @param n column number, zero indexed
		*/
		__host__
		void set_col(vector_type const& v, size_type n)
		{
			ASSERT(n < this->m_J);
			ASSERT(v.size() == this->m_I);
			size_type col = major_type::index((size_type)0,n,this->m_I,this->m_J);
			thrust::copy(v.begin(), v.end(), this->begin()+col);
		}

		/**
		* BLASS routines start
		*/

		/**
		* Scale
		*/
		__host__
		void scal(T a)
		{
			ASSERT(!"Not Implemented");
		}

		///**
		//* Copy
		//*/
		//void copy(my_type const& x)
		//{
		//}

		/**
		* Norm_1. max(A_ij)_i=0,I-1
		*/
		T nrm1() const
		{
			ASSERT(!"Not Implemented");
			return T(); // TODO:
		}

		/**
		* Norm_2. 
		*/
		T nrm2() const
		{
			ASSERT(!"Not Implemented");
			return T(); // TODO:
		}

		/**
		* Norm_infinity. 
		*/
		T nrmi() const
		{
			ASSERT(!"Not Implemented");
			return T(); // TODO:
		}

		void transpose()
		{
			// TODO: There is memory leak here!
			ASSERT(!"Memory leak, how do we fix?")
			 
			my_type* other = new my_type(this->m_J,this->m_I);
			gpulab::dispatch::matrix::transpose(*other, *this, typename super::memory_space());
			*this = *other;
		}

		void flip()
		{
			size_type tmp = this->m_J;
			this->m_J = this->m_I;
			this->m_I = tmp;
		}


		/**
		* Private helpers
		*/
	private:

	};

	//
	///**
	//* Create a new matrix of size I*J and diagonal values of val
	//* Remember to delete after use.
	//* @param T value type
	//* @param size_type size type
	//* @param I number of rows
	//* @param J number of columns
	//* @param val diagonal values
	//*/
	//template <typename T, typename size_type>
	//static matrix<T>* diag(const size_type I, const size_type J, T val)
	//{
	//	matrix<T>* m = new matrix<T>(I,J,0.f);
	//	m->set_diag(val);
	//	return m;
	//}

	///**
	//* Create a new matrix of size I*J with random values.
	//* TODO: this one is slow because it sets one element value at a time,
	//* thus it should only be used for testing.
	//* Remember to delete after use.
	//* @param T value type
	//* @param R randomizer
	//* @param size_type size type
	//* @param I number of rows
	//* @param J number of columns
	//* @param val diagonal values
	//*/
	//template <typename T, typename R, typename size_type>
	//static device::matrix<T>* rand(const size_type I, const size_type J, R randomizer = rand)
	//{
	//	device::matrix<T>* m = new device::matrix<T>(I,J);
	//	for(size_type i=0; i<m->rows(); ++i)
	//	{
	//		for(size_type j=0; j<m->cols(); ++j)
	//		{
	//			(*m)[i][j] = (T)randomizer() / (T)RAND_MAX;
	//		}
	//	}
	//	return m;
	//}

	///**
	//* Create a new identity matrix of size I*J
	//* Remember to delete after use.
	//* @param T value type
	//* @param size_type size type
	//* @param I number of rows
	//* @param J number of columns
	//*/
	//template <typename T, typename size_type>
	//static matrix<T>* identity(const size_type I, const size_type J)
	//{
	//	return diag(I,J,T(1));
	//}


	template <typename T, typename MemorySpace, typename Major>
	void transpose(matrix<T,MemorySpace,Major> & dst, matrix<T,MemorySpace,Major> const& src)
	{
		gpulab::dispatch::matrix::transpose(dst, src, MemorySpace());
	};

} // namespace gpulab


#endif // GPULAB_MATRIX_H
