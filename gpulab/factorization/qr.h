// ==========================================================================
// QR factorization
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_FACTORIZATION_QR_H
#define GPULAB_FACTORIZATION_QR_H

#include <assert.h>

namespace gpulab
{

namespace factorization
{

	/**
	* TODO: NOT DONE. .column(k) is not implemented nor well defined....!
	* 
	* Does a QR factorization of A, such that A = Q[R 0]^T.
	* Ie. if A^(m,n), then Q^(m,m) and R^(n,n).
	*/
	template <class M>
	void qr(M const& A, M& Q, M& R)
	{
		// Assert dimensions
		assert(A.rows() == Q.rows() && A.cols() == Q.cols());
		assert(A.rows() == R.rows() && A.rows() == R.cols());

		typedef typename M::size_type	size_type;
		typedef typename M::value_type	value_type;
		typedef typename M::vector_type	vector_type;

		for(size_type k=0; k<A.cols(); ++k)
		{
			vector_type a_k = A.column(k);
			vector_type q_k = Q.column(k);

			R[k][k] = a_k.nrm2();
			q_k = a_k.scal(R[k][k]);
			
			for(size_type j=k; j<A.cols(); ++j)
			{
				r[k][j] = q_k.dot(A.column(j));
				A.column(j).axpy(-r[k][j], q_k);
			}
		}
	}
	
} // namespace factorization

} // namespace gpulab

#endif // GPULAB_MONITOR_H