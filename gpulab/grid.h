// ==========================================================================
// Grid, a vector wrapper that holds information about 1d, 2d, or 3d 
// dimensions.
// ==========================================================================
// (C)opyright: 2010
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_GRID_H
#define GPULAB_GRID_H

#include <gpulab.h>
#include <gpulab/vector.h>
#include <gpulab/topology.h>
#include <gpulab/grid_properties.h>
#include <gpulab/grid_transformation.h>

#define DIM_X 0x0001
#define DIM_Y 0x0010
#define DIM_Z 0x0100

namespace gpulab
{

	/**
	* Grid, a vector wrapper that holds information about 1d, 2d, or 3d 
	* dimensions.
	* @param T value type.
	* @param MemorySpace memory location (host/device).
	*/
	template <typename T
		    , typename MemorySpace = gpulab::device_memory
			, typename topology_type_ = topology_none<typename gpulab::vector<T,MemorySpace>::size_type,T> >
	class grid : public gpulab::vector<T,MemorySpace>
	{
	private:
		typedef grid<T,MemorySpace,topology_type_>				my_type;
		typedef typename gpulab::vector<T,MemorySpace>			super;

	public:
		typedef typename super::size_type						size_type;
		typedef typename super::value_type						value_type;
		typedef typename super::iterator						iterator;
		typedef topology_type_									topology_type;
		typedef gpulab::grid_properties<size_type,value_type>	property_type;
		typedef typename property_type::dim_value_type			dim_value_type;
		typedef typename property_type::dim_size_type			dim_size_type;
		typedef MemorySpace										memory_space;
		typedef gpulab::grid_transformation<my_type>			transformation_type;

	protected:
		topology_type											m_topology;
		property_type											m_properties;
		transformation_type*									m_transformation;

	public:
		
		/**
		 * This constructor creates an empty \p grid
		 */
		__host__
		grid()
		  : super()
		  , m_topology()
		  , m_properties()
		  , m_transformation(0)
		{}

		/** 
		 * This constructor creates a 1D, 2D, or 3D \p grid
		 * @param local_props, containing local properties
		 * @param topology, containing topology information for distributed grids
		 */
		__host__
		grid(property_type const& local_props, topology_type const& topology)
			: super(local_props.size())
			, m_topology(topology)
			, m_properties(local_props)
			, m_transformation(0)
		{}


		/** 
		 * This constructor creates a 1D, 2D, or 3D \p grid.
		 * Uses the default topology instance to distribute the grid into local
		 * grids with local properties.
		 * @param global_props, containing global properties that will be distributed according to the topology.
		 */
		__host__
		grid(property_type const& global_props)
			: super()
			, m_topology(global_props.bctype0)
			, m_transformation(0)
		{
			// This one sets m_properties
			m_topology.create_local_props(global_props,&this->m_properties);
			this->resize(m_properties.size());
		}

		/**
		 * Copy constructor copies from an exemplar \p grid.
		 * @param v The \p grid to copy.
		 */
		__host__
		grid(const my_type &v)
		  : super(v)
		  , m_properties(v.m_properties)
		  , m_topology(v.topology())
		  , m_transformation(v.m_transformation)
		{}

		/** 
		 * Copy constructor copies from an exemplar \p grid with different types.
		 * @param v The \p grid to copy.
		 */
		template<typename other_type>
		__host__
		grid(other_type const& v)
		  : super(v)
		  , m_properties(v.properties())
		  , m_topology(v.topology())
		  //, m_transformation(v.transformation())
		{
			if(v.transformation())
			{
				ASSERT("Not supported");
			}
		}

		/**
		* Duplicate self and returns pointer.
		* Remember that the pointer returned must be deleted by the receiver.
		*/ 
		my_type* duplicate() const
		{
			my_type* n = new my_type(this->m_properties,this->m_topology);
			n->m_transformation = this->m_transformation;
			return n;
		}

		/**
		* Get topology
		*/
		topology_type& topology()
		{
			return m_topology;
		}

		topology_type const& topology() const
		{
			return m_topology;
		}

		/**
		* Get the local grid properties
		*/
		property_type const& properties() const
		{
			return this->m_properties;
		}

		/**
		* Changes the grid properties and resizes the grid to match the new outer dimensions.
		* Resets all grid values.
		* Does not change topology or transformation info.
		* @param props the new grid properties
		*/
		void set_properties(property_type const& props)
		{
			this->m_properties = props;
			this->resize(props.size()); // also resets all elements to value_type)()
		}

		///**
		//* Get the local grid properties
		//*/
		//property_type const& local_props() const
		//{
		//	return m_topology.local_props();
		//}

		///**
		//* Get the global grid properties
		//*/
		//property_type const& global_props() const
		//{
		//	return m_topology.global_props();
		//}
		//
		
		/**
		* Grid size in x-direction (incl. ghost points)
		*/
		size_type Nx() const { return m_properties.outer_dim.x; }
		
		/**
		* Grid size in y-direction (incl. ghost points)
		*/
		size_type Ny() const { return m_properties.outer_dim.y; }
		
		/**
		* Grid size in z-direction (incl. ghost points)
		*/
		size_type Nz() const { return m_properties.outer_dim.z; }

		/**
		* Internal grid size in x-direction (excl. ghost points)
		*/
		size_type iNx() const { return m_properties.inner_dim.x; }
		
		/**
		* Internal grid size in y-direction (excl. ghost points)
		*/
		size_type iNy() const { return m_properties.inner_dim.y; }
		
		/**
		* Internal grid size in z-direction (excl. ghost points)
		*/
		size_type iNz() const { return m_properties.inner_dim.z; }

		/**
		* Start ghost points
		*/
		dim_size_type const& ghost0() const { return m_properties.ghost0; }

		/**
		* End ghost points
		*/
		dim_size_type const& ghost1() const { return m_properties.ghost1; }

		/**
		* Start offset points
		*/
		dim_size_type const& offset0() const { return m_properties.offset0; }

		/**
		* End offset points
		*/
		dim_size_type const& offset1() const { return m_properties.offset1;	}

		/**
		* Grid spacing
		*/
		dim_value_type const& grid_space() const { return m_properties.delta; }

		/**
		* Grid spacing
		*/
		dim_value_type const& delta() const { return m_properties.delta; }

		/**
		* Local dimensions
		*/
		dim_size_type const& dimension() const { return m_properties.dimension(); }

		/**
		* Distrubuted, returns true if the grid is shared amoung multiple MPI processes.
		*/
		bool distributed() const
		{
			return (m_topology.mpi_size() > 1);
		}

		/**
		* The error norm of a grid funtion is the intergral over all dimensions,
		* thus the norm2 is scaled with the grid sizes.
		*/
		value_type err_nrm2() const
		{
			value_type h = m_properties->delta_prod();
			return sqrt(h*
				thrust::transform_reduce( 
				this->begin(), this->end(), gpulab::square(), value_type(0), thrust::plus<value_type>()
				));
		}

		/**
		* Copy from a vector to the internal points of this grid, ignoring all ghost points.
		*/
		template <typename vector_type>
		void copy_internal(vector_type const& s)
		{
			typedef typename vector_type::const_iterator citerator;
			citerator src = s.begin();
			iterator dest = this->begin();
			
			size_type Nx  = this->Nx();
			size_type Ny  = this->Ny();
			size_type Nz  = this->Nz();
			size_type iNx = this->iNx();

			for(int k=m_properties.ghost0.z; k<Nz-m_properties.ghost1.z; ++k)
			{
				for(int j=this->m_properties.ghost0.y; j<Ny-m_properties.ghost1.y; ++j)
				{
					thrust::copy(src,src+iNx,dest+k*(Nx*Ny)+j*Nx+m_properties.ghost1.x);
					src += iNx;

					// If src ends before interior points, just return
					if(src >= s.end()) return;
				}
			}
		}


		/**
		* Norm_2. sqrt(a*a+b*b+...+z*z)
		*/
		value_type nrm2() const
		{
			value_type n = this->dot(*this);

			// Distributed grids should be a reduce sum first
			if( this->distributed() )
			{
				value_type ntotal;
				MPI_Allreduce(&n , &ntotal, 1, values<value_type>::MPI_TYPE(), MPI_SUM, m_topology.COMM);
				//printf("Rank: %d, n: %f, nt: %f\n", commRank,n,ntotal);
				return sqrt(ntotal);
			}
			else
			{
				// Trivial
				return sqrt(n);

				// TODO: Remove ghost point contributions
				//my_type tmp(*this);
				//reset_ghost_layers(tmp);
				//return ((super)tmp).nrm2();
			}
		}

		/**
		* Updates values as implemented by the topology strategy.
		*/ 
		void update()
		{
			m_topology.update(*this);
		}

		/**
		* Get the transformation.
		*/
		void set_transformation(transformation_type* tranform)
		{
			this->m_transformation = tranform;
		}

		/**
		* Get const transformation.
		*/
		transformation_type* transformation() const
		{
			return this->m_transformation;
		}
	};


namespace kernel
{
	
	template <typename value_type, typename size_type>
	__global__ 
	void reset_ghost_layers(  value_type* src
							, size_type g0x
							, size_type g1x
							, size_type g0y
							, size_type g1y
							, size_type g0z
							, size_type g1z
							, int Nx
							, int Ny
							, int Nz)
	{
		size_type i = IDX1Dx; // x|y coord
		size_type j = IDX1Dy; // vertical coord
		
		if(i>=Nx || j>=Ny)
			return;

		for(size_type k=0; k<Nz; ++k)
		{
			if(i<g0x || i>=Nx-g1x || j<g0y || j>=Ny-g1y || k<g0z || k>=Nz-g1z)
				src[idx(i,j,k,Nx,Ny)] = 0;
		}
	}

} // namespace kernel

	template <typename grid_type>
	void reset_ghost_layers(grid_type& g)
	{
		int Nx = g.Nx();
		int Ny = g.Ny();
		int Nz = g.Nz();

		dim3 block = BLOCK2D(Nx,Ny);
		dim3 grid = GRID2D(Nx,Ny);
		gpulab::kernel::reset_ghost_layers<<<grid,block>>>(RAW_PTR(g),g.ghost0().x,g.ghost1().x,g.ghost0().y,g.ghost1().y,g.ghost0().z,g.ghost1().z,Nx,Ny,Nz);
	}

} // namespace gpulab


#endif // GPULAB_VECTOR_H
