// ==========================================================================
// Sparse Matrix base class
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SPARSE_MATRIX_H
#define GPULAB_SPARSE_MATRIX_H

#include <gpulab/config.h>
#include <gpulab/vector.h>

namespace gpulab
{
	template<typename MemorySpace, typename T>
	struct sparse_matrix_dispatcher
	{ 
		template <typename M, typename V>
		static void mult( M const& m, V const& x, V& b ); // users, go ahead and specialize this 
	};

	/**
	* Base sparse matrix class, shared between both device and host matrices.
	* All sparse matrix formats should extend this class.
	* @param T value type
	* @param MemorySpace memory space indication
	*/
	template <typename T, typename MemorySpace> 
	class sparse_matrix
	{
	public:
		typedef gpulab::sparse_matrix<T,MemorySpace>								my_type;
		typedef MemorySpace															memory_space;
		typedef typename thrust_vector_selector<T, MemorySpace>::type::size_type	size_type;
		typedef typename thrust_vector_selector<T, MemorySpace>::type::value_type	value_type;
		
	private:
		/**
		* Hide default constructor
		*/
		sparse_matrix() {};

	protected:
		size_type	 m_I;						///< Number of rows
		size_type	 m_J;						///< Number of columns
		size_type	 m_nonzeros;				///< Number of non-zero elements
		memory_space m_memory_space;			///< Memory space indicator
		
	public:
		/**
		* Construct a matrix of size I*J with nz non-zeros and value val
		*/
		sparse_matrix(size_type I, size_type J, size_type nz, const T& val)
			: m_I(I)
			, m_J(J)
			, m_nonzeros(nz)
			, m_memory_space()
		{ }

		/**
		* Construct a matrix of size I*J with nz non-zeros and default value
		*/
		sparse_matrix(size_type I, size_type J, size_type nz)
			: m_I(I)
			, m_J(J)
			, m_nonzeros(nz)
			, m_memory_space()
		{ }
		
		/**
		* Construct a matrix of size I*I with nz non-zeros and default value
		*/
		sparse_matrix(size_type I, size_type nz)
			: m_I(I)
			, m_J(I)
			, m_nonzeros(nz)
			, m_memory_space()
		{ }

		/**
		* Copy constructor
		*/
		sparse_matrix(my_type const& m)
			: m_I(m.m_I)
			, m_J(m.m_J)
			, m_nonzeros(m.m_nonzeros)
			, m_memory_space()
		{ }

		///**
		//* Copy constructor from other location
		//*/
		template<typename OtherT, typename OtherMemorySpace>
		__host__ __device__
		sparse_matrix(sparse_matrix<OtherT, OtherMemorySpace> const& m)
			: m_I(m.rows())
			, m_J(m.cols())
			, m_nonzeros(m.nonzeros())
			, m_memory_space()
		{ }

		/**
		* Deconstructor
		*/
		~sparse_matrix()
		{ };

		/**
		* Return number of rows
		*/
		size_type rows() const
		{
			return m_I;
		}

		/**
		* Return number of columns
		*/
		size_type cols() const
		{
			return m_J;
		}

		/**
		* Return number of non-zeros
		*/
		size_type nonzeros() const
		{
			return m_nonzeros;
		}

		/**
		* Private helpers
		*/
	private:

	};

	

} // namespace gpulab


#endif // GPULAB_SPARSE_MATRIX_H
