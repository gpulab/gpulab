// ==========================================================================
// Topology. Defines how to distribute the content of grids.
// ==========================================================================
// (C)opyright: 2010
//
// Author: Stefan Lemvig Glimberg
// Email:  slgl@imm.dtu.dk
// Author: Allan P. Engsig-Karup
// Email:  apek@imm.dtu.dk
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// ==========================================================================

#ifndef GPULAB_TOPOLOGY_H
#define GPULAB_TOPOLOGY_H

#include <gpulab.h>
#include <gpulab/grid_properties.h>
#include <gpulab/io/log.h>

#define GPULAB_TOPOLOGY_BUFFER_SIZE 2048

namespace gpulab
{

namespace kernel
{
	
	template <typename value_type, typename size_type>
	__global__
	void get_boundary_inner_points_1d(value_type const* p
						, value_type* l
						, value_type* r
						, size_type Nx
						, size_type Ny
						, size_type gl
						, size_type gr)
	{
		size_type i = IDX1Dx;
		size_type j = IDX1Dy;

		if(j<Ny)
		{
			if(i<gl) l[j*gl+i] = p[j*Nx+gl+1+i];
			if(i<gr) r[j*gr+i] = p[j*Nx+Nx-2*gr-1+i];
		}
	};

	template <typename value_type, typename size_type>
	__global__
	void set_boundary_outer_points_1d(value_type* p
						, value_type const* l
						, value_type const* r
						, size_type Nx
						, size_type Ny
						, size_type gl
						, size_type gr)
	{
		size_type i = IDX1Dx;
		size_type j = IDX1Dy;

		if(j<Ny)
		{
			if(i<gl) p[j*Nx+i]       = l[j*gl+i];
			if(i<gr) p[j*Nx+Nx-gr+i] = r[j*gr+i];
		}
	};

	
	template <typename value_type, typename size_type>
	__global__ 
	void get_ghost_layer(value_type* dst, value_type* src, dim3 N, size_type gs, int dir, size_type o=1)
	{
		size_type i = IDX1Dx; // x|y coord
		size_type k = IDX1Dy; // vertical coord
		size_type g = 0;

		if(k>=N.z || (dir<2 && i>=N.y) || (dir>1 && i>=N.x)) 
			return;

		switch(dir)
		{
		case 0:
			for(; g<gs; ++g)
				dst[idx(g,i,k,gs,N.y)] = src[idx(gs+g+o,i,k,N.x,N.y)];
			break;
		case 1:
			for(; g<gs; ++g)
				dst[idx(g,i,k,gs,N.y)] = src[idx(N.x-2*gs+g-o,i,k,N.x,N.y)];
			break;
		case 2:
			for(; g<gs; ++g)
				dst[idx(i,g,k,N.x,gs)] = src[idx(i,gs+g+o,k,N.x,N.y)];
			break;
		case 3:
			for(; g<gs; ++g)
				dst[idx(i,g,k,N.x,gs)] = src[idx(i,N.y-2*gs+g-o,k,N.x,N.y)];
			break;
		}
	};

	template <typename value_type, typename size_type>
	__global__ 
	void set_ghost_layer(value_type* src, value_type* dst, dim3 N, size_type gs, int dir, size_type o=1)
	{
		size_type i = IDX1Dx; // x|y coord
		size_type k = IDX1Dy; // vertical coord
		size_type g = 0;

		if(k>=N.z || (dir<2 && i>=N.y) || (dir>1 && i>=N.x)) 
			return;

		switch(dir)
		{
		case 0:
			for(; g<gs; ++g)
				dst[idx(g,i,k,N.x,N.y)]        = src[idx(g,i,k,gs,N.y)];
			break;
		case 1:
			for(; g<gs; ++g)
				dst[idx(N.x-gs+g,i,k,N.x,N.y)] = src[idx(g,i,k,gs,N.y)];
			break;
		case 2:
			for(; g<gs; ++g)
				dst[idx(i,g,k,N.x,N.y)]        = src[idx(i,g,k,N.x,gs)];
			break;
		case 3:
			for(; g<gs; ++g)
				dst[idx(i,N.y-gs+g,k,N.x,N.y)] = src[idx(i,g,k,N.x,gs)];
			break;
		}
	};

	template <typename value_type, typename size_type>
	__global__ 
	void update_periodic_ghost_layer(value_type* dst, dim3 N, size_type gs, int dir, size_type o=1)
	{
		size_type i = IDX1Dx; // x|y coord
		size_type k = IDX1Dy; // vertical coord
		size_type g = 0;

		if(k>=N.z || (dir<2 && i>=N.y) || (dir>1 && i>=N.x)) 
			return;

		switch(dir)
		{
		case 0:
		case 1:
			for(; g<gs; ++g)
			{
				dst[idx(g,i,k,N.x,N.y)]        = dst[idx(N.x-2*gs-1+g,i,k,N.x,N.y)];    // Left side
				dst[idx(N.x-gs+g,i,k,N.x,N.y)] = dst[idx(gs+1+g      ,i,k,N.x,N.y)];	// Right side
			}
			break;
		case 2:
		case 3:
			for(; g<gs; ++g)
			{
				dst[idx(i,g,k,N.x,N.y)]        = dst[idx(i,N.y-2*gs-1+g,k,N.x,N.y)];    // Left side
				dst[idx(i,N.y-gs+g,k,N.x,N.y)] = dst[idx(i,gs+1+g      ,k,N.x,N.y)];	// Right side
			}
			break;
		}
	};
} // namespace kernel


	/**
	* Default grid topology.
	* This is a cartesian topology super class.
	* @param DIMS specifify which of the first dimensions are used (1|2|3)
	*/
	template <typename size_type, typename value_type, int DIMS>
	class topology
	{
	public:	
		typedef grid_properties<size_type,value_type>		property_type;
		typedef typename property_type::dim_value_type		dim_value_type;
		typedef typename property_type::dim_size_type		dim_size_type;
		typedef typename property_type::dim_bool_type		dim_bool_type;

		//gpulab::util::mpi_timer			device_timer;
		//gpulab::util::mpi_timer			mpi_timer;

		int N, S, E, W, T, B;
		int P, Q, R;
		int p, q, r;
		int rank;
		//int overlap;

		/**
		* We made this static to avoid creation of multiple communicators.
		* In this way all topologies with the same template args will use the same communicator.
		* We believe this is OK, however it might be different for other distribution layouts!?
		* Alternatively we could maybe destroy the communicator in the deconstructor, but it we have
		* not succeeded with this...
		*/
		//static MPI_Comm COMM;
		MPI_Comm COMM;

	protected:
		typedef topology<size_type,value_type,DIMS>			my_type;

		/**
		* Default constructor.
		* Uses GPULAB_COMM_WORLD to create new MPI communicator and no periodicity
		*/
		topology() : COMM(MPI_COMM_NULL)
				   , N(MPI_PROC_NULL)
				   , S(MPI_PROC_NULL)
				   , E(MPI_PROC_NULL)
				   , W(MPI_PROC_NULL)
				   , T(MPI_PROC_NULL)
				   , B(MPI_PROC_NULL)
				   , P(1), Q(1), R(1)
				   , p(0), q(0), r(0)
		{
			int periods[3] = {0,0,0};
			this->create_topology(periods);
		}

		/**
		* Constructor with given peridicity.
		* Uses GPULAB_COMM_WORLD to create new MPI communicator 
		*/
		topology(dim_size_type const& periods)
			: COMM(MPI_COMM_NULL)
			, N(MPI_PROC_NULL)
			, S(MPI_PROC_NULL)
			, E(MPI_PROC_NULL)
			, W(MPI_PROC_NULL)
			, T(MPI_PROC_NULL)
			, B(MPI_PROC_NULL)
			, P(1), Q(1), R(1)
			, p(0), q(0), r(0)
		{
			int iperiods[3] = {periods.x==BC_PER, periods.y==BC_PER, periods.z==BC_PER};
			this->create_topology(iperiods);
		}

		/**
		* Constructor that takes a specific MIP communicator and no periodicity
		*/
		topology(MPI_Comm comm) 
			: COMM(comm)
			, N(MPI_PROC_NULL)
			, S(MPI_PROC_NULL)
			, E(MPI_PROC_NULL)
			, W(MPI_PROC_NULL)
			, T(MPI_PROC_NULL)
			, B(MPI_PROC_NULL)
			, P(1), Q(1), R(1)
			, p(0), q(0), r(0)
		{
			int iperiods[3] = {0,0,0};
			this->create_topology(iperiods);
		}

		/**
		* Constructor that takes a specific MIP communicator
		*/
		topology(MPI_Comm comm, dim_size_type& periods) 
			: COMM(comm)
			, N(MPI_PROC_NULL)
			, S(MPI_PROC_NULL)
			, E(MPI_PROC_NULL)
			, W(MPI_PROC_NULL)
			, T(MPI_PROC_NULL)
			, B(MPI_PROC_NULL)
			, P(1), Q(1), R(1)
			, p(0), q(0), r(0)
		{
			int iperiods[3] = {periods.x==BC_PER, periods.y==BC_PER, periods.z==BC_PER};
			this->create_topology(iperiods);
		}

		/**
		* Copy constructor, from (possible same) other type
		*/
		//template <typename other_type>
		topology(my_type const& other)
			: COMM(other.COMM)
			, N(other.N)
			, S(other.S)
			, E(other.E)
			, W(other.W)
			, T(other.T)
			, B(other.B)
			, P(other.P)
			, Q(other.Q)
			, R(other.R)
			, p(other.p)
			, q(other.q)
			, r(other.r)
		{ }


	public:
		
		/**
		* Deconstructor
		*/
		~topology()
		{
			//if(COMM != MPI_COMM_NULL)
			//{
			//	MPI_Comm_free(&COMM);
			//}
		}

		/**
		* No. mpi nodes that share the grid
		*/
		int mpi_size() const
		{
			int n;
			MPI_Comm_size(COMM,&n);
			return n;
		}

		void create_local_props(property_type const& gprops, property_type* lprops)
		{
			// Trivial if only one node
			if(mpi_size() == 1)
			{
				*lprops = gprops;
				return;
			}

			// Create a local grid
			size_type si, ei, sj, ej, sk, ek;
			si = p * (gprops.inner_dim.x / P);
			ei = (p == P-1) ? gprops.inner_dim.x - 1 : si + (gprops.inner_dim.x / P);
			sj = q * (gprops.inner_dim.y / Q);
			ej = (q == Q-1) ? gprops.inner_dim.y - 1 : sj + (gprops.inner_dim.y / Q);
			sk = r * (gprops.inner_dim.z / R);
			ek = (r == R-1) ? gprops.inner_dim.z - 1 : sk + (gprops.inner_dim.z / R);
			
			// Calculate local phys dims
			value_type x0, x1, y0, y1, z0, z1;
			x0 = (si) * gprops.delta.x;
			x1 = (ei) * gprops.delta.x;
			y0 = (sj) * gprops.delta.y;
			y1 = (ej) * gprops.delta.y;
			z0 = (sk) * gprops.delta.z;
			z1 = (ek) * gprops.delta.z;
			lprops->phys0 = dim_value_type(x0,y0,z0);
			lprops->phys1 = dim_value_type(x1,y1,z1);

			// Make sure at least one layer of ghost points for shared borders
			lprops->ghost0 = gprops.ghost0;
			lprops->ghost1 = gprops.ghost1;
			if(lprops->ghost0.x == 0 && W!=-1) lprops->ghost0.x = 1;
			if(lprops->ghost1.x == 0 && E!=-1) lprops->ghost1.x = 1;
			if(lprops->ghost0.y == 0 && S!=-1) lprops->ghost0.y = 1;
			if(lprops->ghost1.y == 0 && N!=-1) lprops->ghost1.y = 1;
			if(lprops->ghost0.z == 0 && B!=-1) lprops->ghost0.z = 1;
			if(lprops->ghost1.z == 0 && T!=-1) lprops->ghost1.z = 1;

			// Set DD B.C. types for local props
			lprops->bctype0 = dim_size_type(  W==-1 ? gprops.bctype0.x : BC_DD
											, S==-1 ? gprops.bctype0.y : BC_DD
											, B==-1 ? gprops.bctype0.z : BC_DD);
			lprops->bctype1 = dim_size_type(  E==-1 ? gprops.bctype1.x : BC_DD
											, N==-1 ? gprops.bctype1.y : BC_DD
											, T==-1 ? gprops.bctype1.z : BC_DD);

			// Set offset for local props
			lprops->offset0 = dim_size_type(  W==-1 ? gprops.offset0.x : 0
											, S==-1 ? gprops.offset0.y : 0
											, B==-1 ? gprops.offset0.z : 0);
			lprops->offset1 = dim_size_type(  E==-1 ? gprops.offset1.x : 0
											, N==-1 ? gprops.offset1.y : 0
											, T==-1 ? gprops.offset1.z : 0);
			
			lprops->set_inner_dim(dim_size_type(ei-si+1,ej-sj+1,ek-sk+1));

			GPULAB_LOG_DBG("CreateLprops: N(%i,%i,%i), n(%i,%i,%i), p0(%4f,%4f,%4f), p1(%4f,%4f,%4f), g0(%i,%i,%i), g1(%i,%i,%i), d(%f,%f,%f)\n"
					, gprops.dimension().x, gprops.dimension().y, gprops.dimension().z
					,lprops->dimension().x,lprops->dimension().y,lprops->dimension().z
					,lprops->phys0.x,lprops->phys0.y,lprops->phys0.z
					,lprops->phys1.x,lprops->phys1.y,lprops->phys1.z
					,lprops->ghost0.x,lprops->ghost0.y,lprops->ghost0.z
					,lprops->ghost1.x,lprops->ghost1.y,lprops->ghost1.z
					,lprops->delta.x,lprops->delta.y,lprops->delta.z);
		}

	protected:


		/**
		* Creates a default cartesian topology based on the number of MPI nodes
		* in the communicator (COMM) and on DIMS.
		* @param periods indicates if any direction is periodic
		*/
		void create_topology(int periods[3])
		{
			// TODO: Get MPI size based on GPULAB_COMM_WORLD
			int size;
			if(COMM==MPI_COMM_NULL)
				MPI_Comm_size(gpulab::GPULAB_COMM_WORLD,&size);
			else
				MPI_Comm_size(COMM,&size);

			// Let MPI decide on the dimensions
			int dims[3] = {0,0,0};
			MPI_Dims_create(size, DIMS, dims);

			// Set topology dimensions
			P = dims[0];
			Q = DIMS < 2 ? 1 : dims[1];
			R = DIMS < 3 ? 1 : dims[2];

			// Create grid communucator
			if(COMM==MPI_COMM_NULL)
			{
				MPI_Cart_create(gpulab::GPULAB_COMM_WORLD,DIMS,dims,periods,1,&COMM);
				GPULAB_LOG_INF("MPI_Cart_create\n");
			}

			//int rank;
			MPI_Comm_rank(COMM,&rank);

			// Get local coords
			int coords[3] = {0,0,0};
			MPI_Cart_coords(COMM,rank,DIMS,coords);
			p = coords[0];
			q = coords[1];
			r = coords[2];

			// Set destinations for North, South, East, West, Top, Bottom
			if(DIMS > 0) MPI_Cart_shift(COMM,0,1,&W,&E);
			if(DIMS > 1) MPI_Cart_shift(COMM,1,1,&S,&N);
			if(DIMS > 2) MPI_Cart_shift(COMM,2,1,&B,&T);

			GPULAB_LOG_DBG("Topo: rank=%i, P=%i, Q=%i, R=%i, p=%i, q=%i, r=%i\n",rank,P,Q,R,p,q,r);
			GPULAB_LOG_DBG("Topo: E=%i, W=%i, N=%i, S=%i, T=%i, B=%i\n",E,W,N,S,T,B);
		}
	};
	
	//topology<typename size_type, typename value_type, int DIMS>::COMM = MPI_COMM_NULL;
	

	/**
	* Default grid topology.
	* No topology, i.e. a grid created with this topology will not be shared,
	* and exist equally on each node.
	*/
	template <typename size_type, typename value_type>
	class topology_none
	{
	protected:
		typedef topology_none<size_type,value_type>			my_type; 

	public:

		typedef grid_properties<size_type,value_type>		property_type;
		typedef typename property_type::dim_value_type		dim_value_type;
		typedef typename property_type::dim_size_type		dim_size_type;
		typedef typename property_type::dim_bool_type		dim_bool_type;

		int N, S, E, W, T, B;
		int P, Q, R;
		int p, q, r;

		MPI_Comm COMM; // Never assigned and should never be used

		/**
		* Default constructor.
		* 
		*/ 
		topology_none() 
			: N(MPI_PROC_NULL)
			, S(MPI_PROC_NULL)
			, E(MPI_PROC_NULL)
			, W(MPI_PROC_NULL)
			, T(MPI_PROC_NULL)
			, B(MPI_PROC_NULL)
			, P(1), Q(1), R(1)
			, p(0), q(0), r(0)
		{}

		/**
		* Constructor from other global properties
		*/ 
		topology_none(dim_size_type const& periods)
			: N(MPI_PROC_NULL)
			, S(MPI_PROC_NULL)
			, E(MPI_PROC_NULL)
			, W(MPI_PROC_NULL)
			, T(MPI_PROC_NULL)
			, B(MPI_PROC_NULL)
			, P(1), Q(1), R(1)
			, p(0), q(0), r(0)
		{}

		/**
		* Constructor from other object of same type
		*/ 
		template <typename other_type>
		topology_none(other_type const& other)
			: N(MPI_PROC_NULL)
			, S(MPI_PROC_NULL)
			, E(MPI_PROC_NULL)
			, W(MPI_PROC_NULL)
			, T(MPI_PROC_NULL)
			, B(MPI_PROC_NULL)
			, P(1), Q(1), R(1)
			, p(0), q(0), r(0)
		{}

		/**
		* No. mpi nodes that share the grid
		*/
		size_type mpi_size() const
		{
			return 1;
		}

		void create_local_props(property_type const& gprops, property_type* lprops)
		{
			*lprops = gprops;
		}
		
		/**
		* Update grid overlapping zones, nothing to be done for this topology.
		*/
		template <typename grid_type>
		void update(grid_type const&) const {}
	};

	
	///**
	//* Topology distrubution in the x dimension.
	//* Grid will be equally diveded along the x-direction by all nodes
	//*/
	//template <typename size_type, typename value_type>
	//class topology_x : public topology<size_type,value_type,1>
	//{
	//public:

	//	typedef grid_properties<size_type,value_type>		property_type;
	//	typedef typename property_type::dim_value_type		dim_value_type;
	//	typedef typename property_type::dim_size_type		dim_size_type;
	//	typedef typename property_type::dim_bool_type		dim_bool_type;

	//protected:
	//	typedef topology_x<size_type,value_type>			my_type; 
	//	typedef topology<size_type,value_type,1>			super;

	//public:
	//	/**
	//	* Default constructor.
	//	* 
	//	*/ 
	//	topology_x(   dim_size_type  const& dim_    = dim_size_type(0,0,0)
	//				, dim_value_type const& x0_     = dim_value_type(value_type(0),value_type(0),value_type(0))
	//				, dim_value_type const& x1_     = dim_value_type(value_type(1),value_type(1),value_type(1))
	//				, dim_size_type  const& g0_     = dim_size_type(0,0,0)
	//				, dim_size_type  const& g1_     = dim_size_type(0,0,0)
	//				, dim_bool_type  const& period_ = dim_bool_type(false,false,false))
	//	{
	//		//GPULAB_LOG_DBG("topology_x c1\n");
	//		this->m_global_props = property_type(dim_,x0_,x1_,g0_,g1_,period_);
	//		this->initialize();
	//	}

	//	/**
	//	* Constructor from other global properties
	//	*/ 
	//	topology_x(property_type const& gprops)
	//	{
	//		//GPULAB_LOG_DBG("topology_x c2\n");
	//		this->m_global_props = gprops;
	//		this->initialize();
	//	}

	//	/**
	//	* Constructor from other object of (possible same) other type
	//	*/ 
	//	template <typename other_type>
	//	topology_x(other_type const& other)
	//		: super(other)
	//	{
	//		//GPULAB_LOG_DBG("topology_x c3\n");
	//	}


	//	/**
	//	* 1D Update grid overlapping zones
	//	* TODO: Only support for 1D and 2D grids
	//	*/
	//	template <typename grid_type>
	//	void update(grid_type& g) const 
	//	{
	//		if(!g.distributed())
	//			return;

	//		size_type Nx     = g.dimension().x;
	//		size_type Ny     = g.dimension().y;
	//		size_type gl     =    g.ghost0().x; // Left  ghost layer width
	//		size_type gr     =    g.ghost1().x; // Right ghost layer width
	//		
	//		gpulab::vector<value_type,gpulab::device_memory> l(max(gl,gr)*Ny); // Make sure buffers are big enough for both send and recv
	//		gpulab::vector<value_type,gpulab::device_memory> r(max(gl,gr)*Ny);

	//		dim3 block = dim3(max(gl,gr),min(32,(int)Ny));
	//		dim3 grid  = dim3(1,(Ny+block.y-1) / block.y);
	//		ASSERT(block.x*block.y < 512);
	//		kernel::get_boundary_inner_points_1d<<<grid,block>>>(RAW_PTR(g),RAW_PTR(l),RAW_PTR(r),Nx,Ny,gl,gr);

	//		//gpulab::io::print(l);
	//		//gpulab::io::print(r);
	//		
	//		gpulab::exchange_vector(l,this->S,this->N);
	//		//GPULAB_LOG_DBG("exc %d, %d, %d\n",l.size(), this->S, this->N);
	//		gpulab::exchange_vector(r,this->N,this->S);
	//		//GPULAB_LOG_DBG("exc %d, %d, %d\n",r.size(), this->N, this->S);

	//		kernel::set_boundary_outer_points_1d<<<grid,block>>>(RAW_PTR(g),RAW_PTR(r),RAW_PTR(l),Nx,Ny,gl,gr);
	//	}


	//};

	
	/**
	* Topology distrubution over the xy plane.
	* Grid will atomatically be equally diveded along the xy-direction by all nodes.
	* It is possible to manually change these properties if alternatives are needed.
	*/
	template <typename size_type, typename value_type>
	class topology_xy : public topology<size_type,value_type,2>
	{
	public:

		typedef grid_properties<size_type,value_type>		property_type;
		typedef typename property_type::dim_value_type		dim_value_type;
		typedef typename property_type::dim_size_type		dim_size_type;

	protected:
		typedef topology_xy<size_type,value_type>			my_type; 
		typedef topology<size_type,value_type,2>			super;

		
	public:
		gpulab::vector<value_type,gpulab::device_memory>	gxz;
		gpulab::vector<value_type,gpulab::device_memory>	gyz;

		/**
		* Default constructor.
		*/
		topology_xy() : super()
		{ 
			gxz.reserve(GPULAB_TOPOLOGY_BUFFER_SIZE*sizeof(value_type));
			gyz.reserve(GPULAB_TOPOLOGY_BUFFER_SIZE*sizeof(value_type));
		}

		/**
		* Constructor taking periodic info.
		*/
		topology_xy(dim_size_type const& periods) : super(periods)
		{ 
			gxz.reserve(GPULAB_TOPOLOGY_BUFFER_SIZE*sizeof(value_type));
			gyz.reserve(GPULAB_TOPOLOGY_BUFFER_SIZE*sizeof(value_type));
		}

		/**
		* Constructor that takes a specific MIP communicator
		*/
		topology_xy(MPI_Comm comm, dim_size_type const& periods) : super(comm,periods)
		{
			gxz.reserve(GPULAB_TOPOLOGY_BUFFER_SIZE*sizeof(value_type));
			gyz.reserve(GPULAB_TOPOLOGY_BUFFER_SIZE*sizeof(value_type));
		}

		/**
		* Constructor from other object of (possible same) other type
		*/ 
		template <typename other_type>
		topology_xy(other_type const& other)
			: super(other)
		{
			gxz.reserve(GPULAB_TOPOLOGY_BUFFER_SIZE*sizeof(value_type));
			gyz.reserve(GPULAB_TOPOLOGY_BUFFER_SIZE*sizeof(value_type));
		}

		/**
		* Update grid overlapping zones
		*/
		template <typename grid_type>
		void update(grid_type& g)
		{
			//this->device_timer.reset();
			//this->mpi_timer.reset();

			if(!g.distributed())
			{
				if(g.properties().bctype0.z == gpulab::BC_DD)
				{
					//TODO: !!!
					ASSERT(!"Vertical domain decomposition are not supportet yet.");
				}

				dim3 N(g.Nx(),g.Ny(),g.Nz());
				if(g.properties().bctype0.x == BC_PER)
				{
					// Create glock/grid for yz-plane for the update in x
					dim3 block = BLOCK2D(N.y,N.z);
					dim3 grid  =  GRID2D(N.y,N.z);
					kernel::update_periodic_ghost_layer<<<grid,block>>>(RAW_PTR(g),N,g.properties().ghost0.x,0);
				}
				if(g.properties().bctype0.y == BC_PER)
				{
					// Create glock/grid for yz-plane for the update in x
					dim3 block = BLOCK2D(N.x,N.z);
					dim3 grid  =  GRID2D(N.x,N.z);
					kernel::update_periodic_ghost_layer<<<grid,block>>>(RAW_PTR(g),N,g.properties().ghost0.y,2);
				}

				return;
			}
			
			//this->device_timer.start();
			// TODO: Naive impl.
			dim3 dim(g.Nx(),g.Ny(),g.Nz());
			int gNyz = max((int)g.properties().ghost0.x,(int)g.properties().ghost1.x)*dim.y*dim.z; // Max ghost points in yz planes
			int gNxz = max((int)g.properties().ghost0.y,(int)g.properties().ghost1.y)*dim.x*dim.z; // Max ghost points in xz planes
			
			//gpulab::vector<value_type,gpulab::device_memory> gyz(gNyz); // Ghost point buffer for yz planes
			//gpulab::vector<value_type,gpulab::device_memory> gxz(gNxz); // Ghost point buffer for xz planes
			gxz.resize(gNxz);
			gyz.resize(gNyz);

			// Create glock/grid for yz and xz-planes
			dim3 blockyz = BLOCK2D(dim.y,dim.z);
			dim3 gridyz  =  GRID2D(dim.y,dim.z);
			dim3 blockxz = BLOCK2D(dim.x,dim.z);
			dim3 gridxz  =  GRID2D(dim.x,dim.z);
			
			//cudaThreadSynchronize();
			//this->device_timer.stop();

			// The order of plane updates is important to ensure corners are properly updated!

			
			// East/West planes
			//this->device_timer.start();
			if(this->W != MPI_PROC_NULL)
				kernel::get_ghost_layer<<<gridyz,blockyz>>>(RAW_PTR(gyz),RAW_PTR(g),dim,g.properties().ghost0.x,0);
			//cudaThreadSynchronize();
			//this->device_timer.stop();
			
			//this->mpi_timer.start();
			if((this->W != MPI_PROC_NULL) || (this->E != MPI_PROC_NULL))
				gpulab::exchange_vector(gyz,this->W,this->E,this->COMM); // Exhange west <- east
			//cudaThreadSynchronize();
			//this->mpi_timer.stop();

			//this->device_timer.start();
			if(this->E != MPI_PROC_NULL)
				kernel::set_ghost_layer<<<gridyz,blockyz>>>(RAW_PTR(gyz),RAW_PTR(g),dim,g.properties().ghost1.x,1);
			//cudaThreadSynchronize();
			//this->device_timer.stop();

			//this->device_timer.start();
			if(this->E != MPI_PROC_NULL)
				kernel::get_ghost_layer<<<gridyz,blockyz>>>(RAW_PTR(gyz),RAW_PTR(g),dim,g.properties().ghost1.x,1);
			//cudaThreadSynchronize();
			//this->device_timer.stop();

			//this->mpi_timer.start();
			if((this->E != MPI_PROC_NULL) || (this->W != MPI_PROC_NULL))
				gpulab::exchange_vector(gyz,this->E,this->W,this->COMM); // Exchange x2
			//cudaThreadSynchronize();
			//this->mpi_timer.stop();

			//this->device_timer.start();
			if(this->W != MPI_PROC_NULL)
				kernel::set_ghost_layer<<<gridyz,blockyz>>>(RAW_PTR(gyz),RAW_PTR(g),dim,g.properties().ghost0.x,0);
			//cudaThreadSynchronize();
			//this->device_timer.stop();
		
			//this->device_timer.start();			
			if(this->S != MPI_PROC_NULL)
				kernel::get_ghost_layer<<<gridxz,blockxz>>>(RAW_PTR(gxz),RAW_PTR(g),dim,g.properties().ghost0.y,2);
			//cudaThreadSynchronize();
			//this->device_timer.stop();
			
			//this->mpi_timer.start();
			if((this->S != MPI_PROC_NULL) || (this->N != MPI_PROC_NULL))
				gpulab::exchange_vector(gxz,this->S,this->N,this->COMM); // Exchange y1
			//cudaThreadSynchronize();
			//this->mpi_timer.stop();

			//this->device_timer.start();
			if(this->N != MPI_PROC_NULL)
				kernel::set_ghost_layer<<<gridxz,blockxz>>>(RAW_PTR(gxz),RAW_PTR(g),dim,g.properties().ghost1.y,3);
			//cudaThreadSynchronize();
			//this->device_timer.stop();
			
			//this->device_timer.start();
			if(this->N != MPI_PROC_NULL)
				kernel::get_ghost_layer<<<gridxz,blockxz>>>(RAW_PTR(gxz),RAW_PTR(g),dim,g.properties().ghost1.y,3);
			//cudaThreadSynchronize();
			//this->device_timer.stop();
			
			//this->mpi_timer.start();
			if((this->N != MPI_PROC_NULL) || (this->S != MPI_PROC_NULL))
				gpulab::exchange_vector(gxz,this->N,this->S,this->COMM); // Exchange y2
			//cudaThreadSynchronize();
			//this->mpi_timer.stop();

			//this->device_timer.start();
			if(this->S != MPI_PROC_NULL)
				kernel::set_ghost_layer<<<gridxz,blockxz>>>(RAW_PTR(gxz),RAW_PTR(g),dim,g.properties().ghost0.y,2);
			//cudaThreadSynchronize();
			//this->device_timer.stop();
		}
	};


} // namespace gpulab


#endif // GPULAB_TOPOLOGY_H
