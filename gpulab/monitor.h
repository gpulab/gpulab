// ==========================================================================
// Monitor for iterative convergence
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_MONITOR_H
#define GPULAB_MONITOR_H

#include <limits>
#include <gpulab/type_traits.h>
#include <gpulab/util/timer.h>

namespace gpulab
{
	
	/**
	* Iteration monitor
	*/
	template <typename value_type, typename timer_type = gpulab::util::device_timer>
	class monitor
	{
	public:

	protected:

		value_type		m_relative_tolerance;
		value_type		m_absolute_tolerance;
		value_type		m_tolerance;
		value_type		m_b_norm;
		value_type		m_r_norm;
		value_type		m_convergence_factor;
		
		int				m_iteration_limit;
		int				m_iteration_count;

		bool			m_profiling;							///< Should the monitor record profile info

		gpulab::vector<value_type,gpulab::host_memory> m_err;	///< Relative errors for each iteration
		gpulab::vector<value_type,gpulab::host_memory> m_q;		///< Convergence for each iteration
		gpulab::vector<value_type,gpulab::host_memory> m_iter;	///< Iterations per solve
		gpulab::vector<value_type,gpulab::host_memory> m_time;	///< Time per solve

		timer_type	m_timer;									///< Time used to solve

	public:

		///**
		//* Constructor 
		//*/
		//monitor(unsigned int iteration_limit, value_type relative_tolerance = 1e-5, value_type absolute_tolerance = 0, bool profile = true)
		//	: m_b_norm(0)
		//	, m_tolerance(0)
		//	, m_r_norm(std::numeric_limits<value_type>::max())
		//	, m_iteration_limit(iteration_limit)
		//	, m_relative_tolerance(relative_tolerance)
		//	, m_absolute_tolerance(absolute_tolerance)
		//	, m_iteration_count(0)
		//	, m_convergence_factor(1)
		//	, m_profiling(profile)
		//{ 
		//	if(m_profiling)
		//	{
		//		m_err.resize(iteration_limit);
		//		m_q.resize(iteration_limit);
		//	}
		//}

		/**
		* Constructor, all info for monitor must be in config
		*/
		monitor()
			: m_b_norm(0)
			, m_tolerance(0)
			, m_r_norm(std::numeric_limits<value_type>::max())
			, m_iteration_count(0)
			, m_convergence_factor(1)
		{ 
			reset();
			GPULAB_CONFIG_GET("monitor_profile", &m_profiling, false);
		}

		/**
		* Reset and reread all monitor variables.
		* This function should (usually) be called whenever an iterative solvers
		* initiates a solve.
		*/ 
		void reset()
		{
			// Reset variables
			m_iteration_count	 = 0;
			m_convergence_factor = values<value_type>::one();
			m_r_norm             = std::numeric_limits<value_type>::max();
			m_b_norm             = gpulab::values<value_type>::zero();
			m_tolerance          = gpulab::values<value_type>::zero();
			
			m_timer.restart();

			// Read variables from config
			GPULAB_CONFIG_GET("iter",&m_iteration_limit,100);
			GPULAB_CONFIG_GET("rtol",&m_relative_tolerance,1e-12);
			GPULAB_CONFIG_GET("atol",&m_absolute_tolerance,1e-12);

			if(m_profiling)
			{
				m_err.resize(0);
				m_q.resize(0);
				m_err.reserve(m_iteration_limit);
				m_q.reserve(m_iteration_limit);
			}
		}

		/**
		* Reset and reread all monitor variables and set new RHS.
		* This function should (usually) be called whenever an iterative solvers
		* initiates a solve.
		*/ 
		template <typename vector_type>
		void reset(vector_type const& b)
		{
			reset();

			// Update tolerance based on new RHS
			m_b_norm = b.nrm2();
			m_tolerance = (m_absolute_tolerance + m_relative_tolerance * m_b_norm);
		}

		/**
		* Returns true if converged in less than the specified iteration limit
		*/
		template <typename vector_type>
		bool finished(vector_type const& r)
		{
			value_type nrm = r.nrm2();
			return finished(nrm);
		}

		/**
		* Returns true if converged in less than the specified iteration limit
		*/
		bool finished(value_type r_norm)
		{
			// Set new error, keep old to check for stagnation
			value_type old_norm = m_r_norm;
			m_r_norm = r_norm;

			bool done = converged() || m_iteration_count >= m_iteration_limit;
			if(done)
			{
				value_type time = m_timer.stop();
				if(m_profiling)
					m_time.push_back(time);
			}

			if(m_profiling)
			{
#ifndef NDEBUG
				if(r_norm > old_norm && m_iteration_count > 1)
					GPULAB_LOG_DBG("Monitor detected divergence or stagnation at iteration: %i\n", m_iteration_count);
#endif
				m_err.push_back(rel_error());
				if(m_err.size()>1)
				{
					m_q.push_back(r_norm/old_norm);
					m_convergence_factor *= r_norm/old_norm;
				}
				if(done)
				{
					m_iter.push_back(m_iteration_count);
				}
			}

			//if(m_iteration_count > 1 && m_r_norm > old_norm)
			//{
			//	printf("Solver stagnated after %d iterations at rel. error %f\n",m_iteration_count,rel_error());
			//	return true;
			//}
			return done;
		}
	 
		/**
		* Returns true if the residual norm is less than the tolerance
		*/
		bool converged() const
		{
			return m_r_norm <= m_tolerance;
		}
		
		/**
		* Returns the absolute error of the residual
		*/
		value_type abs_error() const
		{
			return m_r_norm;
		}

		/**
		* Returns the relative error of the residual
		*/
		value_type rel_error() const
		{
			return m_r_norm / m_b_norm;
		}

		/**
		* Returns the convergence factor, q=i-root(prod(
		*/
		value_type convergence() const
		{
			if(m_iteration_count <= 1 && converged())
				return gpulab::values<value_type>::zero();

			return pow(m_convergence_factor,value_type(1)/value_type(m_iteration_count));
		}

		/**
		* Increment iteration count
		*/
		void operator++(void) {  ++m_iteration_count; }

		value_type const& tolerance()		   const { return m_tolerance;			}
		value_type const& relative_tolerance() const { return m_relative_tolerance; }
		value_type const& absolute_tolerance() const { return m_absolute_tolerance; }

		gpulab::vector<value_type,gpulab::host_memory> const& errors()				const { return m_err;	}
		gpulab::vector<value_type,gpulab::host_memory> const& convergence_hist()	const { return m_q;		}
		gpulab::vector<value_type,gpulab::host_memory> const& iterations()			const { return m_iter;	}
		gpulab::vector<value_type,gpulab::host_memory> const& timers()				const { return m_time;	}

		unsigned int iteration_count() const { return m_iteration_count; }
		unsigned int iteration_limit() const { return m_iteration_limit; }

		timer_type const& timer() { return m_timer; }
	};

} // namespace gpulab

#endif // GPULAB_MONITOR_H
