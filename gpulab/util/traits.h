// ==========================================================================
// Traits and more template stuff
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_UTIL_TRAITS_H
#define GPULAB_UTIL_TRAITS_H

namespace gpulab
{

namespace util
{

	/**
	* Static method val() returns an epsilon (close to zero) value,
	* depending on the value_type in use. 
	*/
	template <typename value_type>
	struct tolerance
	{
		static value_type zero() { return 0; }
	};

	/**
	* EPSILON specialized for float
	*/
	template <>
	struct tolerance<int>
	{
		static float zero() { return 0; }
	};
	
	/**
	* EPSILON specialized for float
	*/
	template <>
	struct tolerance<float>
	{
		static float zero() { return 0.00001f; }
	};

	/**
	* EPSILON specialized for double
	*/
	template <>
	struct tolerance<double>
	{
		static double zero() { return 1e-12; }
	};


} // namespace util

} // namespace gpulab

#endif // GPULAB_UTIL_TRAITS_H