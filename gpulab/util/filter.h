// ==========================================================================
// Filter
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_UTIL_FILTER_H
#define GPULAB_UTIL_FILTER_H

#include <gpulab/config.h>
#include <gpulab/type_traits.h>
#include <gpulab/FD/stencil.h>

namespace gpulab
{ 
namespace util
{
namespace kernel
{
	template <typename value_type>
	__global__
	void filter_1d(value_type* out, value_type const* in, value_type const* c, int rank, int N)
	{
		int i = IDX1D;
		int alpha = (rank-1)/2;

		// Ignore points near boundary
		if(i<alpha || i>=N-alpha)
			return;

		value_type sum = gpulab::values<value_type>::zero();
		for(int k=0; k<rank; ++k)
		{
			sum += in[i-alpha+k]*c[k];
		}
		out[i] = sum;
	}


	template <typename value_type>
	__global__
	void filter_2d(value_type* out, value_type const* in, value_type const* c, int rank, int Nx, int Ny, int dir)
	{
		int i = IDX1Dx;
		int j = IDX1Dy;
		int alpha = (rank-1)/2;

		// Ignore points near boundary
		if(i<alpha || i>=Nx-alpha || j<alpha || j>=Ny-alpha)
			return;

		//value_type sum = gpulab::values<value_type>::zero();
		//for(int k=0; k<rank; ++k)
		//{
		//	sum += ( (dir==0) ? in[idx(i-alpha+k,j,Nx)] : in[idx(i,j-alpha+k,Nx)] )*c[k];
		//}
		//out[idx(i,j,Nx)] = sum;

		//out[idx(i,j,Nx)] = 0.125*(in[idx(i-1,j,Nx)] + in[idx(i+1,j,Nx)] + in[idx(i,j-1,Nx)] + in[idx(i,j+1,Nx)]) + 0.5*in[idx(i,j,Nx)];
		//out[idx(i,j,Nx)] = 0.25*(in[idx(i,j-1,Nx)] + in[idx(i,j+1,Nx)]) + 0.5*in[idx(i,j,Nx)];
		out[idx(i,j,Nx)] = 0.25*(in[idx(i-1,j,Nx)] + in[idx(i+1,j,Nx)]) + 0.5*in[idx(i,j,Nx)];
	}

	template <typename value_type>
	__global__
	void filter_2d(	value_type* out
				  , value_type const* in
				  , value_type const* X
				  , value_type const* Y
				  , value_type dxi1
				  , value_type dxi2
				  , value_type const* c
				  , value_type const* stencil
				  , int rank
				  , int Nx
				  , int Ny
				  , int dir)
	{
		int i = IDX1Dx;
		int j = IDX1Dy;
		int alpha = (rank-1)/2;

		// Ignore points near boundary
		if(i<alpha || i>=Nx-alpha || j<alpha || j>=Ny-alpha)
			return;

		// First order X-derivatives at surface
		value_type x_xi1	= gpulab::FD::kernel::FD2D_x (X,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil,alpha);
		value_type x_xi2	= gpulab::FD::kernel::FD2D_y (X,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil,alpha);
		
		// First order Y-derivatives at surface
		value_type y_xi1	= gpulab::FD::kernel::FD2D_x (Y,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil,alpha);
		value_type y_xi2	= gpulab::FD::kernel::FD2D_y (Y,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil,alpha);
		
		value_type p_xi1 = gpulab::values<value_type>::zero();
		value_type p_xi2 = gpulab::values<value_type>::zero();
		for(int k=0; k<rank; ++k)
		{
			p_xi1 += in[idx(i-alpha+k,j,Nx)]*c[k];
			p_xi2 += in[idx(i,j-alpha+k,Nx)]*c[k];
		}

		// TMP
		p_xi1 = 0.25*(in[idx(i-1,j,Nx)] + in[idx(i+1,j,Nx)]) + 0.5*in[idx(i,j,Nx)];
		p_xi2 = 0.25*(in[idx(i,j-1,Nx)] + in[idx(i,j+1,Nx)]) + 0.5*in[idx(i,j,Nx)];

		value_type px = gpulab::transformation::kernel::u_x(p_xi1, p_xi2, x_xi1, x_xi2, y_xi1, y_xi2);
		value_type py = gpulab::transformation::kernel::u_y(p_xi1, p_xi2, x_xi1, x_xi2, y_xi1, y_xi2);

		out[idx(i,j,Nx)] = (px + py) / 2.0;//(dir==0) ? px : py;
	}

} // namespace kernel

	/**
	* Savitzky Golay filter weights 1D
	*/
	template <typename value_type>
	class savitzky_golay_filter 
	{
	private:
		typedef savitzky_golay_filter<value_type> my_type;

	protected:

		int m_rank;				
		value_type* m_coeffs_h;			///< Matrix with all shifted/non-shifted coeffs (host side)
		value_type* m_coeffs_d;			///< Matrix with all shifted/non-shifted coeffs (device side)
		
	public:

		savitzky_golay_filter(my_type const& s)
			: m_coeffs_h(0)
			, m_coeffs_d(0)
			, m_rank(7)
		{
			create_filter_matrix();
		}

		/**
		* Constructor for the stencil coefficeints.
		*/
		savitzky_golay_filter()
			: m_coeffs_h(0)
			, m_coeffs_d(0)
			, m_rank(7)
		{
			create_filter_matrix();
		}

		/**
		* Deconstructor
		*/
		~savitzky_golay_filter()
		{
			if(m_coeffs_h) free(m_coeffs_h);
			if(m_coeffs_d) cudaFree(m_coeffs_d);
		}

		template <typename V>
		void mult(V const& in, V& out) const
		{
			this->mult(in,out,typename V::memory_space());
		}

		template <typename V>
		void filter(V& v) const
		{
			V tmp(v);
			this->mult(tmp,v,typename V::memory_space());
		}


	private:

		void create_filter_matrix()
		{
			// Fill m_coeffs with shifted and non-shifted coefficients.
			// Allocate space
			m_coeffs_h    = (value_type*)malloc(m_rank*sizeof(value_type));

			m_coeffs_h[0] = -2.0 / 21.0;//-0.095238095238095;
			m_coeffs_h[1] =  3.0 / 21.0;//0.142857142857143;
			m_coeffs_h[2] =  6.0 / 21.0;//0.285714285714286;
			m_coeffs_h[3] =  7.0 / 21.0;//0.333333333333333;
			m_coeffs_h[4] =  6.0 / 21.0;//0.285714285714286;
			m_coeffs_h[5] =  3.0 / 21.0;//0.142857142857143;
			m_coeffs_h[6] = -2.0 / 21.0;//-0.095238095238095;

			// Put coeffs on device
			cudaMalloc((void **)&m_coeffs_d, m_rank*sizeof(value_type));
			cudaMemcpy(m_coeffs_d, m_coeffs_h, m_rank*sizeof(value_type), cudaMemcpyHostToDevice);
		}


		/**
		* Spezialized device version
		* Multiply the stencil to a 2d vector
		*/
		template <typename V>
		void mult(V& in, V& out, device_memory) const
		{
			typedef typename V::size_type size_type;
			size_type N = in.size();
			ASSERT(N == out.size());

			int Nx = in.Nx();
			int Ny = in.Ny();

			if(Ny==1)
			{
				// Setup kernel configuration 1d
				dim3 block(BLOCK1D(N));
				dim3 grid(GRID1D(N));
				gpulab::util::kernel::filter_1d<<<grid,block>>>(RAW_PTR(out), RAW_PTR(in), m_coeffs_d, m_rank, N);
			}
			else
			{
				// Setup kernel configuration 2d
				dim3 grid  = GRID2D(Nx,Ny);
				dim3 block = BLOCK2D(Nx,Ny);

				if(in.transformation()==0)
				{
					gpulab::util::kernel::filter_2d<<<grid,block>>>(RAW_PTR(out), RAW_PTR(in), m_coeffs_d, m_rank, Nx, Ny, 1);
				}
				else
				{
					value_type dxi1  = in.grid_space().x;
					value_type dxi2  = in.grid_space().y;
					gpulab::util::kernel::filter_2d<<<grid,block>>>(RAW_PTR(out)
																  , RAW_PTR(in)
																  , RAW_PTR((*in.transformation()->X()))
																  , RAW_PTR((*in.transformation()->Y()))
																  , dxi1
																  , dxi2
																  , m_coeffs_d
																  , RAW_PTR(in.transformation()->Dx().coeffs_device())
																  , m_rank
																  , Nx
																  , Ny
																  , 0);
					//in.copy(out);
					//gpulab::util::kernel::filter_2d<<<grid,block>>>(RAW_PTR(out)
					//											  , RAW_PTR(in)
					//											  , RAW_PTR((*in.transformation()->X()))
					//											  , RAW_PTR((*in.transformation()->Y()))
					//											  , dxi1
					//											  , dxi2
					//											  , m_coeffs_d
					//											  , RAW_PTR(in.transformation()->Dx().coeffs_device())
					//											  , m_rank
					//											  , Nx
					//											  , Ny
					//											  , 1);
				}
				
			}

			//gpulab::io::print(in,  gpulab::io::TO_BINARY_FILE,1,"in.bin");
			//gpulab::io::print(out, gpulab::io::TO_BINARY_FILE,1,"out.bin");
		}
	};

}; // namespace util
}; // namespace gpulab

#endif // GPULAB_UTIL_FILTER_H
