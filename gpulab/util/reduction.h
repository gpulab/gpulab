// ==========================================================================
// Reduction implementations
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_UTIL_REDUCTION_H
#define GPULAB_UTIL_REDUCTION_H

#include <limits>

namespace gpulab
{
namespace util
{
	/**
	* Summation reduction trait
	*/
	template <typename T>
	struct sum_reduction
	{
		__host__ __device__
		static inline T init()
		{
			return T();
		}
		
		__host__ __device__
		static inline T reduce(T t1, T t2)
		{
			return t1+t2;
		}
	};

	/**
	* Priduct reduction trait
	*/
	template <typename T>
	struct product_reduction
	{
		__host__ __device__
		static inline T init()
		{
			return T();
		}
		
		__host__ __device__
		static inline T reduce(T t1, T t2)
		{
			return t1*t2;
		}
	};

	/**
	* Comparison reduction trait
	*/
	template <typename T>
	struct equal_reduction
	{
		__host__ __device__
		static inline T init()
		{
			return T(1); // true
		}

		__host__ __device__
		static inline T reduce(T t1, T t2)
		{
			return t1==t2;
		}
	};

	/**
	* Maximum reduction trait
	*/
	template <typename T>
	struct max_reduction
	{
		__host__ __device__
		static inline T init()
		{
			return std::numeric_limits<T>::min();
		}

		__host__ __device__
		static inline T reduce(T t1, T t2)
		{
			return (t1 > t2) ? t1 : t2;
		}
	};

	/**
	* Minimum reduction trait
	*/
	template <typename T>
	struct min_reduction
	{
		__host__ __device__
		static inline T init()
		{
			return std::numeric_limits<T>::max();
		}

		__host__ __device__
		static inline T reduce(T t1, T t2)
		{
			return (t1 < t2) ? t1 : t2;
		}
	};

} // namespace util

} // namespace gpulab


#endif // GPULAB_UTIL_REDUCTION_H