// ==========================================================================
// Explicit generates a matrix
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_UTIL_GENERATE_MATRIX_H
#define GPULAB_UTIL_GENERATE_MATRIX_H

#include <gpulab/matrix.h>
#include <gpulab/type_traits.h>

namespace gpulab
{
namespace util
{

	/**
	* Generates an explict matrix from an implicit matrix implemenatiion or function
	*/
	template <typename V, typename Im, typename Ex>
	void generate_matrix(V& e1, V& e2, Im const& impl, Ex& expl)
	{
		typedef typename Ex::value_type		value_type;
		typedef typename Ex::size_type		size_type;
		typedef typename Ex::memory_space	memory_space;

		size_type I = expl.rows();
		size_type J = expl.cols();

		ASSERT(I == e2.size() && J == e1.size());

		value_type zero = gpulab::values<value_type>::zero();
		value_type one  = gpulab::values<value_type>::one();
		e1.fill(zero);
		e2.fill(zero);
		e1[0] = one;

		for(size_type j=0; j<J; ++j)
		{
			impl.mult(e1,e2);
			expl.set_col(e2,j);

			if(j<J-1)
			{
				e1[j]   = zero;
				e1[j+1] = one;
			}
		}

		//gpulab::transpose(expl, tmp);
	}

	/**
	* Generates an explict matrix from an implicit matrix implemenatiion or function
	*/
	template <typename V, typename Im, typename Ex>
	void generate_matrix(V& e, Im const& impl, Ex& expl)
	{
		V v(e.get_props());
		generate_matrix(e,v,impl,expl);
	};

} // namespace util
} // namespace gpulab


#endif // GPULAB_UTIL_GENERATE_MATRIX_H