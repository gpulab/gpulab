// ==========================================================================
// Set values in 2d arrary to 3d arrays and the other way around.
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
// Force Technology - www.forcetechnology.com
//
// Creator: Stefan Glimberg
//          Allan P. Engsig-Karup
//			Ole Lindberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_UTIL_GRID_HELPER_H
#define GPULAB_UTIL_GRID_HELPER_H

namespace gpulab
{
	namespace util
	{
		namespace kernel
		{

			template <typename value_type, typename dim_type>
			__global__
				void set_east_on_top(value_type const* east, value_type* top, 
				dim_type east_N, dim_type east_ghost0, dim_type east_ghost1,	
				dim_type top_N,  dim_type top_ghost0,  dim_type top_ghost1)
			{
				unsigned int top_j = IDX1D;

				if(	top_ghost0.y<=top_j && top_j<top_N.y-top_ghost1.y)
				{
					unsigned int east_j = top_j - top_ghost0.y + east_ghost0.y;
					top[idx(top_N.x-1-top_ghost1.x,top_j,top_N.x)] = east[idx(east_j,east_ghost0.z,east_N.y)];
				}
			};


			template <typename value_type, typename dim_type>
			__global__
				void set_xz_on_xy(
				unsigned int		phi_xz_k,
				unsigned int		phi_xy_j,
				value_type const*	phi_xz, 
				value_type*			phi_xy, 
				dim_type phi_xz_N, dim_type phi_xz_ghost0, dim_type phi_xz_ghost1,	
				dim_type phi_xy_N, dim_type phi_xy_ghost0,  dim_type phi_xy_ghost1)
			{
				unsigned int phi_xy_i = IDX1D;
				if(	phi_xy_ghost0.x<=phi_xy_i && phi_xy_i<phi_xy_N.x-phi_xy_ghost1.x)
				{
					unsigned int phi_xz_i = phi_xy_i - phi_xy_ghost0.x + phi_xz_ghost0.x;
					phi_xy[idx(phi_xy_i,phi_xy_j,phi_xy_N.x)] = phi_xz[idx(phi_xz_i,phi_xz_k,phi_xz_N.x)];
				}
			};


			template <typename value_type, typename dim_type>
			__global__
				void set_yz_on_xy(value_type const* phi_yz, value_type* phi_xy, 
				dim_type phi_yz_N, dim_type phi_yz_ghost0, dim_type phi_yz_ghost1,	
				dim_type phi_xy_N,  dim_type phi_xy_ghost0,  dim_type phi_xy_ghost1)
			{
				unsigned int phi_xy_j = IDX1D;
				if(	phi_xy_ghost0.y<=phi_xy_j && phi_xy_j<phi_xy_N.y-phi_xy_ghost1.y)
				{
					unsigned int phi_xy_i = phi_xy_ghost0.x;  // FIXME: Move to input.
					unsigned int phi_yz_j = phi_xy_j - phi_xy_ghost0.y + phi_yz_ghost0.y;
					unsigned int phi_yz_k = phi_yz_ghost0.z; // FIXME: Move to input.
					phi_xy[idx(phi_xy_i,phi_xy_j,phi_xy_N.x)] = phi_yz[idx(phi_yz_j,phi_yz_k,phi_yz_N.y)];
				}
			};


			///////////////////////////////////////
			// XY planes                         //
			///////////////////////////////////////
			template <typename size_type, typename value_type, typename dim_type>
			__global__
				void set_xy_to_xyz(size_type k, value_type const* src, value_type* dst, 
					dim_type src_N, dim_type src_ghost0, dim_type src_ghost1,	
					dim_type dst_N, dim_type dst_ghost0, dim_type dst_ghost1)
			{
				unsigned int dst_i = IDX1Dx;
				unsigned int dst_j = IDX1Dy;

				if(	dst_ghost0.x<=dst_i && dst_i<dst_N.x-dst_ghost1.x && 
					dst_ghost0.y<=dst_j && dst_j<dst_N.y-dst_ghost1.y)
				{

					unsigned int src_i = dst_i - dst_ghost0.x + src_ghost0.x;
					unsigned int src_j = dst_j - dst_ghost0.y + src_ghost0.y;

					dst[idx(dst_i,dst_j,k,dst_N.x,dst_N.y)] = src[idx(src_i,src_j,src_N.x)];
				}
			};
			template <typename size_type, typename value_type, typename dim_type>
			__global__

				void set_xyz_to_xy(size_type k, value_type const* src, value_type* dst, dim_type N, dim_type ghost0, dim_type ghost1)
			{
				unsigned int i = IDX1Dx;
				unsigned int j = IDX1Dy;

				if(	ghost0.x<=i && i<N.x-ghost1.x && 
					ghost0.y<=j && j<N.y-ghost1.y)
				{
					dst[idx(i,j,N.x)] = src[idx(i,j,k,N.x,N.y)];
				}
			};

			///////////////////////////////////////
			// XZ planes                         //
			///////////////////////////////////////
			template <typename size_type, typename value_type, typename dim_type>
			__global__
				void set_xz_to_xyz(size_type j, value_type const* src, value_type* dst, dim_type N, dim_type ghost0, dim_type ghost1)
			{
				unsigned int i = IDX1Dx;
				unsigned int k = IDX1Dy;

				if(	ghost0.x<=i && i<N.x-ghost1.x && 
					ghost0.z<=k && k<N.z-ghost1.z)
				{
					dst[idx(i,j,k,N.x,N.y)] = src[idx(i,k,N.x)];
				}
			};
			template <typename size_type, typename value_type, typename dim_type>
			__global__

				void set_xyz_to_xz(size_type j, value_type const* src, value_type* dst, dim_type N, dim_type ghost0, dim_type ghost1)
			{
				unsigned int i = IDX1Dx;
				unsigned int k = IDX1Dy;

				if(	ghost0.x<=i && i<N.x-ghost1.x && 
					ghost0.z<=k && k<N.z-ghost1.z)
				{
					dst[idx(i,k,N.x)] = src[idx(i,j,k,N.x,N.y)];
				}
			};

			///////////////////////////////////////
			// YZ planes                         //
			///////////////////////////////////////
			template <typename size_type, typename value_type, typename dim_type>
			__global__
				void set_yz_to_xyz(size_type i, value_type const* src, value_type* dst, dim_type N, dim_type ghost0, dim_type ghost1)
			{
				unsigned int j = IDX1Dx;
				unsigned int k = IDX1Dy;

				if(	ghost0.y<=j && j<N.y-ghost1.y && 
					ghost0.z<=k && k<N.z-ghost1.z)
				{
					dst[idx(i,j,k,N.x,N.y)] = src[idx(j,k,N.y)];
				}
			};
			template <typename size_type, typename value_type, typename dim_type>
			__global__

				void set_xyz_to_yz(size_type i, value_type const* src, value_type* dst, dim_type N, dim_type ghost0, dim_type ghost1)
			{
				unsigned int j = IDX1Dx;
				unsigned int k = IDX1Dy;

				if(	ghost0.y<=j && j<N.y-ghost1.y && 
					ghost0.z<=k && k<N.z-ghost1.z)
				{
					dst[idx(j,k,N.y)] = src[idx(i,j,k,N.x,N.y)];
				}
			};
		}

		///////////////////////////////////////
		// XY planes                         //
		///////////////////////////////////////
		template <typename size_type, typename grid_type>
		void set_xy_to_xyz(size_type k, grid_type const &src, grid_type &dst)
		{
			dim3 grid_xy  = GRID2D(dst.dimension().x,dst.dimension().y);
			dim3 block_xy = BLOCK2D(dst.dimension().x,dst.dimension().y);
			kernel::set_xy_to_xyz<<<grid_xy,block_xy>>>(k,RAW_PTR(src), RAW_PTR(dst),
				dim3(src.dimension().x,	src.dimension().y,	src.dimension().z),
				dim3(src.ghost0().x,	src.ghost0().y,		src.ghost0().z),
				dim3(src.ghost1().x,	src.ghost1().y,		src.ghost1().z),
				dim3(dst.dimension().x,	dst.dimension().y,	dst.dimension().z),
				dim3(dst.ghost0().x,	dst.ghost0().y,		dst.ghost0().z),
				dim3(dst.ghost1().x,	dst.ghost1().y,		dst.ghost1().z));
		};

		template <typename grid_type>
		void set_east_on_top(grid_type const& phi_east, grid_type &phi_top)
		{
			dim3 grid_y  = GRID1D(phi_top.Ny());
			dim3 block_y = BLOCK1D(phi_top.Ny());

			kernel::set_east_on_top<<<grid_y,block_y>>>(RAW_PTR(phi_east), RAW_PTR(phi_top),
				dim3(phi_east.dimension().x,  phi_east.dimension().y,	phi_east.dimension().z),
				dim3(phi_east.ghost0().x,	phi_east.ghost0().y,    phi_east.ghost0().z),
				dim3(phi_east.ghost1().x,	phi_east.ghost1().y,    phi_east.ghost1().z),
				dim3(phi_top.dimension().x,	phi_top.dimension().y,	phi_top.dimension().z),
				dim3(phi_top.ghost0().x,	phi_top.ghost0().y,		phi_top.ghost0().z),
				dim3(phi_top.ghost1().x,	phi_top.ghost1().y,		phi_top.ghost1().z));
		};

		template <typename grid_type>
		void set_xz_on_xy(
			unsigned int phi_xz_k,
			unsigned int phi_xy_j,
			grid_type const& phi_xz, 
			grid_type & phi_xy)
		{
			dim3 grid_x  = GRID1D(phi_xy.Nx());
			dim3 block_x = BLOCK1D(phi_xy.Nx());

			kernel::set_xz_on_xy<<<grid_x,block_x>>>(
				phi_xz_k,
				phi_xy_j,
				RAW_PTR(phi_xz),
				RAW_PTR(phi_xy),
				dim3(phi_xz.dimension().x,  phi_xz.dimension().y,	phi_xz.dimension().z),
				dim3(phi_xz.ghost0().x,		phi_xz.ghost0().y,		phi_xz.ghost0().z),
				dim3(phi_xz.ghost1().x,		phi_xz.ghost1().y,		phi_xz.ghost1().z),
				dim3(phi_xy.dimension().x,	phi_xy.dimension().y,	phi_xy.dimension().z),
				dim3(phi_xy.ghost0().x,		phi_xy.ghost0().y,		phi_xy.ghost0().z),
				dim3(phi_xy.ghost1().x,		phi_xy.ghost1().y,		phi_xy.ghost1().z));
		};



		template <typename grid_type>
		void set_yz_on_xy(grid_type const& phi_yz, grid_type &phi_xy)
		{
			dim3 grid_y  = GRID1D(phi_xy.Ny());
			dim3 block_y = BLOCK1D(phi_xy.Ny());

			kernel::set_yz_on_xy<<<grid_y,block_y>>>(RAW_PTR(phi_yz), RAW_PTR(phi_xy),
				dim3(phi_yz.dimension().x,  phi_yz.dimension().y,	phi_yz.dimension().z),
				dim3(phi_yz.ghost0().x,		phi_yz.ghost0().y,		phi_yz.ghost0().z),
				dim3(phi_yz.ghost1().x,		phi_yz.ghost1().y,		phi_yz.ghost1().z),
				dim3(phi_xy.dimension().x,	phi_xy.dimension().y,	phi_xy.dimension().z),
				dim3(phi_xy.ghost0().x,		phi_xy.ghost0().y,		phi_xy.ghost0().z),
				dim3(phi_xy.ghost1().x,		phi_xy.ghost1().y,		phi_xy.ghost1().z));
		};


		template <typename size_type, typename grid_type>
		void set_xyz_to_xy(size_type k, grid_type const &src, grid_type &dst)
		{
			dim3 grid_xy  = GRID2D(src.dimension().x,src.dimension().y);
			dim3 block_xy = BLOCK2D(src.dimension().x,src.dimension().y);
			kernel::set_xyz_to_xy<<<grid_xy,block_xy>>>(k,RAW_PTR(src), RAW_PTR(dst),
				dim3(src.dimension().x,	src.dimension().y,	src.dimension().z),
				dim3(src.ghost0().x,	src.ghost0().y,		src.ghost0().z),
				dim3(src.ghost1().x,	src.ghost1().y,		src.ghost1().z));
		};

		///////////////////////////////////////
		// XZ planes                         //
		///////////////////////////////////////
		template <typename size_type, typename grid_type>
		void set_xz_to_xyz(size_type j, grid_type const &src, grid_type &dst)
		{
			dim3 grid_xz  = GRID2D(dst.dimension().x,dst.dimension().z);
			dim3 block_xz = BLOCK2D(dst.dimension().x,dst.dimension().z);
			kernel::set_xz_to_xyz<<<grid_xz,block_xz>>>(j,RAW_PTR(src), RAW_PTR(dst),
				dim3(dst.dimension().x,	dst.dimension().y,	dst.dimension().z),
				dim3(dst.ghost0().x,	dst.ghost0().y,		dst.ghost0().z),
				dim3(dst.ghost1().x,	dst.ghost1().y,		dst.ghost1().z));
		};

		template <typename size_type, typename grid_type>
		void set_xyz_to_xz(size_type j, grid_type const &src, grid_type &dst)
		{

			
			dim3 grid_xz  = GRID2D(src.dimension().x,src.dimension().z);
			dim3 block_xz = BLOCK2D(src.dimension().x,src.dimension().z);
			kernel::set_xyz_to_xz<<<grid_xz,block_xz>>>(j,RAW_PTR(src), RAW_PTR(dst), // FIXME i to j
				dim3(src.dimension().x,	src.dimension().y,	src.dimension().z),
				dim3(src.ghost0().x,	src.ghost0().y,		src.ghost0().z),
				dim3(src.ghost1().x,	src.ghost1().y,		src.ghost1().z));
		};

		///////////////////////////////////////
		// YZ planes                         //
		///////////////////////////////////////
		template <typename size_type, typename grid_type>
		void set_yz_to_xyz(size_type i, grid_type const &src, grid_type &dst)
		{
			dim3 grid_yz  = GRID2D(dst.dimension().y,dst.dimension().z);
			dim3 block_yz = BLOCK2D(dst.dimension().y,dst.dimension().z);
			kernel::set_yz_to_xyz<<<grid_yz,block_yz>>>(i,RAW_PTR(src), RAW_PTR(dst),
				dim3(dst.dimension().x,	dst.dimension().y,	dst.dimension().z),
				dim3(dst.ghost0().x,	dst.ghost0().y,		dst.ghost0().z),
				dim3(dst.ghost1().x,	dst.ghost1().y,		dst.ghost1().z));
		};

		template <typename size_type, typename grid_type>
		void set_xyz_to_yz(size_type i, grid_type const &src, grid_type &dst)
		{
			dim3 grid_yz  = GRID2D(src.dimension().y,src.dimension().z);
			dim3 block_yz = BLOCK2D(src.dimension().y,src.dimension().z);
			kernel::set_xyz_to_yz<<<grid_yz,block_yz>>>(i,RAW_PTR(src), RAW_PTR(dst),
				dim3(src.dimension().x,	src.dimension().y,	src.dimension().z),
				dim3(src.ghost0().x,	src.ghost0().y,		src.ghost0().z),
				dim3(src.ghost1().x,	src.ghost1().y,		src.ghost1().z));
		};

	} // namespace util
} // namespace gpulab

#endif // GPULAB_UTIL_GRID_HELPER_H