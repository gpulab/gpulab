// ==========================================================================
// Timer class utilitites
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_UTIL_TIMER_H
#define GPULAB_UTIL_TIMER_H

#define TIME_IN_SECONDS(x) ((double)x / 1000.0)

#include <helper_cuda.h>
#include <helper_timer.h>
#include <time.h>

namespace gpulab
{
namespace util
{

	/**
	* Timer class. Wraps the basic time methods (start, stop, reset, timer) 
	* around the template type T
	* @param T must implement the () operator and return time in seconds
	*/
	template <typename Timer>
	class timer
	{
	private:
		double m_start;		///< Time started
		double m_stop;		///< Time stoped
		double m_total;		///< Total time 
		bool   m_running;	///< Are the timer running or stoped
		Timer  m_timer;		///< The timer functor that return timings

	public:

		/**
		* Default constructor
		*/
		timer()
			: m_total(0)
			, m_start(0)
			, m_stop(0)
			, m_running(false)
		{}

		/**
		* Start timer
		*/
		void start()
		{
			m_start = m_timer();
			m_running = true;
		}

		/**
		* Stop timer
		*/
		double stop()
		{
			m_running = false;
			m_stop = m_timer();
			m_total += m_stop - m_start;
			m_start = m_stop;
			return m_total;
		}

		/**
		* Reset timer
		*/
		double reset()
		{
			double time = stop();
			m_total = 0;
			m_start = time;
			m_stop = time;
			m_running = false;
			return time;
		}

		/**
		*
		*/
		double restart()
		{
			double time = reset();
			start();
			return time;
		}

		/**
		* Return total time
		* If the timer is running, this running part is also added
		*/
		double time() const
		{
			if(m_running)
			{
				return (m_total + (m_timer() - m_start));
			}
			return (m_total);
		}

	};

namespace timers
{
	/**
	* Timer implementation for the host, using clock() to measure time
	*/
	class host_timer
	{
	public:
		double operator()(void) const
		{
			return (double)clock() / (double)CLOCKS_PER_SEC;
		}
	};

	/**
	* Timer implementation for the device, using cutStartTimer() to measure time
	*/
	class device_timer
	{
	private:
		StopWatchInterface* m_gpu_timer;
	public:
		device_timer()
		{
			sdkCreateTimer(&m_gpu_timer);
			sdkResetTimer(&m_gpu_timer);
			sdkStartTimer(&m_gpu_timer);
		}

		~device_timer()
		{
			sdkStopTimer(&m_gpu_timer);
			sdkDeleteTimer(&m_gpu_timer);
		}

		double operator()(void) const
		{
			StopWatchInterface* tmp = m_gpu_timer;
			return TIME_IN_SECONDS(sdkGetTimerValue(&tmp));
		}
	};

	/**
	* Timer implementation for MPI, using MPI_Wtime to measure time
	*/
	class mpi_timer
	{
	public:

		double operator()(void) const
		{
			return MPI_Wtime();
		}
	};

} // namespace timers

	// Typedef for easy access
	typedef gpulab::util::timer<gpulab::util::timers::device_timer>	device_timer;
	typedef gpulab::util::timer<gpulab::util::timers::host_timer>	host_timer;
	typedef gpulab::util::timer<gpulab::util::timers::mpi_timer>	mpi_timer;

	/**
	* Helps select a timer based on the memory space
	*/
	template <typename MemorySpace>
	struct timer_selector
	{
		typedef host_timer timer;
	};
	template <>
	struct timer_selector<gpulab::host_memory>
	{
		typedef host_timer timer;
	};
	template <>
	struct timer_selector<gpulab::device_memory>
	{
		typedef device_timer timer;
	};

} // namespace util

} // namespace gpulab

#endif // GPULAB_UTIL_TIMER_H