// ==========================================================================
// Type Traits
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#include <math.h>

#ifndef GPULAB_UTIL_MATH_H
#define GPULAB_UTIL_MATH_H

namespace gpulab
{
namespace util
{

	class power2
	{
	public:
			
		unsigned ceil( unsigned x ) {
			--x; x |= x >> 1; x |= x >> 2; x |= x >> 4; x |= x >> 8; x |= x >> 16; return ++x;
		}

		unsigned floor( unsigned x ) {
			unsigned y = x;
			--x; x |= x >> 1; x |= x >> 2; x |= x >> 4; x |= x >> 8; x |= x >> 16; ++x;
			x = x == y ? x : x >> 1;
			return x;
		}

		unsigned divisor( unsigned x ) {
			unsigned y = 1;
			while (!(x & y)) {
				y <<= 1;
			}
			return y;
		}
	};

} // namespace util
} // namespace gpulab


#endif // GPULAB_UTIL_MATH_H