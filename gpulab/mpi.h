// ==========================================================================
// MPI handlers
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// Date: November, 2011
// ==========================================================================

#ifndef GPULAB_MPI_H
#define GPULAB_MPI_H

#ifdef TARGET_OS_MAC
	#include <unistd.h>
#endif
#ifdef __linux__
	#include <unistd.h>
#endif
#if defined(_WIN32) || defined(_WIN64)
	#include<Winsock2.h>  
#endif

#include <mpi.h>
#include <iostream>
#include <gpulab/config.h>
#include <gpulab/type_traits.h>
#include <gpulab/vector.h>

// Error handling macro
#define MPI_CHECK(call) \
    if((call) != MPI_SUCCESS) { \
		std::cerr << "MPI error calling \""#call"\"\n"; \
		MPI_Abort(gpulab::GPULAB_COMM_WORLD, -1); }

namespace gpulab
{

	static MPI_Comm GPULAB_COMM_WORLD;

	//struct MPI_TYPES
	//{
	//	template <typename T> static MPI_Datatype get_type()		{ return MPI_FLOAT;				}
	//	
	//	template <> static MPI_Datatype get_type<char>()			{ return MPI_SIGNED_CHAR;		}
	//	template <> static  MPI_Datatype get_type<unsigned char>()	{ return MPI_UNSIGNED_CHAR;		}
	//	//template <> static  MPI_Datatype get_type<byte>()			{ return MPI_BYTE;				}
	//	//template <> static  MPI_Datatype get_type<wchar>()		{ return MPI_WCHAR;				}
	//	template <> static  MPI_Datatype get_type<short>()			{ return MPI_SHORT;				}
	//	template <> static  MPI_Datatype get_type<unsigned short>()	{ return MPI_UNSIGNED_SHORT;	}
	//	template <> static  MPI_Datatype get_type<int>()			{ return MPI_INT;				}
	//	template <> static  MPI_Datatype get_type<unsigned int>()	{ return MPI_UNSIGNED;			}
	//	template <> static  MPI_Datatype get_type<long>()			{ return MPI_LONG;				}
	//	template <> static  MPI_Datatype get_type<unsigned long>()	{ return MPI_UNSIGNED_LONG;		}
	//	template <> static  MPI_Datatype get_type<float>()			{ return MPI_FLOAT;				}
	//	template <> static  MPI_Datatype get_type<double>()			{ return MPI_DOUBLE;			}
	//	template <> static  MPI_Datatype get_type<long double>()	{ return MPI_LONG_DOUBLE;		}
	//	template <> static  MPI_Datatype get_type<long long int>()	{ return MPI_LONG_LONG_INT;		}
	//	template <> static  MPI_Datatype get_type<unsigned long long>() { return MPI_UNSIGNED_LONG_LONG; }
	//	//template <> static  MPI_Datatype get_type<long long>()		{ return MPI_LONG_LONG;			}
	//};

	int GPULAB_rank()
	{
		int rank;
		MPI_CHECK( MPI_Comm_rank(GPULAB_COMM_WORLD,&rank) );
		return rank;
	}

	bool GPULAB_is_root()
	{
		return GPULAB_rank() == 0;
	}


	/**
	* Returns the rank id of the right neighbor (periodicly)
	*/
	int rank_right(bool periodic = true)
	{
		int rank, size;
		MPI_CHECK( MPI_Comm_rank(GPULAB_COMM_WORLD, &rank) );	///< Get current process id
		MPI_CHECK( MPI_Comm_size(GPULAB_COMM_WORLD, &size) );	///< Get number of processes
		return (periodic) ? (rank + 1) % size : min(size-1,rank+1);	
	}

	/**
	* Returns the rank id of the left neighbor (periodicly)
	*/
	int rank_left(bool periodic = true)
	{
		int rank, size;
		MPI_CHECK( MPI_Comm_rank(GPULAB_COMM_WORLD, &rank) );	///< Get current process id
		MPI_CHECK( MPI_Comm_size(GPULAB_COMM_WORLD, &size) );	///< Get number of processes
		return (periodic) ? (rank == 0 ? size-1 : rank-1) : max(0,rank-1);	
	}



	template <typename V>
	void exchange_vector(V& v, int dest, int source, gpulab::host_memory, MPI_Comm comm)
	{
		typedef typename V::value_type value_type;
		
		V recv(v.size());
		MPI_Status s;
		MPI_CHECK( MPI_Sendrecv(v.data()
					, v.size()
					, gpulab::values<value_type>::MPI_TYPE()
					, dest
					, /* tag */ 0
					, recv.data()
					, v.size()
					, gpulab::values<value_type>::MPI_TYPE()
					, source
					, /* recvtag */0
					, comm
					, &s) );
		v.copy(recv);
	}


	template <typename V>
	void exchange_vector(V& v, int dest, int source, gpulab::device_memory, MPI_Comm comm)
	{
		typedef typename V::value_type value_type;
		value_type* ptr1;
		value_type* ptr2;
		cudaStream_t cpy_stream;
		cudaStreamCreate(&cpy_stream);
		
		// Make async device to host copy to ptr1
		ptr1 = (value_type*)malloc(v.size()*sizeof(value_type));
		cudaMemcpyAsync(ptr1,RAW_PTR(v),v.size()*sizeof(value_type),cudaMemcpyDeviceToHost,cpy_stream);
		ptr2 = (value_type*)malloc(v.size()*sizeof(value_type));
		cudaStreamSynchronize(cpy_stream);

		// For some reason this is NOT better!?
		// Use page-locked (pinned) memory for faster transfers
		// Probably because the overhead of creating/freeing pinned memory is more expensive
		//cudaMallocHost(&ptr1,v.size()*sizeof(value_type));
		//cudaMallocHost(&ptr2,v.size()*sizeof(value_type));

		// MPI send-receive
		MPI_Status s;
		MPI_CHECK( MPI_Sendrecv(ptr1
					, v.size()
					, gpulab::values<value_type>::MPI_TYPE()
					, dest
					, /* tag */ 0
					, ptr2
					, v.size()
					, gpulab::values<value_type>::MPI_TYPE()
					, source
					, /* recvtag */0
					, comm
					, &s) );

		// Copy recevied data back to device vector
		cudaMemcpyAsync(RAW_PTR(v),ptr2,v.size()*sizeof(value_type),cudaMemcpyHostToDevice,cpy_stream);

		// Clean up
		free(ptr1);
		cudaStreamSynchronize(cpy_stream);
		free(ptr2);
		cudaStreamDestroy(cpy_stream);

		//cudaFreeHost(ptr1);
		//cudaFreeHost(ptr2);
	}

	template <typename V>
	void exchange_vector(V& v, int dest, int source, MPI_Comm comm = GPULAB_COMM_WORLD)
	{
		gpulab::exchange_vector(v,dest,source,typename V::memory_space(),comm);
	}

	template <typename V>
	void exchange_vector(V& v, int dest, MPI_Comm comm = GPULAB_COMM_WORLD)
	{
		gpulab::exchange_vector(v,dest,dest,comm,typename V::memory_space(),comm);
	}

	/**
	* The error of a grid funtion is the intergral over all dimensions, 
	* thus the norm2 is scaled with the grid sizes.
	*/
	template <typename V, typename T>
	void global_err_nrm2(const V& v, T* res, MPI_Comm comm = GPULAB_COMM_WORLD)
	{
		// For MPI programs this should be a reduce sum first
		int comm_size;
		MPI_Comm_size(comm,&comm_size);
		if( comm_size > 1 )
		{
			T n = v.dot(v);
			T ntotal;
			MPI_CHECK( MPI_Allreduce(&n, &ntotal, 1, values<T>::MPI_TYPE(), MPI_SUM, GPULAB_COMM_WORLD) );
			//printf("Rank: %d, n: %f, nt: %f\n", commRank,n,ntotal);
			T h = v.get_props().delta_prod();
			*res = sqrt(h*ntotal);
		}
		else
		{
			// Trivial
			*res = v.err_nrm2();
		}
	}

	/**
	* Send a state vector to the given destination. Make sure that there is
	* always a matching receive (recv_state) or else it will hang.
	* @param state to send
	* @param dest destination
	*/
	template <typename T>
	void send_state(gpulab::vector<T,gpulab::host_memory>& v, int dest, MPI_Comm const& comm = GPULAB_COMM_WORLD)
	{
		// Send v host data
		MPI_CHECK( MPI_Send(v.data()
					, v.size()
					, gpulab::values<T>::MPI_TYPE()
					, dest
					, /* tag */ 0
					, comm) );
		//gpulab::io::print(v,gpulab::io::TO_SCREEN,1,"send.bin");
	}

	/**
	* Send a state vector to the given destination. Make sure that there is
	* always a matching receive (recv_state) or else it will hang.
	* Template version that will take care of transfereing data from V to a 
	* host memory buffer.
	* @param state to send
	* @param dest destination
	*/
	template <typename V>
	void send_state(V& state, int dest, MPI_Comm const& comm = GPULAB_COMM_WORLD)
	{
		typedef typename V::value_type							value_type;
		typedef gpulab::vector<value_type,gpulab::host_memory>	host_vector_type;
		// Create a vector on the host and move data from device
		host_vector_type tmp(state);
		send_state(tmp, dest, comm);
	}

	/**
	* Send a ode container state to the given destination. Make sure that there is
	* always a matching receive (recv_state) or else it will hang.
	* Template version that will take care of transfereing data from each vector in
	* the container to a host memory buffer.
	* @param state to send
	* @param dest destination
	*/
	template <typename vector_type, template <typename> class container_type >
	void send_state(container_type<vector_type>& state, int dest, MPI_Comm const& comm = GPULAB_COMM_WORLD)
	{
		typedef typename vector_type::const_iterator			const_iterator;
		typedef typename vector_type::value_type				value_type;
		typedef gpulab::vector<value_type,gpulab::host_memory>	host_vector_type;
		typedef typename host_vector_type::iterator				iterator;

		// Create a vector on the host and move data from device
		host_vector_type tmp(state.size());
		iterator it = tmp.begin();
		for(unsigned int i=0; i<state.N; ++i)
		{
			tmp.copy(state[i].begin(),state[i].end(),it);
			it += state[i].size();
		}
		send_state(tmp, dest, comm);
	}
	
	/**
	* Receive a state from the given source. Make sure that there is
	* always a matching send (send_state) or else it will hang.
	* @param state to send
	* @param src source
	*/
	template <typename T>
	void recv_state(gpulab::vector<T,gpulab::host_memory>& v, int src, MPI_Comm const& comm = GPULAB_COMM_WORLD)
	{
		MPI_Status status;
		MPI_CHECK( MPI_Recv(v.data()
					, v.size()
					, gpulab::values<T>::MPI_TYPE()
					, src
					, /* tag */ 0
					, comm
					, &status) );
		//gpulab::io::print(v,gpulab::io::TO_SCREEN,1,"recv.bin");
	}

	/**
	* Receive a state from the given source. Make sure that there is
	* always a matching send (send_state) or else it will hang.
	* Template version that will take care of transfereing data from V to a 
	* host memory buffer.
	* @param state to send
	* @param src source
	*/
	template <typename V>
	void recv_state(V& state, int src, MPI_Comm const& comm = GPULAB_COMM_WORLD)
	{
		typedef typename V::value_type							value_type;
		typedef gpulab::vector<value_type,gpulab::host_memory>	host_vector_type;
		// Allocate space on host
		host_vector_type tmp(state.size());
		// Receive data
		recv_state(tmp, src, comm);
		// Move data from host to state
		state.copy(tmp);
	}

	/**
	* Broadcast a state from the given source. 
	* @param state to broadcast
	* @param src source
	*/
	template <typename T>
	void broadcast_state(gpulab::vector<T,gpulab::host_memory>& v, int src, MPI_Comm const& comm = GPULAB_COMM_WORLD)
	{
		MPI_CHECK( MPI_Bcast(v.data()
					, v.size()
					, gpulab::values<T>::MPI_TYPE()
					, src
					, comm) );
	}

	/**
	* Broadcast a state from the given source.
	* Template version that will take care of transfereing data from V to a 
	* host memory buffer.
	* @param state to broadcast
	* @param src source
	*/
	template <typename V>
	void broadcast_state(V& state, int src, MPI_Comm const& comm = GPULAB_COMM_WORLD)
	{
		typedef typename V::value_type							value_type;
		typedef gpulab::vector<value_type,gpulab::host_memory>	host_vector_type;

		int rank;
		MPI_Comm_rank(comm,&rank);
		if(rank==src)
		{
			// Allocate space and copy data on host 
			host_vector_type tmp(state);
			// Broadcast data
			broadcast_state(tmp, src, comm);
		}
		else
		{
			// Allocate space on host 
			host_vector_type tmp(state.size());
			// Breadcast data
			broadcast_state(tmp, src, comm);
			// Move data from host to device
			state.copy(tmp);
		}
	}

	/**
	* Broadcast an ode container state from the given source.
	* Template version that will take care of transfereing data from vectors 
	* int the container to a host memory buffer.
	* @param state to broadcast
	* @param src source
	*/
	template <typename vector_type, template <typename> class container_type >
	void broadcast_state(container_type<vector_type>& state, int src, MPI_Comm const& comm = GPULAB_COMM_WORLD)
	{
		typedef typename vector_type::const_iterator			const_iterator;
		typedef typename vector_type::value_type				value_type;
		typedef gpulab::vector<value_type,gpulab::host_memory>	host_vector_type;
		typedef typename host_vector_type::iterator				iterator;

		int rank;
		MPI_Comm_rank(comm,&rank);
		if(rank==src)
		{
			// Allocate space and copy data on host 
			host_vector_type tmp(state.size());
			iterator it = tmp.begin();
			for(unsigned int i=0; i<state.N; ++i)
			{
				tmp.copy(state[i].begin(),state[i].end(),it);
				it += state[i].size();
			}
			// Broadcast data
			broadcast_state(tmp, src, comm);
		}
		else
		{
			// Allocate space on host 
			host_vector_type tmp(state.size());
			// Breadcast data
			broadcast_state(tmp, src, comm);
			// Move data from host to device
			state.copy(tmp);
		}
	}

} // namespace gpulab


#endif // GPULAB_MPI_H
