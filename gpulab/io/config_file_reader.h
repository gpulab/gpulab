// ==========================================================================
// Utilities for reading config files
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// Date:    October, 2011
// ==========================================================================

#ifndef GPULAB_IO_CONFIG_FILE_READER_H
#define GPULAB_IO_CONFIG_FILE_READER_H

#include <stdio.h>
#include <string>
#include <map>
#include <gpulab/io/read.h>

#define GPULAB_CONFIG					gpulab::io::config_manager::instance()->config()
#define GPULAB_CONFIG_GET(a,b,c)		gpulab::io::config_manager::instance()->config().get(a,b,c)
#define GPULAB_CONFIG_GET_3_ARGS(a,b,c) gpulab::io::config_manager::instance()->config().get(a,b,c)
#define GPULAB_CONFIG_GET_2_ARGS(a,b)   gpulab::io::config_manager::instance()->config().get(a,b)
#define GPULAB_CONFIG_SET(a,b)			gpulab::io::config_manager::instance()->config().set(a,b)

namespace gpulab
{ 
namespace io
{

	
	/**
	* Read all data from a text file, formatted with the name of the parameter first, then a white space, then the value
	*/
	class config_file_reader
	{
	private:
		std::ifstream						m_file;
		std::string							m_file_path;
		std::string							m_file_dir;
		bool								m_valid;
		std::map<std::string,std::string>	m_map;

	public:
	
		config_file_reader(std::string filename)
			: m_file_path(filename)
			, m_valid(false)
		{
			access_file();
		}


		~config_file_reader()
		{
			if(m_file.is_open())
				m_file.close();
		}

		std::string config_path() const { return m_file_path; }
		std::string config_dir () const { return m_file_dir;  }

		/**
		* Set the value of a parameter.
		* If the parameter does not exists, it will be added.
		*/
		template <typename T>
		void set(std::string name, T const& val)
		{
			std::string strval;
			gpulab::io::to_string<T>(strval,val,std::dec);
			m_map[name] = strval;
		}

		
		/**
		* Get the value of the specified param name.
		* @param name property name to look for
		* @param val the value of the name to look for
		*/
		template <typename T>
		bool get(std::string const& name, T* val)
		{
			bool found = false;
			if(m_map.count(name)>0)
			{
				std::string strval = m_map[name];
				gpulab::io::from_string<T>(*val, strval, std::dec);
				found = true;
			}
			else if(m_valid)
			{
				m_file.open(m_file_path.c_str()); // reopen, otherwise it does not work after a non found get_param ?!
			
				std::string namesearch(name);
				namesearch.append(" ");				
				m_file.seekg(0, std::ios::beg);
				std::string::size_type N = namesearch.size();
				std::string line;
				while (!m_file.eof())
				{
					std::getline (m_file,line);
					if(line.compare(0,N,namesearch) == 0)
					{
						std::string strval = line.substr(N,line.length()-N);
						gpulab::io::from_string<T>(*val, strval, std::dec);
						m_file.close();

						// Value found, so add it to map
						m_map[name] = strval;
						found = true;
					}
				}
				m_file.close();
			}
			return found;
		}
		
		/**
		* Get the value of the specified param name.
		* @param name property name to look for
		* @param val the value of the name to look for
		* @param def default value if name is not found
		*/
		template <typename T1, typename T2>
		bool get(std::string const& name, T1* val, T2 def = T2())
		{
			if(get(name,val))
			{
				return true;
			}
			else
			{
				*val = T1(def); // Casting in here means that the user do not have to do it from the function call itself
				std::stringstream msg;
				msg << "Did not find param \"" << name.c_str() << "\" in config file, using default value: " << *val << "\n";
				GPULAB_LOG_INF(msg.str().c_str());
				set(name, *val); // Set it for next time
			}
			return false;
		}

		/**
		* Get the vector of the specified param name.
		* @param name property name to look for
		* @param vec the vector to put values into
		*/
		template <typename V>
		bool get_vector(std::string name, V* vec)
		{
			typedef typename V::value_type value_type;

			if(m_valid)
			{
				m_file.open(m_file_path.c_str()); // reopen, otherwise it does not work after a non found get_param ?!
			
				name.append(" ");
				m_file.seekg(0, std::ios::beg);
				std::string::size_type N = name.size();
				std::string line;
				while (!m_file.eof())
				{
					std::getline (m_file,line);
					if(line.compare(0,N,name) == 0)
					{
						std::string strval = line.substr(N,line.length()-N);
						
						// If the value is a white space separated list of values, put these values into vec
						if(strval.find(' ')!=std::string::npos)
						{
							std::stringstream ss(strval);
							std::string s;
							value_type val;
							while (getline(ss, s, ' '))
							{
								gpulab::io::from_string<value_type>(val, s, std::dec);
								vec->insert(vec->end(),val);
							}
						}
						// Otherwise a name for a binary vector file is specified
						else
						{
							std::string filepath = (m_file_dir.length() == 0) ? strval : m_file_dir + "/" + strval;
							gpulab::io::read(filepath, *vec, gpulab::io::BINARY);
						}
						m_file.close();
						return true;
					}
				}
				m_file.close();
			}
			printf("Did not find param %s in config file, or the config file is invalid.\n", name.c_str());
			return false;
		}

		/**
		* Get the vector from a file named as the value of the specified param name
		* @param name property name to look for
		* @param vec the vector to put values into
		*/
		template <typename V>
		bool get_vector_from_file(std::string name, V* vec, gpulab::io::FILE_FORMAT format = gpulab::io::BINARY, int overlap = 0)
		{
			typedef typename V::value_type value_type;

			std::string filename;
			bool found = get(name,&filename);
			if(found)
			{
				std::string filepath = (m_file_dir.length() == 0) ? filename : m_file_dir + "/" + filename;
				gpulab::io::read(filename, *vec, format, overlap);
			}
			else
			{
				GPULAB_LOG_INF("Did not find param %s in config file, or the config file is invalid.\n", name.c_str());
				return false;
			}
			return true;
		}

	private:

		void access_file()
		{
			//bool valid = gpulab::io::safe_file_request(m_file_path, m_file);
			m_file.open(m_file_path.c_str());
			if(!m_file.is_open())
			{
				GPULAB_LOG_WRN("Warning: No config file found.\n");
			}
			else
			{
				m_valid = true;
				m_file_dir = (m_file_path.find_last_of("/") == std::string::npos) ? "" : m_file_path.substr(0,m_file_path.find_last_of("/"));
				m_file.close(); // Close it for now. Open when needed.
			}
		}

	};

	/**
	* Manage configuration using the static intance of this class
	*/
	class config_manager
	{
	private:
		static gpulab::io::config_manager*	s_instance;
		gpulab::io::config_file_reader		m_config;

		config_manager(std::string filename)
			: m_config(filename)
		{}

	public:

		static gpulab::io::config_manager* instance(std::string config = "config.ini")
		{
			if(s_instance==0)
			{
				s_instance = new gpulab::io::config_manager(config);
			}
			return s_instance;
		}


		gpulab::io::config_file_reader& config()
		{
			return m_config;
		}

	};
	gpulab::io::config_manager* gpulab::io::config_manager::s_instance = 0;


}; // namespace io
}; // namespace gpulab

#endif // GPULAB_IO_CONFIG_FILE_READER_H
