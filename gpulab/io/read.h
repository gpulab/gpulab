// ==========================================================================
// Print
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_IO_READ_H
#define GPULAB_IO_READ_H

#include <stdio.h>
#include <mpi.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <gpulab/vector.h>
#include <gpulab/config.h>

namespace gpulab
{ 
namespace io
{
	// From http://www.codeguru.com/forum/showthread.php?t=231054
	template <class T>
	bool from_string(T& t, 
					 const std::string& s, 
					 std::ios_base& (*f)(std::ios_base&))
	{
	  std::istringstream iss(s);
	  return !(iss >> f >> t).fail();
	}

	template <class T>
	bool to_string(std::string& s, 
				  T const& t, 
				  std::ios_base& (*f)(std::ios_base&))
	{
	  std::ostringstream oss;
	  if((oss << f << t).fail())
		  return false;
	  s = oss.str();
	  return true;
	}


	class file_not_found_exception : public std::exception
	{ 
	public:
		file_not_found_exception(const char* file)
			: std::exception()
		{
			printf("Warning: File not found (%s)\n",file);
		}
	};

	enum FILE_FORMAT
	{
		TEXT_LINE_SEPARATED,
		BINARY,
		BINARY_SCATTERED
	};

	bool safe_file_request(std::string& filename, std::ifstream& file)
	{
		file.open(filename.c_str());
		if (file.is_open())
		{
			return true;
		}
		else
		{
			printf("Requested file not found: %s\n", filename.c_str());
			
			// Keep asking for file
			while(1)
			{
				printf("Enter the name of the file or press enter to cancel:\n");
				getline(std::cin, filename);
				if(filename.empty())
				{
					printf("Reading file cancelled\n");
					return false;
				}
				file.open(filename.c_str());
				if(file.is_open())
				{
					printf("Found file: %s\n", filename.c_str());
					return true;
				}
			}
		}	
	}

	/**
	* Read into V via READ_FORMAT.
	* @param filename to read from
	* @param dest vector holding file info after reading
	* @param format of the file to be read
	* @return ture if everything succeeded, false otherwise
	*/
	template <typename V>
	void read(const std::string& filename, V& dest, FILE_FORMAT format = TEXT_LINE_SEPARATED, int overlap = 0) throw(gpulab::io::file_not_found_exception)
	{
		typedef typename V::value_type value_type;

		try
		{
			switch(format)
			{
				case BINARY:
				{
					// Use mpi to read
					MPI::Status status;
					int myrank   = MPI::COMM_WORLD.Get_rank();
					int numprocs = MPI::COMM_WORLD.Get_size();
					MPI::File file = MPI::File::Open(MPI::COMM_WORLD, filename.c_str(), MPI::MODE_RDONLY, MPI::INFO_NULL);
					MPI::Offset size = file.Get_size();
					int bufsize = size / sizeof(value_type);

					value_type* buf = (value_type *) malloc(bufsize * sizeof(value_type));
					file.Read(buf, bufsize, gpulab::values<value_type>::MPI_TYPE(), status);
					int count = status.Get_count(gpulab::values<value_type>::MPI_TYPE());
					
					//printf("I am %i and have bufsize %i read from %i to %i, count: %i\n", myrank, bufsize, 0, bufsize, count);

					dest.resize(count);
					dest.copy(buf, buf+count);
					//gpulab::io::print(dest);
					
					free(buf);
					file.Close();

					break;
				}
				case BINARY_SCATTERED:
				{
					
					MPI::Status status;
					int myrank   = MPI::COMM_WORLD.Get_rank();
					int numprocs = MPI::COMM_WORLD.Get_size();

					MPI::File file = MPI::File::Open(MPI::COMM_WORLD, filename.c_str(), MPI::MODE_RDONLY, MPI::INFO_NULL);
					MPI::Offset size = file.Get_size();
					size = size / sizeof(value_type);
					
					int bufsize = (size + numprocs - 1) / numprocs;
					int begin = myrank*bufsize-myrank*overlap;
					int end = (myrank == numprocs-1) ? size-1 : begin + bufsize - 1;
					bufsize = end-begin+1;

					value_type* buf = (value_type *) malloc(bufsize * sizeof(value_type));
					file.Set_view(begin * sizeof(value_type)
								, gpulab::values<value_type>::MPI_TYPE()
								, gpulab::values<value_type>::MPI_TYPE()
								, "native"
								, MPI::INFO_NULL);
					file.Read(buf, bufsize, gpulab::values<value_type>::MPI_TYPE(), status);
					int count = status.Get_count(gpulab::values<value_type>::MPI_TYPE());
					
					//printf("I am %i and have bufsize %i read from %i to %i, count: %i\n", myrank, bufsize, begin, end, count);

					dest.resize(count);
					dest.copy(buf, buf+count);
					//gpulab::io::print(dest);
					
					free(buf);
					file.Close();

					break;
				}
				case TEXT_LINE_SEPARATED:
				{
					std::ifstream file (filename.c_str());
					if (file.is_open())
					{
						unsigned int N = 1 + std::count(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>(), '\n');
						file.seekg(0, std::ios::beg);

						if(N != dest.size())
							dest.resize(N);

						unsigned int i = 0;
						std::string line;
						value_type val;
						while (! file.eof() )
						{
							std::getline (file,line);
							from_string<value_type>(val, line, std::dec);

							//sscanf(line.c_str(), "%f\n", &val);
							dest[i] = val;
							++i;
						}
						file.close();
					}
					else
					{
						throw gpulab::io::file_not_found_exception(filename.c_str());
					}	
					break;
				}
			}
		}
		// Something went wrong, maybe the file does not exist
		catch(std::exception e) 
		{
			e.what();
		}
	}

	
	/**
	* Read into V via READ_FORMAT.
	* @param filename to read from
	* @param dest vector holding file info after reading
	* @param format of the file to be read
	* @return ture if everything succeeded, false otherwise
	*/
	template <typename V>
	void read_or_request(V& dest, std::string filename = "", FILE_FORMAT format = TEXT_LINE_SEPARATED)
	{
		if(!filename.empty())
		{
			try
			{
				io::read(filename,dest,format);
				return;
			}
			catch (io::file_not_found_exception)
			{
				printf("Requested file not found: %s\n", filename.c_str());
			}
		}

		// Keep asking for file
		while(1)
		{
			printf("Enter the name of the file:\n");
			getline(std::cin, filename);
			if(filename.empty())
			{
				printf("Reading file cancelled\n");
				return;
			}
			try
			{
				io::read(filename,dest,format);
				printf("Found and read file: %s\n", filename.c_str());
				return;
			}
			catch (io::file_not_found_exception&)
			{
				printf("Requested file not found: %s\n", filename.c_str());
			}
		}
	

		
		
	}


}; // namespace io
}; // namespace gpulab

#endif // GPULAB_IO_READ_H
