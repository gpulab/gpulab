// ==========================================================================
// Output handlers
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// Date:    August 25th, 2011
// ==========================================================================

#ifndef GPULAB_IO_LOG_H
#define GPULAB_IO_LOG_H

#if defined(_WIN32) || defined(_WIN64)
#include <direct.h>
#else

#endif

#include <algorithm>
#include <sys/stat.h>   // For stat().
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sstream>
#include <string>
#include <time.h>
#include <gpulab/mpi.h>
#include <gpulab/config.h>
#include <gpulab/util/timer.h>
#include <gpulab/io/print.h>

#define LOG_BUFF_SIZE 1024

namespace gpulab
{ 
namespace io
{

	// Level of logging
	enum log_level {log_error, log_warning, log_info, log_debug};

	/** 
	* We want to be able to call logging in a static way.
	* In this case, onw would register a log instance (maybe several?). Then all logging goes through the logger.
	* However, I do see how to do this generically, if the instance is a template class?
	*/
	class logger
	{
	public:
		virtual ~logger () {}
		virtual void print (const char* message) = 0;
	};

	class log_manager
	{
	public: 

	private:
		static gpulab::io::log_manager*		s_instance;
		std::vector<gpulab::io::logger*>	m_log_instances;
		gpulab::io::log_level				m_level;

		typedef std::vector<gpulab::io::logger*>::iterator iterator;

		log_manager()
			: m_log_instances(0)
			, m_level(gpulab::io::log_info)
		{}

	public:

		static gpulab::io::log_manager* instance()
		{
			if(s_instance==0)
			{
				s_instance = new gpulab::io::log_manager();
			}
			return s_instance;
		}

		template <typename log_type>
		void register_logger()
		{
			m_log_instances.push_back(new log_type());
		}

		void set_level(gpulab::io::log_level lvl)
		{
			m_level = lvl;
		}

		void set_level(int lvl)
		{
			m_level = (gpulab::io::log_level)lvl;
		}

		void set_level(std::string const& lvl)
		{
			std::string lvlcmp(lvl);
			std::transform(lvlcmp.begin(), lvlcmp.end(), lvlcmp.begin(), ::tolower);

			if(lvlcmp.compare("debug") == 0)
				m_level = gpulab::io::log_debug;
			else if(lvlcmp.compare("info") == 0)
				m_level = gpulab::io::log_info;
			else if (lvlcmp.compare("warning") == 0)
				m_level = gpulab::io::log_warning;
			else if (lvlcmp.compare("error") == 0)
				m_level = gpulab::io::log_error;
		}

		gpulab::io::log_level const& get_level()
		{
			return m_level;
		}

		void print(gpulab::io::log_level lvl, const char* msg)
		{
			if(lvl>m_level)
				return;

			iterator it  = m_log_instances.begin();
			iterator end = m_log_instances.end();
			while(it!=end)
			{
				(*it)->print(msg);
				++it;
			}
		}
	};
	gpulab::io::log_manager* gpulab::io::log_manager::s_instance = 0;

	/**
	* Temporary log holder that pass along the log msg to registered instances
	**/
	class log_holder
	{
	public:
		log_holder(gpulab::io::log_level lvl, const char* file, int line ) 
			: m_file(file), m_level(lvl), m_line(line)
		{}
	    
		void operator()(const char* format, ... )
		{
			if(m_level > gpulab::io::log_manager::instance()->get_level())
				return;

			char buff[LOG_BUFF_SIZE];
			memset(buff, 0, sizeof(buff));
			va_list args;
			va_start(args, format);
			vsnprintf( buff, sizeof(buff)-1, format, args);
			va_end( args );
			gpulab::io::log_manager::instance()->print(m_level,buff);
		}
	private:
		   gpulab::io::log_level m_level;
		   const char* m_file;
		   int m_line;
	};


#define __GPULAB_LOG(lvl) (gpulab::io::log_holder(lvl, __FILE__, __LINE__ ))
#define GPULAB_LOG_ERR  __GPULAB_LOG(gpulab::io::log_error)
#define GPULAB_LOG_WRN  __GPULAB_LOG(gpulab::io::log_warning)
#define GPULAB_LOG_INF  __GPULAB_LOG(gpulab::io::log_info)
#ifndef NDEBUG
	#define GPULAB_LOG_DBG  __GPULAB_LOG(gpulab::io::log_debug)
#else
	#define GPULAB_LOG_DBG
#endif

	/**
	* Default logging instance that logs to the screen.
	* Displays time and mpi rank in front of log msg.
	**/
	class log_default : public logger
	{
	private:
		double m_starttime;
	public:
		log_default() : m_starttime(MPI_Wtime())
		{}

		void print (const char* msg)
		{
			double time  = MPI_Wtime()-m_starttime;
			int minutes  = ((int)time) / 60;
			int seconds  = ((int)time) % 60;
			int dseconds = (int)(100*(time - (double)seconds) - (60*minutes)*100);

			printf("[%02d:%02d:%02d] [%i] %s", minutes, seconds, dseconds, gpulab::GPULAB_rank(), msg);
		}
	};


	/**
	* DEPRECATED
	* This log writes info to the the screen and a log file.
	* All vector info is written to binare files.
	*/
	template <typename Types>
	class log_all
	{
	protected:
		typedef typename Types::grid_type												grid_type;
		typedef typename Types::value_type												value_type;
		typedef typename Types::size_type												size_type;
		typedef typename gpulab::util::timer_selector<typename grid_type::memory_space>::timer	timer_type;

		int			m_sol_count;			///< No. solutions printed
		int			m_successful_solves;	///< No. successed solves
		int			m_solves;				///< No. total solve attemps
		int			m_total_iter;			///< No. total iterations for total solves
		double		m_total_solve_time;		///< Total time used by equations solvers

		std::string m_logdir;
		FILE *		m_fp;
		timer_type	m_timer;				///< Keeps track of total application time
		

	public:

		template <typename config_type>
		log_all(config_type& config)
			: m_sol_count(0)
			, m_successful_solves(0)
			, m_solves(0)
			, m_total_iter(0)
			, m_total_solve_time(0.0)
		{
			config.get("outdir",   		m_logdir);
			
			m_timer.start();
			create_log();
		}

		log_all(std::string dest)
			: m_sol_count(0)
			, m_successful_solves(0)
			, m_solves(0)
			, m_total_iter(0)
			, m_total_solve_time(0.0)
			, m_logdir(dest)
		{
			m_timer.start();
			create_log();
		}

		log_all()
			: m_sol_count(0)
			, m_successful_solves(0)
			, m_solves(0)
			, m_total_iter(0)
			, m_total_solve_time(0.0)
			, m_logdir("")
		{
			m_timer.start();
			create_log();
		}

		~log_all()
		{
			print("======= SUMMARY =======\n");
			char summary[256];
			sprintf(summary, "Total eq. solves:                        %5d  [sol]\n", m_solves);
			print(summary);
			sprintf(summary, "Total successful eq. solves:             %5d  [sol]\n", m_successful_solves);
			print(summary);
			sprintf(summary, "Total eq. solve time:                  %.5f  [s]\n", m_total_solve_time);
			print(summary);
			sprintf(summary, "Avg. time pr solve:                    %.5f  [s]\n", m_total_solve_time / (double) m_solves);
			print(summary);
			sprintf(summary, "Avg. iterations pr solve:                %5d  [it/sol]\n", (int) (m_total_iter / (double) m_solves));
			print(summary);
			sprintf(summary, "Avg. time pr iteration:               %5f  [s/it]\n", m_total_solve_time / (double) m_total_iter);
			print(summary);

			print("End time");
			char * msg = "\n================================\n=             END              =\n================================\n\n";
			printf(msg);
			if(m_fp)
			{
				fprintf(m_fp, msg);
				fclose(m_fp);
			}
		}
		
		void print(const char* format, ...)
		{
			print_time();
			va_list arglist;
			va_start( arglist, format );
			vprintf( format, arglist );
			va_end( arglist );
			//printf("\n");
			if(m_fp)
			{
				va_start( arglist, format );
				vfprintf(m_fp, format, arglist);
				//fprintf(m_fp, "\n");
			}
		}

		void print_time()
		{
			double time  = m_timer.time(); // Seconds
			int minutes  = ((int)time) / 60;
			int seconds  = ((int)time) % 60;
			int dseconds = 100*(time - (double)seconds) - (60*minutes)*100;

			printf("[%02d:%02d:%02d] [%d] - ", minutes, seconds, dseconds, gpulab::GPULAB_rank());
			if(m_fp) fprintf(m_fp, "[%02d:%02d:%02d] [%d] - ", minutes, seconds, dseconds, gpulab::GPULAB_rank());
		}

		void warning(const char* msg)
		{
			std::cerr << msg << std::endl;
		}
		void error(const char* msg)
		{
			std::cerr << msg << std::endl;
		}

		void print_solution(grid_type const& g, char const* name)
		{
			char fullname[100];
			std::string logname(m_logdir);
			logname.append(logname.length() > 0 ? "/" : "");
			sprintf(fullname,  "%s%s", logname.c_str(), name);
			io::print(g,gpulab::io::TO_MPI_BINARY,1,fullname);
		}

		template <typename M>
		void eqsolver_finished(M& m)
		{
			//if(m_solves==0)
				//io::print(m.errors(),io::TO_BINARY_FILE,1,"err_first_solve.bin");

			++m_solves;
			m_total_iter += m.iteration_count();
			m_total_solve_time += m.timer().time();

			char msg[120];
			if(!m.converged())
			{
				sprintf(msg,"NOT-solved: ");
			}
			else
			{
				++m_successful_solves;
				sprintf(msg,"Solved:     ");
			}
			// Add conv. info
			sprintf(msg,"It.: %3d. Time: %4fs. |r|_rel: %4e. Conv.: %4f\n",m.iteration_count(),m.timer().time(),m.rel_error(),m.convergence());
			print(msg);

			//io::print(m.errors(),gpulab::io::TO_BINARY_FILE,1,"../matlab/err");
		}

		double time_pr_iter() const { return m_total_solve_time / (double) m_total_iter; }

	private:

		void create_log()
		{
			/* Put this back in to create a log dir if none is provided. May cause trouble with MPI
			if(m_logdir.length() == 0)
			{
				time_t ltime;
				struct tm *Tm;
				ltime=time(NULL);
				Tm=localtime(&ltime);
				char logdir[20];
				sprintf(logdir, "%02d-%02d-%04d_%02d.%02d",
					Tm->tm_mday,
					Tm->tm_mon+1,
					Tm->tm_year+1900,
					Tm->tm_hour,
					Tm->tm_min);
				m_logdir.append(logdir);
			}
			*/

			struct stat status;
			stat( m_logdir.c_str(), &status );
			if (status.st_mode & S_IFDIR)
			{
#if defined(_WIN32) || defined(_WIN64)
				if(_mkdir(m_logdir.c_str()))
				{
					printf("Could not create log directory\n");
				}
#endif
			}

			// Create log file
			std::string logname(m_logdir);
			logname.append(logname.length() > 0 ? "/log.txt" : "log.txt");
			if (m_fp = fopen(logname.c_str(), "w"))
			{
				char* msg = "================================\n=             START            =\n================================\n\n";
				
				printf(msg);
#if defined(_WIN32) || defined(_WIN64)
				printf("Runing on:           %s\n", typeid(typename grid_type::memory_space)==typeid(gpulab::device_memory) ? "DEVICE" : "HOST");
				printf("Precision:           %s\n", typeid(typename grid_type::value_type)  ==typeid(float) ?                 "single" : "double");
#endif
				fprintf(m_fp,msg);
#if defined(_WIN32) || defined(_WIN64)
				fprintf(m_fp,"Runing on:           %s\n", typeid(typename grid_type::memory_space)==typeid(gpulab::device_memory) ? "DEVICE" : "HOST");
				fprintf(m_fp,"Precision:           %s\n", typeid(typename grid_type::value_type)  ==typeid(float) ?                 "single" : "double");
#endif
			}
			else
			{
				printf("Could not create log file\n");
			}
		}

	};

	/**
	* Logs nothing. Use this for maximum performance, since no time will be used to write to screen/file
	*/ 
	template <typename Types>
	struct log_silent
	{
		typedef typename Types::grid_type							grid_type;
		typedef typename Types::value_type							value_type;
		typedef typename Types::size_type							size_type;

		void warning(char* msg)
		{}
		void error(char* msg)
		{}
		void print_solution(value_type t, grid_type const& g, char* name)
		{}
		template <typename M>
		void eqsolver_finished(M& m)
		{}
	};

	/**
	* Logs only solver stats to the screen
	*/ 
	template <typename Types>
	struct log_stats
	{
		typedef typename Types::grid_type							grid_type;
		typedef typename Types::value_type							value_type;
		typedef typename Types::size_type							size_type;

		int			m_successful_solves;	///< No. successed solves
		int			m_solves;				///< No. total solve attemps
		int			m_total_iter;			///< No. total iterations for total solves
		double		m_total_solve_time;		///< Total time used by equations solvers

		log_stats(std::string dest = "")
			: m_successful_solves(0)
			, m_solves(0)
			, m_total_iter(0)
			, m_total_solve_time(0.0)
		{
		}

		double time_pr_iter() { return m_total_solve_time / (double) m_total_iter; }

		~log_stats()
		{
			printf("======= SUMMARY =======\n");
			printf("Total eq. solves:                        %5d  [sol]\n", m_solves);
			printf("Total successful eq. solves:             %5d  [sol]\n", m_successful_solves);
			printf("Total eq. solve time:               %4.5f  [s]\n", m_total_solve_time);
			printf("Avg. time pr solve:                   %2.5f  [s]\n", m_total_solve_time / (double) m_solves);
			printf("Avg. iterations pr solve:                %5d  [it/sol]\n", (int) (m_total_iter / (double) m_solves));
			printf("Avg. time pr iteration:              %2.5f  [s/it]\n", time_pr_iter());
			printf("===== SUMMARY END =====\n\n");
		}
		void print(const char* msg)
		{}
		void warning(char* msg)
		{}
		void error(char* msg)
		{}
		void print_solution(value_type t, grid_type const& g, char* name)
		{}
		template <typename M>
		void eqsolver_finished(M& m)
		{
			++m_solves;
			m_total_iter += m.iteration_count();
			m_total_solve_time += m.timer().time();

			if(m.converged())
			{
				++m_successful_solves;
			}
		}
	};
}; // namespace io
}; // namespace gpulab

#endif // GPULAB_IO_LOG_H
