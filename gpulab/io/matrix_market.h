// ==========================================================================
// Matrix market handling
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_IO_MATRIX_MARKET_H
#define GPULAB_IO_MATRIX_MARKET_H

#include <iostream>
#include <fstream>
#include <string>
#include <exception>
#include <gpulab/vector.h>
#include <gpulab/coo_matrix.h>
#include <gpulab/read.h>


namespace gpulab
{
	
namespace io
{
	void read_dimensions(std::ifstream& file, unsigned int& rows, unsigned int& columns, unsigned int& nonzeros)
	{
		std::string line;
		while (! file.eof() )
		{
			std::getline (file,line);
			
			// Skip comments
			if(line == "" || *line.begin() == '%')
			{
				continue;
			}
			else
			{
				sscanf(line.c_str(), "%d %d %d\n", &rows, &columns, &nonzeros);
				break;
			}
		}			
	};

	/**
	* Matrix market format reader for coo matrices
	*/
	template <typename T, typename MemorySpace>
	void read_matrix_market(gpulab::coo_matrix_base<T, MemorySpace>& m, const std::string& filename)
	{
		typedef typename gpulab::coo_matrix_base<T, MemorySpace>::size_type size_type;

		std::ifstream file (filename.c_str());
		std::string line;
		if (file.is_open())
		{
			unsigned int rows = 0;
			unsigned int columns = 0;
			unsigned int entries = 0;
			gpulab::io::read_dimensions(file, rows, columns, entries);

			if(rows > 0 && columns > 0 && entries > 0)
			{
				// Read to host memory
				std::vector<size_type>	i_indices(entries);
				std::vector<size_type>	j_indices(entries);
				std::vector<T>			data(entries);
				//gpulab::array1d<size_type, host_memory> i_indices(entries);
				//gpulab::array1d<size_type, host_memory> j_indices(entries);
				//gpulab::array1d<T, host_memory>			data(entries);

				int entry = 0;
				while (! file.eof() )
				{
					std::getline (file,line);
					
					// Skip comments
					if(line == "" || *line.begin() == '%')
					{
						continue;
					}
					
					unsigned int i;
					unsigned int j;
					T value;
					sscanf(line.c_str(), "%d %d %f\n", &i, &j, &value);
					
					i_indices[entry] = i;
					i_indices[entry] = j;
					data[entry] = value;

					++entry;
					
				}
				m.set(i_indices, j_indices, data);
			}

			file.close();
		}
		else
		{
			throw gpulab::io::file_not_found_exection(filename.c_str());
		}
	};

	/**
	*
	*/
	template <template <typename,typename> class MatrixType, typename T, typename MemorySpace>
	void read_matrix_market(MatrixType<T, MemorySpace>& m, const std::string& filename)
	{
		typedef typename MatrixType<T, MemorySpace>::size_type size_type;

		std::ifstream file (filename.c_str());
		std::string line;
		if (file.is_open())
		{
			unsigned int rows = 0;
			unsigned int columns = 0;
			unsigned int entries = 0;
			gpulab::io::read_dimensions(file, rows, columns, entries);

			if(rows > 0 && columns > 0 && entries > 0)
			{
				// Read to host memory

				//gpulab::host::vector<T> data(entries);
				
				gpulab::vector<size_type, host_memory>	ptr(rows+1);
				gpulab::vector<size_type, host_memory>	indices(entries);
				gpulab::vector<T, host_memory>			data(entries);

				int entry = 0;
				while (! file.eof() )
				{
					std::getline (file,line);
					
					// Skip comments
					if(line == "" || *line.begin() == '%')
					{
						continue;
					}
					
					unsigned int i;
					unsigned int j;
					T value;
					sscanf(line.c_str(), "%d %d %f\n", &i, &j, &value);
					
					ptr[i-1]++;

					/*indices[entry] = j;
					data[entry] = value;*/
					
					++entry;
					
				}
				m.set(indices, ptr, data);
			}

			file.close();
		}
		else
		{}
	};

} // namespace io

} // namespace gpulab


#endif // GPULAB_IO_MATRIX_MARKET_H