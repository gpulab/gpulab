// ==========================================================================
// Print
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_IO_PRINT_H
#define GPULAB_IO_PRINT_H

#define FILENAMESIZE 100

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <gpulab/mpi.h>
#include <gpulab/vector.h>
#include <gpulab/matrix.h>
#include <gpulab/coo_matrix.h>
#include <gpulab/config.h>

namespace gpulab
{ 
namespace io
{
	/**
	* Print options
	*/
	enum PRINT
	{
		TO_SCREEN,
		TO_TEXT_FILE,
		TO_BINARY_FILE,
		TO_MPI_BINARY
	};
	
	
	/**
	* Print info for a matrix.
	*/
	template <typename T>
	void print_info(gpulab::matrix<T,gpulab::host_memory> const& m, PRINT p, char const* name )
	{
		switch(p)
		{
			case TO_SCREEN:
			{
				printf("Matrix\n%s: %dx%d = %d\n\n", name, m.cols(),m.rows(), m.size());					
				break;
			}
			case TO_BINARY_FILE:
			case TO_MPI_BINARY:
			{
				FILE *fp;
				char filename[FILENAMESIZE];
				sprintf(filename, "%s.info", name);
				if (fp = fopen(filename, "w"))
				{
					fprintf(fp, "// Auto generated info file for %s\n", name);
					fprintf(fp, "precision %s\n", type<T>::name());
					fprintf(fp, "Nx %d\n", m.cols());
					fprintf(fp, "Ny %d\n", m.rows());
					fprintf(fp, "N %d\n", m.size());
					fclose(fp);
				}
				else
				{
					printf("Error opening info file: %s\n", filename);
				}
				break;
			}
		}
	}

	
	/**
	* Print info for a grid.
	*/
	template <typename grid_type>
	void print_info(grid_type const& g, PRINT p, char const* name )
	{
		typedef typename grid_type::value_type value_type;
		// Only one makes the info file
		//if(!gpulab::is_root())
		//	return; 

		switch(p)
		{
			case TO_SCREEN:
			{
				printf("Grid\n%s: %dx%dx%d = %d\n\n", name, g.dimension().x, g.dimension().y, g.dimension().z, g.size());
				break;
			}
			case TO_BINARY_FILE:
			case TO_MPI_BINARY:
			{
				FILE *fp;
				char filename[FILENAMESIZE];
				sprintf(filename, "%s.info", name);
				if (fp = fopen(filename, "w"))
				{
					fprintf(fp, "// Auto generated info file for %s\n", name);
					fprintf(fp, "precision %s\n", type<value_type>::name());
					fprintf(fp, "Nx %d\n", g.Nx());
					fprintf(fp, "Ny %d\n", g.Ny());
					fprintf(fp, "Nz %d\n", g.Nz());
					fprintf(fp, "N  %d\n", g.size());
					fprintf(fp, "dx %f\n", g.grid_space().x);
					fprintf(fp, "dy %f\n", g.grid_space().y);
					fprintf(fp, "dz %f\n", g.grid_space().z);
					fprintf(fp, "gx0 %d\n", g.ghost0().x);
					fprintf(fp, "gx1 %d\n", g.ghost1().x);
					fprintf(fp, "gy0 %d\n", g.ghost0().y);
					fprintf(fp, "gy1 %d\n", g.ghost1().y);
					fprintf(fp, "gz0 %d\n", g.ghost0().z);
					fprintf(fp, "gz1 %d\n", g.ghost1().z);

					if(g.distributed())
					{
						fprintf(fp, "P %d\n", g.topology().P);
						fprintf(fp, "Q %d\n", g.topology().Q);
						fprintf(fp, "R %d\n", g.topology().R);
					}
					fclose(fp);
				}
				else
				{
					printf("Error opening info file: %s\n", filename);
				}
				break;
			}
		}
	}
	
	/**
	* Print array v to PRINT argument.
	*/
	template <typename T>
	void print(const T* v, unsigned int N, PRINT p = TO_SCREEN, unsigned int stride = 1, char const* name = "data", int overlap = 0)
	{
		switch(p)
		{
			case TO_SCREEN:
			{
				printf("%s = [\n", name);
				for(unsigned int i=0; i<N; i=i+stride)
				{
					printf("%.15f\n", v[i]);
				}
				printf("]\n");
				break;
			}
			case TO_MPI_BINARY:
			{
				MPI_Status status;
				int myrank, numprocs;
				MPI_Comm_rank(gpulab::GPULAB_COMM_WORLD,&myrank);
				MPI_Comm_size(gpulab::GPULAB_COMM_WORLD,&numprocs);
				MPI_File file;
				MPI_File_open(gpulab::GPULAB_COMM_WORLD, (char*)name, MPI_MODE_CREATE | MPI_MODE_WRONLY , MPI_INFO_NULL, &file);
				int size = N;
				
				int bufsize = (size + numprocs - 1) / numprocs;
				int begin = myrank*bufsize-myrank*overlap;
				int end = (myrank == numprocs-1) ? size-1 : begin + bufsize - 1;
				bufsize = end-begin+1;
				MPI_File_set_view(file
								, begin * sizeof(T)
								, gpulab::values<T>::MPI_TYPE()
								, gpulab::values<T>::MPI_TYPE()
								, "native"
								, MPI_INFO_NULL);
				MPI_File_write(file, (T*)v, bufsize, gpulab::values<T>::MPI_TYPE(), &status);
				int count;
				MPI_Get_count(&status,gpulab::values<T>::MPI_TYPE(),&count);
				
				//printf("Rank: %i. bufsize %i wrtie to %i to %i, count: %i, file: %s\n", myrank, bufsize, begin, end, count, name);
				
				MPI_File_close(&file);

				break;
			}
			case TO_TEXT_FILE:
			{
				FILE *fp;
				char filename[FILENAMESIZE];
				sprintf(filename, "%s_m.m", name);
				if (fp = fopen(filename, "w"))
				{
					fprintf(fp, "%s = [", name);
					for(unsigned int i=0; i<N; i=i+stride)
					{
						fprintf(fp, "%.15f\n", v[i]);
					}
					fprintf(fp, "]\n");
					fclose(fp);
				}
				else
				{
					printf("Error opening file: %s\n", name);
				}
				break;
			}
			case TO_BINARY_FILE:
			{
				FILE *fp;
				if (fp = fopen(name, "wb"))
				{
					fwrite(v, sizeof(T), N, fp);
					fclose(fp);
				}
				else
				{
					printf("Error opening file: %s\n", name);
				}

				break;
			}
			default:
			{
				// Never happens
				break;
			}
		}
	}

	/**
	* Print matrix m to PRINT argument.
	*/
	template <typename T>
	void print(gpulab::matrix<T,gpulab::host_memory> const& m, PRINT p = TO_SCREEN, unsigned int stride = 1, char const* name = "data")
	{
		typedef typename gpulab::matrix<T,gpulab::host_memory>::size_type size_type;

		switch(p)
		{
		case TO_SCREEN:
			print_info(m,p,name);
			printf("%s = [\n", name);
			for(size_type i=0; i<m.rows(); i=i+stride)
			{
				for(size_type j=0; j<m.cols(); j=j+stride)
				{
					printf(" %.15f", m(i,j));
				}
				printf("\n");
			}
			printf("]\n");
			break;
		case TO_TEXT_FILE:
			{
				FILE *fp;
				if (fp = fopen(name, "w"))
				{
					fprintf(fp, "%s = [", name);
					for(size_type i=0; i<m.rows(); i=i+stride)
					{
						for(size_type j=0; j<m.cols(); j=j+stride)
						{
							fprintf(fp, " %.15f", m(i,j));
						}
						fprintf(fp, "\n");
					}
					fprintf(fp, "]\n");
					fclose(fp);

					print_info(m,p,name);
				}
				else
				{
					printf("Error opening file: %s\n", name);
				}
				break;
			}
		case TO_BINARY_FILE:
			{
				FILE *fp;
				if (fp = fopen(name, "wb"))
				{
					fwrite(m.data(), sizeof(T), m.size(), fp);
					fclose(fp);

					print_info(m,p,name);
				}
				else
				{
					printf("Error opening file: %s\n", name);
				}

				break;
			}
		default:
			// Never happens
			break;
		}
	}

	/**
	* Print a generic grid, device grids are converted to host versions.
	*/
	template <typename grid_type>
	void print(grid_type const& g, PRINT p = TO_SCREEN, unsigned int stride = 1, char const* name = "data")
	{
		typedef typename grid_type::value_type value_type;

		char filename[FILENAME_MAX];
		if(g.distributed())
		{
			int rank;
			MPI_Comm_rank(g.topology().COMM,&rank);
			sprintf(filename,"%s.%02i%02i%02i"
							,name
							,g.topology().p
							,g.topology().q
							,g.topology().r);
		}
		else
			sprintf(filename,"%s",name);

		// Print info file
		if(p==TO_BINARY_FILE)
			print_info(g,p,filename);

		gpulab::vector<value_type,gpulab::host_memory> printme(g);
		print(printme, p, stride, filename);
	};

	/**
	* Print a host vector, device vectors are automatically converted to host versions.
	* Only new cards support print from device.
	*/
	template <typename T>
	void print(gpulab::vector<T,gpulab::host_memory> const& v, PRINT p = TO_SCREEN, unsigned int stride = 1, char const* name = "data")
	{
		//print_info(v,p,name);
		print(v.data(), v.size(), p, stride, name);
	};

	/**
	* Print a device vector, device vectors are automatically converted to host versions.
	* Only new cards support print from device.
	*/
	template <typename T>
	void print(gpulab::vector<T,gpulab::device_memory> const& v, PRINT p = TO_SCREEN, unsigned int stride = 1, char const* name = "data")
	{
		gpulab::vector<T,gpulab::host_memory> printme(v);
		print(printme,p,stride,name);
	};

	

	template <typename T, typename M>
	void print(gpulab::matrix<T,M> const& m, PRINT p = TO_SCREEN, unsigned int stride = 1, char const* name = "data")
	{
		// Convert to host vector for printing.. might be expensive, 
		// but on old cards there is no option. How do we do better for new cards?
		gpulab::matrix<T,gpulab::host_memory> printme(m);
		print(printme, p, stride, name);
	};
	
	template <typename T>
	void print(gpulab::coo_matrix<T,gpulab::host_memory> const& m, PRINT p = TO_SCREEN, char const* name = "data")
	{
		switch(p)
		{
		case TO_SCREEN:
			printf("%s = [\n", name);
			for (unsigned int i = 0; i < m.nonzeros(); i++)
				printf("%-6lu\t%-6lu\t%15.6f\n", m.i()[i] + 1, m.j()[i] + 1, m.data()[i]);
			printf("];\n");
			break;
		case TO_TEXT_FILE:
			FILE *fp_m;
			FILE *fp_dat;
			char filename_m[50];
			char filename_dat[50];
			sprintf(filename_m, "%s_m.m", name);
			sprintf(filename_dat, "%s_m.dat", name);
			if (fp_m = fopen(filename_m, "w"))
			{
				if (fp_dat = fopen(filename_dat, "w"))
				{
					// Write .m file
					sprintf(filename_m, "%s_m", name);
					fprintf(fp_m, "load %s;\n", filename_dat);
					fprintf(fp_m, "%s = spconvert(%s);\n", name, filename_m);
					fprintf(fp_m, "clear %s;\n", filename_m);
					fclose(fp_m);

					// Write .dat file
					fprintf(fp_dat, "%-10lu\t%-10lu\t%20.15f\n", m.rows(), m.cols(), 0.0);
					for (unsigned int i = 0; i < m.nonzeros(); i++)
						fprintf(fp_dat, "%-10lu\t%-10lu\t%20.15f\n", m.i()[i] + 1, m.j()[i] + 1, m.data()[i]);
					fclose(fp_dat);
				}
				else
				{
					printf("Error opening dat file\n");
				}
			}
			else
			{
				printf("Error opening m file\n");
			}
			break;
		case TO_BINARY_FILE:
			ASSERT(!"Not implemented");
			break;
		default:
			// Never happens
			break;
		}
	};

	template <typename T, typename M>
	void print(gpulab::coo_matrix<T,M> const& m, PRINT p = TO_SCREEN, char const* name = "data")
	{
		// Convert to host vector for printing.. might be expensive,
		// but on old cards there is no option. How do we do better for new cards?
		gpulab::coo_matrix<T,gpulab::host_memory> printme(m);
		print(printme, p, name);
	};

	/**
	* Print array v to PRINT argument.
	*/
	template <typename T, typename size_type>
	void print3Dxy_gnuplot(const T *v, size_type Nx, size_type Ny, size_type k, double dx, double dy, char const* name = "data")
	{
		FILE *fp;
		char filename[FILENAMESIZE];
		sprintf(filename,"%s", name);
		if (fp = fopen(filename, "w"))
		{
			for(size_type i=0; i<Nx; i++)
			{
				for(size_type j=0; j<Ny; j++)
				{
					fprintf(fp, "%20.15f %20.15f %20.15f\n",dx*i,dy*j, v[i+j*Nx+k*Nx*Ny]);
				}
				fprintf(fp, "\n");
			}
			fclose(fp);
		}
		else
		{
			printf("Error opening file: %s\n", name);
		}
	}

	/**
	* Print a generic grid using gnu plot
	*/
	template <typename grid_type>
	void print3Dxy_gnuplot(grid_type const& g, unsigned int k, char const* name = "data")
	{
		typedef typename grid_type::value_type value_type;
		
		// Convert to host vector for printing.. might be expensive, 
		// but on old cards there is no option. How do we do better for new cards?
		gpulab::vector<value_type,gpulab::host_memory> printme(g);
		//		print(printme, p, stride, name);
		print3Dxy_gnuplot(printme.data(), g.dimension().x, g.dimension().x,k,g.grid_space().x,g.grid_space().y, name);
	};



}; // namespace io
}; // namespace gpulab

#endif // GPULAB_IO_PRINT_H
