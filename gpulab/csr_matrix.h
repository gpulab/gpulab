// ==========================================================================
// Compressed Sparse Row Matrix (CSR)
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Nicolai Fog Gade-Nielsen
// Email:   nfga@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_CSR_MATRIX_H
#define GPULAB_CSR_MATRIX_H

#include <gpulab/config.h>
#include <gpulab/vector.h>
#include <gpulab/sparse_matrix.h>
#include <gpulab/dispatch/csr_matrix.h>

namespace gpulab
{
	
	/**
	* CSR matrix class, shared between both device and host matrices.
	*
	* Example of a simple csr matrix representation
	*
	*           [1 7 0 0]
	*      A  = [0 2 8 0]
	*           [5 0 3 9]
	*           [0 6 0 4]
	*
	* ptr     = [0 2 4 7 9]
	* indices = [0 1 1 2 0 2 3 1 3]
	* data    = [1 7 2 8 5 3 9 6 4]
	*
	*
	*
	*           [1 0 5 0]
	*      A' = [7 2 0 6]
	*           [0 8 3 0]
	*           [0 0 9 4]
	*
	* ptr     = [0 2 5 7 9]
	* indices = [0 2 0 1 3 1 2 2 3]
	* data    = [1 5 7 2 6 8 3 9 4]
	* 
	*
	* @param T value type
	* @param MemorySpace memory space indication
	*/
	template <typename T, typename MemorySpace> 
	class csr_matrix : public gpulab::sparse_matrix<T, MemorySpace>
	{
	public:
		typedef gpulab::csr_matrix<T,MemorySpace>						my_type;
		typedef typename gpulab::sparse_matrix<T, MemorySpace>			super;
		typedef typename super::size_type								size_type;
		typedef typename super::value_type								value_type;
		typedef vector<value_type, MemorySpace>							vector_data_type;
		typedef vector<size_type, MemorySpace>							vector_index_type;
		
	private:
		/**
		* Hide default constructor
		*/
		csr_matrix() {};

	protected:
		vector_data_type	m_data;		///< Vector holding the nonzero data. Size = nonzeros
		vector_index_type	m_indices;	///< column indices. Size = nonzeros
		vector_index_type	m_ptr;		///< row pointers to column indices in m_indices. Size = I+1

	public:
		/**
		* Construct a matrix of size I*I with default value
		*/
		csr_matrix(size_type I, size_type J, vector_index_type const& ptr, vector_index_type const& indices, vector_data_type const& data)
			: super(I, J, data.size())
			, m_data(data.size())
			, m_indices(indices.size())
			, m_ptr(ptr.size())
		{
			this->m_indices.copy(indices);
			this->m_ptr.copy(ptr);
			this->m_data.copy(data);
		}

		/**
		* Copy constructor
		*/
		csr_matrix(my_type const& m)
			: super(m)
			, m_data(m.m_data)
			, m_indices(m.m_nonzeros)
			, m_ptr(m.m_I+1)
		{ }

		///**
		//* Copy constructor from other location
		//*/
		template<typename OtherT, typename OtherMemorySpace>
		csr_matrix(csr_matrix<OtherT, OtherMemorySpace> const &m)
			: super(m)
			, m_data(m.m_data)
			, m_indices(m.m_indices)
			, m_ptr(m.m_ptr)
		{ }

		/**
		* Deconstructor
		*/
		~csr_matrix()
		{ };

		/**
		* Return the data containing <nonzero> values
		*/
		vector_data_type const& data() const
		{
			return m_data;
		}

		/**
		* Return the column index coordinates for the <nonzero> values in m_data
		*/
		vector_index_type const& indices() const
		{
			return m_indices;
		}

		/**
		* Return the ptr row coordinates 
		*/
		vector_index_type const& ptr() const
		{
			return m_ptr;
		}

		template <typename ArrayIndex, typename ArrayValue>
		void set(ArrayIndex const& ptr, ArrayIndex const& indices, ArrayValue const& data)
		{
//			assert(ptr.size() == this->m_ptr.size());
//			assert(indices.size() == this->m_indices.size());
//			assert(data.size() == this->m_data.size());

			this->m_nonzeros = data.size();
			this->m_indices.copy(indices);
			this->m_ptr.copy(ptr);
			this->m_data.copy(data);
		}

		void mult(vector_data_type const& x, vector_data_type& b) const
		{
			gpulab::dispatch::csr_matrix::mult(*this, x, b, MemorySpace());
		}

		void transpose() const
		{
			//gpulab::dispatch::csr_matrix::transpose(*this, MemorySpace());

			// X
			vector_index_type toffsets(this->cols(), 0);
			vector_index_type tptr(this->cols() + 1, 0);
			vector_index_type tindices(this->nonzeros());
			vector_data_type tdata(this->nonzeros());
			size_type tI = this->cols();
			size_type tJ = this->rows();

			// Generate transposed ptr
			for (size_type i = 0; i < this->nonzeros(); i++) // Parallel with atomic add
				tptr[m_indices[i] + 1]++;
			for (size_type i = 1; i < this->cols() + 1; i++) // Sequential
				tptr[i] += tptr[i-1];

			// Generate transposed indices and data
			for(size_type i = 0; i < this->rows(); ++i)
			{
				size_type row_start = m_ptr[i];
				size_type row_end   = m_ptr[i+1];

				for(size_type j = row_start; j < row_end; ++j)
				{
					size_type index = toffsets[i] + j - row_start;
					tdata[index] = m_data[j];
					tindices[index] = i;
					toffsets[i] += 1;
				}
			}

/*
			// Set
			m_ptr.copy(tptr);
			m_indices.copy(tindices);
			m_data.copy(tdata);
			this->m_I = tI;
			this->m_J = tJ;
*/
		}

		/**
		* Private helpers
		*/
	private:

	};
} // namespace gpulab


#endif // GPULAB_CSR_MATRIX_H
