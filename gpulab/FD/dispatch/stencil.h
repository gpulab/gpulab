// ==========================================================================
// Stencil dispatching
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_FD_DISPATCH_STENCIL_H
#define GPULAB_FD_DISPATCH_STENCIL_H

#include <gpulab/type_traits.h>
#include <gpulab/FD/host/stencil.h>
#include <gpulab/FD/device/stencil.h>

namespace gpulab
{
namespace FD
{
namespace dispatch
{

////////////////
// Host Paths //
////////////////
template <typename V, typename S>
void mult(  S const& s
		  , V const& in
		  , V& out
		  , gpulab::host_memory)
{
	ASSERT(!"Not implemented");
	// TODO: make if needed, dispatching is not used for now
    //gpulab::host::mult(s, in, out);
};


//////////////////
// Device Paths //
//////////////////
template <typename V, typename S>
void mult(  S const& s
		  , V const& in
		  , V& out
		  , gpulab::device_memory)
{
	ASSERT(!"Not implemented");
	// TODO: make if needed, dispatching is not used for now
    // gpulab::device::mult(in, out);
};


} // end namespace dispatch
} // end namespace FD
} // end namespace gpulab

#endif // GPULAB_FD_DISPATCH_STENCIL_H