// ==========================================================================
// Stencil
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_FD_STENCIL_H
#define GPULAB_FD_STENCIL_H

#include <gpulab/config.h>
#include <gpulab/device/shared_memory.h>
#include <gpulab/device/const_memory.h>
#include <gpulab/type_traits.h>
#include <gpulab/util/fdcoeffF.h>
#include <gpulab/FD/dispatch/stencil.h>

namespace gpulab
{ 
namespace FD
{

	//#define STENCIL_SIZE 10 // We expect no more than this many entries are needed...
	
	// TODO: How do we make this non-hardcoded?
	//__constant__ float  f_const_stencils[STENCIL_SIZE*STENCIL_SIZE];	// Global constant memory, max 64KB in G80, G92 architecture, may be more in G200
	//__constant__ double d_const_stencils[STENCIL_SIZE*STENCIL_SIZE];	// Global constant memory

namespace kernel
{
	/**
	* Stencil 1d mult kernel
	* Using const memory for the stencil and vector is loaded to shared memory
	*/
	template <typename T>
	__global__
	void stencil_mult_kernel_1d(T const* in, T* out, T const* coeffs, T h, const int alpha, const size_t N, bool periodic)
	{
		// compute the index of the output vector
		int n = (blockDim.x-2*alpha) * blockIdx.x + threadIdx.x - alpha;
		int stride = (blockDim.x-2*alpha) * gridDim.x;
		
		unsigned int offset = alpha;
		if(periodic)
		{
			n = n<0 ? N+n-1 : ((n>=N) ? n-(N-1) : n);
		}

		// Make sure to iterate with stride for very large N
		for(; n<N+alpha; n+=stride)
		{
			// Load into shared memory
			T* Us = gpulab::device::shared_memory<T>::get_pointer();
			if(!periodic)
			{
				offset = n < alpha ? n : n >= N-alpha ? 2*alpha+n-N+1 : alpha;
			}
			Us[threadIdx.x]  = n < 0 || n >= N ? 0.0 : in[n];
			Us[threadIdx.x] *= h;  // Multiply h = 1/dx^Q
			__syncthreads();

			// No ghost cells should enter here
			if(threadIdx.x >= alpha && threadIdx.x < blockDim.x - alpha)
			{
				// Each thread within boundaries computes one element
				if(n >= 0 && n < N)
				{
					unsigned int rank = alpha*2+1;
					T sum = gpulab::values<T>::zero();
					unsigned int row = offset * rank; // What row in the stencil matrix to use
					//T const* coeffs = gpulab::device::const_memory<T>::get_pointer();
					for(unsigned int i = 0; i<rank; ++i)
					{
						sum += coeffs[row+i] * Us[threadIdx.x-offset+i];
					}
					out[n] = sum;
				}
			}
		}
	}

	/**
	* Stencil 2d mult kernel
	* Using const memory for the stencil and vector is loaded to shared memory
	* TODO: shared memory for in
	*/
	template <typename T, typename size_type>
	__global__
	void stencil_mult_kernel_2d(  T const* in
								, T* out
								, T dx
								, T dy
								, const unsigned int alpha
								, const size_type N
								, const size_type M
								, const size_type xstart
								, const size_type xend
								, const size_type ystart
								, const size_type yend)
	{
		// Indecies
		int n = IDX1Dx;
		int m = IDX1Dy;
		
		int rank = alpha*2+1;
		
		// Load stencil coeffs into shared memory
		T* scoeffs = gpulab::device::shared_memory<T>::get_pointer();
		T const* coeffs = gpulab::device::const_memory<T>::get_pointer();
		gpulab::device::load_shared_memory(scoeffs,coeffs,rank*rank);

		// Load grid into shared memory. TODO: gives no speedup !?
		//T* sm = scoeffs+rank*rank;
		//dim3 sdim; dim3 sid;
		//gpulab::device::load_shared_mem_block_2d_v2(sm,in,N,M,(size_type)alpha,&sdim,&sid);
		//__syncthreads();

		// No cells outside boundary should enter here
		if(n >= xstart && n < xend && m >= ystart && m < yend)
		{
			T sum(0);

			unsigned int offset_y = m < alpha ? m : m >= M-alpha ? 2*alpha+m-M+1 : alpha;
			unsigned int row_y = offset_y*rank;

			unsigned int offset_x = n < alpha ? n : n >= N-alpha ? 2*alpha+n-N+1 : alpha;
			unsigned int row_x = offset_x*rank;
				
			for(unsigned int s = 0; s<rank; ++s)
			{
				sum += (dx*scoeffs[row_x+s]*in[m*N+n-offset_x+s]) + (dy*scoeffs[row_y+s]*in[(m-offset_y+s)*N+n]);
				//sum += (dx*scoeffs[row_x+s]*sm[sid.y*sdim.x+sid.x-offset_x+s]) + (dy*scoeffs[row_y+s]*sm[(sid.y-offset_y+s)*sdim.x+sid.x]);
			}
			out[m*N+n] = sum;
		}
	}


	/**
	* Stencil 2d mult kernel with a sigma transformation
	* Using const memory for the stencil and vector is loaded to shared memory
	*/
	template <typename value_type, typename size_type>
	__global__
	void diff_sigma_transform_2d( value_type const* in
								, value_type* out
								, value_type dx
								, value_type dy
								, unsigned int alpha
								, size_type Nx
								, size_type Ny
								, value_type sigma
								, size_type xstart
								, size_type xend
								, size_type ystart
								, size_type yend
								, unsigned int Q)
	{
		int x = IDX1Dx;
		int y = IDX1Dy;

		// No cells outside boundary should enter here
		if(x >= xstart && x < xend && y >= ystart && y < yend)
		{
			int rank = alpha*2+1;
			value_type sum = values<value_type>::zero();

			size_type offset_y = y < alpha ? y : y >= Ny-alpha ? 2*alpha+y-Ny+1 : alpha;
			size_type row_y = offset_y*rank;

			size_type offset_x = x < alpha ? x : x >= Nx-alpha ? 2*alpha+x-Nx+1 : alpha;
			size_type row_x = offset_x*rank;
				
			value_type const* coeffs = gpulab::device::const_memory<value_type>::get_pointer();

			for(size_type s = 0; s<rank; ++s)
			{
				//T sig = sigma[y*Nx+x];
				value_type sig = pow(sigma, (value_type) Q);
				sum += (dx*coeffs[row_x+s]*in[y*Nx+x-offset_x+s]) + (dy*coeffs[row_y+s]*in[(y-offset_y+s)*Nx+x])*(sig);
			}
			out[y*Nx+x] = sum;
		}
	}


	/**
	* Stencil differentiale along x 
	* Using const memory for the stencil and vector is loaded to shared memory
	* TODO: shared memory
	*/
	template <typename value_type, typename size_type>
	__global__
	void diff_x(  value_type const* in
				, value_type* out
				, value_type dx
				, const unsigned int alpha
				, const size_type Nx
				, const size_type Ny
				, const size_type xstart
				, const size_type xend
				, const size_type ystart
				, const size_type yend
				, const size_type zstart
				, const size_type zend)
	{
		// compute the index of the output vector
		size_type x = IDX1Dx;
		size_type y = IDX1Dy;
		size_type z = zstart;
				
		// Get stencil coefficient pointer
		value_type const* coeffs = gpulab::device::const_memory<value_type>::get_pointer();

		// No cells outside boundary should enter here
		if(x >= xstart && x < xend && y >= ystart && y < yend)
		{
			int rank           = alpha*2+1;
			size_type offset_x = x < alpha ? x : x >= Nx-alpha ? 2*alpha+x-Nx+1 : alpha;
			size_type row_x    = offset_x*rank;
			for(; z<zend; ++z)
			{
				value_type sum = gpulab::values<value_type>::zero();
				for(size_type s = 0; s<rank; ++s)
				{
					sum += dx*coeffs[row_x+s]*in[idx(x-offset_x+s,y,z,Nx,Ny)];
				}
				out[idx(x,y,z,Nx,Ny)] = sum;
			}
		}
	}

	/**
	* Stencil differentiale along y 
	* Using const memory for the stencil and vector is loaded to shared memory
	* TODO: shared memory
	*/
	template <typename value_type, typename size_type>
	__global__
	void diff_y(  value_type const* in
				, value_type* out
				, value_type dy
				, const unsigned int alpha
				, const size_type Nx
				, const size_type Ny
				, const size_type xstart
				, const size_type xend
				, const size_type ystart
				, const size_type yend
				, const size_type zstart
				, const size_type zend)
	{
		// compute the index of the output vector
		size_type i = IDX1Dx;
		size_type j = IDX1Dy;
		size_type k = zstart;
				
		// Get stencil coefficient pointer
		value_type const* coeffs = gpulab::device::const_memory<value_type>::get_pointer();

		// No cells outside boundary should enter here
		if(i >= xstart && i < xend && j >= ystart && j < yend)
		{
			int rank         = alpha*2+1;
			size_type offset = j < alpha ? j : j >= Ny-alpha ? 2*alpha+j-Ny+1 : alpha;
			size_type srow   = offset*rank;
			for(; k<zend; ++k)
			{
				value_type sum   = gpulab::values<value_type>::zero();
				for(size_type s = 0; s<rank; ++s)
				{
					sum += coeffs[srow+s]*in[idx(i,j-offset+s,k,Nx,Ny)];
				}
				sum *= dy;

				out[idx(i,j,k,Nx,Ny)] = sum;
			}
		}
	}

	///////////////////
	// Device functions to perform point differentiations
	///////////////////
	template <typename value_type>
	__device__
	value_type FD2D_x(value_type const* u, value_type invdx, int i, int j, int Nx, int Ny, int alpha, value_type const* stencil)
	{
		int rank   = alpha*2+1;
		int offset = i < alpha ? i : i >= Nx-alpha ? 2*alpha+i-Nx+1 : alpha;
		int row    = offset*rank;
		value_type sum   = gpulab::values<value_type>::zero();
		for(int s = 0; s<rank; ++s)
		{
			sum += stencil[row+s]*u[IDX2D(i-offset+s,j,Nx)];
		}
		return invdx*sum;
	}

	template <typename value_type>
	__device__
	value_type FD2D_x(value_type const* u, value_type invdx, int i, int j, int Nx, int Ny, int alpha, value_type const* stencil, int ox)
	{
		int rank   = alpha*2+1;
		int offset = i < alpha+ox ? i-ox : i >= Nx-alpha-ox ? 2*alpha+i-Nx+1+ox : alpha;
		int row    = offset*rank;
		value_type sum   = gpulab::values<value_type>::zero();
		for(int s = 0; s<rank; ++s)
		{
			sum += stencil[row+s]*u[idx(i-offset+s,j,Nx)];
		}
		return invdx*sum;
	}


	template <typename value_type>
	__device__
	value_type FD2D_y(value_type const* u, value_type invdy, int i, int j, int Nx, int Ny, int alpha, value_type const* stencil)
	{
		int rank         = alpha*2+1;
		int offset = j < alpha ? j : j >= Ny-alpha ? 2*alpha+j-Ny+1 : alpha;
		int row    = offset*rank;
		value_type sum   = gpulab::values<value_type>::zero();
		for(int s = 0; s<rank; ++s)
		{
			sum += stencil[row+s]*u[IDX2D(i,j-offset+s,Nx)];
		}
		return invdy*sum;
	}

	
	template <typename value_type>
	__device__
	value_type FD2D_y(value_type const* u, value_type invdy, int i, int j, int Nx, int Ny, int alpha, value_type const* stencil, int oy)
	{
		int rank         = alpha*2+1;
		int offset = j < alpha+oy ? j-oy : j >= Ny-alpha-oy ? 2*alpha+j-Ny+1+oy : alpha;
		int row    = offset*rank;
		value_type sum   = gpulab::values<value_type>::zero();
		for(int s = 0; s<rank; ++s)
		{
			sum += stencil[row+s]*u[idx(i,j-offset+s,Nx)];
		}
		return invdy*sum;
	}

	template <typename value_type>
	__device__
	value_type FD2D_xy(value_type const* u, value_type invdx, value_type invdy, int i, int j, int Nx, int Ny, int alpha, value_type const* stencil, int ox = 0, int oy = 0)
	{
		int rank         = alpha*2+1;
		
		int offset_i = i < alpha+ox ? i-ox : i >= Nx-alpha-ox ? 2*alpha+i-Nx+1+ox : alpha;
		int offset_j = j < alpha+oy ? j-oy : j >= Ny-alpha-oy ? 2*alpha+j-Ny+1+oy : alpha;

		int row_i    = offset_i*rank;
		int row_j    = offset_j*rank;

		value_type sum = gpulab::values<value_type>::zero();
		for(int sj = 0; sj<rank; ++sj)
		{
			value_type tmp = gpulab::values<value_type>::zero();
			for(int si = 0; si<rank; ++si)
			{
				tmp += stencil[row_i+si]*u[idx(i-offset_i+si,j-offset_j+sj,Nx)];
			}
			tmp *= invdx;
			sum += stencil[row_j+sj]*tmp;
		}
		return invdy*sum;
	}


}; // Namespace kernel

	/**
	* A stencil coefficient handler for the Q'th derivative, using N'th order finite differences.
	*/
	template <typename value_type>
	class stencil
	{
	private:
		typedef stencil<value_type> my_type;

		bool m_on_device;

	protected:
		unsigned int m_Q;				///< The order of the derivative
		unsigned int m_P;				///< The minimum order of the FD approximation accuracy (non-centered)
		unsigned int m_rank;			///< Rank, that is the total number of FD points used
		unsigned int m_alpha;			///< Points used for FD on each side of the center point
		value_type* m_coeffs_h;			///< Matrix with all shifted/non-shifted stencils (host side)
		value_type* m_coeffs_d;			///< Matrix with all shifted/non-shifted stencils (device side)
		
	public:

		stencil(my_type const& s)
			: m_P(s.P())
			, m_Q(s.Q())
			, m_alpha(s.alpha())
			, m_rank(s.rank())
			, m_on_device(false)
			, m_coeffs_h(0)
			, m_coeffs_d(0)
		{
			create_stencil_matrix();
		}

		/**
		* Constructor for the stencil coefficeints.
		*/
		stencil(unsigned int Q, unsigned int alpha)
			: m_Q(Q)
			, m_alpha(alpha)
			, m_rank(alpha*2+1)
			, m_P((alpha*2+1)-Q)
			, m_on_device(false)
			, m_coeffs_h(0)
			, m_coeffs_d(0)
		{
			create_stencil_matrix();
		}

		/**
		* Deconstructor
		*/
		~stencil()
		{
			if(m_coeffs_h)
			{
				free(m_coeffs_h);
			}				
			if(m_coeffs_d && m_on_device)
			{
				cudaFree(m_coeffs_d);
			}
		}

		/**
		* Uploads the coefficients to const device memory
		*/
		void upload_coeffs() const
		{
			cudaMemcpyToSymbol(gpulab::device::const_stencils, m_coeffs_h, m_rank*m_rank*sizeof(value_type), 0, cudaMemcpyHostToDevice);
		}

		unsigned int		rank()			const	{ return m_rank;		}
		unsigned int		alpha()			const	{ return m_alpha;		}
		unsigned int		Q()				const	{ return m_Q;			}
		unsigned int		P()				const	{ return m_P;			}
		value_type const*	coeffs_host()	const	{ return m_coeffs_h;	}
		value_type const*	coeffs_device() const 	{ return m_coeffs_d;	}

	private:

		void create_stencil_matrix()
		{
			// Fill m_coeffs with shifted and non-shifted coefficients.
			// Allocate space
			m_coeffs_h    = (value_type*)malloc(m_rank*m_rank*sizeof(value_type));
			value_type* c = (value_type*)malloc(m_rank*sizeof(value_type));
			value_type* x = (value_type*)malloc(m_rank*sizeof(value_type));;

			// Make unit-scaled grid
			for (unsigned int i=0; i<m_rank; ++i)
			{
				x[i] = (value_type)i;
			}

			// Calculate coefficients using fdcoeffF
			for (unsigned int i=0; i<m_rank; ++i)
			{
				value_type xbar = x[i];
				gpulab::util::fdcoeffF(m_Q, xbar, x, c, m_rank);
				value_type* coeff_row = &m_coeffs_h[i*m_rank];
				for (unsigned int j=0; j<m_rank; ++j)
				{
					coeff_row[j] = c[j]; // array storage according to row major order
				}
			}

			// Print stencil
			//gpulab::io::print(m_coeffs,m_rank,gpulab::io::TO_SCREEN,1,"coeffs",m_rank);

			// Clean up
			free(c);
			free(x);

			// Put coeffs on device also
			upload_coeffs_global();
		}

		/**
		* Uploads the coefficients to global device memory
		*/
		void upload_coeffs_global()
		{
			if(!m_on_device)
			{
				cudaMalloc((void **)&m_coeffs_d, m_rank*m_rank*sizeof(value_type));
				cudaMemcpy(m_coeffs_d, m_coeffs_h, m_rank*m_rank*sizeof(value_type), cudaMemcpyHostToDevice);
			}
			m_on_device = true;
		}

	};

	/**
	* Specialized stencil for 1D operations
	*/
	template <typename T>
	class stencil_1d : public stencil<T>
	{
	protected:
		typedef stencil<T>		super;
		typedef stencil_1d<T>	my_type;

	public:

		bool periodic;

		stencil_1d(unsigned int Q, unsigned int alpha, bool periodic_ = false)
			: super(Q,alpha)
			, periodic(periodic_)
		{}

		stencil_1d(my_type const& s)
			: super(s)
			, periodic(s.periodic)
		{}

		/**
		* Generalized template version
		* Multiply the stencil to a vector
		*/
		template<class V>
		void mult(V const& in, V& out) const
		{
			ASSERT(in.size() == out.size());
			this->diff(in,out,typename V::memory_space());
			//gpulab::FD::dispatch::mult(*this, in,out,typename V::memory_space());
		}


	private:
		
		/**
		* Spezialized device version
		* Multiply the stencil to a vector
		*/
		template <typename V>
		void diff(V const& in, V& out, gpulab::device_memory) const
		{
			typedef typename V::size_type size_type;
			size_type M = in.size();
			ASSERT(M == out.size());

			T dx = gpulab::values<T>::one() / pow((T)in.grid_space().x,(T)super::m_Q);
			
			// Depreciated: Make sure coeffs is in const memory
			//super::upload_coeffs();

			//CUDA_SAFE_CALL( cudaThreadSynchronize() );
			//CUDA_SAFE_CALL( cudaGetLastError() );

			// Setup kernel configuration
			dim3 block(BLOCK1D(M + this->m_alpha*2));
			dim3 grid(GRID_SAFE((M + block.x - 2*this->m_alpha-1) / (block.x-2*super::m_alpha)));
			kernel::stencil_mult_kernel_1d<<<grid,block,block.x*sizeof(T)>>>(RAW_PTR(in), RAW_PTR(out), RAW_PTR(super::coeffs_device()), dx, super::m_alpha, M, periodic);
			
			//CUDA_SAFE_CALL( cudaThreadSynchronize() );
			//CUDA_SAFE_CALL( cudaGetLastError() );
		}

		/**
		* Spezialized host version
		* Multiply the stencil to a vector
		*/
		template <typename V>
		void diff(V const& in, V& out, gpulab::host_memory) const
		{
			typename V::size_type N = in.size();
			ASSERT(N == out.size());
			T h = T(1)/pow(in.grid_space().x,(T) super::m_Q);

			for(unsigned int n = 0; n<N; ++n)
			{
				T sum(0);
				unsigned int offset = n < super::m_alpha ? n : n >= N-super::m_alpha ? 2*super::m_alpha+n-N+1 : super::m_alpha;
				unsigned int row = offset*super::m_rank;
				for(unsigned int i = 0; i<=2*super::m_alpha; ++i)
				{
					sum += h * super::m_coeffs[row+i] * in[n-offset+i];
				}
				out[n] = sum;
			}
		}


	};


	/**
	* Stencil for 2D operations
	*/
	template <typename T>
	class stencil_2d : public stencil<T>
	{
	protected:
		typedef stencil<T>		super;
		typedef stencil_2d<T>	my_type;

	public:

		stencil_2d(unsigned int Q, unsigned int alpha)
			: super(Q,alpha)
		{ }

		stencil_2d(my_type const& s)
			: super(s)
		{ }

		/**
		* Generalized template version
		* Multiply the stencil to a 2d vector
		* Calculates the sum of derrivatives: d/dx + d/dy
		* Use the diff_x or diff_y to only calculate derivatives in one direction
		*/
		template<class V>
		void mult(V const& in, V& out) const
		{
			ASSERT(in.size() == out.size());
			// Dispatch
			this->diff(in,out,typename V::memory_space());
			//gpulab::FD::dispatch::diff(*this, in,out,typename V::memory_space());
		}

		/**
		* Generalized template version
		* Differentiates a 2D vector along the x-direction
		*/
		template<class V>
		void diff_x(V const& in, V& out) const
		{
			ASSERT(in.size() == out.size());
			// Dispatch
			this->diff_x(in,out,typename V::memory_space());
		}

		/**
		* Generalized template version
		* Differentiates a 2D vector along the y-direction
		*/
		template<class V>
		void diff_y(V const& in, V& out) const
		{
			ASSERT(in.size() == out.size());
			// Dispatch
			this->diff_y(in,out,typename V::memory_space());
		}

		/**
		* Multiply the stencil with a 2d grid vector.
		* Uses a sigma transformation in the vertical (second) direction.
		* @param host_memory dispatching
		* @param in grid
		* @param out grid holding differentiation
		* @param sigma transformation info
		*/
		template <typename V, typename S>
		void diff_sigma_transform(V const& in, V& out, S const& sigma) const
		{
			// Dispatch
			this->diff_sigma_transform(in,out,sigma,typename V::memory_space());
		}

	private:

		
		/**
		* Spezialized device version
		* Multiply the stencil to a 2d vector
		*/
		template <typename V>
		void diff(V const& in, V& out,device_memory) const
		{
			typedef typename V::value_type	value_type;
			typedef typename V::size_type	size_type;

			size_type N = in.size();
			ASSERT(N == out.size());

			size_type Nx     = in.dimension().x;
			size_type Ny     = in.dimension().y;
			size_type xstart =    in.ghost0().x;
			size_type xend   = Nx-in.ghost1().x;
			size_type ystart =    in.ghost0().y;
			size_type yend   = Ny-in.ghost1().y;
			size_type rank   = 2*super::m_alpha+1;
			value_type dx    = value_type(1)/pow(in.grid_space().x,(value_type) super::m_Q);
			value_type dy    = value_type(1)/pow(in.grid_space().y,(value_type) super::m_Q);

			// Make sure coeffs is in const memory
			super::upload_coeffs();
			
			// Setup kernel configuration
			dim3 block = BLOCK2D(Nx,Ny);
			dim3 grid  = GRID2D(Nx,Ny);
			unsigned int smem = //(block.x+rank-1)*(block.y+rank-1)*sizeof(T) + // for grid points
								rank*rank*sizeof(T);						  // for stencil
			kernel::stencil_mult_kernel_2d<<<grid,block,smem>>>(RAW_PTR(in),RAW_PTR(out),dx,dy,super::m_alpha,Nx,Ny,xstart,xend,ystart,yend);
			
			//CUDA_SAFE_CALL( cudaThreadSynchronize() );
			//CUDA_SAFE_CALL( cudaGetLastError() );
		}

		/**
		* Spezialized host version
		* Multiply the stencil to a 2d grid vector
		*/
		template <typename V>
		void diff(V const& in, V& out,host_memory) const
		{
			typedef typename V::value_type	value_type;
			typedef typename V::size_type	size_type;
			
			size_type N = in.size();
			ASSERT(N == out.size());

			size_type Nx     = in.Nx();
			size_type Ny     = in.Ny();
			size_type xstart =    in.ghost0().x;
			size_type xend   = Nx-in.ghost1().x;
			size_type ystart =    in.ghost0().y;
			size_type yend   = Ny-in.ghost1().y;
			value_type dx    = values<value_type>::one() / pow(in.grid_space().x,(value_type) super::m_Q);
			value_type dy    = values<value_type>::one() / pow(in.grid_space().y,(value_type) super::m_Q);
			
			// Ignore ghost layers
			for(size_type i=ystart; i<yend; ++i)
			{
				size_type offset_y = i < super::m_alpha ? i : i >= Ny-super::m_alpha ? 2*super::m_alpha+i-Ny+1 : super::m_alpha;
				size_type row_y = offset_y*super::m_rank;

				for(size_type j=xstart; j<xend; ++j)
				{
					size_type offset_x = j < super::m_alpha ? j : j >= Nx-super::m_alpha ? 2*super::m_alpha+j-Nx+1 : super::m_alpha;
					size_type row_x = offset_x*super::m_rank;
				
					value_type sum(0);
					for(size_type s = 0; s<super::m_rank; ++s)
					{
						sum += (dx*super::m_coeffs[row_x+s]*in[i*Nx+j-offset_x+s]) + (dy*super::m_coeffs[row_y+s]*in[(i-offset_y+s)*Nx+j]);
					}
					out[i*Nx+j] = sum;
				}
			}
		}

		
		/**
		* Spezialized device version
		*/
		template <typename V>
		void diff_x(V const& in, V& out,device_memory) const
		{
			typedef typename V::value_type	value_type;
			typedef typename V::size_type size_type;

			size_type N = in.size();
			ASSERT(N == out.size());

			size_type Nx     = in.dimension().x;
			size_type Ny     = in.dimension().y;
			size_type Nz     = in.dimension().z;
			size_type xstart =    in.ghost0().x;
			size_type xend   = Nx-in.ghost1().x;
			size_type ystart =    in.ghost0().y;
			size_type yend   = Ny-in.ghost1().y;
			size_type zstart =    in.ghost0().z;
			size_type zend   = Nz-in.ghost1().z;
			value_type dx    = value_type(1)/pow((value_type)in.grid_space().x,(value_type) super::m_Q);
			value_type dy    = value_type(1)/pow((value_type)in.grid_space().y,(value_type) super::m_Q);

			// Make sure coeffs is in const memory
			super::upload_coeffs();
			
			// Setup kernel configuration
			dim3 block = BLOCK2D(Nx,Ny);
			dim3 grid  = GRID2D(Nx,Ny);
			unsigned int smem = (block.x+2*super::m_alpha)*(block.y+2*super::m_alpha)*sizeof(T);

			kernel::diff_x<<<grid,block,smem>>>(RAW_PTR(in),RAW_PTR(out),dx,super::m_alpha,Nx,Ny,xstart,xend,ystart,yend,zstart,zend);
		}

		
		/**
		* Spezialized device version
		*/
		template <typename V>
		void diff_y(V const& in, V& out,device_memory) const
		{
			typedef typename V::value_type	value_type;
			typedef typename V::size_type size_type;

			size_type N = in.size();
			ASSERT(N == out.size());

			size_type Nx     = in.dimension().x;
			size_type Ny     = in.dimension().y;
			size_type Nz     = in.dimension().z;
			size_type xstart =    in.ghost0().x;
			size_type xend   = Nx-in.ghost1().x;
			size_type ystart =    in.ghost0().y;
			size_type yend   = Ny-in.ghost1().y;
			size_type zstart =    in.ghost0().z;
			size_type zend   = Nz-in.ghost1().z;
			value_type dx    = value_type(1)/pow((value_type)in.grid_space().x,(value_type) super::m_Q);
			value_type dy    = value_type(1)/pow((value_type)in.grid_space().y,(value_type) super::m_Q);

			// Setup kernel configuration
			dim3 block = BLOCK2D(Nx,Ny);
			dim3 grid  = GRID2D(Nx,Ny);
			kernel::diff_y<<<grid,block>>>(RAW_PTR(in),RAW_PTR(out),dx,super::m_alpha,Nx,Ny,xstart,xend,ystart,yend,zstart,zend);
		}

		/**
		* Spezialized host version.
		* Multiply the stencil to a 2d grid vector.
		* Uses a sigma transformation in the vertical (second) direction.
		* @param host_memory dispatching
		* @param in grid
		* @param out grid holding differentiation
		* @param sigma transformation info
		*/
		template <typename V, typename S>
		void diff_sigma_transform(V const& in, V& out, S const& sigma, host_memory) const
		{
			typedef typename V::value_type	value_type;
			typedef typename V::size_type	size_type;
			
			size_type N = in.size();
			ASSERT(N == out.size());

			size_type Nx     = in.Nx();
			size_type Ny     = in.Ny();
			size_type xstart =    in.ghost0().x;
			size_type xend   = Nx-in.ghost1().x;
			size_type ystart =    in.ghost0().y;
			size_type yend   = Ny-in.ghost1().y;
			value_type dx    = values<value_type>::one() / pow(in.grid_space().x,(value_type) super::m_Q);
			value_type dy    = values<value_type>::one() / pow(in.grid_space().y,(value_type) super::m_Q);

			// Ignore ghost layers
			for(size_type i=ystart; i<yend; ++i)
			{
				size_type offset_y = i < super::m_alpha ? i : i >= Ny-super::m_alpha ? 2*super::m_alpha+i-Ny+1 : super::m_alpha;
				size_type row_y = offset_y*super::m_rank;

				for(size_type j=xstart; j<xend; ++j)
				{
					size_type offset_x = j < super::m_alpha ? j : j >= Nx-super::m_alpha ? 2*super::m_alpha+j-Nx+1 : super::m_alpha;
					size_type row_x = offset_x*super::m_rank;
				
					value_type sum = values<value_type>::zero();
					for(size_type s = 0; s<this->m_rank; ++s)
					{
						value_type sig = pow(sigma[i*Nx+j], (value_type) this->m_Q);
						sum += (dx*this->m_coeffs[row_x+s]*in[i*Nx+j-offset_x+s]) + (dy*this->m_coeffs[row_y+s]*in[(i-offset_y+s)*Nx+j])*(sig);
					}
					out[i*Nx+j] = sum;
				}
			}
		}


		/**
		* Spezialized device version.
		* Multiply the stencil to a 2d grid vector.
		* Uses a sigma transformation in the vertical (second) direction.
		* @param host_memory dispatching
		* @param in grid
		* @param out grid holding differentiation
		* @param sigma transformation info
		*/
		template <typename V, typename S>
		void diff_sigma_transform(V const& in, V& out, S const& sigma, device_memory) const
		{
			typedef typename V::value_type	value_type;
			typedef typename V::size_type	size_type;
			
			size_type N = in.size();
			ASSERT(N == out.size());

			size_type Nx     = in.Nx();
			size_type Ny     = in.Ny();
			size_type xstart =    in.ghost0().x;
			size_type xend   = Nx-in.ghost1().x;
			size_type ystart =    in.ghost0().y;
			size_type yend   = Ny-in.ghost1().y;
			value_type dx    = values<value_type>::one() / pow(in.grid_space().x,(value_type) super::m_Q);
			value_type dy    = values<value_type>::one() / pow(in.grid_space().y,(value_type) super::m_Q);

			// Make sure coeffs is in const memory
			this->upload_coeffs();
			
			dim3 grid = GRID2D(Nx,Ny);
			dim3 block = BLOCK2D(Nx,Ny);
			// TODO: sigma[0] only works if sigma is a const iterator, not if sigma vary over the domain
			gpulab::FD::kernel::diff_sigma_transform_2d<<<grid,block>>>(RAW_PTR(in), RAW_PTR(out), dx, dy, this->m_alpha, Nx, Ny, sigma[0], xstart, xend, ystart, yend, this->m_Q);
			
		}

	protected:

	
	};


}; // namespace FD
}; // namespace gpulab

#endif // GPULAB_FD_STENCIL_H
