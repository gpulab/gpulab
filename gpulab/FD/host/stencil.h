// ==========================================================================
// Host Stencil
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_FD_HOST_STENCIL_H
#define GPULAB_FD_HOST_STENCIL_H

#include <gpulab/type_traits.h>

namespace gpulab
{
namespace FD
{
namespace host
{


} // end namespace host
} // end namespace FD
} // end namespace gpulab

#endif // GPULAB_FD_HOST_STENCIL_H