// ==========================================================================
// Query for the device properties
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: your name
// Email:   your e-mail
// ==========================================================================

#ifndef GPULAB_DEVICE_DEVICE_PROPERTIES_H
#define GPULAB_DEVICE_DEVICE_PROPERTIES_H

#include <stdio.h>
#include <assert.h>
#include <cuda.h>

namespace gpulab
{ 
namespace device
{

void display_properties( cudaDeviceProp const& pDeviceProp )
{
    printf( "\nDevice Name \t - %s ", pDeviceProp.name );
    printf( "\n**************************************");
    printf( "\nTotal Global Memory\t\t -%d KB", pDeviceProp.totalGlobalMem/1024 );
    printf( "\nShared memory available per block \t - %d KB", pDeviceProp.sharedMemPerBlock/1024 );
    printf( "\nNumber of registers per thread block \t - %d", pDeviceProp.regsPerBlock );
    printf( "\nWarp size in threads \t - %d", pDeviceProp.warpSize );
    printf( "\nMemory Pitch \t - %d bytes", pDeviceProp.memPitch );
    printf( "\nMaximum threads per block \t - %d", pDeviceProp.maxThreadsPerBlock );
    printf( "\nMaximum Thread Dimension (block) \t - %d %d %d", pDeviceProp.maxThreadsDim[0], pDeviceProp.maxThreadsDim[1], pDeviceProp.maxThreadsDim[2] );
    printf( "\nMaximum Thread Dimension (grid) \t - %d %d %d", pDeviceProp.maxGridSize[0], pDeviceProp.maxGridSize[1], pDeviceProp.maxGridSize[2] );
    printf( "\nTotal constant memory \t - %d bytes", pDeviceProp.totalConstMem );
    printf( "\nCUDA ver \t - %d.%d", pDeviceProp.major, pDeviceProp.minor );
    printf( "\nClock rate \t - %d KHz", pDeviceProp.clockRate );
    printf( "\nTexture Alignment \t - %d bytes", pDeviceProp.textureAlignment );
    printf( "\nDevice Overlap \t - %s", pDeviceProp. deviceOverlap?"Allowed":"Not Allowed" );
    printf( "\nNumber of Multi processors \t - %d\n", pDeviceProp.multiProcessorCount );
}

void display_properties()
{
	int dev;
	cudaGetDevice(&dev);
	cudaDeviceProp device_prop;
	cudaGetDeviceProperties(&device_prop, dev);
	gpulab::device::display_properties(device_prop);
}

/**
* Returns the properties for device number = index
* or if index is not set, the active device properties.
*/
const cudaDeviceProp get_device_properties(int index = -1)
{
	if(index == -1)
		cudaGetDevice (&index);

	cudaDeviceProp device_prop;
    int devices = 0;
    cudaGetDeviceCount( &devices );

	if(index < devices)
	{
		cudaGetDeviceProperties(&device_prop, index);
	}
	return device_prop;
}


/**
* Returns the number of GPUs
*/
int get_device_count()
{
    int devices = 2;
    cudaGetDeviceCount( &devices );
	return devices;
}

void print_memory_info()
{
	size_t free, total;
	cudaMemGetInfo (&free, &total);
	printf("Total memory: %4i Mb,  free: %4i Mb,  used: %4i Mb\n",total/(1024*1024),free/(1024*1024),(total-free)/(1024*1024));
}

} // namespace device
} // namespace gpulab 

#endif // GPULAB_DEVICE_DEVICE_PROPERTIES_H