// ==========================================================================
// Shared memory handler
// This is used to avoid conflicts when mixing extern shared memory variables
// with templates.
// Details and reference, see the NVIDIA simpleTemplate example or:
// http://www.naic.edu/~phil/hardware/nvidia/doc/src/simpleTemplates/doc/readme.txt
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// Date:    July 14. 2011
// ==========================================================================

#ifndef GPULAB_DEVICE_SHARED_MEMORY_H
#define GPULAB_DEVICE_SHARED_MEMORY_H

#include <gpulab/config.h>

namespace gpulab
{
namespace device
{
	/**
	* Non-specialized shared memory version
	*/
	template <typename T>
	struct shared_memory
	{
		// Ensure that we won't compile any un-specialized types
		__device__ static T* get_pointer() {
			extern __device__ void error(void);
			error();
			return NULL;
		}
	};
		
	template <>
	struct shared_memory <int>
	{
		__device__ static int* get_pointer() { extern __shared__ int s_int[]; return s_int; }
	};
	template <>
	struct shared_memory <float>
	{
		__device__ static float* get_pointer() { extern __shared__ float s_float[]; return s_float; }
	};
	template <>
	struct shared_memory <volatile float>
	{
		__device__ static volatile float* get_pointer() { extern __shared__ volatile float s_volatile_float[]; return s_volatile_float; }
	};
	template <>
	struct shared_memory <double>
	{
		__device__ static double* get_pointer() { extern __shared__ double s_double[]; return s_double; }
	};
	template <>
	struct shared_memory <volatile double>
	{
		__device__ static volatile double* get_pointer() { extern __shared__ volatile double s_volatile_double[]; return s_volatile_double; }
	};	
	template <>
	struct shared_memory <unsigned int>
	{
		__device__ static unsigned int* get_pointer() { extern __shared__ unsigned int s_unsigned_int[]; return s_unsigned_int; }
	};
	template <>
	struct shared_memory <char>
	{
		__device__ static char* get_pointer() { extern __shared__ char s_char[]; return s_char; }
	};
	template <>
	struct shared_memory <short>
	{
		__device__ static short* get_pointer() { extern __shared__ short s_short[]; return s_short; }
	};
	template <>
	struct shared_memory <long>
	{
		__device__ static long* get_pointer() { extern __shared__ long s_long[]; return s_long; }
	};
	template <>
	struct shared_memory <bool>
	{
		__device__ static bool* get_pointer() { extern __shared__ bool s_bool[]; return s_bool; }
	};


	template <typename value_type>
	__device__ inline
	void load_shared_mem_2d(value_type* smem, value_type const* src, int Nx, int Ny, int alpha, dim3* sdim, dim3* tid)
	{
		int ti, si, sj, Si, Sj;
		sdim->x = blockDim.x+2*alpha;
		sdim->y = blockDim.y+2*alpha;
		for(ti=(threadIdx.x+threadIdx.y*blockDim.x); ti<sdim->x*sdim->y; ti+=blockDim.x*blockDim.y)
		{
			si = ti % sdim->x;
			sj = ti / sdim->x;
			Si = blockDim.x * blockIdx.x - alpha + si;
			Sj = blockDim.y * blockIdx.y - alpha + sj;
			if(Si>=0 && Si<Nx && Sj>=0 && Sj<Ny)
				smem[ti] = src[Si+Sj*Nx];
		}
		tid->x = threadIdx.x + alpha;
		tid->y = threadIdx.y + alpha;

		__syncthreads();
	}


	/**
	* Wraps 2d shared memory reading with a ghost (alpha) layer in each direction.
	* Make sure that at least (2*alpha+blockDim.x)*(2*alpha+blockDim.y) is allocated for smem.
	* @smem shared memory destination
	* @src global memory to read from
	* @Nx x-dimension of global memory
	* @Ny y-dimension of global memory
	* @alpha size of extra halo-layer to read
	* @sdim contains actual dimension of smem after execution
	* @sid contains (x,y) ids corresponding to global mem, i.e. src[idx(x,y,Nx)] == smem[idx(sid.x,sid.y,sdim.x)]
	*/
	template <typename value_type>
	__device__
	void load_shared_mem_block_2d(volatile value_type* smem, value_type const* src, int Nx, int Ny, int alpha, dim3* sdim, dim3* sid)
	{
		// Beginning of thread block
		int j1 = blockDim.x*blockIdx.x;
		int i1 = blockDim.y*blockIdx.y;
		
		// End of smem block
		int j2 = min(Nx, j1 + blockDim.x + alpha);
		int i2 = min(Ny, i1 + blockDim.y + alpha);
		
		// Beginning of smem block
		int j0 = max(0,min((Nx)-(2*alpha+1),j1-alpha));
		int i0 = max(0,min((Ny)-(2*alpha+1),i1-alpha));
		
		// Id relative to smem
		sid->x = threadIdx.x + j1-j0;
		sid->y = threadIdx.y + i1-i0;

		// Smem size
		sdim->x = j2-j0;
		sdim->y = i2-i0;
		
		int tid = blockDim.x*threadIdx.y+threadIdx.x;
		// Loop over the entire smem block
		for(;tid < sdim->x*sdim->y; tid+=blockDim.x*blockDim.y)
		{
			j2 = j0 + (tid - sdim->x*(tid/sdim->x));
			i2 = i0 + (tid / sdim->x);
			smem[tid] = src[i2*Nx+j2];
		}
		__syncthreads();
	};




	template <typename value_type, typename size_type>
	__device__
	void load_shared_mem_block_2d_v2(volatile value_type* smem, value_type const* src, size_type Nx, size_type Ny, size_type alpha, dim3* sdim, dim3* sid)
	{
		// Dimension of the smem block
		sdim->x = blockDim.x+2*alpha;
		sdim->y = blockDim.y+2*alpha;
		
		// Thread index with alpha offset
		sid->x = threadIdx.x+alpha;
		sid->y = threadIdx.y+alpha;
		
		// Beginning of smem block in global x,y xoords
		int sx0 = blockDim.x*blockIdx.x - alpha;
		int sy0 = blockDim.y*blockIdx.y - alpha;

		// Loop and write to smem from global mem
		int tid = blockDim.x*threadIdx.y + threadIdx.x;
		for(; tid<sdim->x*sdim->y; tid+=blockDim.x*blockDim.y)
		{
			int sx = sx0 + (tid % sdim->x); // x coord to read from
			int sy = sy0 + (tid / sdim->x); // y coord to read from
			
			// Make sure it is in bounds
			if(sx>=0 && sx<Nx && sy>=0 && sy<Ny)
				smem[tid] = src[sy*Nx + sx];
		}
		__syncthreads();
	};


	/** 
	* Load N elements from global memory to shared memory (per block)
	* Uses a 2D layout of the block to calculate identifiers
	*/
	template <typename value_type, typename size_type>
	__device__
	inline void load_shared_memory(value_type* smem, value_type const* src, size_type N)
	{
		size_type tx = blockDim.x*threadIdx.y + threadIdx.x;
		
		// Load stencil into smem
		for(; tx < N; tx+=(blockDim.x*blockDim.y))
		{
			smem[tx] = src[tx];
		}
		__syncthreads();
	}

} // namespace device
} // namespace gpulab

#endif // GPULAB_DEVICE_SHARED_MEMORY_H
