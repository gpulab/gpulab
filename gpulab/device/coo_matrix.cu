// ==========================================================================
// Coo Matrix kernels
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================


#ifndef GPULAB_DEVICE_COO_MATRIX_CU
#define GPULAB_DEVICE_COO_MATRIX_CU

#include <cuda.h>

namespace gpulab
{
	namespace device
	{
		#if __CUDA_ARCH__ >= 130 // Double precision only in CC 1.3 or greater

		// Atomic double add implementation for CUDA devices without native support.
		// Based on http://forums.nvidia.com/index.php?s=&showtopic=67691&view=findpost&p=380935 by hqyang
		static inline __device__ double atomicAdd(double *address, double val)
		{
			  unsigned long long int i_val = __double_as_longlong(val);
			  unsigned long long int tmp0 = 0;
			  unsigned long long int tmp1;

			  while( (tmp1 = atomicCAS((unsigned long long int *)address, tmp0, i_val)) != tmp0)
			  {
					  tmp0 = tmp1;
					  i_val = __double_as_longlong(val + __longlong_as_double(tmp1));
			  }

			  return __longlong_as_double(tmp0);
		}

		#endif

		#if __CUDA_ARCH__ < 200 // CC 2.0 and greater have native atomicAdd for floats

		// Atomic float add implementation for CUDA devices without native support.
		// Based on http://forums.nvidia.com/index.php?s=&showtopic=67691&view=findpost&p=380935 by hqyang
		static inline __device__ float atomicAdd(float *address, float val)
		{
			  int i_val = __float_as_int(val);
			  int tmp0 = 0;
			  int tmp1;

			  while( (tmp1 = atomicCAS((int *)address, tmp0, i_val)) != tmp0)
			  {
					  tmp0 = tmp1;
					  i_val = __float_as_int(val + __int_as_float(tmp1));
			  }

			  return __int_as_float(tmp0);
		}

		#else

		// Invoke native atomic add
		static inline __device__ float atomicAdd(float *address, float val)
		{
			return ::atomicAdd(address, val);
		}

		#endif

		template <typename value_type, typename size_type>
		__global__
		void coo_matrix_mult_unsorted_kernel(size_type const* i, size_type const* j, value_type const* data, value_type const* x, value_type* b, size_type nonzeros, size_type I, size_type J)
		{
			for (size_type idx = blockDim.x * blockIdx.x + threadIdx.x; idx < nonzeros; idx += blockDim.x * gridDim.x)
			{
				size_type idx_i = i[idx];
				size_type idx_j = j[idx];
				value_type val = data[idx];
				atomicAdd(b + idx_i, val * x[idx_j]);
			}
		}

		template <typename value_type, typename size_type>
		__global__
		void coo_matrix_mult_rowsorted_kernel(size_type const* i, size_type const* j, value_type const* data, value_type const* x, value_type* b, size_type nonzeros, size_type I, size_type J)
		{
			extern __shared__ value_type s_vals[];
			size_type* s_rows = (size_type*) (s_vals + blockDim.x);

			for (size_type idx = blockDim.x * blockIdx.x + threadIdx.x; idx < nonzeros; idx += blockDim.x * gridDim.x)
			{
				// Compute value
				size_type idx_i = i[idx];
				size_type idx_j = j[idx];
				value_type val = data[idx];
				s_vals[threadIdx.x] = val * x[idx_j];
				s_rows[threadIdx.x] = idx_i;
				__syncthreads();

			    // Segmented reduction in shared memory for an entire block based on 'Efficient Sparse Matrix-Vector Multiplication on CUDA' by Nathan Bell and Michael Garland
				int lane = threadIdx.x;
			    if( lane >= 1   && s_rows[lane] == s_rows[lane - 1] ) { value_type prev_val = s_vals[lane - 1]; __syncthreads(); s_vals[lane] += prev_val; __syncthreads(); };
			    if( lane >= 2   && s_rows[lane] == s_rows[lane - 2] ) { value_type prev_val = s_vals[lane - 2]; __syncthreads(); s_vals[lane] += prev_val; __syncthreads(); };
			    if( lane >= 4   && s_rows[lane] == s_rows[lane - 4] ) { value_type prev_val = s_vals[lane - 4]; __syncthreads(); s_vals[lane] += prev_val; __syncthreads(); };
			    if( lane >= 8   && s_rows[lane] == s_rows[lane - 8] ) { value_type prev_val = s_vals[lane - 8]; __syncthreads(); s_vals[lane] += prev_val; __syncthreads(); };
			    if( lane >= 16  && s_rows[lane] == s_rows[lane - 16] ) { value_type prev_val = s_vals[lane - 16]; __syncthreads(); s_vals[lane] += prev_val; __syncthreads(); };
			    if( lane >= 32  && s_rows[lane] == s_rows[lane - 32] ) { value_type prev_val = s_vals[lane - 32]; __syncthreads(); s_vals[lane] += prev_val; __syncthreads(); };
				if( lane >= 64  && s_rows[lane] == s_rows[lane - 64] ) { value_type prev_val = s_vals[lane - 64]; __syncthreads(); s_vals[lane] += prev_val; __syncthreads(); };
				if( lane >= 128 && s_rows[lane] == s_rows[lane - 128] ) { value_type prev_val = s_vals[lane - 128]; __syncthreads(); s_vals[lane] += prev_val; __syncthreads(); };
				if( lane >= 256 && s_rows[lane] == s_rows[lane - 256] ) { value_type prev_val = s_vals[lane - 256]; __syncthreads(); s_vals[lane] += prev_val; __syncthreads(); };
				if( lane >= 512 && s_rows[lane] == s_rows[lane - 512] ) { value_type prev_val = s_vals[lane - 512]; __syncthreads(); s_vals[lane] += prev_val; __syncthreads(); };

			    // Atomic add
				if (threadIdx.x == blockDim.x - 1 || s_rows[threadIdx.x + 1] != idx_i)
					atomicAdd(b + idx_i, s_vals[threadIdx.x]);
			}
		}

		// SLOW, WORSE THAN NAIVE, DO NOT USE CURRENTLY
		template <typename value_type, typename size_type>
		__global__
		void coo_matrix_mult_colsorted_kernel(size_type const* i, size_type const* j, value_type const* data, value_type const* x, value_type* b, size_type nonzeros, size_type I, size_type J)
		{
			extern __shared__ value_type s_x[];
			size_type* s_cols = (size_type*) (s_x + blockDim.x);

			for (size_type idx = blockDim.x * blockIdx.x + threadIdx.x; idx < nonzeros; idx += blockDim.x * gridDim.x)
			{
				// Load values
				size_type idx_i = i[idx];
				size_type idx_j = j[idx];
				value_type val = data[idx];
				s_cols[threadIdx.x] = idx_j;
				__syncthreads();

				// Load x
				s_x[threadIdx.x] = 0;
				if( threadIdx.x == 0 || s_cols[threadIdx.x] != s_cols[threadIdx.x - 1] )
					s_x[threadIdx.x] = x[idx_j];
				__syncthreads();

				// Segmented reduction in shared memory for an entire block based on 'Efficient Sparse Matrix-Vector Multiplication on CUDA' by Nathan Bell and Michael Garland
				int lane = threadIdx.x;
				if( lane >= 1   && s_cols[threadIdx.x] == s_cols[threadIdx.x - 1] ) { value_type prev_val = s_x[threadIdx.x - 1]; __syncthreads(); s_x[threadIdx.x] += prev_val; __syncthreads(); };
				if( lane >= 2   && s_cols[threadIdx.x] == s_cols[threadIdx.x - 2] ) { value_type prev_val = s_x[threadIdx.x - 2]; __syncthreads(); s_x[threadIdx.x] += prev_val; __syncthreads(); };
				if( lane >= 4   && s_cols[threadIdx.x] == s_cols[threadIdx.x - 4] ) { value_type prev_val = s_x[threadIdx.x - 4]; __syncthreads(); s_x[threadIdx.x] += prev_val; __syncthreads(); };
				if( lane >= 8   && s_cols[threadIdx.x] == s_cols[threadIdx.x - 8] ) { value_type prev_val = s_x[threadIdx.x - 8]; __syncthreads(); s_x[threadIdx.x] += prev_val; __syncthreads(); };
				if( lane >= 16  && s_cols[threadIdx.x] == s_cols[threadIdx.x - 16] ) { value_type prev_val = s_x[threadIdx.x - 16]; __syncthreads(); s_x[threadIdx.x] += prev_val; __syncthreads(); };
				if( lane >= 32  && s_cols[threadIdx.x] == s_cols[threadIdx.x - 32] ) { value_type prev_val = s_x[threadIdx.x - 32]; __syncthreads(); s_x[threadIdx.x] += prev_val; __syncthreads(); };
				if( lane >= 64  && s_cols[threadIdx.x] == s_cols[threadIdx.x - 64] ) { value_type prev_val = s_x[threadIdx.x - 64]; __syncthreads(); s_x[threadIdx.x] += prev_val; __syncthreads(); };
				if( lane >= 128 && s_cols[threadIdx.x] == s_cols[threadIdx.x - 128] ) { value_type prev_val = s_x[threadIdx.x - 128]; __syncthreads(); s_x[threadIdx.x] += prev_val; __syncthreads(); };
				if( lane >= 256 && s_cols[threadIdx.x] == s_cols[threadIdx.x - 256] ) { value_type prev_val = s_x[threadIdx.x - 256]; __syncthreads(); s_x[threadIdx.x] += prev_val; __syncthreads(); };
				if( lane >= 512 && s_cols[threadIdx.x] == s_cols[threadIdx.x - 512] ) { value_type prev_val = s_x[threadIdx.x - 512]; __syncthreads(); s_x[threadIdx.x] += prev_val; __syncthreads(); };

				// Atomic add
				atomicAdd(b + idx_i, val * s_x[threadIdx.x]);
			}
		}
	} // namespace device

} // namespace gpulab

#endif // GPULAB_DEVICE_COO_MATRIX_CU
