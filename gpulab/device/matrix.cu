// ==========================================================================
// Matrix kernels
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DEVICE_MATRIX_CU 
#define GPULAB_DEVICE_MATRIX_CU

#include <gpulab/config.h>
#include <gpulab/type_traits.h>
#include <gpulab/device/shared_memory.h>

#define TILE_WIDTH 16

namespace gpulab
{
namespace device
{
namespace matrix
{
namespace kernel
{
		/**
		* Set diagonal elements to value
		*/
		template <typename T, typename size_type, typename major_to, typename major_from>
		__global__
		void change_major_kernel(T* to, T const* from, size_type const I, size_type const J)
		{
			const size_type i = blockDim.y * blockIdx.y + threadIdx.y;
			const size_type j = blockDim.x * blockIdx.x + threadIdx.x;
			
			if(i < I && j < J)
			{
				to[major_to::index(i,j,I,J)] = from[major_from::index(i,j,I,J)];
			}
		}

		/**
		* Set diagonal elements to value
		*/
		template <typename T, typename size_type, typename major>
		__global__
		void set_diag_kernel(T* data, T const value, const size_type I, const size_type J)
		{
			size_type idx = blockDim.x * blockIdx.x + threadIdx.x;
			idx = major::index(idx, idx, I, J);
			if(idx < I*J)
			{
				data[idx] = value;
			}
		}

		/**
		* Set diagonal elements to values in vector v
		*/
		template <typename T, typename size_type, typename major>
		__global__
		void set_diag_vector_kernel(T* data, T const* v, const size_type I, const size_type J)
		{
			size_type idx = blockDim.x * blockIdx.x + threadIdx.x;
			size_type idx_diag = major::index(idx, idx, I, J);
			if(idx_diag < I*J)
			{
				data[idx_diag] = v[idx];
			}
		}

		/**
		* Set diagonal elements to values in matrix m
		*/
		template <typename T, typename size_type, typename major>
		__global__
		void set_diag_matrix_kernel(T* data, T const* m, const size_type I, const size_type J)
		{
			size_type idx = blockDim.x * blockIdx.x + threadIdx.x;
			idx = major::index(idx, idx, I, J);
			if(idx < I*J)
			{
				data[idx] = m[idx];
			}
		}

		/**
		* 
		*/
		template <typename T, typename vector_type, typename major>
		__global__
		void set_cols_kernel(T* dst, vector_type* src, const unsigned int I, const unsigned int J, const unsigned int cols)
		{
			const unsigned int j = blockDim.x * blockIdx.x + threadIdx.x;
			const unsigned int i = blockDim.y * blockIdx.y + threadIdx.y;

			if(i < I && j < J && i < cols && src[i].size() > j)
			{
				dst[major::index(i,j,I,J)] = src[0].data()[i];
			}
		}

				/**
		* 
		*/
		template <typename T, typename major>
		__global__
		void set_col_kernel(T* dst, T* src, const unsigned int col, const unsigned int I, const unsigned int J, const unsigned int N)
		{
			const unsigned int idx = blockDim.x * blockIdx.x + threadIdx.x;

			if(idx < I && col < J && idx < N)
			{
				dst[major::index(idx, col, I, J)] = src[idx]; // This is only coallesed for column major
			}
		}


		/**
		* Matrix vector multiplication, row major. 
		* TODO: Optimize for shared memory
		* Note: Column major is better performance wise!
		*/
		template <typename T, typename size_type>
		__global__
			void matrix_vector_mult_kernel(T const* A, T const* x, T* b, size_type I, size_type J, gpulab::row_major)
		{
			extern __shared__ T s_x[];
			size_type idx = blockDim.x*blockIdx.x + threadIdx.x + gridDim.x*blockIdx.y*blockDim.x;

			if(idx < I)
			{
				unsigned int tile_idx;
				T val = T();
				for(int i=0; i < J; i+=blockDim.x)
				{
					//// Load x into shared memory
					//tile_idx = i + threadIdx.x;
					//if(tile_idx < J)
					//	s_x[threadIdx.x] = x[tile_idx];

					//__syncthreads();

					for(int j=0; j<blockDim.x; ++j)
					{
						tile_idx = i + j;
						if(tile_idx < J)
							val += A[idx*J + tile_idx] * x[tile_idx];//s_x[j];
					}

				}
				b[idx] = val;
			}
		}


		/**
		* Matrix vector multiplication, column major. 
		*/
		template <typename T, typename size_type>
		__global__
		void matrix_vector_mult_kernel(T const *A, T const *x, T *result, size_type m, size_type n, gpulab::column_major)
		{
			T* s_mem = shared_memory<T>::get_pointer();

			size_type row = threadIdx.x + blockIdx.x*blockDim.x + gridDim.x*blockIdx.y*blockDim.x;
			size_type tile;
			T sum = T();

			for (tile = 0; tile < n/blockDim.x; tile++) {
				s_mem[threadIdx.x] = x[threadIdx.x + tile*blockDim.x];
				if (blockDim.x > 32) __syncthreads();
#pragma unroll
				for (int i = 0; i < blockDim.x; i++) {
					sum += A[row]*s_mem[i]; A += m;
				}
				if (blockDim.x > 32) __syncthreads();
			}
			unsigned int nrest = n - tile*blockDim.x;
			if (nrest) {
				if (threadIdx.x < nrest) s_mem[threadIdx.x] = x[threadIdx.x + tile*blockDim.x];
				if (blockDim.x > 32) __syncthreads();
#pragma unroll
				for (int i = 0; i < nrest; i++) {
					sum += A[row]*s_mem[i]; A += m;
				}
				if (blockDim.x > 32) __syncthreads();
			}
			if (row < m) result[row] = sum;
		}



		/**
		* Assumes row major
		*/
		template <typename T, typename size_type>
		__global__
		void matrix_vector_mult_naive_kernel(T const* A, T const* x, T* b, size_type I, size_type J)
		{
			size_type idx = blockDim.x * blockIdx.x + threadIdx.x;

			if(idx < I)
			{
				T val = T();
				for(int i=0; i < J; ++i)
				{
					val += A[idx*J + i] * x[i];
				}
				b[idx] = val;
			}
		}

		/**
		* Matrix matrix multiplication
		* TODO: What happens when I or J is not dividable with TILE_WIDTH ??
		*/
		template <typename T, typename size_type>
		__global__
		void matrix_matrix_mult_kernel(T const* M, T const* N, T* P, size_type I, size_type J)
		{
			__shared__ T s_M[TILE_WIDTH][TILE_WIDTH];
			__shared__ T s_N[TILE_WIDTH][TILE_WIDTH];

			int bx = blockIdx.x; int by = blockIdx.y;
			int tx = threadIdx.x; int ty = threadIdx.y;
			
			int row = by * TILE_WIDTH + ty;
			int col = bx * TILE_WIDTH + tx;

			T val = T();
			for(int i=0; i<I/TILE_WIDTH; ++i)
			{
				s_M[ty][tx] = M[row*J + (i*TILE_WIDTH + tx)];
				s_N[ty][tx] = N[(i*TILE_WIDTH + ty)*J + col];
				__syncthreads();

				for(int j=0; j<TILE_WIDTH; ++j)
				{
					val += s_M[ty][j] * s_N[j][tx];
				}
			}
			P[row][col] = val;
		}
} // end namespace kernel
} // end namespace matrix
} // end namespace device
} // end namespace gpulab


#endif // GPULAB_DEVICE_MATRIX_CU
