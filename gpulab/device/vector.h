// ==========================================================================
// Device side Vector Functions
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DEVICE_VECTOR_H
#define GPULAB_DEVICE_VECTOR_H

#include <gpulab/device/reduction.cu>
#include <thrust/device_ptr.h>

namespace gpulab
{
namespace device
{
namespace vector
{
namespace kernel
{
	template <typename T>
	__global__
	void axpy( const T a
			 , T const* x
			 , T* y
			 , unsigned int N)
	{
		unsigned int idx = blockDim.x*blockIdx.x + threadIdx.x;
		if(idx < N)
		{
			y[idx] = a*x[idx] + y[idx];
		}
	}

	template <typename T>
	__global__
	void axpy( const T a1
			 , T const* x1
			 , const T a2
			 , T const* x2
			 , const T a3
			 , T const* x3
			 , T* y
			 , unsigned int N)
	{
		unsigned int idx = blockDim.x*blockIdx.x + threadIdx.x;
		if(idx < N)
		{
			y[idx] += a1*x1[idx] + a2*x2[idx] + a3*x3[idx];
		}
	}
} // kernel

	/**
	* DOT core array wrapper
	*/
	template <typename T>
	T dot(  const T* v1
				   , const T* v2
				   , const unsigned int N)
	{
		return gpulab::device::reduce_pair<
			gpulab::util::product_reduction<T>, gpulab::util::sum_reduction<T> >(v1, v2, N);
	}

	/**
	* DOT vector wrapper
	*/
	template <typename V>
	typename V::value_type dot(  const V& v1
							   , const V& v2)
	{
		return device::vector::dot(thrust::raw_pointer_cast(&v1[0]), thrust::raw_pointer_cast(&v2[0]), v1.size());
	}

	/**
	* NRM1 core array wrapper
	*/
	template <typename T>
	T nrm1( const T* v1
				   , const unsigned int N)
	{
		return gpulab::device::reduce< gpulab::util::sum_reduction<T> >(v1, N);
	}

	/**
	* NRM1 vector wrapper
	*/
	template <typename V>
	typename V::value_type nrm1( const V& v1 )
	{
		return nrm1(thrust::raw_pointer_cast(&v1[0]), v1.size());
	}

	/**
	* AXPY core array wrapper
	*/ 
	template <typename T>
	void axpy( T a
			 , const T* x
			 , T* y
			 , unsigned int N)
	{
		kernel::axpy<<<GRID1D(N),BLOCK1D(N)>>>(a,thrust::raw_pointer_cast(&x[0]),thrust::raw_pointer_cast(&y[0]),N);
	}

	/**
	* AXPY vector wrapper
	*/
	template <typename V, typename T>
	void axpy( T a1
			 , const V& x1
			 , T a2
			 , const V& x2
			 , T a3
			 , const V& x3
			 , V& y)
	{
		unsigned int N = y.size();
		kernel::axpy<<<GRID1D(N),BLOCK1D(N)>>>(a1,RAW_PTR(x1),a2,RAW_PTR(x2),a3,RAW_PTR(x3),RAW_PTR(y),N);
	}
} // end namespace vector
} // end namespace device
} // end namespace gpulab

#endif // GPULAB_DEVICE_VECTOR_H
