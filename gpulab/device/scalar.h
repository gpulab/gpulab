// ==========================================================================
// Device scalar
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DEVICE_SCALAR_CU
#define GPULAB_DEVICE_SCALAR_CU

namespace gpulab
{

namespace device
{

	/**
	* This is a wrapper for a value that resides on the device.
	* Host operations can be passed to device using overloaded 
	* operations here.
	* Do not ever use this in kernels.. it makes no sense to do so
	*/
	template <typename T>
	class scalar
	{
	private:
		T* m_ptr;

	public:
		scalar(T* ptr)
			: m_ptr(ptr)
		{}

		T const& operator=(T const& val)
		{
			CUDA_SAFE_CALL( cudaMemcpy (m_ptr, &val, sizeof(T), cudaMemcpyHostToDevice) );
			return val;
		}

		__host__
		T val() const
		{
			T val;
			CUDA_SAFE_CALL( cudaMemcpy (&val, (T*)m_ptr, sizeof(T), cudaMemcpyDeviceToHost) );
			return val;
		}

		__device__
		T value() const
		{
			return *m_ptr;
		}

	};


} // namespace device

} // namespace gpulab


#endif // GPULAB_DEVICE_SCALAR_CU