// ==========================================================================
// Reduction CUDA kernel
// Inspired by 
// http://developer.download.nvidia.com/compute/cuda/1_1/Website/projects/reduction/doc/reduction.pdf
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DEVICE_REDUCTION_CU
#define GPULAB_DEVICE_REDUCTION_CU

#include <helper_cuda.h>
#include <gpulab/util/reduction.h>

namespace gpulab
{
namespace device
{

	/**
	* Reduces the input data array using the reduction method implemented 
	* in the reduction template argument R.
	* Details see http://developer.download.nvidia.com/compute/cuda/1_1/Website/projects/reduction/doc/reduction.pdf
	* @param T value type
	* @param R reduction trait
	* @param block_size
	* @param g_idata global input data
	* @param g_odata global output data
	* @param n number of elements to reduce
	*/
	template <typename R, unsigned int block_size, typename T, typename size_type>
	__global__
	void reduce_kernel(T const* g_idata, T * g_odata, size_type n)
	{
		extern __shared__ T sdata[];

		unsigned int tid = threadIdx.x;
		unsigned int i = blockIdx.x*(block_size*2) + tid;
		unsigned int grid_size = block_size*2*gridDim.x;
		sdata[tid] = R::init();

		while (i < n)
		{
			sdata[tid] = R::reduce(sdata[tid], (i+block_size < n) ? R::reduce(g_idata[i], g_idata[i+block_size]) : g_idata[i]);
			i += grid_size;
		}
		__syncthreads();

		// Loop unrolling
		if (block_size >= 512) { if (tid < 256) { sdata[tid] = R::reduce(sdata[tid], sdata[tid + 256]); } __syncthreads(); }
		if (block_size >= 256) { if (tid < 128) { sdata[tid] = R::reduce(sdata[tid], sdata[tid + 128]); } __syncthreads(); }
		if (block_size >= 128) { if (tid < 64) { sdata[tid] = R::reduce(sdata[tid], sdata[tid + 64]); } __syncthreads(); }
		// Last warp unrolling. One warp, so no need to sync
		if (tid < 32) {
			if (block_size >= 64) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 32]); __syncthreads();
			if (block_size >= 32) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 16]); __syncthreads();
			if (block_size >= 16) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 8]); __syncthreads();
			if (block_size >= 8) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 4]); __syncthreads();
			if (block_size >= 4) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 2]); __syncthreads();
			if (block_size >= 2) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 1]); __syncthreads();
		}
		// Write result
		if (tid == 0)
			g_odata[blockIdx.x] = sdata[0];
	};

	/**
	* Reduces the combined input data arrays using the reduction method implemented
	* in the reduction template argument R.
	* Details see http://developer.download.nvidia.com/compute/cuda/1_1/Website/projects/reduction/doc/reduction.pdf
	* @param T value type
	* @param R reduction trait
	* @param block_size
	* @param g_idataA global input data 1
	* @param g_idataB global input data 2
	* @param g_odata global output data
	* @param n number of elements to reduce
	*/
	template <typename R_pair, typename R, unsigned int block_size, typename T, typename size_type>
	__global__
	void reduce_pair_kernel(T const* g_idataA, T const* g_idataB, T *g_odata, size_type n)
	{
		extern __shared__ T sdata[];

		unsigned int tid = threadIdx.x;
		unsigned int i = blockIdx.x*block_size + tid;
		unsigned int grid_size = block_size*gridDim.x;
		sdata[tid] = R::init();

		while (i < n)
		{
			sdata[tid] = R::reduce(sdata[tid], R_pair::reduce(g_idataA[i], g_idataB[i]));
			i += grid_size;
		}
		__syncthreads();

		// Loop unrolling
		if (block_size >= 512) { if (tid < 256) { sdata[tid] = R::reduce(sdata[tid], sdata[tid + 256]); } __syncthreads(); }
		if (block_size >= 256) { if (tid < 128) { sdata[tid] = R::reduce(sdata[tid], sdata[tid + 128]); } __syncthreads(); }
		if (block_size >= 128) { if (tid < 64) { sdata[tid] = R::reduce(sdata[tid], sdata[tid + 64]); } __syncthreads(); }
		// Last warp unrolling. One warp, so no need to sync
		if (tid < 32) {
			if (block_size >= 64) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 32]);
			if (block_size >= 32) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 16]);
			if (block_size >= 16) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 8]);
			if (block_size >= 8) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 4]);
			if (block_size >= 4) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 2]);
			if (block_size >= 2) sdata[tid] = R::reduce(sdata[tid], sdata[tid + 1]);
		}
		// Write result
		if (tid == 0)
			g_odata[blockIdx.x] = sdata[0];
	};

	/**
	* Reduces the vector into the return value, using the template reducuction argument
	* @param T_out return type
	* TODO: change/analyse what number of threads to use how many elements to process per thread
	* @param R reduction object, implementing static reduce(a,b) and init()
	*/
	template <typename R, typename T, typename size_type>
	T reduce(T const* v, size_type N)
	{
		// Would each thread to read up to 128 elements
		const unsigned int block = 128;
		unsigned int grid = (N / (block*128)) + 1;
		
		// Allocate space for the reduction result
		T* d_reduced;
		checkCudaErrors( cudaMalloc((void**)&d_reduced, grid*sizeof(T)) );

		cudaThreadSynchronize(); // Make sure device is ready

		// Reduce into d_reduced, which is not very large
		reduce_kernel<R, block><<<grid, block, block*sizeof(T)>>>(v, d_reduced, N);
		
		// Move to host and do the last part. TODO: not sure this is the best way
		T* h_out = (T*)malloc(grid*sizeof(T));
		checkCudaErrors( cudaMemcpy(h_out, d_reduced, grid*sizeof(T), cudaMemcpyDeviceToHost) );
		checkCudaErrors( cudaFree(d_reduced) );

		T out = h_out[0];
		for(unsigned int i=1; i<grid; ++i)
			out = R::reduce(out, h_out[i]);

		free(h_out);
		return out;
	};
	
	/**
	* Reduces the vector into the return value, using the template reducuction argument
	* @param T_out return type
	* @param R reduction object, implementing static reduce(a,b) and init()
	*/
	template <typename R_pair, typename R, typename T, typename size_type>
	T reduce_pair(T const* v1, T const* v2, size_type N)
	{
		const unsigned int block = 128;
		unsigned int grid = (N / (block*128)) + 1;
		
		// Allocate space for the reduction result
		T* d_reduced;
		checkCudaErrors( cudaMalloc((void**)&d_reduced, grid*sizeof(T)) );

		// Reduce into d_reduced, which is at most 64 in length
		reduce_pair_kernel<R_pair, R, block><<<grid, block, block*sizeof(T)>>>(v1, v2, d_reduced, N);

		// Move to host and do the last part. TODO: not sure this is the best way
		T* h_out = (T*)malloc(grid*sizeof(T));
		checkCudaErrors( cudaMemcpy(h_out, d_reduced, grid*sizeof(T), cudaMemcpyDeviceToHost) );
		checkCudaErrors( cudaFree(d_reduced) );

		T out = h_out[0];
		for(unsigned int i=1; i<grid; ++i)
			out = R::reduce(out, h_out[i]);

		free(h_out);
		return out;
	};

} // namespace device

} // namespace gpulab


#endif // GPULAB_DEVICE_REDUCTION_CU
