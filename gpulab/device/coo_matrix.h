// ==========================================================================
// COO Matrix device version
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DEVICE_COO_MATRIX_H
#define GPULAB_DEVICE_COO_MATRIX_H

#include <stdio.h>
#include <assert.h>

#include <gpulab/config.h>
#include <gpulab/coo_matrix.h>
#include <gpulab/device/coo_matrix.cu>

namespace gpulab
{
namespace device
{
namespace coo_matrix
{
		/**
		* Calculates Ax = b
		*/
		template <typename M, typename V>
		void mult(M const& A, V const& x, V &b)
		{
			// Reset b
			b.fill(0);

			// Invoke kernel
			dim3 block(MEAN_THREADS);
			dim3 grid((A.nonzeros() + (block.x - 1)) / block.x);
			if (grid.x > 65535)
				grid.x = 65535;

			if (A.sorted() == 1) {
				gpulab::device::coo_matrix_mult_rowsorted_kernel<<<grid, block, block.x*(sizeof(typename M::value_type) + sizeof(typename M::size_type))>>>( thrust::raw_pointer_cast(&A.i()[0])
																			, thrust::raw_pointer_cast(&A.j()[0])
																			, thrust::raw_pointer_cast(&A.data()[0])
																			, thrust::raw_pointer_cast(&x[0])
																			, thrust::raw_pointer_cast(&b[0])
																			, A.nonzeros()
																			, A.rows()
																			, A.cols());
			} else { // Col sorted uses unsorted.
				gpulab::device::coo_matrix_mult_unsorted_kernel<<<grid, block>>>( thrust::raw_pointer_cast(&A.i()[0])
																			, thrust::raw_pointer_cast(&A.j()[0])
																			, thrust::raw_pointer_cast(&A.data()[0])
																			, thrust::raw_pointer_cast(&x[0])
																			, thrust::raw_pointer_cast(&b[0])
																			, A.nonzeros()
																			, A.rows()
																			, A.cols());
			}
			cudaThreadSynchronize();
		}

		/**
		* Calculates A'x = b
		*/
		template <typename M, typename V>
		void multT(M const& A, V const& x, V &b)
		{
			// Reset b
			b.fill(0);

			// Invoke kernel
			dim3 block(MEAN_THREADS);
			dim3 grid((A.nonzeros() + (block.x - 1)) / block.x);
			if (grid.x > 65535)
				grid.x = 65535;

			if (A.sorted() == 2) {
				gpulab::device::coo_matrix_mult_rowsorted_kernel<<<grid, block, block.x*(sizeof(typename M::value_type) + sizeof(typename M::size_type))>>>( thrust::raw_pointer_cast(&A.j()[0])
																			, thrust::raw_pointer_cast(&A.i()[0])
																			, thrust::raw_pointer_cast(&A.data()[0])
																			, thrust::raw_pointer_cast(&x[0])
																			, thrust::raw_pointer_cast(&b[0])
																			, A.nonzeros()
																			, A.rows()
																			, A.cols());
			} else { // Col sorted uses unsorted.
				gpulab::device::coo_matrix_mult_unsorted_kernel<<<grid, block>>>( thrust::raw_pointer_cast(&A.j()[0])
																			, thrust::raw_pointer_cast(&A.i()[0])
																			, thrust::raw_pointer_cast(&A.data()[0])
																			, thrust::raw_pointer_cast(&x[0])
																			, thrust::raw_pointer_cast(&b[0])
																			, A.nonzeros()
																			, A.rows()
																			, A.cols());
			}

			cudaThreadSynchronize();
		}
} // namespace coo_matrix
} // namespace device
} // namespace gpulab

#endif // GPULAB_DEVICE_COO_MATRIX_H
