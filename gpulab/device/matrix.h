// ==========================================================================
// Matrix device version
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DEVICE_MATRIX_H
#define GPULAB_DEVICE_MATRIX_H

#include <stdio.h>
#include <assert.h>

#include <gpulab/config.h>
#include <gpulab/type_traits.h>
#include <gpulab/util/math.h>
#include <gpulab/device/matrix.cu>

namespace gpulab
{
namespace device
{
namespace matrix
{
	///**
	//* Set all values in the matrix
	//* @param value to be set
	//*/
	//void set(T value)
	//{
	//	cudaThreadSynchronize(); // Make sure device is ready
	//	const unsigned int block = MAX_THREADS;
	//	const unsigned int grid = (m_count+(block-1)) / block;
	//	set_kernel<<<grid, block>>>(this->m_first, value, this->m_count);
	//}

	///**
	//* Set first n columns to the values in the list of vectors v
	//* @param v vectors to be set
	//* @param n number of cols to set
	//*/
	//void set_cols(vector_type const v[], size_type n)
	//{
	//	if(n>0)
	//	{
	//		cudaThreadSynchronize(); // Make sure device is ready
	//		n = min(n, this->m_J);
	//		size_type col = 0;
	//		do
	//		{
	//			size_type m = min(this->m_I, v[col].size());
	//			dim3 block(MAX_THREADS);
	//			dim3 grid((m+(block.x-1)) / block.x);
	//			set_col_kernel<T, major><<<grid, block>>>(this->m_first, &(*v[col].begin()), col, this->m_I, this->m_J, v[col].size());
	//		}
	//		while(++col < n);
	//	}
	//}


	///**
	//* Set all diagonal values in the matrix
	//* @param value to be set
	//*/
	//void set_diag(T value)
	//{
	//	cudaThreadSynchronize(); // Make sure device is ready
	//	const unsigned int block = MAX_THREADS;
	//	const unsigned int grid = (min(m_I, m_J)+(block-1)) / block;
	//	set_diag_kernel<T, size_type, major><<<grid, block>>>(this->m_first, value, this->m_I, this->m_J);
	//}

	///**
	//* Set all diagonal values in the matrix
	//* @param v vector of values to be set
	//*/
	//void set_diag(vector_type const& v)
	//{
	//	assert(v.size() == min(m_J, m_I));

	//	cudaThreadSynchronize(); // Make sure device is ready
	//	const unsigned int block = MAX_THREADS;
	//	const unsigned int grid = (min(m_I, m_J)+(block-1)) / block;
	//	set_diag_vector_kernel<<<grid, block>>>(this->m_first, v.begin(), this->m_I, this->m_J);
	//}

	///**
	//* Set all diagonal values in the matrix
	//* @param m matrix of diagonal values to be set
	//*/
	//void set_diag(my_type const& m)
	//{
	//	assert(m.rows() == m_I && m.cols() == m_J);

	//	cudaThreadSynchronize(); // Make sure device is ready
	//	const unsigned int block = MAX_THREADS;
	//	const unsigned int grid = (min(m_I, m_J)+(block-1)) / block;
	//	set_diag_matrix_kernel<<<grid, block>>>(this->m_first, m.m_first, this->m_I, this->m_J);
	//}

	

	/**
	* Multiplication Ax = b.
	*/
	template <typename M, typename V>
	void mult(M const& A, V const& x, V &b)
	{
		ASSERT(A.rows() == b.size() && A.cols() == x.size());
		cudaThreadSynchronize(); // Make sure device is ready

		dim3 block(MAX_THREADS);
		int gridsize_x = (int)ceil((double)A.rows()/(double)MAX_THREADS);
		gpulab::util::power2 pow2;
		if (gridsize_x > 65535)
		{
			gridsize_x = min(pow2.divisor(gridsize_x), 65535);
		}
		int gridsize_y = (int)ceil((double)A.rows()/((double)MAX_THREADS*gridsize_x));
		ASSERT(gridsize_y < 65535);
		dim3 grid(gridsize_x, gridsize_y);
		
		gpulab::device::matrix::kernel::matrix_vector_mult_kernel<<<grid, block, block.x*sizeof(float)>>>(
			  thrust::raw_pointer_cast(&A[0])
			, thrust::raw_pointer_cast(&x[0])
			, thrust::raw_pointer_cast(&b[0])
			, A.rows(), A.cols()
			, A.major());
		//matrix::kernel::matrix_vector_mult_naive_kernel<<<grid, block>>>( thrust::raw_pointer_cast(&A[0]),  thrust::raw_pointer_cast(&x[0]),  thrust::raw_pointer_cast(&b[0]), A.rows(), A.cols());
	}

	template <typename M>
	void transpose(M& dst, M const& src)
	{
		ASSERT(!"Not implemented");
	}

	///**
	//* Reciprocal. this = val/this.
	//*/
	//void reciprocal(T const& val) const
	//{
	//	cudaThreadSynchronize(); // Make sure device is ready

	//	const unsigned int block = MAX_THREADS;
	//	const unsigned int grid = (m_count+(block-1)) / block;
	//	reciprocal_kernel<<<grid, block>>>(this->m_count, val, this->m_first);
	//}


} // namespace matrix
} // namespace device

	/////**
	////* Device major change
	////*/
	//template <typename T, typename size_type, typename major_to, typename major_from>
	//void change_major(T* to, T const* from, size_type const I, size_type const J, major_to, major_from, gpulab::device_memory)
	//{
	//	cudaThreadSynchronize(); // Make sure device is ready
	//	dim3 block(THREADS_2D, THREADS_2D);
	//	dim3 grid((I+(block.x-1)) / block.x, (J+(block.y-1)) / block.y);
	//	gpulab::device::change_major_kernel<T, size_type, major_to, major_from><<<grid, block>>>(to, from, I, J);
	//}

} // namespace gpulab

#endif // GPULAB_DEVICE_MATRIX_H