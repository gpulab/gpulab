// ==========================================================================
// Compressed Sparse Row Matrix kernels
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================


#ifndef GPULAB_DEVICE_CSR_MATRIX_CU
#define GPULAB_DEVICE_CSR_MATRIX_CU

namespace gpulab
{
	namespace device
	{
		/**
		* One warp per row. 
		* 
		* Inspired (pretty much copied) from "Efficient Sparse Matrix-Vector 
		* multiplication on CUDA" by Nathan Bell and Michael Garland.
		*/
		template <typename size_type, typename T>
		__global__
		void csr_mult_kernel_per_warp(size_type const* ptr, size_type const* indices, T const* data, T const* x, T* b, size_type const I)
		{
			const size_type idx      = blockDim.x * blockIdx.x + threadIdx.x;	// Global thread index
			const size_type warp_idx = idx / WARP_SIZE;							// Global warp index
			const size_type lane     = idx & (WARP_SIZE - 1);					// Thread index in warp

			extern __shared__ T vals[];

			size_type row = warp_idx;
			if(row < I)
			{
				size_type row_start = ptr[row];
				size_type row_end   = ptr[row+1];

				vals[threadIdx.x] = T(0);
				for(size_type j=row_start+lane; j<row_end; j+=WARP_SIZE)
				{
					vals[threadIdx.x] += data[j] * x[indices[j]];
				}

				// Warp reduction
				if(lane < 16) vals[threadIdx.x] += vals[threadIdx.x + 16];
				if(lane <  8) vals[threadIdx.x] += vals[threadIdx.x +  8];
				if(lane <  4) vals[threadIdx.x] += vals[threadIdx.x +  4];
				if(lane <  2) vals[threadIdx.x] += vals[threadIdx.x +  2];
				if(lane <  1) vals[threadIdx.x] += vals[threadIdx.x +  1];

				if(lane == 0)
					b[row] = vals[threadIdx.x];
			}
		}

		/**
		* One row per thread. 
		*/ 
		template <typename size_type, typename T>
		__global__
		void csr_mult_kernel_per_row(size_type const* ptr, size_type const* indices, T const* data, T const* x, T* b, size_type const I)
		{
			const size_type row = blockDim.x * blockIdx.x + threadIdx.x;
			
			if(row < I)
			{
				size_type row_start = ptr[row];
				size_type row_end   = ptr[row+1];

				T val(0);
				for(size_type j=row_start; j<row_end; ++j)
				{
					val += data[j] * x[indices[j]];
				}
				b[row] = val;
			}

		}


	} // namespace device

} // namespace gpulab

#endif // GPULAB_DEVICE_COO_MATRIX_CU
