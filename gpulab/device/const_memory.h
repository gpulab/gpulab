// ==========================================================================
// Shared memory handler
// This is used to avoid conflicts when mixing extern shared memory variables
// with templates.
// Details and reference, see the NVIDIA simpleTemplate example or:
// http://www.naic.edu/~phil/hardware/nvidia/doc/src/simpleTemplates/doc/readme.txt
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// Date:    July 14. 2011
// ==========================================================================

#ifndef GPULAB_DEVICE_CONST_MEMORY_H
#define GPULAB_DEVICE_CONST_MEMORY_H

#include <gpulab/config.h>

namespace gpulab
{
namespace device
{
	#define MAX_STENCIL_SIZE 16 // We expect no more than this many entries are needed... ever!
	
	// TODO: How do we make this non-hardcoded?
	__constant__ float  float_const_stencils[MAX_STENCIL_SIZE*MAX_STENCIL_SIZE];	// Global constant memory, max 64KB in G80, G92 architecture, may be more in G200
	__constant__ double double_const_stencils[MAX_STENCIL_SIZE*MAX_STENCIL_SIZE];	// Global constant memory

	__constant__ double const_stencils[MAX_STENCIL_SIZE*MAX_STENCIL_SIZE];	// Global constant memory

	/**
	* Non-specialized shared memory version
	*/
	template <typename T>
	struct const_memory
	{
		// Ensure that we won't compile any un-specialized types
		__device__ static T* get_pointer() {
			extern __device__ void error(void);
			error();
			return NULL;
		}
	};
		
	template <>
	struct const_memory <float>
	{
		__device__ __host__ static float const* get_pointer() { return (float const*)const_stencils; }
	};
	template <>
	struct const_memory <double>
	{
		__device__ __host__ static double const* get_pointer() { return const_stencils; }
	};

	/**
	* NOTE: Constants needs to be in the same scope as cudaMemcpyToSymbol.. so this might be useless
	*/
	template <typename vector_type>
	void upload_const_memory(typename vector_type::value_type* dst, vector_type const& src)
	{
		CUDA_SAFE_CALL( cudaMemcpyToSymbol(dst, RAW_PTR(src), src.size()*sizeof(typename vector_type::value_type), 0, cudaMemcpyHostToDevice) );
	}

	/**
	* NOTE: Constants needs to be in the same scope as cudaMemcpyToSymbol.. so this might be useless
	*/
	template <typename value_type>
	void upload_const_memory(value_type* dst, value_type src)
	{
		CUDA_SAFE_CALL( cudaMemcpyToSymbol(dst, &src, sizeof(value_type), 0, cudaMemcpyHostToDevice) );
	}

} // namespace device
} // namespace gpulab

#endif // GPULAB_DEVICE_CONST_MEMORY_H