// ==========================================================================
// Compressed Sparse Row Matrix device version
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DEVICE_CSR_MATRIX_H
#define GPULAB_DEVICE_CSR_MATRIX_H

#include <stdio.h>
#include <assert.h>

#include <gpulab/config.h>
#include <gpulab/csr_matrix.h>
#include <gpulab/device/csr_matrix.cu>

namespace gpulab
{

namespace device
{

namespace csr_matrix
{

/*
		void print(size_type stride = 1) const
		{
			stride = max(1, stride); // Prevents stride from being 0
			printf("M_csr_d = \n");
			
			// Copy all to host first, maybe best when stride == 1
			size_type indices_bytes = sizeof(size_type)  *  this->m_nonzeros;
			size_type ptr_bytes     = sizeof(size_type)  * (this->m_I+1);
			size_type data_bytes    = sizeof(value_type) *  this->m_nonzeros;

			size_type*  indices = (size_type*) malloc(indices_bytes);
			size_type*  ptr     = (size_type*) malloc(ptr_bytes);
			value_type* data    = (value_type*)malloc(data_bytes);

			CUDA_SAFE_CALL( cudaMemcpy ((size_type*) indices, (size_type*) this->m_indices.data(), indices_bytes, cudaMemcpyDeviceToHost) );
			CUDA_SAFE_CALL( cudaMemcpy ((size_type*) ptr,     (size_type*) this->m_ptr.data(),     ptr_bytes,     cudaMemcpyDeviceToHost) );
			CUDA_SAFE_CALL( cudaMemcpy ((value_type*)data,    (value_type*)this->m_data.data(),    data_bytes,    cudaMemcpyDeviceToHost) );

			cudaThreadSynchronize();

			//printf("ptr     = [");
			//for(size_type i=0; i<this->m_I+1; i+=stride)
			//{
			//	printf(" %d ", ptr[i]);
			//}
			//printf("]\n");
			//printf("indices = [");
			//for(size_type i=0; i<this->m_nonzeros; i+=stride)
			//{
			//	printf("    %d ", indices[i]);
			//}
			//printf("]\n");
			//printf("data    = [");
			//for(size_type i=0; i<this->m_nonzeros; i+=stride)
			//{
			//	printf("%.3f ", data[i]);
			//}
			//printf("]\n");

			for(size_type i=0; i<m_I; i+=stride)
			{
				size_type row_start = ptr[i];
				size_type row_end   = ptr[i+1];

				printf("    ");
				for(size_type j=0; j<m_J; j+=stride)
				{
					value_type val;
					if(row_start<row_end && j == indices[row_start])
					{
						val = data[row_start];
						++row_start;
					}
					else
					{
						val = 0;
					}
					printf(" %.3f ", val);
				}
				printf("\n");
			}

			free(indices);
			free(ptr);
			free(data);
		}
*/

		/**
		* Calculates Ax = b, where this = A
		*/
		template <typename V>
		void mult(V const &x, V& b) const
		{
			// No need to reset b, since every row is handled

			dim3 block(MAX_THREADS);
			if(this->nonzeros() > (WARP_SIZE * this->m_I))	// Warp based. Fast when nonzeros per row is large (32 or more)
			{
			dim3 grid( ((WARP_SIZE * this->m_I) + (block.x - 1)) / block.x);
			gpulab::device::csr_mult_kernel_per_warp<<<grid, block, sizeof(T) * MAX_THREADS>>>( 
															  this->m_ptr.data()
															, this->m_indices.data()
															, this->m_data.data()
															, x.data()
															, b.data()
															, this->m_I);
			}
			else // One thred per row
			{
			dim3 grid((this->m_I + (block.x - 1)) / block.x);
			gpulab::device::csr_mult_kernel_per_row<<<grid, block>>>( 
															  this->m_ptr.data()
															, this->m_indices.data()
															, this->m_data.data()
															, x.data()
															, b.data()
															, this->m_I);
			}
		}
} // namespace csr_matrix

} // namespace device

} // namespace gpulab

#endif // GPULAB_DEVICE_CSR_MATRIX_H
