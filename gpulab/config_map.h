// ==========================================================================
// Utilities for config mapping
// ==========================================================================
// (C)opyright: 2010
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Author: Stefan L. Glimberg
// Email:   slgl@imm.dtu.dk
//
// Author: Allan P. Engsig-Karup
// Email:   apek@imm.dtu.dk
//
// Date:    September, 2012
// ==========================================================================

#ifndef GPULAB_CONFIG_MAP_H
#define GPULAB_CONFIG_MAP_H

#include <stdio.h>
#include <string>
#include <map>
#include <gpulab/io/read.h>

namespace gpulab
{ 	
	/**
	* A generic version of a map. The default config type in the library
	*/
	class config_map : public std::map<std::string,std::string>
	{
	private:
		typedef std::map<std::string,std::string>	super;

	public:

		/**
		* Set the value of a parameter.
		* If the parameter does not exists, it will be added, otherwise overwritten.
		*/
		template <typename T>
		void set(std::string name, T const& val)
		{
			std::string strval;
			gpulab::io::to_string<T>(strval,val,std::dec);
			super::operator[](name) = strval;
		}


		//template <typename T>
		//T operator[] (const std::string& name) const
		//{
		//	if(this->count(name)>0)
		//	{
		//		T val;
		//		std::string strval = super::operator[](name);
		//		gpulab::io::from_string<T>(val, strval, std::dec);
		//		return val;
		//	}
		//	return T();
		//}

		/**
		* Get the value of the specified param name.
		* @param name property name to look for
		* @param val the value of the name to look for
		* @param def default value if name is not found
		*/
		template <typename T>
		bool get(std::string name, T& val, T def = T())
		{
			if(this->count(name)>0)
			{
				std::string strval = super::operator[](name);
				gpulab::io::from_string<T>(val, strval, std::dec);
				return true;
			}

			val = def;

			//std::stringstream msg;
			//msg << "Did not find param " << name.c_str() << " in config map, using value " << val << "\n";
			//printf(msg.str().c_str());
			return false;
		}

		/**
		* Get the vector of the specified param name.
		* @param name property name to look for
		* @param vec the vector to put values into
		*/
		template <typename V>
		bool get_vector(std::string name, V& vec)
		{
			typedef typename V::value_type value_type;

			if(this->count(name)>0)
			{
				std::string strval = super::operator[](name);

				std::stringstream ss(strval);
				std::string s;
				value_type val;
				while (getline(ss, s, ' '))
				{
					gpulab::io::from_string<value_type>(val, s, std::dec);
					vec.insert(vec.end(),val);
				}
				return true;
			}

			std::stringstream msg;
			msg << "Did not find vector " << name.c_str() << " in config map\n";
			printf(msg.str().c_str());
			return false;
		}
	};

}; // namespace gpulab

#endif // GPULAB_CONFIG_MAP_H
