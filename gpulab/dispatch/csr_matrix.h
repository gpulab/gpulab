// ==========================================================================
// CSR Matrix Function Dispatching
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Nicolai Fog Gade-Nielsen
// Email:   nfga@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DISPATCH_CSR_MATRIX_H
#define GPULAB_DISPATCH_CSR_MATRIX_H

#include <gpulab/type_traits.h>
#include <gpulab/host/csr_matrix.h>
//#include <gpulab/device/csr_matrix.h> TODO

namespace gpulab
{
namespace dispatch
{
namespace csr_matrix
{

	////////////////
	// Host Paths //
	////////////////
	template <typename M, typename V>
	void mult(M const& A
			, V const& x
			, V& b
			, gpulab::host_memory)
	{
		gpulab::host::csr_matrix::mult(A, x, b);
	}

	//////////////////
	// Device Paths //
	//////////////////
	template <typename M, typename V>
	void mult(M const& A
			, V const& x
			, V& b
			, gpulab::device_memory)
	{
		//gpulab::device::csr_matrix::mult(A, x, b); TODO
	}

} // end namespace csr_matrix
} // end namespace dispatch
} // end namespace gpulab

#endif // GPULAB_DISPATCH_CSR_MATRIX_H
