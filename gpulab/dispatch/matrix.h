// ==========================================================================
// Matrix Function Dispatching
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DISPATCH_MATRIX_H
#define GPULAB_DISPATCH_MATRIX_H

#include <gpulab/type_traits.h>
#include <gpulab/host/matrix.h>
#include <gpulab/device/matrix.h>

namespace gpulab
{
namespace dispatch
{
namespace matrix
{

	////////////////
	// Host Paths //
	////////////////
	template <typename M, typename V>
	void mult(M const& A
			, V const& x
			, V& b
			, gpulab::host_memory)
	{
		gpulab::host::matrix::mult(A, x, b);
	}

	template <typename M>
	void transpose(M& dst
				 , M const& src
				 , gpulab::host_memory)
	{
		gpulab::host::matrix::transpose(dst, src);
	}

	//////////////////
	// Device Paths //
	//////////////////
	template <typename M, typename V>
	void mult(M const& A
			, V const& x
			, V& b
			, gpulab::device_memory)
	{
		gpulab::device::matrix::mult(A, x, b);
	}

	template <typename M>
	void transpose(M& dst
				 , M const& src
				 , gpulab::device_memory)
	{
		gpulab::device::matrix::transpose(dst, src);
	}

} // end namespace matrix
} // end namespace dispatch
} // end namespace gpulab

#endif // GPULAB_DISPATCH_MATRIX_H
