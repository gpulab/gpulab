// ==========================================================================
// COO Matrix Function Dispatching
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Nicolai Fog Gade-Nielsen
// Email:   nfga@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DISPATCH_COO_MATRIX_H
#define GPULAB_DISPATCH_COO_MATRIX_H

#include <gpulab/type_traits.h>
#include <gpulab/host/coo_matrix.h>
#include <gpulab/device/coo_matrix.h>

namespace gpulab
{
namespace dispatch
{
namespace coo_matrix
{

	////////////////
	// Host Paths //
	////////////////
	template <typename M, typename V>
	void mult(M const& A
			, V const& x
			, V& b
			, gpulab::host_memory)
	{
		gpulab::host::coo_matrix::mult(A, x, b);
	}

	template <typename M, typename V>
	void multT(M const& A
			, V const& x
			, V& b
			, gpulab::host_memory)
	{
		gpulab::host::coo_matrix::multT(A, x, b);
	}

	//////////////////
	// Device Paths //
	//////////////////
	template <typename M, typename V>
	void mult(M const& A
			, V const& x
			, V& b
			, gpulab::device_memory)
	{
		gpulab::device::coo_matrix::mult(A, x, b);
	}

	template <typename M, typename V>
	void multT(M const& A
			, V const& x
			, V& b
			, gpulab::device_memory)
	{
		gpulab::device::coo_matrix::multT(A, x, b);
	}
} // end namespace coo_matrix
} // end namespace dispatch
} // end namespace gpulab

#endif // GPULAB_DISPATCH_COO_MATRIX_H
