// ==========================================================================
// Vector Functions
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_DISPATCH_VECTOR_H
#define GPULAB_DISPATCH_VECTOR_H

#include <gpulab/type_traits.h>
#include <gpulab/host/vector.h>
#include <gpulab/device/vector.h>

namespace gpulab
{
namespace dispatch
{
namespace vector
{

	////////////////
	// Host Paths //
	////////////////
	template <typename V>
	typename V::value_type dot(  const V& v1
							   , const V& v2
							   , gpulab::host_memory)
	{
		return gpulab::host::vector::dot(v1, v2);
	}

	template <typename V>
	typename V::value_type nrm1( const V& v1
							   , gpulab::host_memory)
	{
		return gpulab::host::vector::nrm1(v1);
	}

	template <typename V>
	void axpy( typename V::value_type a1
			 , V const& x1
			 , typename V::value_type a2
			 , V const& x2
			 , typename V::value_type a3
			 , V const& x3
			 , V& y
			 , gpulab::host_memory)
	{
		gpulab::host::vector::axpy(a1, x1, a2, x2, a3, x3, y);
	}

	//////////////////
	// Device Paths //
	//////////////////
	template <typename V>
	typename V::value_type dot(  const V& v1
							   , const V& v2
							   , gpulab::device_memory)
	{
		return gpulab::device::vector::dot(v1, v2);
	}

	template <typename V>
	typename V::value_type nrm1( const V& v1
							   , gpulab::device_memory)
	{
		return gpulab::device::vector::nrm1(v1);
	}

	template <typename V>
	void axpy( typename V::value_type a1
			 , V const& x1
			 , typename V::value_type a2
			 , V const& x2
			 , typename V::value_type a3
			 , V const& x3
			 , V& y
			 , gpulab::device_memory)
	{
		gpulab::device::vector::axpy(a1, x1, a2, x2, a3, x3, y);
	}

} // end namespace vector
} // end namespace dispatch
} // end namespace gpulab

#endif // GPULAB_DISPATCH_VECTOR_H