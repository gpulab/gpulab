// ==========================================================================
// Solves a triangular system
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_MATH_TRIANGULAR_SOLVE_H
#define GPULAB_MATH_TRIANGULAR_SOLVE_H

#include <assert.h>

namespace gpulab
{

namespace math
{

	
	/**
	* Upper triangular solver (Backward substitution).
	* Solves for x, where Ax = b, assuming that A is in upper triangular form.
	* @param A matrix
	* @param x vector
	* @param b vector
	* @param n size of the triangle
	*/
	template <typename matrix_type, typename vector_type>
	void backward_substitution(matrix_type const& A, vector_type& x, vector_type const& b, int n)
	{
		typedef typename vector_type::value_type value_type;

		for(int i=n-1; i>=0; --i)
		{
			x[i] = b[i];
			for (int j=i+1; j < n; ++j)
			{
				x[i] -= A(i,j) * x[j];
			}
			x[i] /= A(i,i);
		}
	}


	/**
	* Upper triangular solver (Backward substitution).
	* Solves for x, where Ax = b, assuming that A is in upper triangular form.
	* TODO: does it work when rows != cols ?
	*/
	template <typename matrix_type, typename vector_type>
	void backward_substitution(matrix_type const& A, vector_type& x, vector_type const& b)
	{
		backward_substitution(A, x, b, min(A.cols(), A.rows()));
	}	


} // namespace math

} // namespace gpulab

#endif // GPULAB_MATH_TRIANGULAR_SOLVE_H