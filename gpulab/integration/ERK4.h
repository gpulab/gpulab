// ==========================================================================
// Explicit 4th order Runge Kutta time integration
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#include <gpulab/type_traits.h>

#ifndef GPULAB_INTEGRATION_ERK4_H
#define GPULAB_INTEGRATION_ERK4_H

namespace gpulab
{
namespace integration
{

	/**
	* Explicit Runge-Kutta 4. order accurate time integrator
	*/ 
	struct ERK4
	{
		/**
		* Time integration routine
		* @param fun functor or function handle that calculates the right hand side of the ode
		* @param x the state at time t_start
		* @param t_start start time
		* @param dt time step size
		* @param N numbers of time steps to take
		*/
		template <typename F, typename T, typename V>
		void operator()(F fun, V & x, T t_start, T tend, T dt)
		{
			// Solver Parameters
			//unsigned int N  = (unsigned int)ceil((t_end-t_start)/h);	//< Number of steps
			T t             = t_start;									//< Time
			T halfdt        = gpulab::values<T>::half()*dt;				//< Half step size
			T one			= gpulab::values<T>::one();					//< One in right precision
			T two           = gpulab::values<T>::two();					//< Two in right precision
			T dtsixth		= dt /(T)6;

			V* Y  = x.duplicate();
			V* F1 = x.duplicate();
			V* F2 = x.duplicate();
			V* F3 = x.duplicate();
			V* F4 = x.duplicate();

			while(t < tend)
			{
				// Adjust dt for last time step
				if(tend-t < dt)
				{
					dt      = tend-t;
					halfdt  = gpulab::values<T>::half()*dt;
					dtsixth	= dt /(T)6;
				}

				// TODO: can we maybe avoid some of the data copying here?

				// Stage 1
				Y->copy(x);
				//F1->copy(x);
				(*fun)(t, *Y, *F1, dt);
				
				// Stage 2
				Y->copy(x);
				Y->axpy(halfdt, *F1);
				//F2->copy(x);
				(*fun)(t+halfdt, *Y, *F2, dt);
				
				// Stage 3
				Y->copy(x);
				Y->axpy(halfdt, *F2);
				//F3->copy(x);
				(*fun)(t+halfdt, *Y, *F3, dt);

				// Stage 4
				Y->copy(x);
				Y->axpy(dt, *F3);
				//F4->copy(x);
				(*fun)(t+dt, *Y, *F4, dt);

				// Update stage
				//F1.axpy(two, F2);		// TODO: These axpys can maybe be collected into one kernel for better performance
				//F1.axpy(two, F3);		// TODO: Memory can be reduced if F1 is updated each step?
				//F1.axpy(one, F4);
				F1->axpy(two,*F2,two,*F3,one,*F4); 
				x.axpy(dtsixth,*F1);

				// Next step
				t += dt;
			}
			delete Y;
			delete F1;
			delete F2;
			delete F3;
			delete F4;
		}
	};

}; // namespace integration
}; // namespace gpulab

#endif // GPULAB_INTEGRATION_ERK4_H