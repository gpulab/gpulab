// ==========================================================================
// Time integration solver, should be a super class of a specialized solver
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// Date:    August 25th, 2011
// ==========================================================================

#ifndef GPULAB_INTEGRATION_ODE_SOLVER_H
#define GPULAB_INTEGRATION_ODE_SOLVER_H

#include <stdarg.h>

namespace gpulab
{ 
namespace integration
{
	/**
	* A containter for multiple grids, useful when the ode contains several differential equations
	*/
	template <typename grid_type>
	struct ode_container
	{
		typedef typename grid_type::value_type			value_type;
		typedef typename grid_type::size_type			size_type;
		typedef ode_container<grid_type>				my_type;

		grid_type** elems;
		size_type	N;

		ode_container(my_type const& m)
			: N(m.N)
		{
			elems = (grid_type**)calloc(N,sizeof(grid_type*));
			for(unsigned int i = 0; i < N; i++) {
				elems[i] = new grid_type(*m.elems[i]);
			}
		}

		template <typename other_type>
		ode_container(other_type const& m)
			: N(m.N)
		{
			elems = (grid_type**)calloc(N,sizeof(int));
			for(unsigned int i = 0; i < N; i++) {
				elems[i] = new grid_type(*m.elems[i]);
			}
		}

		ode_container(int n, ...)
			: N(n)
		{
			elems = (grid_type**)calloc(n,sizeof(grid_type*));
			va_list ap;
			va_start(ap, n);
			
			for(int i = 0; i < n; i++) {
				grid_type* elem = new grid_type(*((grid_type*)va_arg(ap, grid_type*)));
				elems[i] = elem;
			}
			va_end(ap);
		}

		~ode_container()
		{
			for(unsigned int i = 0; i < N; i++)
				delete elems[i];
			free(elems);
		}

		my_type* duplicate() const
		{
			my_type* dup = new my_type(0);
			dup->N = this->N;
			dup->elems = (grid_type**)calloc(this->N,sizeof(grid_type*));
			for(unsigned int i = 0; i < this->N; i++) {
				dup->elems[i] = this->elems[i]->duplicate();
			}
			return dup;
		}

		void axpy(value_type a, my_type const& x)
		{
			for(unsigned int i=0; i<N; ++i)
				elems[i]->axpy(a,*x.elems[i]);
		}

		void axpy(value_type a1, my_type const& x1, value_type a2, my_type const& x2, value_type a3, my_type const& x3)
		{
			for(unsigned int i=0; i<N; ++i)
				elems[i]->axpy(a1,*x1.elems[i],a2,*x2.elems[i],a3,*x3.elems[i]);
		}

		void copy(my_type const& x)
		{
			for(unsigned int i=0; i<N; ++i)
				elems[i]->copy(*x.elems[i]);
		}

		/**
		* Copy from another type that is not a state, maybe just a vector
		*/
		template <typename other_type>
		void copy(other_type const& x)
		{
			typedef typename other_type::const_iterator iterator;
			iterator begin = x.begin();
			iterator end;

			for(unsigned int i=0; i<N; ++i)
			{
				end = begin + elems[i]->size();
				elems[i]->copy(begin,end);
				begin = end;
			}
		}

		grid_type& operator[](size_type i)
		{
			ASSERT(i<N);
			return *elems[i];
		}

		size_type size() const
		{
			size_type s = 0;
			for(unsigned int i=0; i<N; ++i)
				s += elems[i]->size();
			return s;
		}
	};


	template <typename Types>
	class ode_solver
	{
	public:
		typedef typename Types::grid_type					grid_type;
		typedef typename grid_type::value_type				value_type;
		typedef typename grid_type::size_type				size_type;
		typedef typename Types::time_integrator_type		time_integrator_type;

		typedef ode_container<grid_type>					container_type;

	protected:
		value_type				m_time;
		value_type				m_dt;
		time_integrator_type	m_integrator;	///< Time integrator
		container_type*			m_state;		///< State variable
		unsigned int			m_step;

		/**
		* ODE solver constructor.
		*/ 
		ode_solver()
			: m_time(gpulab::values<value_type>::zero())
			, m_step(0)
			, m_state(0)
		{ }

		ode_solver(container_type& cont)
			: m_time(gpulab::values<value_type>::zero())
			, m_step(0)
			, m_state(0)
		{
			set_state(cont);
		}

		~ode_solver()
		{
			if(m_state)
				delete m_state;
		}

	public:

		void take_steps(size_type steps, value_type dt)
		{
			for(size_type step=0; step<steps; ++step)
			{
				take_step(dt);
			}
		}

		void take_step(value_type dt)
		{
			m_dt = dt;
			m_time = m_integrator(this, *m_state, m_time, m_dt, 1);
			++m_step;
		}

		container_type&	state() { return *m_state; }

		void set_state(container_type& cont)
		{
			// Clean up
			if(m_state)
				delete m_state;

			m_state = &cont;
		}

		/**
		* Calculate the rhs of the differential eqs.
		*/
		void operator()(value_type t, container_type& state, container_type& rhs){}
		
		value_type				time() const	{ return this->m_time;	}
		value_type				dt()   const	{ return this->m_dt;	}
		time_integrator_type&	integrator()	{ return m_integrator;	}

	};


}; // namespace integration
}; // namespace gpulab

#endif // GPULAB_INTEGRATION_ODE_SOLVER_H
