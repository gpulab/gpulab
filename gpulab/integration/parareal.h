// ==========================================================================
// Parareal, parallel time integration
// ==========================================================================
// (C)opyright: 2010
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Author: Stefan Lemvig Glimberg
// Email:  slgl@imm.dtu.dk
//
// Author: Allan S. Nielsen
// Email:  
//
// Author: Allan P. Engsig-Karup
// Email:  apek@imm.dtu.dk
//
// Date:   October, 2012
// ==========================================================================

#include <gpulab/config.h>
#include <gpulab/io/log.h>
#include <gpulab/io/print.h>

#ifndef GPULAB_INTEGRATION_PARAREAL_H
#define GPULAB_INTEGRATION_PARAREAL_H

namespace gpulab
{
namespace integration
{

	/**
	* Parareal time integrator
	* @param G coarse integrator
	* @param F fine integrator
	*/ 
	template <typename G, typename F>
	class parareal
	{
	protected:
		
		MPI_Comm					m_comm;
		G							m_coarse_integrator;
		F							m_fine_integrator;
		int							m_mpi_rank;
		int							m_mpi_size;
		int							m_R;			///< Relation between fine and coarse op.
		int							m_K;			///< Predictor/corrector iterations

	public:
		/**
		* Default constructor.
		* Setup MPI stuff needed for parareal
		*/
		parareal()
			: m_R(4)
			, m_K(1)
		{
			gpulab::init();
			m_comm = gpulab::GPULAB_COMM_WORLD;
			MPI_Comm_rank(m_comm, &m_mpi_rank);
			MPI_Comm_size(m_comm, &m_mpi_size);
			GPULAB_LOG_DBG("Initializing Parareal %i/%i (GPULAB_COMM_WORLD)\n",m_mpi_rank,m_mpi_size);
		}

		/**
		* Constructor taking a specific communicator to work with
		*/
		parareal(MPI_Comm const& comm)
			: m_comm(comm)
			, m_R(4)
			, m_K(1)
		{
			gpulab::init();
			MPI_Comm_rank(m_comm, &m_mpi_rank);
			MPI_Comm_size(m_comm, &m_mpi_size);
			GPULAB_LOG_DBG("Initializing Parareal %i/%i (comm)\n",m_mpi_rank,m_mpi_size);
		}

		int& R() { return m_R; }
		int& K() { return m_K; }

		/**
		* Time integration routine
		* @param fun functor or function handle that calculates the right hand side of the ode
		* @param x the state at time t_start
		* @param t_start start time
		* @param dt time step size (for fine operator)
		* @param tend end time
		*/
		template <typename RHS, typename T, typename V>
		void operator()(RHS fun, V & x, T t0, T tend, T dt)
		{
			// Make sure there are multiple nodes
			if(m_mpi_size == 1)
			{
				GPULAB_LOG_INF("Parareal: Only 1 node available, using fine integrator\n");
				return m_fine_integrator(fun,x,t0,tend,dt);
			}

			// Solver Parameters
			int K		 = min(m_mpi_size,m_K);					
			int R		 = m_R;
			T ti		 = (tend-t0)/(T)m_mpi_size;				///< Time for each node i
			T ti0		 = t0+ti*(T)m_mpi_rank;					///< Start time for node i
			T ti1		 = ti0+ti;								///< End time for node i
			T dT		 = dt*(T)R;								///< Coarse time step
			int conv	 = 0;
			int conv_next= 0;
			V xc(x);											///< Corase solution
			MPI_Status status;

			// Print debug info
			GPULAB_LOG_DBG("Initiate PA using fully distributed task scheduling. \n");
			GPULAB_LOG_DBG("Using dt=%e, dT=%e, time interval [%f, %f]\n", dt, dT, ti0, ti1);

			/************************************************************/
			/* The actual parareal algorithm is initiated  */
			/************************************************************/

			// Iteration 0
			if(m_mpi_rank == 0)
			{
				GPULAB_LOG_DBG("PA: mpi_rank zero begin\n");
				// Do coarse integration from state0
				m_coarse_integrator(fun,xc,ti0,ti1,dT);
				// Send result to proc. 1
				GPULAB_LOG_DBG("PA: Send coarse %i --> %i\n",m_mpi_rank, 1);
				gpulab::send_state(xc, 1, m_comm);
				// Propagate using fine solver
				m_fine_integrator(fun,x,ti0,ti1,dt);
				// Set conv
				conv = 1;
				// Send conv to proc. mpi_rank id 1
				MPI_CHECK(MPI_Send(&conv,1,MPI_INT,1,0,m_comm));
				// Send fine solution to proc. mpi_rank id 1
				GPULAB_LOG_DBG("PA: Send fine   %i --> %i\n",m_mpi_rank, 1);
				gpulab::send_state(x, 1, m_comm);
				// mpi_rank id 0 finished
				GPULAB_LOG_DBG("PA: mpi_rank zero is done \n");
				// Broadcast final state from m_mpi_size-1
				gpulab::broadcast_state(x,m_mpi_size-1,m_comm);
				return;
			}

			V** xp = (V**) malloc(m_mpi_size*sizeof(V*));
			if(m_mpi_rank > 0)
			{
				// Receive solution from proc. mpi_rank id - 1
				xp[0] = x.duplicate();
				gpulab::recv_state(*xp[0],m_mpi_rank-1, m_comm);
				GPULAB_LOG_DBG("PA: Recv coarse %i <-- %i\n",m_mpi_rank, m_mpi_rank-1);
				xc.copy(*xp[0]);

				// Do coarse propagation
				m_coarse_integrator(fun,xc,ti0,ti1,dT);
				// Send result to proc. mpi_rank id + 1
				if(m_mpi_rank != m_mpi_size-1)
				{
					GPULAB_LOG_DBG("PA: Send coarse %i --> %i\n",m_mpi_rank, m_mpi_rank+1);
					gpulab::send_state(xc, m_mpi_rank + 1, m_comm);
				}
			}

			GPULAB_LOG_DBG("PA: Intial coarse phase done\n");

			// Next K steps
			for(int k=1;k<=K;k++)
			{
				GPULAB_LOG_DBG("Iteration %i, conv_next=%i, conv=%i\n",k,conv_next,conv);
				// Fine propagation
				x.copy(*xp[k-1]);
				m_fine_integrator(fun,x,ti0,ti1,dt);

				// Check for convergence
				if(conv_next)
				{
					if(m_mpi_rank != m_mpi_size-1)
					{
						conv = 1;
						MPI_CHECK(MPI_Send(&conv,1,MPI_INT,m_mpi_rank+1,0,m_comm));
						GPULAB_LOG_DBG("PA: Send fine   %i --> %i\n",m_mpi_rank, m_mpi_rank+1);
						gpulab::send_state(x, m_mpi_rank + 1, m_comm);
					}

					// Broadcast final state from m_mpi_size-1
					gpulab::broadcast_state(x,m_mpi_size-1,m_comm);

					// Clean up
					for(int s=0;s<k;++s) delete xp[s];
					free(xp);
					GPULAB_LOG_DBG("Done! Conv_next\n");
					return;
				}

				// Receive convergence and data from mpi_rank - 1
				MPI_CHECK(MPI_Recv(&conv,1,MPI_INT,m_mpi_rank-1,0,m_comm,&status));
				xp[k] = x.duplicate();
				gpulab::recv_state(*xp[k],m_mpi_rank-1, m_comm);
				GPULAB_LOG_DBG("PA: Recv fine   %i <-- %i\n",m_mpi_rank, m_mpi_rank-1);

				// First part of predictor/corrector
				x.axpy(-1,xc);

				// Propagate using coarse integrator
				xc.copy(*xp[k]);
				m_coarse_integrator(fun,xc,ti0,ti1,dT);

				// Final part of predictor/corrector
				x.axpy( 1,xc);

				// Check for convergence
				if(conv /* TODO: && (errPred>1.5*errFin[mpi_rank+1])*/ )
				{
					conv = 0;
					conv_next = 1;
				}
				if( m_mpi_rank != m_mpi_size-1 )
				{
					MPI_CHECK(MPI_Send(&conv,1,MPI_INT,m_mpi_rank+1,0,m_comm));
					GPULAB_LOG_DBG("PA: Send fine   %i --> %i\n",m_mpi_rank, m_mpi_rank+1);
					gpulab::send_state(x, m_mpi_rank + 1, m_comm);
				}
				if( conv )
				{
					// Broadcast final state from m_mpi_size-1
					gpulab::broadcast_state(x,m_mpi_size-1,m_comm);
					
					// Clean up
					for(int s=0;s<=k;++s) delete xp[s];
					free(xp);
					GPULAB_LOG_DBG("Done! Converged\n");
					return;
				}
			}
			
			// Broadcast final state from m_mpi_size-1
			gpulab::broadcast_state(x,m_mpi_size-1,m_comm);

			// Clean up
			for(int s=0;s<=K;++s) delete xp[s];
			free(xp);
			GPULAB_LOG_DBG("Done! k==K\n");
		}
	};

}; // namespace integration
}; // namespace gpulab

#endif // GPULAB_INTEGRATION_PARAREAL_H
