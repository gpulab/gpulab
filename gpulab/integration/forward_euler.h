// ==========================================================================
// Explicit forward Euler time integration
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// Date:    May, 2012
// ==========================================================================

#include <gpulab/type_traits.h>

#ifndef GPULAB_INTEGRATION_FORWARD_EULER_H
#define GPULAB_INTEGRATION_FORWARD_EULER_H

namespace gpulab
{
namespace integration
{

	/**
	* Explicit 1. order forward Euler time integrator
	*/ 
	struct forward_euler
	{
		/**
		* Time integration routine
		* @param fun functor or function handle that calculates the right hand side of the ode
		* @param x the state at time t_start
		* @param t_start start time
		* @param dt time step size
		* @param tend final time
		* @param N numbers of time steps to take
		*/
		template <typename F, typename T, typename V>
		//T operator()(F fun, V & x, T t_start, T dt, unsigned int N)
		void operator()(F fun, V & x, T t_start, T tend, T dt)
		{
			// Solver Parameters
			T t = t_start;												///< Time
			V rhs(x);													///< Rhs vector

			//for(unsigned int n=0; n<N; ++n)
			while(t < tend)
			{
				// Adjust dt for last time step
				if(tend-t < dt)
				{
					dt = tend-t;
				}

				(*fun)(t, x, rhs, dt);
				
				// Update stage
				x.axpy(dt,rhs);

				// Next step
				t += dt;
			}
		}
	};

}; // namespace integration
}; // namespace gpulab

#endif // GPULAB_INTEGRATION_FORWARD_EULER_H