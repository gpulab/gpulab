// ==========================================================================
// Transformation methods
// ==========================================================================
// (C)opyright: 2010
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================
// See the thrust examples for some great examples
// ==========================================================================

#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

//#include <gpulab/io/log.h>
#include <thrust/tuple.h>
#include <thrust/functional.h>

namespace gpulab
{

	template <typename T>
	struct axpy_functor : public thrust::binary_function<T,T,T>
	{
		const T a;
		axpy_functor(T _a) : a(_a) {}

		__host__ __device__
		T operator()(const T& x, const T& y) const
		{ 
			return a * x + y;
		}
	};

	template <typename T>
	struct axpby_functor : public thrust::binary_function<T,T,T>
	{
		const T a;
		const T b;
		axpby_functor(T _a, T _b) : a(_a), b(_b) {}

		__host__ __device__
		T operator()(const T& x, const T& y) const
		{ 
			return a * x + b * y;
		}
	};

	template <typename T>
	struct scal_functor : public thrust::unary_function<T,T>
	{
		const T a;
		scal_functor(T _a) : a(_a) {}

		__host__ __device__
		T operator()(const T& x) const
		{ 
			return a * x;
		}
	};

	template <typename T>
	struct plus_scalar_functor: public thrust::unary_function<T,T>
	{
		const T a;
		plus_scalar_functor(T _a) : a(_a) {}

		__host__ __device__
		T operator()(const T& x) const
		{ 
			return a + x;
		}
	};

	/**
	* Define the transformation f(x) -> x^2
	*/
	struct square
	{
		template <typename T>
		__host__ __device__
		T operator()(T x)
		{
			return x*x;
		}
	};

	struct mult_tuple
	{
		template <typename T>
		__host__ __device__
		T operator()(thrust::tuple<T,T> v)
		{
			return thrust::get<0>(v) * thrust::get<1>(v);
		}
	};

	template<typename T>
	struct is_equal_tuple
	{
		__host__ __device__
		bool operator()(thrust::tuple<T,T> v)
		{
			return (thrust::get<0>(v) == thrust::get<1>(v));
		}
	};

	template<typename T>
	struct is_equal : public thrust::binary_function<T,T,bool>
	{
		/*! Function call operator. The return value is <tt>lhs && rhs</tt>.
		*/
		__host__ __device__
		bool operator()(const T &lhs, const T &rhs) const 
		{
			return lhs && rhs;
		}
	};

	template<typename T>
	struct abs_maximum : public thrust::binary_function<T,T,T>
	{
		/*! Function call operator. The return value is <tt>max(abs(lhs), abs(rhs))</tt>.
		*/
		__host__ __device__
		T operator()(const T &lhs, const T &rhs) const 
		{
			return (abs(lhs) > abs(rhs)) ? abs(lhs) : abs(rhs);
		}
	};
} // namespace gpulab


#endif // TRANSFORMATION_H
