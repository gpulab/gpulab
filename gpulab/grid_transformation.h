// ==========================================================================
// Grid transformation functions
// ==========================================================================
// (C)opyright: 2010
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_GRID_TRANSFORMATION_H
#define GPULAB_GRID_TRANSFORMATION_H

#include <gpulab/FD/stencil.h>

namespace gpulab
{

namespace transformation
{
namespace kernel
{
	template <typename value_type>
	__device__ __host__
	inline
	value_type jacobian(  value_type x_xi1
						, value_type x_xi2
						, value_type y_xi1
						, value_type y_xi2)
	{
		return x_xi1*y_xi2 - x_xi2*y_xi1;
	}

	template <typename value_type>
	__device__ __host__
	inline
	value_type u_x(   value_type u_xi1
					, value_type u_xi2
					, value_type x_xi1
					, value_type x_xi2
					, value_type y_xi1
					, value_type y_xi2)
	{
		return (y_xi2*u_xi1 - y_xi1*u_xi2) / jacobian(x_xi1,x_xi2,y_xi1,y_xi2);
	}

	template <typename value_type>
	__device__ __host__
	inline
	value_type u_y(   value_type u_xi1
					, value_type u_xi2
					, value_type x_xi1
					, value_type x_xi2
					, value_type y_xi1
					, value_type y_xi2)
	{
		return (x_xi1*u_xi2 - x_xi2*u_xi1) / jacobian(x_xi1,x_xi2,y_xi1,y_xi2);
	}

	template <typename value_type>
	__device__ __host__
	inline
	value_type u_xx(  value_type u_xi1
					, value_type u_xi2
					, value_type u_xi1xi1
					, value_type u_xi2xi2
					, value_type u_xi1xi2
					, value_type x_xi1
					, value_type x_xi2
					, value_type x_xi1xi1
					, value_type x_xi2xi2
					, value_type x_xi1xi2
					, value_type y_xi1
					, value_type y_xi2
					, value_type y_xi1xi1
					, value_type y_xi2xi2
					, value_type y_xi1xi2)
	{
		value_type J = jacobian(x_xi1,x_xi2,y_xi1,y_xi2);
		return   (y_xi2*y_xi2*u_xi1xi1 - 2*y_xi1*y_xi2*u_xi1xi2 + y_xi1*y_xi1*u_xi2xi2)/(J*J)
		      + ((y_xi2*y_xi2*y_xi1xi1 - 2*y_xi1*y_xi2*y_xi1xi2 + y_xi1*y_xi1*y_xi2xi2)*(x_xi2*u_xi1 - x_xi1*u_xi2)
			  +  (y_xi2*y_xi2*x_xi1xi1 - 2*y_xi1*y_xi2*x_xi1xi2 + y_xi1*y_xi1*x_xi2xi2)*(y_xi1*u_xi2 - y_xi2*u_xi1))/(J*J*J);
	}

	template <typename value_type>
	__device__ __host__
	inline
	value_type u_yy(  value_type u_xi1
					, value_type u_xi2
					, value_type u_xi1xi1
					, value_type u_xi2xi2
					, value_type u_xi1xi2
					, value_type x_xi1
					, value_type x_xi2
					, value_type x_xi1xi1
					, value_type x_xi2xi2
					, value_type x_xi1xi2
					, value_type y_xi1
					, value_type y_xi2
					, value_type y_xi1xi1
					, value_type y_xi2xi2
					, value_type y_xi1xi2)
	{
		value_type J = jacobian(x_xi1,x_xi2,y_xi1,y_xi2);
		return   (x_xi2*x_xi2*u_xi1xi1 - 2*x_xi1*x_xi2*u_xi1xi2 + x_xi1*x_xi1*u_xi2xi2)/(J*J) 
			  + ((x_xi2*x_xi2*y_xi1xi1 - 2*x_xi1*x_xi2*y_xi1xi2 + x_xi1*x_xi1*y_xi2xi2)*(x_xi2*u_xi1 - x_xi1*u_xi2) 
			  +  (x_xi2*x_xi2*x_xi1xi1 - 2*x_xi1*x_xi2*x_xi1xi2 + x_xi1*x_xi1*x_xi2xi2)*(y_xi1*u_xi2 - y_xi2*u_xi1))/(J*J*J);
	}


	template <typename size_type, typename value_type>
	__device__
	inline
	void FD2D_x(  value_type* u_x
				, value_type const* u
				, value_type const* X
				, value_type const* Y
				, value_type dxi1
				, value_type dxi2
				, size_type i
				, size_type j
				, size_type Nx
				, size_type Ny
				, size_type alpha
				, value_type const* stencil)
	{
		// First order derivatives
		value_type u_xi1 = gpulab::FD::kernel::FD2D_x(u,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil);
		value_type u_xi2 = gpulab::FD::kernel::FD2D_y(u,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil);
		
		value_type x_xi1 = gpulab::FD::kernel::FD2D_x(X,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil,alpha);
		value_type x_xi2 = gpulab::FD::kernel::FD2D_y(X,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil,alpha);
		
		value_type y_xi1 = gpulab::FD::kernel::FD2D_x(Y,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil,alpha);
		value_type y_xi2 = gpulab::FD::kernel::FD2D_y(Y,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil,alpha);

		*u_x = gpulab::transformation::kernel::u_x(u_xi1,u_xi2,x_xi1,x_xi2,y_xi1,y_xi2);
	}

	template <typename size_type, typename value_type>
	__device__
	inline
	void FD2D_y(  value_type* u_y
				, value_type const* u
				, value_type const* X
				, value_type const* Y
				, value_type dxi1
				, value_type dxi2
				, size_type i
				, size_type j
				, size_type Nx
				, size_type Ny
				, size_type alpha
				, value_type const* stencil)
	{
		// First order derivatives
		value_type u_xi1 = gpulab::FD::kernel::FD2D_x(u,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil);
		value_type u_xi2 = gpulab::FD::kernel::FD2D_y(u,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil);
		
		value_type x_xi1 = gpulab::FD::kernel::FD2D_x(X,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil,alpha);
		value_type x_xi2 = gpulab::FD::kernel::FD2D_y(X,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil,alpha);
		
		value_type y_xi1 = gpulab::FD::kernel::FD2D_x(Y,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil,alpha);
		value_type y_xi2 = gpulab::FD::kernel::FD2D_y(Y,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil,alpha);

		*u_y = gpulab::transformation::kernel::u_y(u_xi1,u_xi2,x_xi1,x_xi2,y_xi1,y_xi2);
	}

	template <typename size_type, typename value_type>
	__device__
	inline
	void FD2D_xxyy( value_type* u_xx
				, value_type* u_yy
				, value_type const* u
				, value_type const* X
				, value_type const* Y
				, value_type dxi1
				, value_type dxi2
				, size_type i
				, size_type j
				, size_type Nx
				, size_type Ny
				, size_type alpha
				, value_type const* stencil1
				, value_type const* stencil2)
	{
		// First, second, and mixed u-derivatives
		value_type u_xi1    = gpulab::FD::kernel::FD2D_x(u,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil1);
		value_type u_xi2    = gpulab::FD::kernel::FD2D_y(u,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil1);
		value_type u_xi1xi1 = gpulab::FD::kernel::FD2D_x(u,1.f/(dxi1*dxi1),i,j,Nx,Ny,alpha,stencil2);
		value_type u_xi2xi2 = gpulab::FD::kernel::FD2D_y(u,1.f/(dxi2*dxi2),i,j,Nx,Ny,alpha,stencil2);
		value_type u_xi1xi2 = gpulab::FD::kernel::FD2D_xy(u,1.f/dxi1,1.f/dxi2,i,j,Nx,Ny,alpha,stencil1);
		
		// First, second, and mixed X-derivatives
		value_type x_xi1    = gpulab::FD::kernel::FD2D_x(X,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil1,alpha);
		value_type x_xi2    = gpulab::FD::kernel::FD2D_y(X,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil1,alpha);
		value_type x_xi1xi1 = gpulab::FD::kernel::FD2D_x(X,1.f/(dxi1*dxi1),i,j,Nx,Ny,alpha,stencil2,alpha);
		value_type x_xi2xi2 = gpulab::FD::kernel::FD2D_y(X,1.f/(dxi2*dxi2),i,j,Nx,Ny,alpha,stencil2,alpha);
		value_type x_xi1xi2 = gpulab::FD::kernel::FD2D_xy(X,1.f/dxi1,1.f/dxi2,i,j,Nx,Ny,alpha,stencil1,alpha,alpha);

		// First, second, and mixed Y-derivatives
		value_type y_xi1    = gpulab::FD::kernel::FD2D_x(Y,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil1,alpha);
		value_type y_xi2    = gpulab::FD::kernel::FD2D_y(Y,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil1,alpha);
		value_type y_xi2xi2 = gpulab::FD::kernel::FD2D_y(Y,1.f/(dxi2*dxi2),i,j,Nx,Ny,alpha,stencil2,alpha);
		value_type y_xi1xi1 = gpulab::FD::kernel::FD2D_x(Y,1.f/(dxi1*dxi1),i,j,Nx,Ny,alpha,stencil2,alpha);
		value_type y_xi1xi2 = gpulab::FD::kernel::FD2D_xy(Y,1.f/dxi1,1.f/dxi2,i,j,Nx,Ny,alpha,stencil1,alpha,alpha);

		*u_xx = gpulab::transformation::kernel::u_xx(u_xi1, u_xi2, u_xi1xi1, u_xi2xi2, u_xi1xi2, x_xi1, x_xi2, x_xi1xi1, x_xi2xi2, x_xi1xi2, y_xi1, y_xi2, y_xi1xi1, y_xi2xi2, y_xi1xi2);
		*u_yy = gpulab::transformation::kernel::u_xx(u_xi1, u_xi2, u_xi1xi1, u_xi2xi2, u_xi1xi2, x_xi1, x_xi2, x_xi1xi1, x_xi2xi2, x_xi1xi2, y_xi1, y_xi2, y_xi1xi1, y_xi2xi2, y_xi1xi2);
	}

	template <typename size_type, typename value_type>
	__device__
	value_type FD2D(  value_type * u_x
					, value_type * u_y
					, value_type * u_xx
					, value_type * u_yy
					, value_type * u_xy
					, value_type const* u
					, value_type const* X
					, value_type const* Y
					, value_type dxi1
					, value_type dxi2
					, size_type i
					, size_type j
					, size_type Nx
					, size_type Ny
					, size_type alpha
					, value_type const* stencil1
					, value_type const* stencil2)
	{
		// First, second, and mixed u-derivatives
		value_type u_xi1    = gpulab::FD::kernel::FD2D_x(u,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil1);
		value_type u_xi2    = gpulab::FD::kernel::FD2D_y(u,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil1);
		value_type u_xi1xi1 = gpulab::FD::kernel::FD2D_x(u,1.f/(dxi1*dxi1),i,j,Nx,Ny,alpha,stencil2);
		value_type u_xi2xi2 = gpulab::FD::kernel::FD2D_y(u,1.f/(dxi2*dxi2),i,j,Nx,Ny,alpha,stencil2);
		value_type u_xi1xi2 = gpulab::FD::kernel::FD2D_xy(u,1.f/dxi1,1.f/dxi2,i,j,Nx,Ny,alpha,stencil1);
		
		// First, second, and mixed X-derivatives
		value_type x_xi1    = gpulab::FD::kernel::FD2D_x(X,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil1,alpha);
		value_type x_xi2    = gpulab::FD::kernel::FD2D_y(X,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil1,alpha);
		value_type x_xi1xi1 = gpulab::FD::kernel::FD2D_x(X,1.f/(dxi1*dxi1),i,j,Nx,Ny,alpha,stencil2,alpha);
		value_type x_xi2xi2 = gpulab::FD::kernel::FD2D_y(X,1.f/(dxi2*dxi2),i,j,Nx,Ny,alpha,stencil2,alpha);
		value_type x_xi1xi2 = gpulab::FD::kernel::FD2D_xy(X,1.f/dxi1,1.f/dxi2,i,j,Nx,Ny,alpha,stencil1,alpha,alpha);

		// First, second, and mixed Y-derivatives
		value_type y_xi1    = gpulab::FD::kernel::FD2D_x(Y,value_type(1)/dxi1,i,j,Nx,Ny,alpha,stencil1,alpha);
		value_type y_xi2    = gpulab::FD::kernel::FD2D_y(Y,value_type(1)/dxi2,i,j,Nx,Ny,alpha,stencil1,alpha);
		value_type y_xi2xi2 = gpulab::FD::kernel::FD2D_y(Y,1.f/(dxi2*dxi2),i,j,Nx,Ny,alpha,stencil2,alpha);
		value_type y_xi1xi1 = gpulab::FD::kernel::FD2D_x(Y,1.f/(dxi1*dxi1),i,j,Nx,Ny,alpha,stencil2,alpha);
		value_type y_xi1xi2 = gpulab::FD::kernel::FD2D_xy(Y,1.f/dxi1,1.f/dxi2,i,j,Nx,Ny,alpha,stencil1,alpha,alpha);

		// Jacobian
		value_type J     = transformation::kernel::jacobian(x_xi1,x_xi2,y_xi1,y_xi2);
		value_type J_xi1 = (x_xi1xi1*y_xi2 - y_xi1xi1*x_xi2 - x_xi1xi2*y_xi1 + y_xi1xi2*x_xi1);
		value_type J_xi2 = (x_xi1xi2*y_xi2 - y_xi1xi2*x_xi2 - x_xi2xi2*y_xi1 + y_xi2xi2*x_xi1);

		// Output
		*u_x = (y_xi2*u_xi1 - y_xi1*u_xi2) / J;
		*u_y = (x_xi1*u_xi2 - x_xi2*u_xi1) / J;

		*u_xx =  (y_xi2*y_xi2*u_xi1xi1 - 2*y_xi1*y_xi2*u_xi1xi2 + y_xi1*y_xi1*u_xi2xi2)/(J*J)
		      + ((y_xi2*y_xi2*y_xi1xi1 - 2*y_xi1*y_xi2*y_xi1xi2 + y_xi1*y_xi1*y_xi2xi2)*(x_xi2*u_xi1 - x_xi1*u_xi2)
			  +  (y_xi2*y_xi2*x_xi1xi1 - 2*y_xi1*y_xi2*x_xi1xi2 + y_xi1*y_xi1*x_xi2xi2)*(y_xi1*u_xi2 - y_xi2*u_xi1))/(J*J*J);

		*u_yy =  (x_xi2*x_xi2*u_xi1xi1 - 2*x_xi1*x_xi2*u_xi1xi2 + x_xi1*x_xi1*u_xi2xi2)/(J*J) 
			  + ((x_xi2*x_xi2*y_xi1xi1 - 2*x_xi1*x_xi2*y_xi1xi2 + x_xi1*x_xi1*y_xi2xi2)*(x_xi2*u_xi1 - x_xi1*u_xi2) 
			  +  (x_xi2*x_xi2*x_xi1xi1 - 2*x_xi1*x_xi2*x_xi1xi2 + x_xi1*x_xi1*x_xi2xi2)*(y_xi1*u_xi2 - y_xi2*u_xi1))/(J*J*J);

		*u_xy = ((x_xi1*y_xi2 + x_xi2*y_xi1)*u_xi1xi2 - x_xi1*y_xi1*u_xi2xi2 - x_xi2*y_xi2*u_xi1xi1)/(J*J) 
		      + ((x_xi1*y_xi2xi2 - x_xi2*y_xi1xi2)/(J*J) + (x_xi2*y_xi2*J_xi1 - x_xi1*y_xi2*J_xi2)/(J*J*J))*u_xi1 
			  + ((x_xi2*y_xi1xi1 - x_xi1*y_xi1xi2)/(J*J) + (x_xi1*y_xi1*J_xi2 - x_xi2*y_xi1*J_xi1)/(J*J*J))*u_xi2;
	}

	//template <typename value_type>
	//__device__ __host__
	//inline
	//value_type u_xy(  value_type u_xi1
	//				, value_type u_xi2
	//				, value_type u_xi1xi1
	//				, value_type u_xi2xi2
	//				, value_type u_xi1xi2
	//				, value_type x_xi1
	//				, value_type x_xi2
	//				, value_type y_xi1
	//				, value_type y_xi2)
	//{
	//	value_type J = jacobian(x_xi1,x_xi2,y_xi1,y_xi2);
	//	return   ((x_xi1*y_xi2 + x_xi2*y_xi1)*u_xi1xi2 - x_xi1*y_xi1*u_xi2xi2 - x_xi2*y_xi2*u_xi1xi1)/(J*J) 
	//	       + ((x_xi1*y_xi2xi2 - x_xi2*y_xi1xi2)/(J*J) + (x_xi2*y_xi2*J_xi1 - x_xi1*y_xi2*J_xi2)/(J*J*J))*u_xi1 
	//		   + ((x_xi2*y_xi1xi1 - x_xi1*y_xi1xi2)/(J*J) + (x_xi1*y_xi1*J_xi2 - x_xi2*y_xi1*J_xi1)/(J*J*J))*u_xi2;;
	//}
}
}

	template <typename grid_type_>
	class grid_transformation
	{
	public:
		typedef grid_type_									grid_type;
		typedef typename grid_type::value_type				value_type;
		typedef typename gpulab::FD::stencil_2d<value_type>	stencil_type;

	private:

		// Computational grid reference
		grid_type* m_U;

		// Physical grids
		grid_type*	m_X;
		grid_type*	m_Y;

		// First order derivative
		grid_type*	m_X_xi1;
		grid_type*	m_X_xi2;
		grid_type*	m_Y_xi1;
		grid_type*	m_Y_xi2;
		
		// Second order derivative
		grid_type*	m_X_xi1xi1;
		grid_type*	m_X_xi2xi2;
		grid_type*	m_Y_xi1xi1;
		grid_type*	m_Y_xi2xi2;

		// Mixed derivative
		grid_type*	m_X_xi1xi2;
		grid_type*	m_Y_xi1xi2;

		// Jacobian (calculate them in kernel)
		//grid_type*	m_J;
		//grid_type*	m_J_xi1;
		//grid_type*	m_J_xi2;

		stencil_type m_Dx;
		stencil_type m_Dxx;

	public:
		grid_transformation(int alpha)
			: m_X(0)
			, m_Y(0)
			, m_Dx(1,alpha)
			, m_Dxx(2,alpha)
		{}

		/**
		* (Re)calculate all xi-derivatives based on X and Y
		**/ 
		void update()
		{
			// Make sure X and Y are set
			if(!m_X || !m_Y)
			{
				GPULAB_LOG_WRN("Warning: Could not make grid transformation because no valid X and/or Y grids were set\n");
				return;
			}

			// First order derivatives
			m_Dx.diff_x(*m_X,*m_X_xi1);
			m_Dx.diff_y(*m_X,*m_X_xi2);
			m_Dx.diff_x(*m_Y,*m_Y_xi1);
			m_Dx.diff_y(*m_Y,*m_Y_xi2);

			// Second order derivatives
			m_Dxx.diff_x(*m_X,*m_X_xi1xi1);
			m_Dxx.diff_y(*m_X,*m_X_xi2xi2);
			m_Dxx.diff_x(*m_Y,*m_Y_xi1xi1);
			m_Dxx.diff_y(*m_Y,*m_Y_xi2xi2);

			// Mixed derivatives
			m_Dx.diff_y(*m_X_xi1,*m_X_xi1xi2);
			m_Dx.diff_y(*m_Y_xi1,*m_Y_xi1xi2);
		}

		// Getters and setters
		void set_X(grid_type* X)	{ this->m_X = X; }
		void set_Y(grid_type* Y)	{ this->m_Y = Y; }

		grid_type const* X()		const { return this->m_X;		}
		grid_type const* Y()		const { return this->m_Y;		}

		// First order derivative
		grid_type const* X_xi1()	const { return this->m_X_xi1;	 }
		grid_type const* X_xi2()	const { return this->m_X_xi2;	 }
		grid_type const* Y_xi1()	const { return this->m_Y_xi1;	 }
		grid_type const* Y_xi2()	const { return this->m_Y_xi2;	 }
		// Second order derivative
		grid_type const* X_xi1xi1() const { return this->m_X_xi1xi1; }
		grid_type const* X_xi2xi2() const { return this->m_X_xi2xi2; }
		grid_type const* Y_xi1xi1() const { return this->m_Y_xi1xi1; }
		grid_type const* Y_xi2xi2() const { return this->m_Y_xi2xi2; }
		// Mixed derivative
		grid_type const* X_xi1xi2() const { return this->m_X_xi1xi2; }
		grid_type const* Y_xi1xi2() const { return this->m_Y_xi1xi2; }
	
		// Stencil getters			
		stencil_type const& Dx()  const { return m_Dx;  }
		stencil_type const& Dxx() const { return m_Dxx; }
	};

} // namespace gpulab


#endif // GPULAB_GRID_TRANSFORMATION_H
