// ==========================================================================
// Grid properties. Info class for creating a grid
// ==========================================================================
// (C)opyright: 2010
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_GRID_PROPERTIES_H
#define GPULAB_GRID_PROPERTIES_H

#include <gpulab/config.h>

namespace gpulab
{

	// Boundary conditions
	enum BC_TYPE
	{
		BC_NONE = 0,	// None (default)
		BC_DIR  = 1,	// Dirichlet
		BC_NEU  = 2,	// Neumann
		BC_PER  = 3,	// Periodic
		BC_ABC  = 4,	// Absorbing
		BC_DD   = 5		// Domain Decomposed
	};
	
	/**
	* A container for (up to) three dimensional info of type T
	*/
	template <typename T>
	struct grid_dim
	{
		T x, y, z;

		grid_dim(T vx = T(0), T vy = T(0), T vz = T(0)) : x(vx), y(vy), z(vz) {}

		template <typename T_> ///< specilization for other precisions
		grid_dim(grid_dim<T_> const& o) : x(o.x), y(o.y), z(o.z) {}

		void operator=(grid_dim<T> const& o) { x = o.x; y = o.y; z = o.z; }
		bool operator==(grid_dim<T> const& o) const { return x == o.x && y == o.y && z == o.z; }
	};

	
	template <typename size_type, typename value_type>
	struct grid_properties
	{
		typedef grid_properties<size_type,value_type>		my_type;

		typedef grid_dim<size_type>							dim_size_type;
		typedef grid_dim<value_type>						dim_value_type;
		typedef grid_dim<bool>								dim_bool_type;

		dim_size_type										inner_dim;	///< Dimensions / resolution
		dim_size_type										outer_dim;	///< Dimensions / resolution
		dim_value_type										phys0;		///< Physical start dimensions
		dim_value_type										phys1;		///< Physical end dimensions
		dim_value_type										delta;		///< Grid spaces
		dim_size_type										ghost0;		///< Ghost points at the start of each dimension
		dim_size_type										ghost1;		///< Ghost points at the end of each dimension
		dim_size_type										offset0;	///< Offset points at the start of each dimension
		dim_size_type										offset1;	///< Offset points at the end of each dimension
		dim_size_type										bctype0;	///< Boundary conditions at the start of each dimension
		dim_size_type										bctype1;	///< Boundary conditions at the end of each dimension

		/** 
		 * Default constructor
		 */
		grid_properties()
			: inner_dim(0,0,0)
			, outer_dim(0,0,0)
			, phys0(value_type(0),value_type(0),value_type(0))
			, phys1(value_type(1),value_type(1),value_type(1))
			, ghost0(0,0,0)
			, ghost1(0,0,0)
			, offset0(0,0,0)
			, offset1(0,0,0)
			, bctype0(BC_DIR,BC_DIR,BC_DIR)
			, bctype1(BC_DIR,BC_DIR,BC_DIR)
			, delta(value_type(0),value_type(0),value_type(0))
		{ }


		/** 
		 * This constructor creates 1D, 2D, or 3D \p grid properties.
		 * If dim_ is set to 0 for any of the three dimensions, it is
		 * changed into 1, because we want N = Nx*Ny*Nz != 0.
		 * @param dim_ The number of elements in each dimension
		 * @param x0_ Physical start coordinates in each direction
		 * @param x1_ Physical end coordinates in each direction
		 * @param g0_ Ghost points at the start of each dimension
		 * @param g1_ Ghost points at the end of each dimension
		 * @param offset0_ Offset points at the start of each dimension
		 * @param offset1_ Offset points at the end of each dimension
		 * @param bctype0_ B.C. types at the start of each dimension
		 * @param bctype1_ B.C. types at the end of each dimension
		 */
		grid_properties(dim_size_type  const& dim_
					  , dim_value_type const& x0_
					  , dim_value_type const& x1_
					  , dim_size_type  const& g0_      = dim_size_type(0,0,0)
					  , dim_size_type  const& g1_      = dim_size_type(0,0,0)
					  , dim_size_type  const& offset0_ = grid_dim<size_type>(0,0,0)
					  , dim_size_type  const& offset1_ = grid_dim<size_type>(0,0,0)
					  , dim_size_type  const& bctype0_ = grid_dim<BC_TYPE>(BC_NONE,BC_NONE,BC_NONE)
					  , dim_size_type  const& bctype1_ = grid_dim<BC_TYPE>(BC_NONE,BC_NONE,BC_NONE))
			: phys0(x0_)
			, phys1(x1_)
			, ghost0(g0_)
			, ghost1(g1_)
			, offset0(offset0_)
			, offset1(offset1_)
			, bctype0(bctype0_)
			, bctype1(bctype1_)
		{
			// Make sure that Nx*Ny*Nz != 0
			inner_dim.x = dim_.x==0 ? 1 : dim_.x;
			inner_dim.y = dim_.y==0 ? 1 : dim_.y;
			inner_dim.z = dim_.z==0 ? 1 : dim_.z;

			// Adjust outer dimensions
			outer_dim.x = inner_dim.x+ghost0.x+ghost1.x;
			outer_dim.y = inner_dim.y+ghost0.y+ghost1.y;
			outer_dim.z = inner_dim.z+ghost0.z+ghost1.z;

			// Calculate grid spcaings
			delta.x = (phys1.x-phys0.x)/(value_type)(inner_dim.x-1);
			delta.y = (phys1.y-phys0.y)/(value_type)(inner_dim.y-1);
			delta.z = (phys1.z-phys0.z)/(value_type)(inner_dim.z-1);
		}

		/** 
		 * This constructor creates 1D, 2D, or 3D \p grid properties.
		 * If dim_ is set to 0 for any of the three dimensions, it is
		 * changed into 1, because we want N = Nx*Ny*Nz != 0.
		 * @param dim_ The number of elements in each dimension
		 * @param bctype0_ B.C. types at the start of each dimension
		 * @param bctype1_ B.C. types at the end of each dimension
		 */
		grid_properties(dim_size_type  const& dim_
					  , dim_size_type  const& bctype0_
					  , dim_size_type  const& bctype1_)
			: phys0(value_type(0),value_type(0),value_type(0))
			, phys1(value_type(1),value_type(1),value_type(1))
			, bctype0(bctype0_)
			, bctype1(bctype1_)
		{
			// Set ghost layers based on BC_TYPE
			ghost0.x = (bctype0.x == BC_NONE || bctype0.x == BC_DIR) ? 0 : 1;
			ghost0.y = (bctype0.y == BC_NONE || bctype0.y == BC_DIR) ? 0 : 1;
			ghost0.z = (bctype0.z == BC_NONE || bctype0.z == BC_DIR) ? 0 : 1;
			ghost1.x = (bctype1.x == BC_NONE || bctype1.x == BC_DIR) ? 0 : 1;
			ghost1.y = (bctype1.y == BC_NONE || bctype1.y == BC_DIR) ? 0 : 1;
			ghost1.z = (bctype1.z == BC_NONE || bctype1.z == BC_DIR) ? 0 : 1;

			// Set offset based on BC_TYPE
			offset0.x = (bctype0.x == BC_DIR) ? 1 : 0;
			offset0.y = (bctype0.y == BC_DIR) ? 1 : 0;
			offset0.z = (bctype0.z == BC_DIR) ? 1 : 0;
			offset1.x = (bctype1.x == BC_DIR) ? 1 : 0;
			offset1.y = (bctype1.y == BC_DIR) ? 1 : 0;
			offset1.z = (bctype1.z == BC_DIR) ? 1 : 0;

			// Make sure that Nx*Ny*Nz != 0
			inner_dim.x = dim_.x==0 ? 1 : dim_.x;
			inner_dim.y = dim_.y==0 ? 1 : dim_.y;
			inner_dim.z = dim_.z==0 ? 1 : dim_.z;

			// Adjust outer dimensions
			outer_dim.x = inner_dim.x+ghost0.x+ghost1.x;
			outer_dim.y = inner_dim.y+ghost0.y+ghost1.y;
			outer_dim.z = inner_dim.z+ghost0.z+ghost1.z;

			// Calculate grid spcaings
			delta.x = (phys1.x-phys0.x)/(value_type)(inner_dim.x-1);
			delta.y = (phys1.y-phys0.y)/(value_type)(inner_dim.y-1);
			delta.z = (phys1.z-phys0.z)/(value_type)(inner_dim.z-1);
		}

		/** 
		 * This constructor creates 1D, 2D, or 3D \p grid properties.
		 * If dim_ is set to 0 for any of the three dimensions, it is
		 * changed into 1, because we want N = Nx*Ny*Nz != 0.
		 * @param dim_ The number of elements in each dimension
		 * @param bctype0_ B.C. types at the start of each dimension
		 * @param bctype1_ B.C. types at the end of each dimension
		 */
		grid_properties(dim_size_type  const& dim_)
			: phys0(value_type(0),value_type(0),value_type(0))
			, phys1(value_type(1),value_type(1),value_type(1))
			, bctype0(BC_NONE,BC_NONE,BC_NONE)
			, bctype1(BC_NONE,BC_NONE,BC_NONE)
			, ghost0(0,0,0)
			, ghost1(0,0,0)
			, offset0(0,0,0)
			, offset1(0,0,0)
		{
			// Make sure that Nx*Ny*Nz != 0
			inner_dim.x = dim_.x==0 ? 1 : dim_.x;
			inner_dim.y = dim_.y==0 ? 1 : dim_.y;
			inner_dim.z = dim_.z==0 ? 1 : dim_.z;

			// Outer dimensions equals inner dim
			outer_dim = inner_dim;

			// Calculate grid spcaings
			delta.x = (phys1.x-phys0.x)/(value_type)(inner_dim.x-1);
			delta.y = (phys1.y-phys0.y)/(value_type)(inner_dim.y-1);
			delta.z = (phys1.z-phys0.z)/(value_type)(inner_dim.z-1);
		}

		template <typename other_size_type, typename other_value_type>
		grid_properties(grid_properties<other_size_type,other_value_type> const& g)
			: inner_dim(g.inner_dim)
			, outer_dim(g.outer_dim)
			, phys0(g.phys0)
			, phys1(g.phys1)
			, delta(g.delta)
			, ghost0(g.ghost0)
			, ghost1(g.ghost1)
			, offset0(g.offset0)
			, offset1(g.offset1)
			, bctype0(g.bctype0)
			, bctype1(g.bctype1)
		{ }

		my_type & operator=(my_type const& p)
		{
			if(this != &p)
			{
				this->phys0    = p.phys0;
				this->phys1    = p.phys1;
				this->ghost0   = p.ghost0;
				this->ghost1   = p.ghost1;
				this->outer_dim = p.outer_dim;
				this->inner_dim = p.inner_dim;
				this->delta     = p.delta;
				this->offset0  = p.offset0;
				this->offset1  = p.offset1;
				this->bctype0  = p.bctype0;
				this->bctype1  = p.bctype1;
			}
			return *this;
		}

		/**
		* No. total grid points
		*/
		size_type size() const
		{
			return outer_dim.x*outer_dim.y*outer_dim.z;
		};

		/**
		* The outer dimensions.
		*/
		dim_size_type const& dimension() const
		{
			return outer_dim;
		};

		/**
		* Degrees of freedom, based on inner points
		*/
		size_type dof() const
		{
			return inner_dim.x*inner_dim.y*inner_dim.z;
		};

		/**
		* Returns the product of all (active) grid spacings
		*/
		value_type delta_prod() const
		{
			value_type dx = (inner_dim.x<2) ? gpulab::values<value_type>::one() : delta.x;
			value_type dy = (inner_dim.y<2) ? gpulab::values<value_type>::one() : delta.y;
			value_type dz = (inner_dim.z<2) ? gpulab::values<value_type>::one() : delta.z;
			return dx*dy*dz;
		}

		void set_offset0(grid_dim<size_type> const& offset0_){offset0 = offset0_;}
		void set_offset1(grid_dim<size_type> const& offset1_){offset1 = offset1_;}

		
		void set_inner_dim(grid_dim<size_type> const& dim)
		{
			inner_dim = dim;
			outer_dim = grid_dim<size_type>(dim.x+ghost0.x+ghost1.x,dim.y+ghost0.y+ghost1.y,dim.z+ghost0.z+ghost1.z);
			delta     = grid_dim<value_type>( (phys1.x-phys0.x)/(value_type)(dim.x-1)
											 ,(phys1.y-phys0.y)/(value_type)(dim.y-1)
											 ,(phys1.z-phys0.z)/(value_type)(dim.z-1));
		}
	};

} // namespace gpulab


#endif // GPULAB_GRID_PROPERTIES_H
