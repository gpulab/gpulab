// ==========================================================================
// Vector 1d
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_VECTOR_H
#define GPULAB_VECTOR_H

#include <gpulab/config.h>
#include <gpulab/type_traits.h>
#include <gpulab/transformation.h>
#include <gpulab/dispatch/vector.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/reduce.h>

namespace gpulab
{
	/**
	* Memory space allocator dispatching
	*/
	template <typename T, typename MemorySpace>
	struct thrust_vector_selector
	{
		typedef MemorySpace type;
	};
	
	/**
	* Template specialized for host memory
	*/
	template <typename T>
	struct thrust_vector_selector<T, gpulab::host_memory>
	{
		typedef typename thrust::host_vector<T> type;
	};

	/**
	* Template specialized for device memory
	*/
	template <typename T>
	struct thrust_vector_selector<T, gpulab::device_memory>
	{
		typedef typename thrust::device_vector<T> type;
	};

	/**
	* A generic memory specific wrapper for a thrust vector.
	* The proper thrust vector (host/device) is automatically chosen 
	* based on the second template paramter MemorySpace.
	* @param T value type.
	* @param MemorySpace memory location (host/device).
	*/
	template <typename T, typename MemorySpace>
	class vector : public thrust_vector_selector<T, MemorySpace>::type
	{
	private:
		typedef vector<T, MemorySpace>									my_type;
		typedef typename thrust_vector_selector<T, MemorySpace>::type	super;

	public:
		typedef typename super::size_type								size_type;
		typedef typename super::value_type								value_type;
		typedef typename super::iterator								iterator;
		typedef MemorySpace												memory_space;
	
		/** This constructor creates an empty \p vector.
		*/
		__host__
		vector(void)
		: super()
		{}

		/** This constructor creates a \p vector with copies
		 *  of an exemplar element.
		 *  @param n The number of elements to initially create.
		 *  @param value An element to copy.
		 */
		__host__
		explicit vector(size_type n, const value_type &value = value_type())
		: super(n,value)
		{}

		/** Copy constructor copies from an exemplar \p vector.
		 *  @param v The \p vector to copy.
		 */
		__host__
		vector(const my_type &v)
		: super(v)
		{}

		/** Copy constructor copies from an exemplar \p vector with different type.
		 *  @param v The \p vector to copy.
		 */
		template<typename OtherT>
		__host__ 
		vector(const vector<OtherT,MemorySpace> &v)
		: super(v.size())
		{ 
			this->copy(v);
		}

		/** Copy constructor copies from an exemplar \p vector with different type 
		 *  and memory space.
		 *  @param v The \p vector to copy.
		 */
		template<typename OtherT, typename OtherMemorySpace>
		__host__ 
		vector(const vector<OtherT,OtherMemorySpace> &v)
		  : super(v) {}

		/** Assign operator copies from an exemplar \p vector with different type.
		 *  @param v The \p vector to copy.
		 */
		template<typename OtherT, typename OtherMemorySpace>
		__host__ 
		vector &operator=(const vector<OtherT,OtherMemorySpace> &v)
		{
			super::operator=(v);
			return *this; 
		}

		/** Copy constructor copies from an exemplar \c std::vector.
		 *  @param v The <tt>std::vector</tt> to copy.
		 */
		template<typename OtherT, typename OtherAlloc>
		__host__
		vector(const std::vector<OtherT,OtherAlloc> &v)
		: super(v)
		{}

		/** Assign operator copies from an exemplar <tt>std::vector</tt>.
		 *  @param v The <tt>std::vector</tt> to copy.
		 */
		template<typename OtherT, typename OtherAlloc>
		__host__
		vector &operator=(const std::vector<OtherT,OtherAlloc> &v)
		{ 
			super::operator=(v);
			return *this;
		}

		/** This constructor builds a \p device_vector from a range.
		 *  @param first The beginning of the range.
		 *  @param last The end of the range.
		 */
		template<typename InputIterator>
		__host__
		vector(InputIterator first, InputIterator last)
		: super(first,last)
		{}

		/**
		*
		*/
		T* raw_ptr()
		{
			return thrust::raw_pointer_cast(&(*this));
		}

		/*T* raw_ptr() const
		{
			T const* tmp = thrust::raw_pointer_cast(&(*this));
			return 0;//thrust::raw_pointer_cast(&(*this));
		}*/

		/**
		* Duplicate self and returns pointer.
		* Remember that the pointer returned must be deleted by the receiver.
		*/ 
		my_type* duplicate() const
		{
			return new my_type((this->size()));
		}

		/**
		* Fill all values into the vector
		* @param value to be set
		*/
		void fill(const T value)
		{
			thrust::fill(this->begin(), this->end(), value);
		}

		/**
		* Swaps this vector with another vector
		* @param vector vector to swap with
		*/
		void swap(my_type& vector)
		{
			thrust::swap(*this, vector);
		}

		/**
		* Copy all values from src into the vector
		* @param src vector with elements to copy from
		*/
		void copy(my_type const& src)
		{
			ASSERT(this->size() == src.size());
			thrust::copy(src.begin(), src.end(), this->begin());
		}

		/**
		* Copy all values from first to last into the vector starting from begin
		* @param first iterator pointing to the first source element
		* @param last  iterator pointing to the last  source element
		* @param begin iterator pointing to the first entry of the destination
		*/
		template <typename input_iterator>
		void copy(input_iterator first, input_iterator last, iterator begin)
		{
			ASSERT(last - first <= this->end() - begin);
			thrust::copy(first, last, begin);
		}

		/**
		* Copy all values from first to last into the vector
		* @param first iterator pointing to the first source element
		* @param last  iterator pointing to the last  source element
		*/
		template <typename input_iterator>
		void copy(input_iterator first, input_iterator last)
		{
			this->copy(first, last, this->begin());
		}

		/**
		* Copy all values from src of different type into the vector. Works for differenct value_types i.e.
		* @param other_type other vector type
		* @param src vector with elements to copy from
		*/
		template <typename other_type>
		void copy(other_type const& src)
		{
			ASSERT(this->size() == src.size());
			thrust::copy(src.begin(), src.end(), this->begin());
		}

		/**
		* Minimum value in the vector
		*/
		T minimum() const
		{
			return thrust::reduce(this->begin(), this->end(), value_type(0), thrust::minimum<value_type>());
		}

		/**
		* Maximum value in the vector
		*/
		T maximum() const
		{
			return thrust::reduce(this->begin(), this->end(), value_type(0), thrust::maximum<value_type>());
		}

		/**
		* Equals
		*/
		bool equals(my_type const& x) const
		{
			return 
				thrust::transform_reduce(
				  thrust::make_zip_iterator(thrust::make_tuple(this->begin(), x.begin()))
				, thrust::make_zip_iterator(thrust::make_tuple(this->end(), x.end()))
				, gpulab::is_equal_tuple<T>()
				, true
				, gpulab::is_equal<T>());
		}

		/**
		* Dot product
		*/
		T dot(my_type const& v) const
		{
			// TODO: Hmmm.. it seems that we had an error in our (gpulab) dot implementation, check this!
			// TODO: Test if thrust functions faster?
			//return gpulab::dispatch::vector::dot(*this, v, memory_space());
			return 
				thrust::transform_reduce(
				  thrust::make_zip_iterator(thrust::make_tuple(this->begin(), v.begin()))
				, thrust::make_zip_iterator(thrust::make_tuple(this->end(), v.end()))
				, gpulab::mult_tuple()
				, values<T>::zero()
				, thrust::plus<T>());
		}

		/**
		* Scale
		*/
		void scal(T a)
		{
			//gpulab::dispatch::scal(*this, a, memory_space());
			thrust::transform(this->begin(), this->end(), this->begin(), gpulab::scal_functor<T>(a));
		}

		/**
		* Norm_1. The sum of all elements
		*/
		T nrm1() const
		{
			return gpulab::dispatch::vector::nrm1(*this, memory_space());
			//return thrust::reduce(this->begin(), this->end(), value_type(0), thrust::plus<value_type>());
		}

		/**
		* Norm_2. sqrt(a*a+b*b+...+z*z)
		*/
		value_type nrm2() const
		{
			return sqrt(
				thrust::transform_reduce( 
				this->begin(), this->end(), gpulab::square(), value_type(0), thrust::plus<value_type>()
				));
		}

		/**
		* Norm_infinity. Maximum element value.
		*/
		T nrmi() const
		{
			return thrust::reduce(this->begin(), this->end(), value_type(0), gpulab::abs_maximum<value_type>());
		}

		/**
		* The error norm of a grid funtion is the intergral over all dimensions.
		* Since a vector has no grid info, this is simple the norm2
		*/
		value_type err_nrm2() const
		{
			return nrm2();
		}

		/**
		* AXPY this = a*x + this
		* Note: Benchmarks show that using our own kernels vs thrust is compareable, but gives a speedup of ~3 in Debug mode.
		*/
		void axpy(T a, my_type const& x)
		{
			thrust::transform(x.begin(), x.end(), this->begin(), this->begin(), gpulab::axpy_functor<T>(a));
			//gpulab::dispatch::vector::axpy(a, x, *this, memory_space());
		}

		/**
		* AXPY this = a*x + this
		* Note: Benchmarks show that using our own kernels vs thrust is compareable, but gives a speedup of ~3 in Debug mode.
		*/
		void axpy(T a1, my_type const& x1, T a2, my_type const& x2, T a3, my_type const& x3)
		{
			//thrust::transform(x.begin(), x.end(), this->begin(), this->begin(), gpulab::axpy_functor<T>(a));
			gpulab::dispatch::vector::axpy(a1, x1, a2, x2, a3, x3, *this, memory_space());
		}

		/**
		* AXPBY this = a*x + b*this
		*/
		void axpby(const T a, const T b, my_type const& x)
		{
			thrust::transform(x.begin(), x.end(), this->begin(), this->begin(), gpulab::axpby_functor<T>(a,b));
			//gpulab::dispatch::vector::axpby(*this, a, b, x, memory_space());
		}

	};



} // namespace gpulab


#endif // GPULAB_VECTOR_H
