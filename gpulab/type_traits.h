// ==========================================================================
// Type Traits
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_TYPE_TRAITS_H
#define GPULAB_TYPE_TRAITS_H

#include <stdio.h>
#include <string>
#include <float.h>
#include <mpi.h>

// ASSERT helper
#define RLSASSERT(cond) if( !(cond) ) \
{ printf( "Assertion error line %d, file(%s)\nAssertion error: \"" #cond "\"", \
	__LINE__, __FILE__ );\
getchar();exit(1);}
// ASSERT definition
#ifdef NDEBUG
#define ASSERT(cond)
#else
#define ASSERT(cond) RLSASSERT(cond)
#endif

namespace gpulab
{
	/**
	* Device memory space identifier
	*/
	struct device_space_tag {};
	/**
	* Host memory space identifier
	*/
	struct host_space_tag {};

	/**
	* Typedefs for easy memory space access
	*/
	typedef gpulab::device_space_tag device_memory;
	typedef gpulab::host_space_tag   host_memory;

	/**
	* Returns true if x is not NaN
	* @param x value to test for NaN
	*/
	template <typename valye_type>
	__device__ __host__
	inline bool is_number(valye_type x) { return (x==x); };

	/**
	* Returns true if x is not NaN and not INF
	* @param x value to test for NaN
	*/
	__device__ __host__ inline bool is_finite_number(long double x) { return (x <= LDBL_MAX && x >= -LDBL_MAX); };
	__device__ __host__ inline bool is_finite_number(double x)		{ return (x <= DBL_MAX && x >= -DBL_MAX);	};
	__device__ __host__ inline bool is_finite_number(float  x)		{ return (x <= FLT_MAX && x >= -FLT_MAX);	};

	/**
	* Column major indexing. Returns the index inside a matrix, given the row, 
	* column, and matrix dimensions.
	*/
	struct column_major
	{
		template <typename size_type>
		__host__ __device__
		static inline size_type index(size_type i, size_type j, size_type I, size_type J)
		{
			return (j*I+i);
		}
	};

	/**
	* TODO: Deprecated: we use cublas, that only support column major...?
	* Row major indexing. Returns the index inside a matrix, given the row, 
	* column, and matrix dimensions.
	*/
	struct row_major
	{
		template <typename size_type>
		__host__ __device__
		static inline size_type index(size_type i, size_type j, size_type I, size_type J)
		{
			return (i*J+j);
		}
	};


	struct types
	{
		template <typename T1, typename T2>
		__device__ __host__ static bool	equals(T1&,T2&)	{ return false; }
		template <typename T>
		__device__ __host__ static bool	equals(T&,T&)	{ return true;	}

	};


	 /** 
	 * Return false for any variable that is not specified
	 */
	template <typename T>
	struct type
	{
		static const bool is_double = false;
		__device__ __host__ static char*	name()	{ return "unknown"; }
	};

	 /** 
	 * Return double spezialization
	 */
	template <>
	struct type<double>
	{
		static const bool is_double = true;
		__device__ __host__ static char*	name()	{ return "double"; }

	};

	 /** 
	 * Return float spezializations
	 */
	template <>
	struct type<float>
	{
		static const bool is_double = false;
		
		__device__ __host__ static char*	name()	{ return "float"; }
	};



	// Mixed precision decider
	template <typename T>
	struct precision
	{
		typedef float low;
		typedef double high;
	};


	/**
	* Value traits
	*/
	template<typename value_type>
	struct values;

	template<>
	struct values<int>
	{
		__device__ __host__ static int			zero()		{ return 0;  }
		__device__ __host__ static int			quarter()	{ return 0; }
		__device__ __host__ static int			half()		{ return 0; }
		__device__ __host__ static int			one()		{ return 1;  }
		__device__ __host__ static int			two()		{ return 2;  }
		__device__ __host__ static int			three()		{ return 3;  }
		__device__ __host__ static int			tolerance()	{ return 0;  }
		__device__ __host__ static int			gravity()	{ return 10; }
		__device__ __host__ static MPI_Datatype	MPI_TYPE()	{ return MPI_INT; }
		
	};

	template<>
	struct values<unsigned int>
	{
		__device__ __host__ static unsigned int zero()		{ return 0u;  }
		__device__ __host__ static unsigned int quarter()	{ return 0u;  }
		__device__ __host__ static unsigned int half()		{ return 0u;  }
		__device__ __host__ static unsigned int one()		{ return 1u;  }
		__device__ __host__ static unsigned int two()		{ return 2u;  }
		__device__ __host__ static unsigned int tree()		{ return 3u;  }
		__device__ __host__ static unsigned int tolerance()	{ return 0u;  }
		__device__ __host__ static unsigned int gravity()	{ return 10u; }
		__device__ __host__ static MPI_Datatype	MPI_TYPE()	{ return MPI_UNSIGNED; }
	};

	template<>
	struct values<float>
	{
		__device__ __host__ static float		zero()		{ return 0.0f;	}
		__device__ __host__ static float		quarter()	{ return 0.25f;	}
		__device__ __host__ static float		half()		{ return 0.5f;	}
		__device__ __host__ static float		one()		{ return 1.0f;	}
		__device__ __host__ static float		two()		{ return 2.0f;	}
		__device__ __host__ static float		three()		{ return 3.0f;	}
		__device__ __host__ static float		tolerance()	{ return 1e-6f; }
		__device__ __host__ static float		gravity()	{ return 9.82f; }
		__device__ __host__ static MPI_Datatype	MPI_TYPE()	{ return MPI_FLOAT; }
	};

	template<>
	struct values<double>
	{
		__device__ __host__ static double		zero()		{ return 0.0;   }
		__device__ __host__ static double		quarter()	{ return 0.25;  }
		__device__ __host__ static double		half()		{ return 0.5;   }
		__device__ __host__ static double		one()		{ return 1.0;   }
		__device__ __host__ static double		two()		{ return 2.0;   }
		__device__ __host__ static double		three()		{ return 3.0;   }
		__device__ __host__ static double		tolerance()	{ return 1e-14; }
		__device__ __host__ static double		gravity()	{ return 9.82;  }
		__device__ __host__ static MPI_Datatype	MPI_TYPE()	{ return MPI_DOUBLE; }
	};

	template<>
	struct values<std::string>
	{
		__device__ __host__ static std::string	zero()		{ return "0";   }
		__device__ __host__ static std::string	quarter()	{ return "0.25";  }
		__device__ __host__ static std::string	half()		{ return "0.5";   }
		__device__ __host__ static std::string	one()		{ return "1";   }
		__device__ __host__ static std::string	two()		{ return "2";   }
		__device__ __host__ static std::string	three()		{ return "3";   }
		__device__ __host__ static std::string	tolerance()	{ return "1e-14"; }
		__device__ __host__ static std::string	gravity()	{ return "9.82";  }
		__device__ __host__ static MPI_Datatype	MPI_TYPE()	{ return MPI_WCHAR; }
	};

} // namespace gpulab


#endif // GPULAB_TYPE_TRAITS_H
