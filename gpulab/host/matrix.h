// ==========================================================================
// Matrix host version
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_HOST_MATRIX_H
#define GPULAB_HOST_MATRIX_H

#include <stdio.h>
#include <assert.h>

#include <gpulab/config.h>
#include <gpulab/type_traits.h>
#include <gpulab/matrix.h>
#include <gpulab/host/vector.h>

namespace gpulab
{
namespace host
{
namespace matrix
{
	/**
	* Multiplication Ax = b. Where A = this.
	*/
	template <typename M, typename V>
	void mult(M const& A, V const& x, V &b)
	{
		typedef typename M::size_type	size_type;
		typedef typename M::value_type	value_type;

		ASSERT(A.rows() == b.size() && A.cols() == x.size());
		
		size_type i, j;
		size_type I = A.rows();
		size_type J = A.cols();
		value_type sum;
		for(i=0; i<I; ++i)
		{
			sum = value_type(0);
			for(j=0; j<J; ++j)
			{
				sum += A(i,j) * x[j];
			}
			b[i] = sum;
		}
	}

	/**
	* Transpose
	*/
	template <typename M>
	void transpose(M& dst, M const& src)
	{
		typedef typename M::size_type	size_type;

		ASSERT(dst.rows() == src.cols() && dst.cols() == src.rows());

		size_type i, j;
		size_type I = src.rows();
		size_type J = src.cols();
		for(j=0; j<J; ++j)
		{
			for(i=0; i<I; ++i)
			{
				dst(j,i) = src(i,j);
			}

		}
	}

	///**
	//* Reciprocal. this = val/this.
	//*/
	//void reciprocal(T const& val) const
	//{
	//	iterator iter = this->begin();
	//	iterator end = this->end();
	//	T zero(0);
	//	while(iter != end)
	//	{
	//		if(*iter != zero)
	//		{
	//			*iter = val / *iter;
	//		}
	//		++iter;
	//	}
	//}

} // namespace matrix
} // namespace host

	///**
	//* Host major change
	//*/
	//template <typename T, typename size_type, typename major_to, typename major_from>
	//void change_major(T* to, T const* from, size_type const I, size_type const J, major_to, major_from, std::allocator<T> const& alloc)
	//{
	//	for(size_type i=0; i<I; ++i)
	//	{
	//		for(size_type j=0; j<J; ++j)
	//		{
	//			to[major_to::index(i,j,I,J)] = from[major_from::index(i,j,I,J)];
	//		}
	//	}
	//}

} // namespace gpulab

#endif // GPULAB_HOST_MATRIX_H