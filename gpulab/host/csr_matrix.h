// ==========================================================================
// Compressed Sparse Row Matrix host version
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Nicolai Fog Gade-Nielsen
// Email:   nfga@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_HOST_CSR_MATRIX_H
#define GPULAB_HOST_CSR_MATRIX_H

#include <stdio.h>
#include <assert.h>

#include <gpulab/config.h>
#include <gpulab/csr_matrix.h>

namespace gpulab
{
namespace host
{
namespace csr_matrix
{
		/**
		* Calculates Ax = b, where this = A
		*/
		template <typename M, typename V>
		void mult(M const& A, V const& x, V &b)
		{
			typedef typename M::size_type			size_type;
			typedef typename M::value_type			value_type;
			typedef typename M::vector_data_type	vector_data_type;
			typedef typename M::vector_index_type	vector_index_type;

			vector_index_type ptr = A.ptr();
			vector_index_type indices = A.indices();
			vector_data_type data = A.data();

			value_type val;
			for(size_type i = 0; i < A.rows(); ++i)
			{
				size_type row_start = ptr[i];
				size_type row_end   = ptr[i+1];

				val = 0;
				for(size_type j = row_start; j < row_end; ++j)
				{
					val += x[indices[j]] * data[j];
				}
				b[i] = val;
			}
		}

} // namespace csr_matrix
} // namespace host
} // namespace gpulab

#endif // GPULAB_HOST_CSR_MATRIX_H
