// ==========================================================================
// Sparse Coordinate Matrix host version
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Nicolai Fog Gade-Nielsen
// Email:   nfga@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_HOST_COO_MATRIX_H
#define GPULAB_HOST_COO_MATRIX_H

#include <stdio.h>
#include <assert.h>

#include <gpulab/config.h>
#include <gpulab/coo_matrix.h>

namespace gpulab
{
namespace host
{
namespace coo_matrix
{
		/**
		* Calculates Ax = b, where this = A
		*/
		template <typename M, typename V>
		void mult(M const& A, V const& x, V &b)
		{
			typedef typename M::size_type			size_type;
			typedef typename M::value_type			value_type;
			typedef typename M::vector_data_type	vector_data_type;
			typedef typename M::vector_index_type	vector_index_type;

			vector_index_type const & i = A.i();
			vector_index_type const & j = A.j();
			vector_data_type const & data = A.data();

			// Reset b
			b.fill(0);

			// Multiply
			for (size_type m = 0; m < A.nonzeros(); ++m)
				b[i[m]] += data[m] * x[j[m]];
		}

		/**
		* Calculates A'x = b, where this = A
		*/
		template <typename M, typename V>
		void multT(M const& A, V const& x, V &b)
		{
			typedef typename M::size_type			size_type;
			typedef typename M::value_type			value_type;
			typedef typename M::vector_data_type	vector_data_type;
			typedef typename M::vector_index_type	vector_index_type;

			vector_index_type const & i = A.j();
			vector_index_type const & j = A.i();
			vector_data_type const & data = A.data();

			// Reset b
			b.fill(0);

			// Multiply
			for (size_type m = 0; m < A.nonzeros(); ++m)
				b[i[m]] += data[m] * x[j[m]];
		}
} // namespace coo_matrix
} // namespace host
} // namespace gpulab

#endif // GPULAB_HOST_COO_MATRIX_H
