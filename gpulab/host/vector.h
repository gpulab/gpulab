// ==========================================================================
// Host side Vector Functions
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_HOST_VECTOR_H
#define GPULAB_HOST_VECTOR_H

namespace gpulab
{
namespace host
{
namespace vector
{
	/**
	* Dot product. TODO: Implement a reduce function to handle this
	*/
	template <typename V>
	typename V::value_type dot(  const V& v1
							   , const V& v2)
	{
		typedef typename V::value_type value_type;
		typedef typename V::size_type size_type;
	    
		value_type dot(0);
		for(size_type i=0; i<v1.size(); ++i) 
		{
			dot += v1[i] * v2[i];
		}
		return dot;
	}

	/**
	* Nrm1. TODO: Implement a reduce function to handle this
	*/
	template <typename V>
	typename V::value_type nrm1( const V& v1 )
	{
		typedef typename V::value_type value_type;
		typedef typename V::size_type size_type;
	    
		value_type nrm(0);
		for(size_type i=0; i<v1.size(); ++i) 
		{
			nrm += v1[i];
		}
		return nrm;
	}

	template <typename T>
	void axpy( T a
			 , T const* x
			 , T* y
			 , unsigned int N)
	{
		for(unsigned int i=0; i<N; ++i)
		{
			y[i] = a*x[i] + y[i];
		}
	
	}

	template <typename V, typename T>
	void axpy( T a
			 , V const& x
			 , V& y)
	{
		ASSERT(x.size()==y.size());
		axpy(a,  x.data(), y.data(), x.size());
	}
} // end namespace vector
} // end namespace host
} // end namespace gpulab

#endif // GPULAB_HOST_VECTOR_H