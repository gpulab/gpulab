// ==========================================================================
// Bi-conjugate Gradient Stabilized solver
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Nicolai Fog Gade-Nielsen
// Email:   nfga@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_BICGSTAB_H
#define GPULAB_SOLVERS_BICGSTAB_H

#include <assert.h>
#include <gpulab/monitor.h>
#include <gpulab/io/print.h>

namespace gpulab
{
namespace solvers
{

	/**
	* Bi-Conjugate gradient for normal residual solver for Ax = b
	* Based on: http://en.wikipedia.org/wiki/Biconjugate_gradient_stabilized_method
	*/
	template <typename matrix_type, typename vector_type>
	void bicgstab(matrix_type const& A, vector_type& x, vector_type const& b, monitor<typename vector_type::value_type>& m)
	{
		typedef typename vector_type::size_type	 size_type;
		typedef typename vector_type::value_type value_type;

		// Reset
		m.reset(b);

		// Init
		vector_type r(b.size());
		A.mult(x, r);
		r.axpby(value_type(1), value_type(-1), b);

		vector_type r0(r);

		value_type rho_prev;
		value_type alpha = 1;
		value_type omega = 1;

		vector_type v(b.size(), value_type(0));
		vector_type p(x.size(), value_type(0));

		value_type rho = 1;

		// Additional allocation
		vector_type t(b.size());
		value_type beta;

		// Main loop
		value_type r_dot = r.dot(r);

		while(1)
		{
			rho_prev = rho;
			rho = r0.dot(r);
			if (rho == 0 || !isfinite(rho)) 				// Breakdown
				break;

			beta = (rho / rho_prev) * (alpha / omega);
			if (beta == 0 || !isfinite(beta)) 				// Breakdown
				break;

			p.axpy(-omega, v);
			p.axpby(value_type(1), beta, r);

			A.mult(p, v);

			value_type r0v = r0.dot(v);
			if (r0v == 0 || !isfinite(r0v))					// Breakdown
				break;
			alpha = rho / (r0v);
			if (alpha == 0 || !isfinite(alpha))				// Breakdown
				break;

			vector_type& s = r;
			s.axpy(-alpha, v);
			A.mult(s, t);

			value_type tt = t.dot(t);
			if (tt == 0 || !isfinite(tt))					// Breakdown
				break;
			omega = t.dot(s) / tt;
			if (omega == 0 || !isfinite(r0v))				// Breakdown
				break;

			// Update
			x.axpy(alpha, p);
			x.axpy(omega, s);

			// s is reference to r already
			r.axpy(-omega, t);
			r_dot = r.dot(r);

			++m;
			if (m.finished(sqrt(r_dot)))
				break;
		}
	}

	/**
	* Conjugate gradient for normal residual solver for Ax = b with default monitor.
	* @return monitor with convergence information
	*/
	template <typename matrix_type, typename vector_type>
	monitor<typename vector_type::value_type> bicgstab(matrix_type const& A, vector_type& x, vector_type const& b)
	{
		monitor<typename vector_type::value_type> m(b);
		bicgstab(A, x, b, m);
		return m;
	}

} // namespace solvers
} // namespace gpulab

#endif // GPULAB_SOLVERS_BICGSTAB_H
