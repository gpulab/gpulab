// ==========================================================================
// Multigrid monitor
// ==========================================================================
// (C)opyright: 2010
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Author: Stefan Lemvig Glimberg
// Email:  slgl@imm.dtu.dk
//
// Author: Allan P. Engsig-Karup
// Email:  apek@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_MULTIGRID_MONITOR_H
#define GPULAB_SOLVERS_MULTIGRID_MONITOR_H

#include <gpulab/monitor.h>
#include <gpulab/type_traits.h>

namespace gpulab
{

namespace solvers
{

	// Cycle types
	enum CYCLE
	{
		V,  // 0: V-cycle 
		F,  // F-cycle
		W,  // W-cycle
		FMG // Full multigrid
	};

	// Weighting
	enum WEIGHT
	{
		HW,  // Half weighting
		FW   // Full weighting
	};

	/**
	* A function that helps determine the maximum number of restirctions based on a grid of size Nx times Ny
	* @param N number of points in x|y|z
	*/ 
	static int max_mg_restrictions(unsigned int N)
	{
		// TODO: Maybe it is better to just call HALF and count restrictions and get rid of alpha
		int K = (int)round(log2((double)N-1)) - 2;
		return K;
	};


	///< A monitor holds and stores information from the iterative solver
	template <typename value_type, typename timer_type = gpulab::util::device_timer>
	class multigrid_monitor : public monitor<value_type, timer_type>
	{
	protected:
		typedef monitor<value_type, timer_type>	super;
		
		int m_v1;             ///< No. pre-smoothenings
		int m_v2;             ///< No. post-smoothenings
		int m_vc;             ///< No. smoothenings on coarsest grid
		int m_K;              ///< Cycle levels

		//int	m_smoother;	///< Smoother
		//int	m_weight;   ///< Weighting
		CYCLE	m_cycle;		///< Cycle
		
	public:

		/**
		* All info for monitor must be in config
		*/
		multigrid_monitor()
			: super()
			, m_cycle(V)
		{
			reset();
		}

		void reset()
		{
			super::reset();
			this->reset_local();			
		}

		template <typename vector_type>
		void reset(vector_type const& b)
		{
			super::reset(b);
			this->reset_local();
		}
		
		int const& v1()	const	{ return m_v1; }
		int const& v2()	const	{ return m_v2; }
		int const& vc()	const	{ return m_vc; }
		int const& K()	const	{ return m_K; }

		//SMOOTHER	const& smoother()	const	{ return m_smoother; }
		//WEIGHT	const& weight()		const	{ return m_weight; }
		CYCLE		const& cycle()		const	{ return m_cycle; }

	private: 
		void reset_local()
		{
			GPULAB_CONFIG_GET("K"	  ,&m_K,	-1);
			GPULAB_CONFIG_GET("v1"	  ,&m_v1,	 2);
			GPULAB_CONFIG_GET("v2"	  ,&m_v2,	 2);
			GPULAB_CONFIG_GET("vc"	  ,&m_vc,	 5);
			std::string cycle;
			GPULAB_CONFIG_GET("mgcycle",&cycle,std::string("V"));
			if ( cycle == "V" )
			  m_cycle = V;
			else if ( cycle == "F" )
			  m_cycle = F;
			else if ( cycle == "FMG" )
			  m_cycle = FMG;
			else if ( cycle == "W" )
			  m_cycle = W;

			//if(m_K<0)
			//{
			//	int Nx, Ny, Nz;
			//	GPULAB_CONFIG_GET("Nx",&Nx,0);
			//	GPULAB_CONFIG_GET("Ny",&Ny,0);
			//	GPULAB_CONFIG_GET("Nz",&Nz,0);
			//	m_K = gpulab::solvers::max_mg_restrictions(max(Nx,max(Ny,Nz)));
			//	//printf("Multigrid: using K = %d\n", m_K);
			//}
		}
	};

} // namespace solvers

} // namespace gpulab

#endif // GPULAB_SOLVERS_MULTIGRID_H
