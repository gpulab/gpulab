// ==========================================================================
// Defect Correction Solver
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_DEFECT_CORRECTION_H
#define GPULAB_SOLVERS_DEFECT_CORRECTION_H

#include <gpulab/solvers/defect_correction_types.h>
#include <gpulab/type_traits.h>
#include <gpulab/monitor.h>

namespace gpulab
{

namespace solvers
{

	template <typename types>
	class defect_correction
	{
	public:
		typedef typename types::value_type					value_type;
		typedef typename types::vector_type					vector_type;
		typedef typename types::matrix_type					matrix_type;
		typedef typename types::preconditioner_type			preconditioner_type;
		//typedef typename preconditioner_type::matrix_type	pre_matrix_type;
		typedef typename types::monitor_type				monitor_type;

	protected:

		matrix_type const&		m_A;
		monitor_type&			m_monitor;
		preconditioner_type*	m_precond;
//		bool					m_precond_internal;

	public:
		defect_correction(matrix_type const& A, monitor_type& m)
			: m_A(A)
			, m_monitor(m)
			, m_precond(0)
//			, m_precond_internal(true)
		{
			
			//pre_matrix_type pA(m_A);
			//m_precond = new preconditioner_type(pA,m);

		}

		~defect_correction()
		{
//			if(m_precond && m_precond_internal)
//				delete m_precond;
		}

		void set_preconditioner(preconditioner_type& precond)
		{
//			delete m_precond;
//			m_precond_internal = false;
			m_precond = &precond;
		}

		preconditioner_type* get_preconditioner()
		{
			return m_precond;
		}

		void solve(vector_type& x, vector_type const& b)
		{
			if(!m_precond)
			{
				GPULAB_LOG_WRN("No preconditioner set for defect correction\n");
				return;
			}

			// Set/reset monitor
			m_monitor.reset(b);
			
			// Allocate space for residual and delta x
			vector_type* r = x.duplicate();
			vector_type* d = x.duplicate();

			// Calculate inital residual
			m_A.satisfy_bc(x,b);
			m_A.residual(x,b,*r);

			// Stop based on first residual
			if(!m_monitor.finished(*r))
			{
				while(1)
				{
					// Reset quess
					d->fill(0);

					// Solve using pre-conditioner
					(*m_precond)(*d,*r);

					// Update solution
					x.axpy(gpulab::values<value_type>::one(),*d);

					// Next iteration
					++m_monitor;

					// Close enough to stop based on defect
					if(m_monitor.finished(*d))
					{
						break;
					}
					//GPULAB_LOG_DBG("DC: iter %d, r_rel %e\n", m_monitor.iteration_count(), m_monitor.rel_error());

					// Calculate residual
					m_A.satisfy_bc(x,b);
					m_A.residual(x,b,*r);
				}
			}
			//m_A.satisfy_bc(x,b);
			
			// Clean up
			delete r;
			delete d;
			
			//m_monitor.timer().stop();
			GPULAB_LOG_DBG("DC: iter %d, r_rel %e\n", m_monitor.iteration_count(), m_monitor.rel_error());
		}

	 	monitor_type& monitor()
		{
			return m_monitor;
		}

	};

	/**
	* Solves a system of the form Ax = b
	* @param A matrix
	* @param x solution vector, may contain start guess
	* @param b right hand side
	* @param precond pre-conditioner
	* @param m monitor with iteration information
	*/
	template <typename V, typename M, typename P>
	void defect_correction_(M const& A, V& x, V const& b, P& precond, monitor<typename V::value_type> & m)
	{
		typedef typename V::value_type	value_type;

		m.set_rhs(b);
		m.reset_iteration_count();
		
		// Allocate space for residual and delta x
		V r(x.get_props());
		V d(x.get_props());

		while(1)
		{
			A.mult(x,r);
			r.axpby(1, -1, b);
		
			// Close enough to stop
			if(m.finished(r))
			{
				break;
			}

			// Reset quess
			d.fill(0);

			// Solve using pre-conditioner
			precond(A,d,r);
			
			// Update solution
			x.axpy(values<value_type>::one(),d);

			// Next iteration
			++m;
		}
		
	};
	

} // namespace solvers

} // namespace gpulab

#endif // GPULAB_SOLVERS_DEFECT_CORRECTION_H