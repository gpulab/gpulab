// ==========================================================================
// Multigrid smoothers
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_MULTIGRID_SMOOTHERS_H
#define GPULAB_SOLVERS_MULTIGRID_SMOOTHERS_H

#include <gpulab/type_traits.h>
#include <gpulab/config.h>

namespace gpulab
{
namespace solvers
{
namespace kernel
{
	template <typename T, typename size_type>
	__global__
	void jacobi_smooth_2d(T const* x, T const* b
						, T* out
						, T dx
						, T dy
						, const size_type Nx
						, const size_type Ny
						, size_type xstart
						, size_type xend
						, size_type ystart
						, size_type yend)
	{
		// compute the index of the output vector
		int j = IDX1Dx;
		int i = IDX1Dy;
	
		T half = gpulab::values<T>::half();
		T dxdx = dx*dx;
		T dydy = dy*dy;

		if(j>=xstart && j<xend && i>=ystart && i<yend)
		{
			out[i*Nx+j] = -half*((b[i*Nx+j]*(dydy)*dxdx - dxdx*(x[(i-1)*Nx+j] + x[(i+1)*Nx+j]) - dydy*(x[i*Nx+(j-1)] + x[i*Nx+(j+1)]))/(dxdx+dydy));
		}
	};

	/**
	* Non-shared mem version.
	*
	* dim3 block = BLOCK2D(Nx,Ny);
	* dim3 grid  = GRID2D((Nx>>1)+1,(Ny>>1)+1); // Only half size in each direction
	*/
	template <typename value_type, typename size_type>
	__global__
		void gauss_seidel_rb_2d_v1(value_type* x
						  , value_type const* b
						  , value_type dx
						  , value_type dy
						  , size_t Nx
						  , size_t Ny
						  , size_type xstart
						  , size_type xend
						  , size_type ystart
						  , size_type yend
						  , int offset)
	{
		// compute the index of the output vector
		size_type j = 2*IDX1Dx + offset;
		size_type i = 2*IDX1Dy;

		value_type half = gpulab::values<value_type>::half();
		value_type dxdx = dx*dx;
		value_type dydy = dy*dy;

		if(j>=xstart && j<xend && i>=ystart && i<yend)
		{
			x[i*Nx+j] = -half*((b[i*Nx+j]*(dydy)*dxdx - dxdx*(x[(i-1)*Nx+j] + x[(i+1)*Nx+j]) - dydy*(x[i*Nx+(j-1)] + x[i*Nx+(j+1)]))/(dxdx+dydy));
		}
		++i;
		++j;
		if(j>=xstart && j<xend && i>=ystart && i<yend)
		{
			x[i*Nx+j] = -half*((b[i*Nx+j]*(dydy)*dxdx - dxdx*(x[(i-1)*Nx+j] + x[(i+1)*Nx+j]) - dydy*(x[i*Nx+(j-1)] + x[i*Nx+(j+1)]))/(dxdx+dydy));
		}
	};

	/*
	* Kernel for RB Gauss Seidel.
	* Only works on internal points, and assumes there is at least one layer all around internal points.
	*/
	template <typename value_type, typename size_type>
	__global__
	void gauss_seidel_rb_2d_v2(value_type* x
						  , value_type const* b
						  , value_type dx
						  , value_type dy
						  , size_t Nx
						  , size_t Ny
						  , size_type xstart
						  , size_type xend
						  , size_type ystart
						  , size_type yend
						  , int offset)
	{
		// compute the index of the output vector
		size_type j = 2*IDX1Dx + offset;
		size_type i = 2*IDX1Dy;

		value_type half = gpulab::values<value_type>::half();
		value_type dxdx = dx*dx;
		value_type dydy = dy*dy;

		// Use shared memory
		value_type* smem = gpulab::device::shared_memory<value_type>::get_pointer();
		size_type tx = threadIdx.x;
		size_type ty = threadIdx.y;
		size_type bd = blockDim.x;
		
		smem[ty*bd+tx]   = x[i*Nx+j+1];
		smem[2*ty*bd+tx] = x[(i+1)*Nx+j];
		__syncthreads();

		value_type left, up, right, down;
		if(tx==0)
			left = x[i*Nx+(j-1)];
		else
			left = smem[ty*bd+tx-1];
		if(ty==0)
			up = x[(i-1)*Nx+j];
		else
			up = smem[(2*ty-1)*bd+tx];

		right = smem[ty*bd+tx];
		down = smem[2*ty*bd+tx];

		if(j>=xstart && j<xend && i>=ystart && i<yend)
		{
			x[i*Nx+j] = -half*((b[i*Nx+j]*(dydy)*dxdx - dxdx*(up + down) - dydy*(left + right))/(dxdx+dydy));
		}

		// Go to next
		++i;
		++j;

		smem[ty*bd+tx]   = x[i*Nx+j+1];
		smem[2*ty*bd+tx] = x[(i+1)*Nx+j];
		__syncthreads();

		if(tx==0)
			left = x[i*Nx+(j-1)];
		else
			left = smem[ty*bd+tx-1];
		if(ty==0)
			up = x[(i-1)*Nx+j];
		else
			up = smem[(2*ty-1)*bd+tx];

		right = smem[ty*bd+tx];
		down = smem[2*ty*bd+tx];

		if(j>=xstart && j<xend && i>=ystart && i<yend)
		{
			x[i*Nx+j] = -half*((b[i*Nx+j]*(dydy)*dxdx - dxdx*(up + down) - dydy*(left + right))/(dxdx+dydy));
		}
		

		//bool use = ((i>>1) == (i+1>>1)) == ((j>>1) == (j+1>>1)) == rb;
		
	};



}; // namespace kernel

	/**
	* 1D jacobi smoothing
	*/
	template <typename Types>
	class jacobi_1d
	{
	protected:
		typedef typename Types::value_type			value_type;
		typedef typename Types::vector_type			vector_type;
		typedef typename Types::matrix_type			matrix_type;
	
	public:

		jacobi_1d(matrix_type const& A)
		{


		}

		/**
		* Ax=b
		*/
		void mult(vector_type const& x, vector_type& b)
		{

		}

		void smooth(unsigned int iterations, vector_type& x, vector_type const& b)
		{

		}


	};

	
	/**
	* This is the jacobi smoother of a 2d second order elliptic eq.
	* Using a 3-point low-order stencil.
	*/
	template <typename types>
	class jacobi_2d
	{
	public:
		typedef typename types::value_type		value_type;
		typedef typename types::matrix_type		matrix_type;
		typedef typename types::monitor_type	monitor_type;
		
		matrix_type const& m_A;

		jacobi_2d(matrix_type const& A, monitor_type& m)
			: m_A(A)
		{
		}

		template <typename V>
		void smooth(unsigned int iter, V& x, V const& b)
		{
			this->smooth(iter,x,b,typename V::memory_space());
		}
		
		
	private:

		template <typename V>
		void smooth(unsigned int iter, V& x, V const& b, gpulab::host_memory)
		{
			unsigned int Nx = x.dimension().x;
			unsigned int Ny = x.dimension().y;
			V x_(x);
			// TODO: include the grid space?
			//real_type h = x.grid_space().x;
			//h = h*h;

			for(unsigned int it=0; it<iter; ++it)
			{
				for (unsigned int i = 1; i < Ny-1; ++i)
				{
					for (unsigned int j = 1; j < Nx-1; ++j)
					{
						x_[i*Nx+j] = 0.25f*(x[(i-1)*Nx+j] + x[(i+1)*Nx+j] + x[i*Nx+(j-1)] + x[i*Nx+(j+1)] - b[i*Nx+j]);
					}
				}
				x.swap(x_);
				m_A.satisfy_bc(x);
			}
		}


		
		template <typename V>
		void smooth(unsigned int iter, V& x, V const& b, gpulab::device_memory)
		{
			typedef typename V::value_type	value_type;
			typedef typename V::size_type	size_type;

			size_type Nx     = x.Nx();
			size_type Ny     = x.Ny();
			size_type xstart =    x.ghost0().x;
			size_type xend   = Nx-x.ghost1().x;
			size_type ystart =    x.ghost0().y;
			size_type yend   = Ny-x.ghost1().y;
			value_type dx    = x.grid_space().x;
			value_type dy    = x.grid_space().y;

			m_A.satisfy_bc(x);
			V x_(x);
			
			//value_type* x0 = RAW_PTR(x);
			//value_type* x1 = RAW_PTR(x_);
			//value_type* tmp;


			dim3 block = BLOCK2D(Nx,Ny);
			dim3 grid  = GRID2D(Nx,Ny);
			
			// TODO: add shared mem optimization
			//unsigned int smem = (block.x+2)*(block.y+2)*sizeof(value_type);
			for(unsigned int it=0; it<iter; ++it)
			{
				kernel::jacobi_smooth_2d<<<grid,block>>>(RAW_PTR(x),RAW_PTR(b),RAW_PTR(x_),dx,dy,Nx,Ny,xstart,xend,ystart,yend);
				swap(x,x_);
				m_A.satisfy_bc(x);
			}
		}
	};


	
	/**
	* This is the Red-Black Gauss-Seidel smoother of a 2d second order elliptic eq.
	* Using a 3-point low-order stencil.
	* Updating only internal points, either inside ghost layers or at least one point inside.
	*/
	template <typename types>
	class gauss_seidel_rb_2d
	{
	private:
		typedef typename types::matrix_type		matrix_type;

		matrix_type const& m_A;

	public:
		
		template <typename monitor_type>
		gauss_seidel_rb_2d(matrix_type const& A, monitor_type&)
			: m_A(A)
		{}

		template <typename V>
		void operator()(V& x, V const& b)
		{
			smooth(1,x,b);
		}

		template <typename V>
		void smooth(unsigned int iter, V& x, V const& b)
		{
			// Dispatch
			this->smooth(iter, x, b, typename V::memory_space());
		}
		
	private:

		template <typename V>
		void smooth(unsigned int iter, V& x, V const& b, gpulab::host_memory)
		{
			typedef typename V::value_type	value_type;
			typedef typename V::size_type	size_type;

			size_type Nx = x.dimension().x;
			size_type Ny = x.dimension().y;
			value_type half = gpulab::values<value_type>::half();
			value_type dx = x.grid_space().x;
			value_type dy = x.grid_space().y;
			dx = dx*dx;
			dy = dy*dy;

			for(unsigned int it=0; it<iter; ++it)
			{
				for (unsigned int i = 1; i < Ny-1; ++i)
				{
					for (unsigned int j = 1; j < Nx-1; ++j)
					{
						//x[i*Nx+j] = 0.25*(x[(i-1)*Nx+j] + x[(i+1)*Nx+j] + x[i*Nx+(j-1)] + x[i*Nx+(j+1)] - dx*b[i*Nx+j]); // This is for dx==dy
						x[i*Nx+j] = -half*((b[i*Nx+j]*dy*dx - dx*(x[(i-1)*Nx+j] + x[(i+1)*Nx+j]) - dy*(x[i*Nx+(j-1)] + x[i*Nx+(j+1)]))/(dx+dy));
					}
				}
			}
			
		}


		
		template <typename V>
		void smooth(unsigned int iter, V& x, V const& b, gpulab::device_memory)
		{
			typedef typename V::value_type	value_type;
			typedef typename V::size_type	size_type;
			
			size_type Nx     = x.Nx();
			size_type Ny     = x.Ny();
			size_type xstart = x.ghost0().x == 0 ? 1    :    x.ghost0().x;
			size_type xend   = x.ghost1().x == 0 ? Nx-1 : Nx-x.ghost1().x;
			size_type ystart = x.ghost0().y == 0 ? 1    :    x.ghost0().y;
			size_type yend   = x.ghost1().y == 0 ? Ny-1 : Ny-x.ghost1().y;
			
			value_type dx = x.grid_space().x;
			value_type dy = x.grid_space().y;

			dim3 block = BLOCK2D(Nx,Ny);
			dim3 grid  = GRID2D((Nx>>1)+1,(Ny>>1)+1); // Only half size in each direction
			for(unsigned int it=0; it<iter; ++it)
			{
				// Make sure boundary conditions are set
				m_A.satisfy_bc(x);

				gpulab::solvers::kernel::gauss_seidel_rb_2d_v1<<<grid,block>>>(RAW_PTR(x),RAW_PTR(b),dx,dy,Nx,Ny,xstart,xend,ystart,yend,0);	///< Red update
				gpulab::solvers::kernel::gauss_seidel_rb_2d_v1<<<grid,block>>>(RAW_PTR(x),RAW_PTR(b),dx,dy,Nx,Ny,xstart,xend,ystart,yend,1);	///< Black update
			}
		}
	};

} // namespace solvers

} // namespace gpulab

#endif // GPULAB_SOLVERS_MULTIGRID_HANDLERS_H
