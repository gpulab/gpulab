// ==========================================================================
// Multigrid Types
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_MULTIGRID_TYPES_H
#define GPULAB_SOLVERS_MULTIGRID_TYPES_H

#include <gpulab/solvers/multigrid_monitor.h>

namespace gpulab
{

namespace solvers
{
	/**
	* Multigrid type binder class
	*/ 
	template<
		  typename vector_type_
		, typename matrix_type_
		, template< typename > class smoother_type_
		, template< typename > class grid_handler_type_
		, typename monitor_type_ = gpulab::solvers::multigrid_monitor<typename vector_type_::value_type>
	>
	class multigrid_types
	{
	public:
		typedef multigrid_types<
		  vector_type_
		, matrix_type_
		, smoother_type_
		, grid_handler_type_
		, monitor_type_> types;

		typedef typename vector_type_::value_type	value_type;

		typedef vector_type_					vector_type;
		//typedef preconditioner_type_			preconditioner_type;
		typedef matrix_type_					matrix_type;
		typedef monitor_type_					monitor_type;
		typedef smoother_type_<types>			smoother_type;
		typedef grid_handler_type_<types>		grid_handler_type;

	};


} // namespace solvers

} // namespace gpulab

#endif // GPULAB_SOLVERS_MULTIGRID_TYPES_H