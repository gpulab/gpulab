// ==========================================================================
// Multigrid Solver
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_MULTIGRID_H
#define GPULAB_SOLVERS_MULTIGRID_H

#include <sstream>

#include <gpulab/type_traits.h>
#include <gpulab/solvers/multigrid_monitor.h>
#include <gpulab/solvers/multigrid_types.h>
#include <gpulab/solvers/multigrid_handlers.h>
#include <gpulab/solvers/multigrid_smoothers.h>

namespace gpulab
{

namespace solvers
{

	template <typename Types>
	class multigrid
	{
	public:
		typedef typename Types::value_type				value_type;
		typedef typename Types::vector_type				vector_type;
		typedef typename Types::matrix_type				matrix_type;
		//typedef typename Types::preconditioner_type		preconditioner_type;
		//typedef typename preconditioner_type::vector_type   pre_vector_type;
		typedef typename Types::smoother_type			smoother_type;
		typedef typename Types::grid_handler_type		grid_handler_type;
		typedef typename Types::monitor_type			monitor_type;

		smoother_type				m_smoother;		///<
	protected:

		monitor_type&				m_monitor;		///< Monitor for iterations, tolerance, smoother, cycle, and weighting
		grid_handler_type			m_grid_handler;	///<
		matrix_type const&			m_A;

		vector_type**				m_d;			///< Defect vectors at each level
		vector_type**				m_e;			///< Error vectors at each level
		vector_type**				m_b;			///< RHS vectors at each level
		int							m_K;			///< Flag that tells how many levels is allocated

	public:

		/**
		* Multigrid solver constructor
		* @param A matrix for solving Ax=b
		* @param monitor contains convergence history, iteration count etc.
		*/ 
		multigrid(matrix_type const& A, monitor_type& monitor)
			: m_monitor(monitor)
			, m_smoother(A,monitor)
			, m_A(A)
			, m_K(-1)
			, m_e(0)
			, m_d(0)
			, m_b(0)
		{ }

		/**
		* Destructor. Clean up from preallocated memory
		*/
		~multigrid()
		{
			// Clean up
			for(int k=0; k<=m_K; ++k)
			{
				if(m_d[k]) delete m_d[k];
				if(m_e[k]) delete m_e[k];
				if(m_b[k]) delete m_b[k];
			}

			if(m_d) delete[] m_d;
			if(m_e) delete[] m_e;
			if(m_b) delete[] m_b;
		}

		/**
		* Return monitor
		*/
		monitor_type& monitor()
		{
			return m_monitor;
		}

		/**
		* Set a user specific preconditioner
		* @param P preconditioner, implementing operator()
		*/
		//void set_preconditioner(preconditioner_type const& P)
		//{
		//	m_P = &P;
		//	m_smoother.set_preconditioner(P);
		//}

		/**
		* Solves the system Ax=b with multigrid
		* @param x Vector to solve for
		* @param b Right hand side
		*/
		void solve(vector_type& sol, vector_type const& rhs)
		{
			// Update the rhs for the monitor
			m_monitor.reset(rhs);
			m_monitor.timer().reset();

			// If first solve, allocate memory for each level
			initialize_memory(sol);

			// Allocate space for residual and correction:
			vector_type* res = sol.duplicate();
			vector_type* cor = sol.duplicate();

			// Satisfy boundary conditions on initial solution:
//			m_A.satisfy_bc_init(sol,rhs);
			m_A.satisfy_bc(sol,rhs);

			// Calculate initial residual:
			//m_A.mult(sol,*res);
			//res.axpby(1, -1, rhs);
			m_A.residual(sol,rhs,*res);
			
			// Stop based on first residual
			if(!m_monitor.finished(*res))
			{
				++m_monitor;
				while(1)
				{
					// Correction guess:
					cor->fill(0);

					// Calculate correction:
					(*this)(*cor,*res);

					// Update solution:
					sol.axpy(gpulab::values<value_type>::one(),*cor);

					// Next iteration:
					++m_monitor;

					// Calculate residual:
					m_A.mult(sol,*res);
					res->axpby(1, -1, rhs);

					// Calculate relative residual / close enough to stop based on correction or residual:
					if(m_monitor.finished(*res))
						break;
				}
			}

			delete res;
			delete cor;

			m_monitor.timer().stop();


			//// Start geometric cycles
			//GMM(x,b);

			//// End timer
			//m_monitor.timer().stop();
		}

		
		/**
		* This operator is invoked when applying a multigrid cycle, usually as a preconditioner
		* @param x Vector to solve for
		* @param b Right hand side
		*/
		template <typename grid_other>
		void operator()(grid_other& x, grid_other const& b)
		{
			vector_type x_(x); // Hmm.. no need to copy zero quess
			vector_type b_(b);

			(*this)(x_,b_);

			x.copy(x_);
		}

		/**
		* This operator is invoked when applying a multigrid cycle, usually as a preconditioner
		* @param x Vector to solve for
		* @param b Right hand side
		*/
		void operator()(vector_type& x, vector_type const& b)
		{
			initialize_memory(x);

			// Start timer
			//m_monitor.timer().start();
			unsigned int gamma = (m_monitor.cycle()==F || m_monitor.cycle()==W) ? 2: 1; // Gamma determines cycle structure

			if(m_monitor.cycle()==FMG)
				FMGCYC(0,m_A,x,b);
			else
				MGCYC(0,gamma,m_A,x,b);
			//printf("\n\n");

			// End timer
			//m_monitor.timer().stop();
		}

	protected:

		/**
		* Initializes memory to be used for every MG level
		*/
		void initialize_memory(vector_type const& x)
		{
			if(m_K < 0)
			{
				int Kmax = m_monitor.K();
				if(Kmax<0)
				{
					// Max possible levels
					Kmax = (int)round(log2((double)max((int)x.Nx(),max((int)x.Ny(),(int)x.Nz()))-1)) - 2;
				}

				m_e = new vector_type*[Kmax+1];
				m_d = new vector_type*[Kmax+1];
				m_b = new vector_type*[Kmax+1];

				// Allocate memory for finest level
				m_d[0] = x.duplicate();
				m_e[0] = 0;
				m_b[0] = 0;
				GPULAB_LOG_DBG("MG allocated (%4i,%4i,%4i)\n",m_d[0]->iNx(),m_d[0]->iNy(),m_d[0]->iNz());

				// Allocate memory for k'th level
				int k = 0;
				int stop = 0;
				for(; k<Kmax; ++k)
				{
					m_d[k+1] = m_grid_handler.HALF(*m_d[k]);

					stop += m_d[k+1]->size() == m_d[k]->size();
					if(m_d[k+1]->distributed())
					{
						if(stop)
						{
							GPULAB_LOG_DBG("MG: I connot restrict furter and request a stop\n");
						}
						int recv;
						MPI_Allreduce(&stop, &recv, 1, MPI_INT, MPI_SUM, m_d[k+1]->topology().COMM);
						stop += recv;
					}

					// Clean up and break if nothing halfed or we reached a grid with even dimensions
					if(stop > 0)
					{
						delete m_d[k+1];
						break;
					}

					m_e[k+1] = m_d[k+1]->duplicate();
					m_b[k+1] = m_d[k+1]->duplicate();
					GPULAB_LOG_DBG("MG allocated (%4i,%4i,%4i)\n",m_d[k+1]->iNx(),m_d[k+1]->iNy(),m_d[k+1]->iNz());

					// If one dim is even, stop next iter
					stop += ((m_d[k+1]->iNx()+1) % 2) + ((m_d[k+1]->iNy()+1) % 2) + ((m_d[k+1]->iNz()+1) % 2);
				}
				m_K = k;
			}
			
		}

		/**
		* Geometric Multigrid - works pretty much as defect correction
		* @param gamma determines cycle structure
		* @param x vector to solve for
		* @param b right hand side
		*/
		void GMM(vector_type& x, vector_type const& b)
		{
			vector_type* r = x.duplicate();		// Residual vector
			vector_type* d = x.duplicate();		// Defect correction vector

			while (1)
			{
				// Calculate residual
				m_A.satisfy_bc(x,b);
				m_A.mult(x,*r);
				r->axpby(1.,-1.,b);

				// Done yet
				if(m_monitor.finished(*r))
				{
					break;
				}

				// Reset quess
				d->fill(0);

				// Start MG cycles
				(*this)(*d,*r);

				// Update solution
				x.axpy(1.,*d);
				++m_monitor;
			}
		}

		/**
		* Full Multigrid Cycle
		*/
		template <typename M, typename V>
		void FMGCYC(unsigned int K	// MG level
				   , M const& A		// Matrix A
				   , V& x			// Guess, holds the solution after execution
				   , V const& b)	// Right hand side of eq to be solved
		{
			if(K<m_K)
			{
				V* cx = m_grid_handler.HALF(x);
				V* cb = m_grid_handler.HALF(b);

				A.satisfy_bc(x);
				//A.satisfy_bc(b);

				m_grid_handler.restrict(x,*cx);
				m_grid_handler.restrict(b,*cb);

				FMGCYC(K+1,A,*cx,*cb);

				A.satisfy_bc(*cx);
				m_grid_handler.prolongate(x,*cx);

				delete cx;
				delete cb;
			}

			// Do one cycle
			MGCYC(K,1,A,x,b);
		}

		/**
		* Multigrid cycle
		*/
		template <typename M, typename V>
		unsigned int MGCYC( unsigned int K  // Number of restrictions/interpolations
				   , unsigned int gamma     // Gamma
				   , M const& A   // Matrix A
				   , V& x         // Guess, holds the solution after execution
				   , V const& b)  // Right hand side of eq to be solved
		{
			//printf("K = %d\n", K);
			typedef typename V::size_type	size_type;

			if(K>=m_K) // Coarsest level
			{
				// Iterate vc times on coarsest level
				m_smoother.smooth(m_monitor.vc(),x,b);

				if (m_monitor.cycle()==F)
					gamma = 1;
			}
			else
			{
				// Start pre-smoothing
				m_smoother.smooth(m_monitor.v1(),x,b);

				// Compute defect
				A.satisfy_bc(x);
				A.residual(x,b,*m_d[K]);
				
				// Make sure boundary conditions are set before restriction
				A.satisfy_bc(*m_d[K]); 
				
				// Restrict defect
				m_grid_handler.restrict(*m_d[K],*m_b[K+1]);
				
				// Reset guess for next iteration
				m_e[K+1]->fill(0.0);

				// Recursive restriction solve
				unsigned int iter = gamma;
				for(unsigned int i=1; i<=iter; ++i)
				{
					gamma = MGCYC(K+1,gamma,A,*m_e[K+1],*m_b[K+1]);
					//printf("K = %d\n", K);

					if(K==0) // Ensures only one outer iteration
						break;
				}

				// Make sure boundary conditions are set before prolongation
				A.satisfy_bc(*m_e[K+1]);
				
				// Prolongate the error estimate
				m_d[K]->fill(0.0);
				m_grid_handler.prolongate(*m_d[K],*m_e[K+1]);

				// Make sure boundary conditions are set after prolongation
				//A.satisfy_bc(*m_d[K]);

				// Correction
				x.axpy(1.,*m_d[K]);
				
				// Start post-smoothing
				m_smoother.smooth(m_monitor.v2(),x,b);
			}
			// Make sure boundary conditions are set before we leave
			//A.satisfy_bc(x);

			return gamma;
		}
	};
	

} // namespace solvers

} // namespace gpulab

#endif // GPULAB_SOLVERS_MULTIGRID_H
