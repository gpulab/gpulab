// ==========================================================================
// Multigrid Restrictions
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_MULTIGRID_HANDLERS_H
#define GPULAB_SOLVERS_MULTIGRID_HANDLERS_H

#include <gpulab/type_traits.h>
#include <gpulab/solvers/multigrid_types.h>

namespace gpulab
{
namespace solvers
{
namespace kernel
{
	/**
	 * Prolongation gather operations
	 */
	template <typename value_type, typename size_type>
	__global__
	void prolongate_x(value_type* fine, value_type const* coarse, size_type Nxf, size_type Nyf, size_type Nxc, size_type Nyc, size_type xstart, size_type xend, size_type ystart, size_type yend)
	{
		size_type xf = IDX1Dx + xstart;
		size_type yf = IDX1Dy + ystart;

		if(yf<yend && xf<xend)
		{
			// Constants
			value_type half = gpulab::values<value_type>::half();

			size_type xc = (xf+xstart)/2;
			if((xf-xstart) % 2 == 0) // Injection
			{
				fine[idx(xf,yf,Nxf)] = coarse[idx(xc,yf,Nxc)];
			}
			else // Bi-interpolate
			{
				fine[idx(xf,yf,Nxf)] = half*(coarse[idx(xc,yf,Nxc)] + coarse[idx(xc+1,yf,Nxc)]);
			}
		}
	}

	/**
	 * Prolongation gather operations
	 */
	template <typename value_type, typename size_type>
	__global__
	void prolongate_y(value_type* fine, value_type const* coarse, size_type Nxf, size_type Nyf, size_type Nxc, size_type Nyc, size_type xstart, size_type xend, size_type ystart, size_type yend)
	{
		size_type xf = IDX1Dx + xstart;
		size_type yf = IDX1Dy + ystart;

		if(yf<yend && xf<xend)
		{
			// Constants
			value_type half = gpulab::values<value_type>::half();

			size_type yc = (yf+ystart)/2;
			if((yf-ystart) % 2 == 0) // Injection
			{
				fine[idx(xf,yf,Nxf)] = coarse[idx(xf,yc,Nxc)];
			}
			else // Bi-interpolate
			{
				fine[idx(xf,yf,Nxf)] = half*(coarse[idx(xf,yc,Nxc)] + coarse[idx(xf,yc+1,Nxc)]);
			}
		}
	}


	template <typename value_type, typename size_type>
	__global__
	void restrict(value_type const* fine, value_type* coarse, size_type Nxf, size_type Nyf, size_type Nxc, size_type Nyc, size_type xstart, size_type xend, size_type ystart, size_type yend)
	{
		size_type j = IDX1Dx + xstart;		// Coarse ids
		size_type i = IDX1Dy + ystart + 1;  // Add one because of surface layer

		size_type i2 = i*2-ystart;
		size_type j2 = j*2-xstart;
			
		// Constants
		value_type oneeight(0.125);
		value_type four(4);

		value_type f  = fine[i2*Nxf+j2];
		value_type fl = (j==0)    ?  f : fine[i2*Nxf+j2-1];
		value_type fr = (j>=Nxc-1) ? f : fine[i2*Nxf+j2+1];
		value_type ft = (i==0)    ?  f : fine[(i2-1)*Nxf+j2];
		value_type fb = (i>=Nyc-1) ? f : fine[(i2+1)*Nxf+j2];

		if(i<yend && j<xend)
		{
			coarse[i*Nxc+j] = oneeight*(four*f + fl + fr + ft + fb);
		}
	}

	template <typename value_type, typename size_type>
	__global__
	void restrict_x(value_type const* fine, value_type* coarse, size_type Nx, size_type Ny, size_type Nxh, size_type Nyh, size_type xstart, size_type xend, size_type ystart, size_type yend)
	{
		size_type j = IDX1Dx + xstart;		// Coarse ids
		size_type i = IDX1Dy + ystart + 1;  // Add one because of surface layer

		// Constants
		value_type onefourth = gpulab::values<value_type>::quarter();
		value_type two = gpulab::values<value_type>::two();

		if(i<yend && j<xend)
		{
			size_type j2 = j*2-xstart;
			coarse[i*Nxh+j] = onefourth*(two*fine[i*Nx+j2] + fine[i*Nx+(j2-1)] + fine[i*Nx+(j2+1)]); // TODO: If there is no ghost points this is bad for j=0 !!!
		}
	}

	// 3D kernels
	template <typename value_type, typename dim_type>
	__global__
	void FD3DMG_prolongate_gather( value_type* fine
					  , value_type const* coarse
					  , dim_type Nf
					  , dim_type Nc
					  , dim_type g0
					  , dim_type g1
					  , dim_type off0
					  , dim_type off1)
	{
		unsigned int xf = IDX1Dx + g0.x + off0.x;
		unsigned int yf = IDX1Dy + g0.y + off0.y;
		unsigned int zf =          g0.z + off0.z;

		if(xf<Nf.x-g1.x-off1.x && yf<Nf.y-g1.y-off1.y)
		{
			// Constants
			value_type half = gpulab::values<value_type>::half();

			if(Nc.x < Nf.x) // x-prolongation
			{
				unsigned int xc = (xf+g0.x)/2;
				if((xf-g0.x) % 2 == 0) // Injection
				{
					for(; zf<Nf.z-g1.z-off1.z; ++zf)
					{
						fine[idx(xf,yf,zf,Nf.x,Nf.y)] = coarse[idx(xc,yf,zf,Nc.x,Nc.y)];
					}
				}
				else // Bi-interpolate
				{	
					for(; zf<Nf.z-g1.z-off1.z; ++zf)
					{
						fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xc,yf,zf,Nc.x,Nc.y)] + coarse[idx(xc+1,yf,zf,Nc.x,Nc.y)]);
					}
				}
			}

			if(Nc.y < Nf.y) // y-prolongation
			{
				unsigned int yc = (yf+g0.y)/2;
				if((yf-g0.y) % 2 == 0) // Injection
				{
					for(; zf<Nf.z-g1.z-off1.z; ++zf)
					{
						fine[idx(xf,yf,zf,Nf.x,Nf.y)] = coarse[idx(xf,yc,zf,Nc.x,Nc.y)];
					}
				}
				else // Bi-interpolate
				{	
					for(; zf<Nf.z-g1.z-off1.z; ++zf)
					{
						fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xf,yc,zf,Nc.x,Nc.y)] + coarse[idx(xf,yc+1,zf,Nc.x,Nc.y)]);
					}
				}
			}

			if(Nc.z < Nf.z) // z-prolongation
			{
				for(; zf<Nf.z-g1.z-off1.z; ++zf)
				{
					unsigned int zc = (zf+g0.z)/2;
					if((zf-g0.z) % 2 == 0) // Injection
					{
						fine[idx(xf,yf,zf,Nf.x,Nf.y)] = coarse[idx(xf,yf,zc,Nc.x,Nc.y)];
					}
					else // Bi-interpolate
					{
						fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xf,yf,zc,Nc.x,Nc.y)] + coarse[idx(xf,yf,zc+1,Nc.x,Nc.y)]);
					}
				}
			}
		}
	};

	template <typename value_type, typename dim_type>
	__global__
	void FD3DMG_prolongate_gather_xyz( value_type* fine
					  , value_type const* coarse
					  , dim_type Nf
					  , dim_type Nc
					  , dim_type g0
					  , dim_type g1
					  , dim_type off0
					  , dim_type off1)
	{
		unsigned int xf = IDX1Dx + g0.x + off0.x;
		unsigned int yf = IDX1Dy + g0.y + off0.y;
		unsigned int zf =          g0.z + off0.z;

		if(xf<Nf.x-g1.x-off1.x && yf<Nf.y-g1.y-off1.y)
		{
			// Constants
			value_type half = gpulab::values<value_type>::half();

			for(; zf<Nf.z-g1.z-off1.z; ++zf)
			{
				unsigned int xc = (xf+g0.x)/2;
				unsigned int yc = (yf+g0.y)/2;
				unsigned int zc = (zf+g0.z)/2;

				bool evenx = ((xf-g0.x) % 2) == 0;
				bool eveny = ((yf-g0.y) % 2) == 0;
				bool evenz = ((zf-g0.z) % 2) == 0;

				// Injection
				if(evenx && eveny && evenz)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = coarse[idx(xc,yc,zc,Nc.x,Nc.y)];
				// Linear interpolation in z
				else if(evenx && eveny)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xc,yc,zc,Nc.x,Nc.y)] + coarse[idx(xc,yc,zc+1,Nc.x,Nc.y)]);
				// Linear interpolation in y
				else if(evenx && evenz)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xc,yc,zc,Nc.x,Nc.y)] + coarse[idx(xc,yc+1,zc,Nc.x,Nc.y)]);
				// Linear interpolation in x
				else if(evenz && eveny)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xc,yc,zc,Nc.x,Nc.y)] + coarse[idx(xc+1,yc,zc,Nc.x,Nc.y)]);
				// Bi-linear interpolation in yz
				else if(evenx)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*half*(coarse[idx(xc,yc  ,zc  ,Nc.x,Nc.y)]
															 + coarse[idx(xc,yc  ,zc+1,Nc.x,Nc.y)]
															 + coarse[idx(xc,yc+1,zc  ,Nc.x,Nc.y)]
															 + coarse[idx(xc,yc+1,zc+1,Nc.x,Nc.y)]);
				// Bi-linear interpolation in xz
				else if(eveny)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*half*(coarse[idx(xc  ,yc,zc  ,Nc.x,Nc.y)]
															 + coarse[idx(xc  ,yc,zc+1,Nc.x,Nc.y)]
															 + coarse[idx(xc+1,yc,zc  ,Nc.x,Nc.y)]
															 + coarse[idx(xc+1,yc,zc+1,Nc.x,Nc.y)]);
				// Bi-linear interpolation in xy
				else if(evenz)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*half*(coarse[idx(xc  ,yc  ,zc,Nc.x,Nc.y)]
															 + coarse[idx(xc+1,yc  ,zc,Nc.x,Nc.y)]
															 + coarse[idx(xc  ,yc+1,zc,Nc.x,Nc.y)]
															 + coarse[idx(xc+1,yc+1,zc,Nc.x,Nc.y)]);
				// Bi-linear interpolation in xyz
				else
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*half*half*(coarse[idx(xc  ,yc  ,zc  ,Nc.x,Nc.y)]
																  + coarse[idx(xc  ,yc  ,zc+1,Nc.x,Nc.y)]
																  + coarse[idx(xc  ,yc+1,zc  ,Nc.x,Nc.y)]
																  + coarse[idx(xc  ,yc+1,zc+1,Nc.x,Nc.y)]
																  + coarse[idx(xc+1,yc  ,zc  ,Nc.x,Nc.y)]
																  + coarse[idx(xc+1,yc  ,zc+1,Nc.x,Nc.y)]
																  + coarse[idx(xc+1,yc+1,zc  ,Nc.x,Nc.y)]
																  + coarse[idx(xc+1,yc+1,zc+1,Nc.x,Nc.y)]);
			}
		}
	};


	template <typename value_type, typename dim_type>
	__global__
	void FD3DMG_prolongate_gather_xy( value_type* fine
					  , value_type const* coarse
					  , dim_type Nf
					  , dim_type Nc
					  , dim_type g0
					  , dim_type g1
					  , dim_type off0
					  , dim_type off1)
	{
		unsigned int xf = IDX1Dx + g0.x + off0.x;
		unsigned int yf = IDX1Dy + g0.y + off0.y;
		unsigned int zf =          g0.z + off0.z;

		if(xf<Nf.x-g1.x-off1.x && yf<Nf.y-g1.y-off1.y)
		{
			// Constants
			value_type half = gpulab::values<value_type>::half();

			for(; zf<Nf.z-g1.z-off1.z; ++zf)
			{
				unsigned int xc = (xf+g0.x)/2;
				unsigned int yc = (yf+g0.y)/2;

				bool evenx = ((xf-g0.x) % 2) == 0;
				bool eveny = ((yf-g0.y) % 2) == 0;
				// Injection
				if(evenx && eveny)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = coarse[idx(xc,yc,zf,Nc.x,Nc.y)];
				// Linear interpolation in y
				else if (evenx)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xc,yc,zf,Nc.x,Nc.y)] + coarse[idx(xc,yc+1,zf,Nc.x,Nc.y)]);
				// Linear interpolation in x
				else if (eveny)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xc,yc,zf,Nc.x,Nc.y)] + coarse[idx(xc+1,yc,zf,Nc.x,Nc.y)]);
				// Bi-linear interpolation
				else
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*half*(coarse[idx(xc  ,yc  ,zf,Nc.x,Nc.y)]
															 + coarse[idx(xc+1,yc  ,zf,Nc.x,Nc.y)]
															 + coarse[idx(xc  ,yc+1,zf,Nc.x,Nc.y)]
															 + coarse[idx(xc+1,yc+1,zf,Nc.x,Nc.y)]);
			}
		}
	};

	template <typename value_type, typename dim_type>
	__global__
	void FD3DMG_prolongate_gather_xz( value_type* fine
					  , value_type const* coarse
					  , dim_type Nf
					  , dim_type Nc
					  , dim_type g0
					  , dim_type g1
					  , dim_type off0
					  , dim_type off1)
	{
		unsigned int xf = IDX1Dx + g0.x + off0.x;
		unsigned int yf = IDX1Dy + g0.y + off0.y;
		unsigned int zf =          g0.z + off0.z;

		if(xf<Nf.x-g1.x-off1.x && yf<Nf.y-g1.y-off1.y)
		{
			// Constants
			value_type half = gpulab::values<value_type>::half();

			for(; zf<Nf.z-g1.z-off1.z; ++zf)
			{
				unsigned int xc = (xf+g0.x)/2;
				unsigned int zc = (zf+g0.z)/2;

				bool evenx = ((xf-g0.x) % 2) == 0;
				bool evenz = ((zf-g0.z) % 2) == 0;
				// Injection
				if(evenx && evenz)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = coarse[idx(xc,yf,zc,Nc.x,Nc.y)];
				// Linear interpolation in z
				else if (evenx)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xc,yf,zc,Nc.x,Nc.y)] + coarse[idx(xc,yf,zc+1,Nc.x,Nc.y)]);
				// Linear interpolation in x
				else if (evenz)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xc,yf,zc,Nc.x,Nc.y)] + coarse[idx(xc+1,yf,zc,Nc.x,Nc.y)]);
				// Bi-linear interpolation
				else
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*half*(coarse[idx(xc  ,yf,zc  ,Nc.x,Nc.y)]
															 + coarse[idx(xc+1,yf,zc  ,Nc.x,Nc.y)]
															 + coarse[idx(xc  ,yf,zc+1,Nc.x,Nc.y)]
															 + coarse[idx(xc+1,yf,zc+1,Nc.x,Nc.y)]);
			}
		}
	};

	
	template <typename value_type, typename dim_type>
	__global__
	void FD3DMG_prolongate_gather_yz( value_type* fine
					  , value_type const* coarse
					  , dim_type Nf
					  , dim_type Nc
					  , dim_type g0
					  , dim_type g1
					  , dim_type off0
					  , dim_type off1)
	{
		unsigned int xf = IDX1Dx + g0.x + off0.x;
		unsigned int yf = IDX1Dy + g0.y + off0.y;
		unsigned int zf =          g0.z + off0.z;

		if(xf<Nf.x-g1.x-off1.x && yf<Nf.y-g1.y-off1.y)
		{
			// Constants
			value_type half = gpulab::values<value_type>::half();

			for(; zf<Nf.z-g1.z-off1.z; ++zf)
			{
				unsigned int yc = (yf+g0.y)/2;
				unsigned int zc = (zf+g0.z)/2;

				bool eveny = ((yf-g0.y) % 2) == 0;
				bool evenz = ((zf-g0.z) % 2) == 0;
				// Injection
				if(eveny && evenz)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = coarse[idx(xf,yc,zc,Nc.x,Nc.y)];
				// Linear interpolation in z
				else if (eveny)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xf,yc,zc,Nc.x,Nc.y)] + coarse[idx(xf,yc,zc+1,Nc.x,Nc.y)]);
				// Linear interpolation in y
				else if (evenz)
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*(coarse[idx(xf,yc,zc,Nc.x,Nc.y)] + coarse[idx(xf,yc+1,zc,Nc.x,Nc.y)]);
				// Bi-linear interpolation
				else
					fine[idx(xf,yf,zf,Nf.x,Nf.y)] = half*half*(coarse[idx(xf,yc  ,zc  ,Nc.x,Nc.y)]
															 + coarse[idx(xf,yc+1,zc  ,Nc.x,Nc.y)]
															 + coarse[idx(xf,yc  ,zc+1,Nc.x,Nc.y)]
															 + coarse[idx(xf,yc+1,zc+1,Nc.x,Nc.y)]);
			}
		}
	};

	/**
	* Restricts all three dimensions at once using half weighting
	*/
	template <typename value_type, typename size_type>
	__global__
	void FD3DMG_restriction_half_weighting(  value_type const* fine
	                 , value_type* coarse
					 , size_type Nxf
					 , size_type Nyf
					 , size_type Nzf
					 , size_type Nxc
					 , size_type Nyc
					 , size_type Nzc
					 , size_type xstart
					 , size_type xend
					 , size_type ystart
					 , size_type yend
					 , size_type zstart
					 , size_type zend)
	{
		size_type xc = IDX1Dx + xstart;	// Coarse ids
		size_type yc = IDX1Dy + ystart;
		size_type zc = 1;				// Start from 1 because of surface layer

		if(yc<yend && xc<xend)
		{
			// Constants
			value_type onetwelfth(1.f/12.f);
			value_type six(6);

			size_type xf = xc*2-xstart;
			size_type yf = yc*2-ystart;
			
			for(; zc<zend; ++zc)
			{
				size_type zf = zc*2-zstart;
				coarse[idx(xc,yc,zc,Nxc,Nyc)] = onetwelfth*( six*fine[idx(xf  ,yf  ,zf  ,Nxf,Nyf)]
													           + fine[idx(xf+1,yf  ,zf  ,Nxf,Nyf)]
													 	       + fine[idx(xf-1,yf  ,zf  ,Nxf,Nyf)]
															   + fine[idx(xf  ,yf+1,zf  ,Nxf,Nyf)]
															   + fine[idx(xf  ,yf-1,zf  ,Nxf,Nyf)]
															   + fine[idx(xf  ,yf  ,zf+1,Nxf,Nyf)]
															   + fine[idx(xf  ,yf  ,zf-1,Nxf,Nyf)]);
			}
		}
	};

	/**
	* Restricts all three dimensions at once using full weighting
	*/
	template <typename value_type, typename dim_type>
	__global__
		void FD3DMG_restriction_full_weighting_xyz(value_type const* fine
		, value_type* coarse
		, dim_type Nf
		, dim_type Nc
		, dim_type g0
		, dim_type g1
		, dim_type off0
		, dim_type off1)
	{
		unsigned int xc = IDX1Dx + g0.x+off0.x;	// Coarse ids
		unsigned int yc = IDX1Dy + g0.y+off0.y;
		unsigned int zc = g0.z + off0.z;

		if(xc<Nc.x-g1.x-off1.x && yc<Nc.y-g1.y-off1.y)
		{
			// Constants
			value_type onetwelfth(1.f/12.f);
			value_type one(1.0);
			value_type two(2.0);
			value_type four(4.0);
			value_type eight(8.0);
			value_type sixtyfour_inv(1.0/64.0);

			unsigned int xf = xc*2-g0.x;
			unsigned int yf = yc*2-g0.y;

			for(; zc<Nc.z-g1.z-off1.z; ++zc)
			{
				unsigned int zf = zc*2-g0.z;

				coarse[idx(xc,yc,zc,Nc.x,Nc.y)] = sixtyfour_inv*( 
				+   one*fine[idx(xf-1, yf-1, zf-1, Nf.x, Nf.y)]
				+   two*fine[idx(xf  , yf-1, zf-1, Nf.x, Nf.y)]
				+   one*fine[idx(xf+1, yf-1, zf-1, Nf.x, Nf.y)]
				+   two*fine[idx(xf-1, yf  , zf-1, Nf.x, Nf.y)]
				+  four*fine[idx(xf  , yf  , zf-1, Nf.x, Nf.y)]
				+   two*fine[idx(xf+1, yf  , zf-1, Nf.x, Nf.y)]
				+   one*fine[idx(xf-1, yf+1, zf-1, Nf.x, Nf.y)]
				+   two*fine[idx(xf  , yf+1, zf-1, Nf.x, Nf.y)]
				+   one*fine[idx(xf+1, yf+1, zf-1, Nf.x, Nf.y)]
				+   two*fine[idx(xf-1, yf-1, zf  , Nf.x, Nf.y)]
				+  four*fine[idx(xf  , yf-1, zf  , Nf.x, Nf.y)]
				+   two*fine[idx(xf+1, yf-1, zf  , Nf.x, Nf.y)]
				+  four*fine[idx(xf-1, yf  , zf  , Nf.x, Nf.y)]
				+ eight*fine[idx(xf  , yf  , zf  , Nf.x, Nf.y)]
				+  four*fine[idx(xf+1, yf  , zf  , Nf.x, Nf.y)]
				+   two*fine[idx(xf-1, yf+1, zf  , Nf.x, Nf.y)]
				+  four*fine[idx(xf  , yf+1, zf  , Nf.x, Nf.y)]
				+   two*fine[idx(xf+1, yf+1, zf  , Nf.x, Nf.y)]
				+   one*fine[idx(xf-1, yf-1, zf+1, Nf.x, Nf.y)]
				+   two*fine[idx(xf  , yf-1, zf+1, Nf.x, Nf.y)]
				+   one*fine[idx(xf+1, yf-1, zf+1, Nf.x, Nf.y)]
				+   two*fine[idx(xf-1, yf  , zf+1, Nf.x, Nf.y)]
				+  four*fine[idx(xf  , yf  , zf+1, Nf.x, Nf.y)]
				+   two*fine[idx(xf+1, yf  , zf+1, Nf.x, Nf.y)]
				+   one*fine[idx(xf-1, yf+1, zf+1, Nf.x, Nf.y)]
				+   two*fine[idx(xf  , yf+1, zf+1, Nf.x, Nf.y)]
				+   one*fine[idx(xf+1, yf+1, zf+1, Nf.x, Nf.y)]);
			}
		}
	};

	/**
	* Restricts xy dimensions at once using full weighting
	*/
	template <typename value_type, typename dim_type>
	__global__
		void FD3DMG_restriction_full_weighting_xy(value_type const* fine
		, value_type* coarse
		, dim_type Nf
		, dim_type Nc
		, dim_type g0
		, dim_type g1
		, dim_type off0
		, dim_type off1)
	{
		unsigned int xc = IDX1Dx + g0.x+off0.x;	// Coarse ids
		unsigned int yc = IDX1Dy + g0.y+off0.y;
		unsigned int z = g0.z + off0.z;

		if(xc<Nc.x-g1.x-off1.x && yc<Nc.y-g1.y-off1.y)
		{
			// Constants
			value_type one(1.0);
			value_type two(2.0);
			value_type four(4.0);
			value_type sixteen_inv(1.0/16.0);

			unsigned int xf = xc*2-g0.x;
			unsigned int yf = yc*2-g0.y;

			for(; z<Nc.z-g1.z-off1.z; ++z)
			{
				coarse[idx(xc,yc,z,Nc.x,Nc.y)] = sixteen_inv*( 
				+  one*fine[idx(xf-1, yf-1, z  , Nf.x, Nf.y)]
				+  two*fine[idx(xf  , yf-1, z  , Nf.x, Nf.y)]
				+  one*fine[idx(xf+1, yf-1, z  , Nf.x, Nf.y)]
				+  two*fine[idx(xf-1, yf  , z  , Nf.x, Nf.y)]
				+ four*fine[idx(xf  , yf  , z  , Nf.x, Nf.y)]
				+  two*fine[idx(xf+1, yf  , z  , Nf.x, Nf.y)]
				+  one*fine[idx(xf-1, yf+1, z  , Nf.x, Nf.y)]
				+  two*fine[idx(xf  , yf+1, z  , Nf.x, Nf.y)]
				+  one*fine[idx(xf+1, yf+1, z  , Nf.x, Nf.y)]);
			}
		}
	};

	
	/**
	* Restricts xz dimensions at once using full weighting
	*/
	template <typename value_type, typename dim_type>
	__global__
		void FD3DMG_restriction_full_weighting_xz(value_type const* fine
		, value_type* coarse
		, dim_type Nf
		, dim_type Nc
		, dim_type g0
		, dim_type g1
		, dim_type off0
		, dim_type off1)
	{
		unsigned int xc = IDX1Dx + g0.x+off0.x;	// Coarse ids
		unsigned int y  = IDX1Dy + g0.y+off0.y;
		unsigned int zc = g0.z + off0.z;

		if(xc<Nc.x-g1.x-off1.x && y<Nc.y-g1.y-off1.y)
		{
			// Constants
			value_type one(1.0);
			value_type two(2.0);
			value_type four(4.0);
			value_type sixteen_inv(1.0/16.0);

			unsigned int xf = xc*2-g0.x;

			for(; zc<Nc.z-g1.z-off1.z; ++zc)
			{
				unsigned int zf = zc*2-g0.z;

				coarse[idx(xc,y,zc,Nc.x,Nc.y)] = sixteen_inv*( 
				+  one*fine[idx(xf-1, y  , zf-1, Nf.x, Nf.y)]
				+  two*fine[idx(xf  , y  , zf-1, Nf.x, Nf.y)]
				+  one*fine[idx(xf+1, y  , zf-1, Nf.x, Nf.y)]
				+  two*fine[idx(xf-1, y  , zf  , Nf.x, Nf.y)]
				+ four*fine[idx(xf  , y  , zf  , Nf.x, Nf.y)]
				+  two*fine[idx(xf+1, y  , zf  , Nf.x, Nf.y)]
				+  one*fine[idx(xf-1, y  , zf+1, Nf.x, Nf.y)]
				+  two*fine[idx(xf  , y  , zf+1, Nf.x, Nf.y)]
				+  one*fine[idx(xf+1, y  , zf+1, Nf.x, Nf.y)]);
			}
		}
	};

	
	/**
	* Restricts yz dimensions at once using full weighting
	*/
	template <typename value_type, typename dim_type>
	__global__
		void FD3DMG_restriction_full_weighting_yz(value_type const* fine
		, value_type* coarse
		, dim_type Nf
		, dim_type Nc
		, dim_type g0
		, dim_type g1
		, dim_type off0
		, dim_type off1)
	{
		unsigned int x  = IDX1Dx + g0.x+off0.x;	// Coarse ids
		unsigned int yc = IDX1Dy + g0.y+off0.y;
		unsigned int zc = g0.z + off0.z;

		if(x<Nc.x-g1.x-off1.x && yc<Nc.y-g1.y-off1.y)
		{
			// Constants
			value_type one(1.0);
			value_type two(2.0);
			value_type four(4.0);
			value_type sixteen_inv(1.0/16.0);

			unsigned int yf = yc*2-g0.y;

			for(; zc<Nc.z-g1.z-off1.z; ++zc)
			{
				unsigned int zf = zc*2-g0.z;

				coarse[idx(x,yc,zc,Nc.x,Nc.y)] = sixteen_inv*( 
				+  one*fine[idx(x  , yf-1, zf-1, Nf.x, Nf.y)]
				+  two*fine[idx(x  , yf  , zf-1, Nf.x, Nf.y)]
				+  one*fine[idx(x  , yf+1, zf-1, Nf.x, Nf.y)]
				+  two*fine[idx(x  , yf-1, zf  , Nf.x, Nf.y)]
				+ four*fine[idx(x  , yf  , zf  , Nf.x, Nf.y)]
				+  two*fine[idx(x  , yf+1, zf  , Nf.x, Nf.y)]
				+  one*fine[idx(x  , yf-1, zf+1, Nf.x, Nf.y)]
				+  two*fine[idx(x  , yf  , zf+1, Nf.x, Nf.y)]
				+  one*fine[idx(x  , yf+1, zf+1, Nf.x, Nf.y)]);
			}
		}
	};


	/**
	* Restricts one dimension
	*/
	template <typename value_type, typename dim_type>
	__global__
	void FD3DMG_restriction_one_dimension_full_weighting(  value_type const* fine
	                 , value_type* coarse
					 , dim_type Nf // N Fine grid
					 , dim_type Nc // N Coarse grid
					 , dim_type g0
					 , dim_type g1
					 , dim_type off0
					 , dim_type off1)
	{
		unsigned int xc = IDX1Dx + g0.x+off0.x;	// Coarse ids
		unsigned int yc = IDX1Dy + g0.y+off0.y;
		unsigned int zc =          g0.z+off0.z;

		if(xc<Nc.x-g1.x-off1.x && yc<Nc.y-g1.y-off1.y)
		{
			// Constants
			value_type quarter = gpulab::values<value_type>::quarter();
			value_type two     = gpulab::values<value_type>::two();

			unsigned int xf = (Nc.x < Nf.x) ? xc*2-g0.x : xc;
			unsigned int yf = (Nc.y < Nf.y) ? yc*2-g0.y : yc;
			
			unsigned int xdif = (Nc.x < Nf.x) ? 1 : 0;
			unsigned int ydif = (Nc.y < Nf.y) ? 1 : 0;
			unsigned int zdif = (Nc.z < Nf.z) ? 1 : 0;

			for(; zc<Nc.z-g1.z-off1.z; ++zc)
			{
				unsigned int zf = (Nc.z < Nf.z) ? zc*2-g0.z : zc;
				coarse[idx(xc,yc,zc,Nc.x,Nc.y)] = quarter*( two*fine[idx(xf     ,yf     ,zf     ,Nf.x,Nf.y)]
													          + fine[idx(xf+xdif,yf+ydif,zf+zdif,Nf.x,Nf.y)]
													 	      + fine[idx(xf-xdif,yf-ydif,zf-zdif,Nf.x,Nf.y)] );
			}
		}
	};

} // namespace kernel

	/**
	* 2D restriction and prolongation handler.
	* Uses semi-coarsening, i.e. only restrict the dimension with most points.
	*/
	template <typename Types>
	class grid_handler_2d
	{
	protected:
		typedef typename Types::size_type						size_type;
		typedef typename Types::value_type						value_type;
		typedef grid_dim<size_type>								grid_dim_type;

	public:

		/**
		* Halfs the inner dimensions and returns new properties with the same ghost layers as before
		*/
		template <typename grid_type>
		static grid_type* HALF(grid_type const& g)
		{
			typedef typename grid_type::topology_type		topology_type;
			typedef typename topology_type::property_type	property_type;

			size_type i = g.properties().inner_dim.x;
			size_type j = g.properties().inner_dim.y;
			size_type k = g.properties().inner_dim.z;
			
			grid_dim_type half_dim(i,j,k);

			// This is standart coarsening, and it seems to work best for repetitions of waves in x.
			//grid_dim_type half_dim(((x-1)>>1)+1,((y-1)>>1)+1,((z-1)>>1)+1);
			//return grid_properties_type(half_dim,prop.phys_0,prop.phys_1,prop.ghost_0,prop.ghost_1);
			
			// Semi coarsen in x
			if(i > j)
			{
				half_dim.x = ((i-1)>>1)+1;
			}
			// Semi coarsen in y
			else if(j > i)
			{
				half_dim.y = ((j-1)>>1);
			}
			// Standart coarsen
			else
			{
				half_dim = grid_dim_type(((i-1)>>1)+1,((j-1)>>1)+1,((k-1)>>1)+1);
			}

			property_type coarse_props(g.properties());
			coarse_props.set_inner_dim(half_dim);
			grid_type* newgrid = g.duplicate();
			newgrid->set_properties(coarse_props);
			return newgrid;
		}

		/**
		* Doubles the inner dimensions and returns new properties with the same ghost layers as before
		*/
		template <typename grid_properties_type>
		static grid_properties_type DOUBLE(grid_properties_type const& gprop)
		{
			size_type i = gprop.inner_dim.x;
			size_type j = gprop.inner_dim.y;
			size_type k = gprop.inner_dim.z;
			
			// This is standart coarsening, and it seems to work best for repetitions of waves in x.
			//grid_dim_type double_dim(((x-1)<<1)+1,((y-1)<<1)+1,((z-1)<<1)+1);
			//return grid_properties_type(double_dim,prop.phys_0,prop.phys_1,prop.ghost_0,prop.ghost_1);

			// Semi coarsen in x
			if(i < j)
			{
				grid_dim_type double_dim(((i-1)<<1)+1,j,k);
				grid_properties_type props(gprop);
				props.set_inner_dim(double_dim);
				return props;
			} 
			// Semi coarsen in y
			else if(j < i)
			{
				grid_dim_type double_dim(i,((j-1)<<1)+1,k);
				grid_properties_type props(gprop);
				props.set_inner_dim(double_dim);
				return props;
			}
			// Standart coarsen
			else
			{
				grid_dim_type double_dim(((i-1)<<1)+1,((j-1)<<1)+1,((k-1)<<1)+1);
				grid_properties_type props(gprop);
				props.set_inner_dim(double_dim);
				return props;
			}
		}

		template <typename V1, typename V2>
		void restrict(V1 const& fine, V2& coarse) const
		{
			// Dispatch
			restrict(fine,coarse,typename V1::memory_space());
		}

		template <typename V1, typename V2>
		void prolongate(V1& fine, V2 const& coarse) const
		{
			// Dispatch
			prolongate(fine,coarse,typename V1::memory_space());	
		}

		/**
		* TODO: Move to global scope (there is also one like this in 3d version)
		* Doubles the inner dimensions of x and returns new properties with the same ghost layers as before
		*/
		template <int DIM, typename grid_properties_type>
		static grid_properties_type DOUBLEX(grid_properties_type const& gprop)
		{
			size_type i = gprop.inner_dim.x;
			size_type j = gprop.inner_dim.y;
			size_type k = gprop.inner_dim.z;
			grid_dim_type double_dim( (DIM & DIM_X) ? ((i-1)<<1)+1 : i
									, (DIM & DIM_Y) ? ((j-1)<<1)+1 : j
									, (DIM & DIM_Z) ? ((k-1)<<1)+1 : k);
			grid_properties_type props(gprop);
			props.set_inner_dim(double_dim);
			return props;
		}

	private:

		template <typename V1, typename V2>
		void prolongate(V1& fine, V2 const& coarse, gpulab::host_memory) const
		{
			typedef typename V1::value_type value_type;

			size_type Nx     = fine.dimension().x;
			size_type Ny     = fine.dimension().y;
			size_type Nxh    = coarse.dimension().x;
			size_type Nyh    = coarse.dimension().y;
			size_type xstart =     coarse.ghost0().x;
			size_type xend   = Nxh-coarse.ghost1().x;
			size_type ystart =     coarse.ghost0().y;
			size_type yend   = Nyh-coarse.ghost1().y;
			value_type half(0.5);
			value_type quater(0.25);

			fine.fill(0);

			// Ignore border, because it is zero
			for (unsigned int i = ystart+1; i < yend; ++i)
			{
				for (unsigned int j = xstart; j < xend; ++j)
				{
					int i2 = i*2;
					int j2 = j*2-xstart;

					// Scattering like
					//       | 1 2 1 |
					// 1/4 * | 2 4 2 |
					//       | 1 2 1 |
					fine[(i2-1)*Nx+(j2-1)] += quater*coarse[i*Nxh+j];
					fine[(i2-1)*Nx+ j2   ] += half  *coarse[i*Nxh+j];
					fine[(i2-1)*Nx+(j2+1)] += quater*coarse[i*Nxh+j];
					fine[ i2   *Nx+(j2-1)] += half  *coarse[i*Nxh+j];
					fine[ i2   *Nx+ j2   ]  =        coarse[i*Nxh+j];
					fine[ i2   *Nx+(j2+1)] += half  *coarse[i*Nxh+j];
					fine[(i2+1)*Nx+(j2-1)] += quater*coarse[i*Nxh+j];
					fine[(i2+1)*Nx+ j2   ] += half  *coarse[i*Nxh+j];
					fine[(i2+1)*Nx+(j2+1)] += quater*coarse[i*Nxh+j];
				}
			}
		}

		template <typename V1, typename V2>
		void prolongate(V1& fine, V2 const& coarse, gpulab::device_memory) const
		{
			typedef typename V1::value_type value_type;
			typedef typename V1::size_type size_type;

			size_type Nxf    = fine.dimension().x;
			size_type Nyf    = fine.dimension().y;
			size_type Nxc    = coarse.dimension().x;
			size_type Nyc    = coarse.dimension().y;
			size_type xstart =     coarse.ghost0().x;
			size_type xend   = Nxc-coarse.ghost1().x;
			size_type ystart =     coarse.ghost0().y;
			size_type yend   = Nyc-coarse.ghost1().y;

			fine.fill(0);

			if(Nyf == Nyc)
			{
				xstart =     fine.ghost0().x;
				xend   = Nxf-fine.ghost1().x;
				ystart =     fine.ghost0().y;
				yend   = Nyf-fine.ghost1().y;
				dim3 grid  = GRID2D(xend-xstart,yend-ystart);
				dim3 block = BLOCK2D(xend-xstart,yend-ystart);
				kernel::prolongate_x<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nxf,Nyf,Nxc,Nyc,xstart,xend,ystart,yend);
			}
			else if(Nxf == Nxc)
			{
				ASSERT(!"Not implemented");
			}
			else
			{
				//dim3 grid  = GRID2D(xend-xstart,yend-ystart);
				//dim3 block = BLOCK2D(xend-xstart,yend-ystart);
				//kernel::prolongate<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nxf,Nyf,Nxc,Nyc,xstart,xend,ystart,yend);

				// Gather, use fine grid as block/grid setup
				V1 finex(this->DOUBLEX<DIM_X>(coarse.properties()),coarse.topology());	// Tmp grid for x-prolonagtion

				Nxf    =  finex.dimension().x;
				Nyf    =  finex.dimension().y;
				xstart =     finex.ghost0().x;
				xend   = Nxf-finex.ghost1().x;
				ystart =     finex.ghost0().y;
				yend   = Nyf-finex.ghost1().y;
				dim3 grid  = GRID2D(xend-xstart,yend-ystart);
				dim3 block = BLOCK2D(xend-xstart,yend-ystart);
				kernel::prolongate_x<<<grid,block>>>(RAW_PTR(finex),RAW_PTR(coarse),Nxf,Nyf,Nxc,Nyc,xstart,xend,ystart,yend);

				Nxf    =  fine.dimension().x;
				Nyf    =  fine.dimension().y;
				Nxc    = finex.dimension().x;
				Nyc    = finex.dimension().y;
				xstart =     fine.ghost0().x;
				xend   = Nxf-fine.ghost1().x;
				ystart =     fine.ghost0().y;
				yend   = Nyf-fine.ghost1().y;
				dim3 grid2  = GRID2D(xend-xstart,yend-ystart);
				dim3 block2 = BLOCK2D(xend-xstart,yend-ystart);
				kernel::prolongate_y<<<grid2,block2>>>(RAW_PTR(fine),RAW_PTR(finex),Nxf,Nyf,Nxc,Nyc,xstart,xend,ystart,yend);
			}
		}

		template <typename V1, typename V2>
		void restrict(V1 const& fine, V2& coarse, gpulab::host_memory) const
		{
			typedef typename V1::value_type value_type;
			typedef typename V1::size_type size_type;

			size_type Nx     = fine.dimension().x;
			size_type Ny     = fine.dimension().y;
			size_type Nxh    = coarse.dimension().x;
			size_type Nyh    = coarse.dimension().y;
			size_type xstart =     coarse.ghost0().x;
			size_type xend   = Nxh-coarse.ghost1().x;
			size_type ystart =     coarse.ghost0().y;
			size_type yend   = Nyh-coarse.ghost1().y;

			// Constants
			value_type oneeight(0.125);
			value_type four(4);

			// Internal loop
			for (size_type i = ystart+1; i < yend; ++i)
			{
				for (size_type j = xstart; j < xend; ++j)
				{
					size_type i2 = i*2;
					size_type j2 = j*2-xstart;
					coarse[i*Nxh+j] = oneeight*(four*fine[i2*Nx+j2] + fine[(i2-1)*Nx+j2] + fine[(i2+1)*Nx+j2] + fine[i2*Nx+(j2-1)] + fine[i2*Nx+(j2+1)]);
				}
			}
		}

		template <typename V1, typename V2>
		void restrict(V1 const& fine, V2& coarse, gpulab::device_memory) const
		{
			typedef typename V1::value_type value_type;
			typedef typename V1::size_type size_type;

			size_type Nx     = fine.dimension().x;
			size_type Ny     = fine.dimension().y;
			size_type Nxh    = coarse.dimension().x;
			size_type Nyh    = coarse.dimension().y;
			size_type xstart =     coarse.ghost0().x;
			size_type xend   = Nxh-coarse.ghost1().x;
			size_type ystart =     coarse.ghost0().y;
			size_type yend   = Nyh-coarse.ghost1().y;

			if(Nyh == Ny)
			{
				dim3 grid  = GRID2D(Nxh,Nyh);
				dim3 block = BLOCK2D(Nxh,Nyh);
				kernel::restrict_x<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nx,Ny,Nxh,Nyh,xstart,xend,ystart,yend);
			}
			else if(Nx == Nxh)
			{
				ASSERT(!"Not implemented");
			}
			else
			{
				dim3 grid  = GRID2D(Nxh,Nyh);
				dim3 block = BLOCK2D(Nxh,Nyh);
				kernel::restrict<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nx,Ny,Nxh,Nyh,xstart,xend,ystart,yend);
			}
		}

	};

	
	/**
	* 3D restriction and prolongation, including boundary layers accoring to ghost points.
	*/
	template <typename Types>
	class grid_handler_3d
	{
	protected:
		typedef typename Types::vector_type::size_type			size_type;
		typedef typename Types::vector_type::value_type			value_type;
		typedef grid_dim<size_type>								grid_dim_type;
		
	public:

		/**
		* Halfs the inner dimensions based on isotropy and returns new properties with the same ghost layers as before
		*/
		template <typename grid_type>
		static grid_type* HALF(grid_type const& g)
		{
			typedef typename grid_type::topology_type topology_type;
			typedef typename grid_type::property_type property_type;

			size_type i = g.iNx();
			size_type j = g.iNy();
			size_type k = g.iNz();

			// Physical grid spacing:
			value_type dx = g.delta().x;
			value_type dy = g.delta().y;
			value_type dz = g.delta().z;

			size_type Nxf = i;
			size_type Nyf = j;
			size_type Nzf = k;

			// Coarsening strategy:
			// 1) X-Y first, then Z.
			// 2) Independent X-Y-Z.
			// 3) Towards Isotropy on fine?
			// 3) Towards Isotropy on coarse?

			value_type grid_space_limit = 2.0*min(dx,dy);

			size_type Nxc=Nxf;
			if (dx <= grid_space_limit && Nxf>3)
				Nxc = ((Nxf-1)>>1)+1;

			size_type Nyc=Nyf;
			if (dy <= grid_space_limit && Nyf>3) 
				Nyc = ((Nyf-1)>>1)+1;

			size_type Nzc=Nzf;
			if (dz <= grid_space_limit && Nzf>3)
				Nzc = ((Nzf-1)>>1)+1;

			grid_dim_type half_dim(Nxc,Nyc,Nzc);

			// Semi-coarsening always in x and y if they are larger than z. Only in z if z==x && z==y
			//grid_dim_type half_dim((i>k || i==j) ? ((i-1)>>1)+1 : i
			//					  ,(j>k || i==j) ? ((j-1)>>1)+1 : j
			//					  ,(k >= i && k >= j) ? ((k-1)>>1)+1 : k);

			property_type coarse_props(g.properties());
			coarse_props.set_inner_dim(half_dim);
			grid_type* newgrid = g.duplicate();
			newgrid->set_properties(coarse_props);
			return newgrid;
		}

		/**
		* Halfs the inner (DIM)-dimension and returns new properties with the same ghost layers as before
		*/
		template <int DIM, typename grid_properties_type>
		static grid_properties_type HALFX(grid_properties_type const& gprop)
		{
			size_type i = gprop.inner_dim.x;
			size_type j = gprop.inner_dim.y;
			size_type k = gprop.inner_dim.z;

			grid_dim_type half_dim(   (DIM & DIM_X) ? ((i-1)>>1)+1 : i
									, (DIM & DIM_Y) ? ((j-1)>>1)+1 : j
									, (DIM & DIM_Z) ? ((k-1)>>1)+1 : k);
			grid_properties_type half_prop(gprop);
			half_prop.set_inner_dim(half_dim);
			return half_prop;
		}

		/**
		* Doubles the inner dimensions and returns new properties with the same ghost layers as before
		*/
		template <typename grid_type>
		static grid_type* DOUBLE(grid_type const& g)
		{
			typedef typename grid_type::topology_type		topology_type;
			typedef typename topology_type::property_type	property_type;

			size_type i = g.properties().inner_dim.x;
			size_type j = g.properties().inner_dim.y;
			size_type k = g.properties().inner_dim.z;

			// Semi-coarsening only in the largest direction(s)
			//(x <= y && x <= z) ? ((x-1)<<1)+1 : x;
			//(y <= x && y <= z) ? ((y-1)<<1)+1 : y;
			//(z <= x && z <= y) ? ((z-1)<<1)+1 : z;
			
			// Semi-coarsening always in x and y if they are larger than z. Only in z if z==x && z==y
			grid_dim_type double_dim( (i <= j && i <= k) ? ((i-1)<<1)+1 : i
									 ,(j <= i && j <= k) ? ((j-1)<<1)+1 : j
									 ,(k <= i && k <= j) ? ((k-1)<<1)+1 : k);

			property_type prop(g.properties());
			prop.set_inner_dim(double_dim);
			return new grid_type(prop,g.topology());
		}

		/**
		* Doubles the inner (DIM)-dimensions of x and returns new properties with the same ghost layers as before
		*/
		template <int DIM, typename grid_properties_type>
		static grid_properties_type DOUBLEX(grid_properties_type const& gprop)
		{
			size_type x = gprop.inner_dim.x;
			size_type y = gprop.inner_dim.y;
			size_type z = gprop.inner_dim.z;
			grid_dim_type double_dim( (DIM & DIM_X) ? ((x-1)<<1)+1 : x
									, (DIM & DIM_Y) ? ((y-1)<<1)+1 : y
									, (DIM & DIM_Z) ? ((z-1)<<1)+1 : z);
			grid_properties_type double_prop(gprop);
			double_prop.set_inner_dim(double_dim);
			return double_prop;
		}

		template <typename V1, typename V2>
		void restrict(V1 const& fine, V2& coarse) const
		{
			// Dispatch
			restrict(fine,coarse,typename V1::memory_space());
		}

		template <typename V1, typename V2>
		void prolongate(V1& fine,V2 const& coarse) const
		{
			// Dispatch
			prolongate(fine,coarse,typename V1::memory_space());	
		}

	private:

		template <typename V1, typename V2>
		void prolongate(V1& fine, V2 const& coarse, gpulab::host_memory) const
		{
			ASSERT(!"Not implemented");
		}

		template <typename V1, typename V2>
		void prolongate(V1& fine, V2 const& coarse, gpulab::device_memory) const
		{
			//fine.fill(0);
			dim3 Nf(fine.Nx(),fine.Ny(),fine.Nz());
			dim3 Nc(coarse.Nx(),coarse.Ny(),coarse.Nz());
			dim3 g0(fine.ghost0().x,fine.ghost0().y,fine.ghost0().z);
			dim3 g1(fine.ghost1().x,fine.ghost1().y,fine.ghost1().z);
			dim3 off0(fine.offset0().x,fine.offset0().y,fine.offset0().z);
			dim3 off1(fine.offset1().x,fine.offset1().y,fine.offset1().z);
			dim3 grid  = GRID2D(Nf.x,Nf.y);
			dim3 block = BLOCK2D(Nf.x,Nf.y);

			if(Nf.x > Nc.x && Nf.y > Nc.y && Nf.z > Nc.z) // All coarsening
			{
				//// X prolongation
				//V1 finex(this->DOUBLEX<DIM_X>(coarse.properties()));	// Tmp grid for x-prolonagtion
				//prolongate_sweep(finex,coarse);
				//// Y prolongation
				//V1 finey(this->DOUBLEX<DIM_Y>(finex.properties()));	// Tmp grid for y-prolonagtion
				//prolongate_sweep(finey,finex);
				//// Z prolongation
				//prolongate_sweep(fine,finey);
				
				kernel::FD3DMG_prolongate_gather_xyz<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);
			}
			else if(Nf.x > Nc.x && Nf.y > Nc.y && Nf.z == Nc.z) // x and y coarsening
			{
				//// X prolongation
				//V1 finex(this->DOUBLEX<DIM_X>(coarse.properties()));	// Tmp grid for x-prolonagtion
				//prolongate_sweep(finex,coarse);
				//// Y prolongation
				//prolongate_sweep(fine,finex);

				kernel::FD3DMG_prolongate_gather_xy<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);
			}
			else if(Nf.x > Nc.x && Nf.z > Nc.z && Nf.y == Nc.y) // x and z coarsen
			{
				kernel::FD3DMG_prolongate_gather_xz<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);
			}
			else if(Nf.y > Nc.y && Nf.z > Nc.z && Nf.x == Nc.x) // y and z coarsen
			{
				kernel::FD3DMG_prolongate_gather_yz<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);
			}
			else // x or y or z coarsening only
			{
				kernel::FD3DMG_prolongate_gather<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);
			}
		}

		template <typename V1, typename V2>
		void prolongate_sweep(V1& fine, V2 const& coarse) const
		{
			dim3 Nf(fine.dimension().x,fine.dimension().y,fine.dimension().z);
			dim3 Nc(coarse.dimension().x,coarse.dimension().y,coarse.dimension().z);
			dim3 g0(fine.ghost0().x,fine.ghost0().y,fine.ghost0().z);
			dim3 g1(fine.ghost1().x,fine.ghost1().y,fine.ghost1().z);
			dim3 off0(fine.offset0().x,fine.offset0().y,fine.offset0().z);
			dim3 off1(fine.offset1().x,fine.offset1().y,fine.offset1().z);

			dim3 grid  = GRID2D(Nf.x,Nf.y);
			dim3 block = BLOCK2D(Nf.x,Nf.y);
			kernel::FD3DMG_prolongate_gather<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);
		}

		template <typename V1, typename V2>
		void restrict(V1 const& fine, V2& coarse, gpulab::host_memory) const
		{
			ASSERT(!"Not implemented");
		}

		template <typename V1, typename V2>
		void restrict(V1 const& fine, V2& coarse, gpulab::device_memory) const
		{
			size_type Nxf =   fine.dimension().x;
			size_type Nyf =   fine.dimension().y;
			size_type Nzf =   fine.dimension().z;
			size_type Nxc = coarse.dimension().x;
			size_type Nyc = coarse.dimension().y;
			size_type Nzc = coarse.dimension().z;

			dim3 Nf(fine.dimension().x,fine.dimension().y,fine.dimension().z);
			dim3 Nc(coarse.dimension().x,coarse.dimension().y,coarse.dimension().z);
			dim3 g0(coarse.ghost0().x,coarse.ghost0().y,coarse.ghost0().z);
			dim3 g1(coarse.ghost1().x,coarse.ghost1().y,coarse.ghost1().z);
			dim3 off0(coarse.offset0().x,coarse.offset0().y,coarse.offset0().z);
			dim3 off1(coarse.offset1().x,coarse.offset1().y,coarse.offset1().z);

			dim3 grid  = GRID2D(Nxc,Nyc);
			dim3 block = BLOCK2D(Nxc,Nyc);

			if(Nxf > Nxc && Nyf > Nyc && Nzf > Nzc) // All coarsening
			{
				kernel::FD3DMG_restriction_full_weighting_xyz<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);
			}
			else if(Nxf > Nxc && Nyf > Nyc && Nzf == Nzc) // x and y coarsening
			{
				kernel::FD3DMG_restriction_full_weighting_xy<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);
			}
			else if(Nxc < Nxf && Nyc == Nyf && Nzc < Nzf) // xz
			{
				kernel::FD3DMG_restriction_full_weighting_xz<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);			
			}
			else if(Nxc == Nxf && Nyc < Nyf && Nzc < Nzf) // yz
			{
				kernel::FD3DMG_restriction_full_weighting_yz<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);
			}
			else // x or y or z coarsening
			{
				restrict_sweep(fine,coarse);
			}
		}

		template <typename V1, typename V2>
		void restrict_sweep(V1 const& fine, V2& coarse) const
		{
			dim3 Nf(fine.dimension().x,fine.dimension().y,fine.dimension().z);
			dim3 Nc(coarse.dimension().x,coarse.dimension().y,coarse.dimension().z);
			dim3 g0(coarse.ghost0().x,coarse.ghost0().y,coarse.ghost0().z);
			dim3 g1(coarse.ghost1().x,coarse.ghost1().y,coarse.ghost1().z);
			dim3 off0(coarse.offset0().x,coarse.offset0().y,coarse.offset0().z);
			dim3 off1(coarse.offset1().x,coarse.offset1().y,coarse.offset1().z);
			dim3 grid  = GRID2D(Nc.x,Nc.y);
			dim3 block = BLOCK2D(Nc.x,Nc.y);
			kernel::FD3DMG_restriction_one_dimension_full_weighting<<<grid,block>>>(RAW_PTR(fine),RAW_PTR(coarse),Nf,Nc,g0,g1,off0,off1);
		}

	};



} // namespace solvers

} // namespace gpulab

#endif // GPULAB_SOLVERS_MULTIGRID_HANDLERS_H
