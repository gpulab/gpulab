// ==========================================================================
// Conjugate Gradient solver
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_CG_H
#define GPULAB_SOLVERS_CG_H

#include <assert.h>
#include <gpulab/monitor.h>

namespace gpulab
{

namespace solvers
{
	/**
	* CG type binder class
	*/ 
	template<
		  typename vector_type_
		, typename matrix_type_
		, typename monitor_type_ = gpulab::monitor<typename vector_type_::value_type>
		, typename preconditioner_type_ = matrix_type_
	>
	class cg_types
	{
	public:
		typedef cg_types<
		  vector_type_
		, matrix_type_
		, monitor_type_
		, preconditioner_type_> types;

		typedef typename vector_type_::value_type	value_type;

		typedef vector_type_						vector_type;
		typedef preconditioner_type_				preconditioner_type;
		typedef matrix_type_						matrix_type;
		typedef monitor_type_						monitor_type;
	};

	/**
	* Conjugate gradient solver for Ax = b
	*/
	template <typename Types>
	class cg
	{
	public:
		typedef typename Types::value_type		value_type;
		typedef typename Types::vector_type		vector_type;
		typedef typename Types::matrix_type		matrix_type;
		typedef typename Types::monitor_type	monitor_type;
	
	protected:

		monitor_type&			m_monitor;
		matrix_type const&		m_A;

	public:

		cg(matrix_type const& A, monitor_type& m)
			: m_A(A)
			, m_monitor(m)
		{}

		void solve(vector_type& x, vector_type const& b)
		{
			// reset
			x.fill(value_type(0));
			m_monitor.reset(b);
			m_monitor.timer().restart();

			// Initialize vectors
			vector_type r(b);
			vector_type p(b);
			vector_type w(b.topology());

			value_type alpha;
			value_type beta;
			value_type r_dot = r.dot(r);
			value_type r_dot_prev;
			value_type one = values<value_type>::one();

			while(1)
			{
				m_A.mult(p, w);
				r_dot_prev = r_dot;
				alpha = r_dot_prev / p.dot(w);
				x.axpy(alpha, p);
				r.axpy(-alpha, w);
				r_dot = r.dot(r);
				
				if (m_monitor.finished(sqrt(r_dot)))
					break;
				++m_monitor;

				beta = r_dot / r_dot_prev;
				p.axpby(one, beta, r);
			}
			m_monitor.timer().stop();
		}
	};

	///**
	//* Conjugate gradient solver for Ax = b with default monitor.
	//* @return monitor with convergence information
	//*/
	//template <typename matrix_type, typename vector_type>
	//monitor<typename vector_type::value_type> cg(matrix_type const& A, vector_type& x, vector_type const& b)
	//{
	//	monitor<typename vector_type::value_type> m(b);
	//	cg(A, x, b, m);
	//	return m;
	//}

	/**
	* Preconditioned conjugate gradient solver
	*/
	template <typename p_matrix_type, typename matrix_type, typename vector_type>
	void pcg(p_matrix_type const& P, matrix_type const& A, vector_type& x, vector_type const& b, monitor<typename vector_type::value_type>& m)
	{
		typedef typename vector_type::size_type	 size_type;
		typedef typename vector_type::value_type value_type;

		// reset
		x.fill(value_type(0));
		m.reset(b);

		// Initialize vectors
		vector_type r(b);
		vector_type p(b);
		vector_type z(b.size());
		vector_type w(b.size());

		value_type alpha;
		value_type beta;
		value_type rho;
		value_type one(1);

		value_type tau = 0.;
		value_type tau_prev;

		while(1)
		{
			P.mult(r, z);
			tau_prev = tau;
			tau = z.dot(r);

			if(m.iteration_count() == 0)
			{
				beta = 0.;
				p.copy(z);
			}
			else
			{
				beta = tau / tau_prev;
				p.axpby(one,beta,z);
			}

			A.mult(p, w);
			alpha = tau / p.dot(w);
			x.axpy(alpha, p);
			r.axpy(-alpha, w);
			rho = r.dot(r);

			++m;			
			if (m.finished(sqrt(rho)))
				break;
		}
	}

	/**
	* Preconditioned conjugate gradient solver
	* @return monitor with convergence information
	*/
	template <typename p_matrix_type, typename matrix_type, typename vector_type>
	monitor<typename vector_type::value_type> pcg(p_matrix_type const& P, matrix_type const& A, vector_type& x, vector_type const& b)
	{
		monitor<typename vector_type::value_type> m(b);
		pcg(P, A, x, b, m);
		return m;
	}

} // namespace solvers

} // namespace gpulab

#endif // GPULAB_SOLVERS_CG_H
