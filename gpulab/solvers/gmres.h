// ==========================================================================
// Generalized Minimal RESidual solver (GMRES)
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// Date:    July 13. 2011.
// ==========================================================================

#ifndef GPULAB_SOLVERS_GMRES_H
#define GPULAB_SOLVERS_GMRES_H

#include <gpulab/vector.h>
#include <gpulab/matrix.h>
#include <gpulab/monitor.h>
#include <gpulab/math/triangular_solve.h>
#include <gpulab/type_traits.h>

namespace gpulab
{

namespace solvers
{
	/**
	* Class wrapping in the GMRES solver.
	* Normally one should use the gpulab::solvers::gmres(...) method, 
	* which use this class to solve the system of eauations.
	*/
	template <typename types , unsigned int M = 32>
	class gmres_solver
	{
	public:
		typedef typename types::value_type				value_type;
		typedef typename types::vector_type				vector_type;
		typedef typename types::matrix_type				matrix_type;
		typedef typename types::monitor_type			monitor_type;

	protected:
		typedef typename vector_type::memory_space		memory_space;
		typedef gpulab::vector<value_type,host_memory>	host_vector;
		typedef gpulab::matrix<value_type,host_memory>	host_matrix;
		typedef gpulab::matrix<value_type,memory_space>	dense_matrix_type;
		
		unsigned int		m_M;			///< Number of iterations before restart
		vector_type			m_v[M+1];		///< Host/Device side vectors
		host_vector			m_g;			///< Host side vector
		host_vector			m_y;			///< Host side vector
		value_type			m_beta;			///< Norm of the residual
		host_matrix			m_H;			///< Host side matrix
		matrix_type const&	m_A;			///< Reference to host/device matrix A
		monitor_type&		m_monitor;		///< Reference to the monitor
		
	protected:
		
	public:

		gmres_solver(matrix_type const& A_, monitor_type& m_)
			: m_A(A_)
			, m_monitor(m_)
			, m_M(M)
			, m_g(M+1)
			, m_y(M)
			, m_H(M+1,M)
		{
		}

		~gmres_solver()
		{
			// Clean up
		}

		/**
		* Solve Ax=b, with A pased to the constructor and the options set in the monitor
		* @param monit option settings
		*/
		void solve(vector_type& x, vector_type const& b)
		{
			// Initialize m_v's
			if(m_v[0].size() == 0)
			{
				for(unsigned int i=0; i<M+1; ++i)
				{
					m_v[i].set_props(b);
				}
			}

			value_type one = values<value_type>::one();

			// Reset monitor
			m_monitor.reset(b);
			m_monitor.timer().reset();

			if(x.size() < m_M) // For sure do not need more than x.size() iterations
			{
				m_M = x.size();
				m_y.resize(m_M);
			}

			m_A.mult(x, m_v[0]);
			m_v[0].axpby(one,-one,b);

			while(true)
			{
				m_beta = m_v[0].nrm2();
				//printf("beta=%f\n",m_beta);
				
				++m_monitor;
				if(m_monitor.finished(m_beta))
				{
					break;
				}

				m_v[0].scal(one/m_beta);
				m_g[0] = m_beta;

				arnoldi_mgs();
				rotate_hessenberg();
				update(x);

				//++m_monitor;
				//if(m_monitor.finished(m_v[0])) // TODO: Check using the norm stored in the last entry of g
				//{
				//	break;
				//}

				m_A.mult(x, m_v[0]);
				m_v[0].axpby(one,-one,b);
			}
			m_monitor.timer().stop();
		}

	protected:

		/**
		* Arnoldi process, Modified Gram-Schmidt
		*/
		void arnoldi_mgs()
		{
			for(unsigned int j=0; j<m_M; ++j)
			{
				m_A.mult(m_v[j], m_v[j+1]);
				for(unsigned int i=0; i<=j; ++i)
				{
					m_H(i,j) = m_v[i].dot(m_v[j+1]);
					m_v[j+1].axpy(-m_H(i,j), m_v[i]);
				}
				m_H(j+1,j) = m_v[j+1].nrm2();
				if(abs(m_H(j+1,j)) < gpulab::values<value_type>::tolerance())
				{
					m_M = j+1;
					m_y.resize(m_M);
					break;
				}
				value_type d = gpulab::values<value_type>::one() / m_H(j+1,j);
				m_v[j+1].scal(d);
			}
		}

		/**
		* Compute the minimizer y of ||beta*e_1 - H*y||, then x = V*y
		* To solve this, we compute the upper triangular matrix version of H
		* using Givens plane rotations.
		*/
		void rotate_hessenberg()
		{
			// Rotate Hessenberg matrix
			value_type s, c, d;
			for(unsigned int i=0; i<m_M; ++i)
			{
				d = sqrt(pow(m_H(i,i),2) + pow(m_H(i+1,i),2));
				s = m_H(i+1,i) / d;
				c = m_H(i,i)   / d;

				for(unsigned int j=i; j<m_M; ++j)
				{
					d = m_H(i,j);
					m_H(i  ,j) =  c*m_H(i,j) + s*m_H(i+1,j);
					m_H(i+1,j) = -s*d        + c*m_H(i+1,j);
				}

				m_g[i]   =  c*m_g[i];
				m_g[i+1] = -s*m_g[i];
			}
		}

		/**
		* Calculate the minimizer, solve H*y = g
		* and calculate x = x_0 + V*y
		*/
		void update(vector_type& x)
		{
			// Solve H*y = g
			gpulab::math::backward_substitution(m_H, m_y, m_g, m_M);

			// Calculate x = x_0 + V*y
			dense_matrix_type V(x.size(), m_M);
			for(unsigned int j=0; j<m_M; ++j)
				V.set_col(m_v[j], j);
			V.mult(m_y,m_v[0]); // Will move m_y to device if necessary
			x.axpy(gpulab::values<value_type>::one(), m_v[0]);
		}
	};

	
	/**
	* GMRES solver for Ax = b
	* Matrix vector operations for the input data are performed on the device or host, 
	* depending on where input data is located. 
	* Matrix vector operations for the Hessenberg and minimum residual are always
	* performed on the host, because small systems are cheaper on host.
	* @param A matrix A
	* @param x vector x
	* @param b vector b
	* @param monit monitor, holding stop criterias
	* @param m number of iterations between restarts
	*/
	template <unsigned int M, typename matrix_type_, typename vector_type_>
	void gmres(matrix_type_ const& A, vector_type_& x, vector_type_ const& b, monitor<typename vector_type_::value_type>& monit)
	{
		typedef struct {
			typedef vector_type_ vector_type;
			typedef matrix_type_ matrix_type;
		} gmres_types;
		gpulab::solvers::gmres_solver<gmres_types, M> solver(A, x, b);
		solver.solve(monit);
	};


} // namespace solvers

} // namespace gpulab

#endif // GPULAB_SOLVERS_GMRES_H
