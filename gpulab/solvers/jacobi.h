// ==========================================================================
// Jacobi solver
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_JACOBI
#define GPULAB_SOLVERS_JACOBI

#include <assert.h>

namespace gpulab
{

namespace solvers
{

	template <typename matrix_type, typename vector_type, typename C>
	void jacobi(matrix_type const& A, vector_type& x, vector_type const& b, C const& config)
	{
		typedef typename matrix_type::size_type	 size_type;
		typedef typename matrix_type::value_type value_type;
		int iterations = 50;

		// reset
		x.set(value_type(0));

		// Create N and M
		matrix_type M_inv(A.rows(), A.cols());
		matrix_type N(A);
		N.scal(value_type(-1));
		size_type minimum = min(A.rows(), A.cols());
		N.set_diag(value_type(0));
		M_inv.set_diag(A);
		M_inv.reciprocal(1.f);

		vector_type Nx(x.size());
		//vector_type r(x.size());

		while(iterations-- > 0)
		{
			N.mult(x, Nx);
			Nx.axpy(1.f, b);
			M_inv.mult(Nx, x);

			/*A.mult(x, r);
			r.axpy(value_type(-1), b);
			value_type nrm = r.nrm1();*/
		}
	}

} // namespace solvers

} // namespace gpulab

#endif // GPULAB_SOLVERS_JACOBI