// ==========================================================================
// Defect Correction Solver
// ==========================================================================
// (C)opyright: 2010
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Stefan Lemvig Glimberg
// Email:   slgl@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_DEFECT_CORRECTION_TYPES_H
#define GPULAB_SOLVERS_DEFECT_CORRECTION_TYPES_H

#include <gpulab/monitor.h>

namespace gpulab
{

namespace solvers
{
	/**
	* Multigrid type binder class
	*/ 
	template<
		  typename vector_type_
		, typename matrix_type_
		, typename monitor_type_
		, typename preconditioner_type_
	>
	class defect_correction_types
	{
	public:
		typedef defect_correction_types<
		  vector_type_
		, matrix_type_
		, monitor_type_
		, preconditioner_type_> types;

		typedef typename vector_type_::value_type				value_type;
		//typedef typename preconditioner_type_::monitor_type		monitor_type;
		
		typedef vector_type_									vector_type;
		typedef matrix_type_									matrix_type;
		typedef monitor_type_									monitor_type;
		typedef preconditioner_type_							preconditioner_type;
	};
} // namespace solvers

} // namespace gpulab

#endif // GPULAB_SOLVERS_DEFECT_CORRECTION_TYPES_H