// ==========================================================================
// Conjugate Gradient for Normal Residual solver
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Nicolai Fog Gade-Nielsen
// Email:   nfga@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_SOLVERS_CGNR_H
#define GPULAB_SOLVERS_CGNR_H

#include <assert.h>
#include <gpulab/monitor.h>

namespace gpulab
{
namespace solvers
{

	/**
	* Conjugate gradient for normal residual solver for Ax = b
	*/
	template <typename matrix_type, typename vector_type>
	void cgnrS(matrix_type const& A, vector_type& x, vector_type const& b, monitor<typename vector_type::value_type>& m)
	{
		typedef typename vector_type::size_type	 size_type;
		typedef typename vector_type::value_type value_type;

		// Store transpose and sort by row for performance
		matrix_type At(A);
		At.transpose();
		At.sort(1);

		// Reset
		m.reset(b);

		// Init
		vector_type r(A.rows());
		A.mult(x, r);
		r.axpby(value_type(1), value_type(-1), b);

		vector_type d(At.rows());
		At.mult(r, d);

		value_type Atr_dot = d.dot(d);

		// Allocate
		value_type Atr_dot_prev;
		vector_type Ad(A.rows());
		vector_type Atr(At.rows());
		value_type Ad_dot;
		value_type alpha;
		value_type beta;
		value_type r_dot;
		value_type one(1);

		while(1)
		{
			A.mult(d, Ad);
			Ad_dot = Ad.dot(Ad);
			alpha = Atr_dot / Ad_dot;

			x.axpy(alpha, d);
			r.axpy(-alpha, Ad);
			r_dot = r.dot(r);

			++m;
			if (m.finished(sqrt(r_dot)))
				break;

			At.mult(r, Atr);
			Atr_dot_prev = Atr_dot;
			Atr_dot = Atr.dot(Atr);
			beta = Atr_dot / Atr_dot_prev;
			d.axpby(one, beta, Atr);
		}
	}

	/**
	* Conjugate gradient for normal residual solver for Ax = b
	*/
	template <typename matrix_type, typename vector_type>
	void cgnrI(matrix_type const& A, vector_type& x, vector_type const& b, monitor<typename vector_type::value_type>& m)
	{
		typedef typename vector_type::size_type	 size_type;
		typedef typename vector_type::value_type value_type;

		// Reset
		m.reset(b);

		// Init
		vector_type r(A.rows());
		A.mult(x, r);
		r.axpby(value_type(1), value_type(-1), b);

		vector_type d(A.cols());
		A.multT(r, d);

		value_type Atr_dot = d.dot(d);

		// Allocate
		value_type Atr_dot_prev;
		vector_type Ad(A.rows());
		vector_type Atr(A.cols());
		value_type Ad_dot;
		value_type alpha;
		value_type beta;
		value_type r_dot;
		value_type one(1);

		while(1)
		{
			A.mult(d, Ad);
			Ad_dot = Ad.dot(Ad);
			alpha = Atr_dot / Ad_dot;

			x.axpy(alpha, d);
			r.axpy(-alpha, Ad);
			r_dot = r.dot(r);

			++m;
			if (m.finished(sqrt(r_dot)))
				break;

			A.multT(r, Atr);
			Atr_dot_prev = Atr_dot;
			Atr_dot = Atr.dot(Atr);
			beta = Atr_dot / Atr_dot_prev;
			d.axpby(one, beta, Atr);
		}
	}

	/**
	* Conjugate gradient for normal residual solver for Ax = b with default monitor.
	* @return monitor with convergence information
	*/
	template <typename matrix_type, typename vector_type>
	monitor<typename vector_type::value_type> cgnrS(matrix_type const& A, vector_type& x, vector_type const& b)
	{
		monitor<typename vector_type::value_type> m(b);
		cgnrS(A, x, b, m);
		return m;
	}

	/**
	* Conjugate gradient for normal residual solver for Ax = b with default monitor.
	* @return monitor with convergence information
	*/
	template <typename matrix_type, typename vector_type>
	monitor<typename vector_type::value_type> cgnrI(matrix_type const& A, vector_type& x, vector_type const& b)
	{
		monitor<typename vector_type::value_type> m(b);
		cgnrI(A, x, b, m);
		return m;
	}

} // namespace solvers
} // namespace gpulab

#endif // GPULAB_SOLVERS_CGNR_H
