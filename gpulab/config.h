#ifndef GPULAB_CONFIG_H
#define GPULAB_CONFIG_H

#include <cuda.h>
#include <helper_cuda.h>
//#include <cutil.h> //NOTE: cutil.h is obsolete in CUDA v5.0 [OLD]


/**
* General defines
*/
#define MAX_PATH_LENGTH 128

/**
* CUDA defines
*/
// TODO: Where/how do we make a cude version definition?
#if CUDA_VERSION >= 10000

#define MAX_THREADS 512
#define MAX_BLOCKS ...

#else 

// TODO: How do we make this in a device specific way?
#define MAX_BLOCKS 65535
#define MAX_THREADS 512
#define MEAN_THREADS 256
#define THREADS_2D 16
#define THREADS_3D 8
#define WARP_SIZE 32

// Min and max defines
#ifndef MIN
#define MIN(a,b) ((a<b)?a:b)
#endif
#ifndef MAX
#define MAX(a,b) ((a>b)?a:b)
#endif

#define GRID1D(N)  (MIN((N+MEAN_THREADS-1)/MEAN_THREADS,MAX_BLOCKS))
#define BLOCK1D(N) (N<MEAN_THREADS?N:MEAN_THREADS)

#define GRID2D(N,M)  dim3(MIN((N+THREADS_2D-1)/THREADS_2D,MAX_BLOCKS),MIN((M+THREADS_2D-1)/THREADS_2D,MAX_BLOCKS))
#define BLOCK2D(N,M) dim3((N<THREADS_2D?N:THREADS_2D),(M<THREADS_2D?M:THREADS_2D))

#define GRID_SAFE(N) (MIN(N,MAX_BLOCKS))


// Memory index calculators
#define IDX1D (blockDim.x*blockIdx.x+threadIdx.x)
#define IDX1Dx (blockDim.x*blockIdx.x+threadIdx.x)
#define IDX1Dy (blockDim.y*blockIdx.y+threadIdx.y)
#define IDX1Dz (blockDim.z*blockIdx.z+threadIdx.z)

#define TIDx  (threadIdx.x)
#define TIDy  (threadIdx.y)
#define TID2D (threadIdx.y*blockDim.x+threadIdx.x)


#define RAW_PTR(p) (thrust::raw_pointer_cast(&(p)[0]))
#endif

#define IDX2D(x,y,Nx)      ((y)*(Nx)+(x))
#define IDX3D(x,y,z,Nx,Ny) ((z)*(Nx)*(Ny)+(y)*(Nx)+(x))

namespace gpulab
{
	/**
	* 2D indexing
	*/
	template <typename s1, typename s2, typename s3>
	__device__ __host__
	s3 idx(const s1 x, const s2 y, const s3 Nx)
	{
		return y*Nx+x;
	};

	/**
	* 3D indexing
	*/
	template <typename s1, typename s2, typename s3, typename s4, typename s5>
	__device__ __host__
	s4 idx(const s1 x, const s2 y, const s3 z, const s4 Nx, const s5 Ny)
	{
		return z*(Nx*Ny)+y*Nx+x;
	};
}; // namespace gpulab


#endif // GPULAB_CONFIG_H