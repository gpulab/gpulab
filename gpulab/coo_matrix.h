// ==========================================================================
// Sparse Coordinate Matrix (COO)
// ==========================================================================
// (C)opyright: 2011
// License....
//
// DTU Compute - http://compute.dtu.dk
// GPULab - http://gpulab.imm.dtu.dk/
//
// Creator: Nicolai Fog Gade-Nielsen
// Email:   nfga@imm.dtu.dk
// ==========================================================================

#ifndef GPULAB_COO_MATRIX_H
#define GPULAB_COO_MATRIX_H

#include <gpulab/config.h>
#include <gpulab/vector.h>
#include <gpulab/sparse_matrix.h>
#include <gpulab/dispatch/coo_matrix.h>

#include <thrust/gather.h>
#include <thrust/sequence.h>
#include <thrust/sort.h>

namespace gpulab
{

	/**
	* COO matrix class, shared between both device and host matrices.
	*
	* Example of a simple coo matrix representation
	*
	*           [1 7 0 0]
	*      A  = [0 2 8 0]
	*           [5 0 3 9]
	*           [0 6 0 4]
	*
	* i       = [0 0 1 1 2 2 2 3 3]
	* j       = [0 1 1 2 0 2 3 1 3]
	* data    = [1 7 2 8 5 3 9 6 4]
	*
	* @param T value type
	* @param MemorySpace memory space indication
	*/
	template <typename T, typename MemorySpace> 
	class coo_matrix : public gpulab::sparse_matrix<T, MemorySpace>
	{
	public:
		typedef gpulab::coo_matrix<T,MemorySpace>						my_type;
		typedef typename gpulab::sparse_matrix<T, MemorySpace>			super;
		typedef typename super::size_type								size_type;
		typedef typename super::value_type								value_type;
		typedef vector<value_type, MemorySpace>							vector_data_type;
		typedef vector<size_type, MemorySpace>							vector_index_type;

	private:
		/**
		* Hide default constructor
		*/
		coo_matrix() {};

	protected:
		vector_data_type	m_data;		///< Vector holding the nonzero data. Size = nonzeros.
		vector_index_type	m_i;		///< row indices. Size = nonzeros.
		vector_index_type	m_j;		///< row column. Size = nonzeros.
		int 				m_sorted;	///< 0 is no sorting, 1 is rowsorted, 2 is column sorted. TODO: Make enum

	public:
		/**
		* Construct a matrix of size I*I with default value
		*/
		coo_matrix(size_type I, size_type J, vector_index_type const& i, vector_index_type const& j, vector_data_type const& data, int sorted = 0)
			: super(I, J, data.size())
			, m_i(i.size())
			, m_j(j.size())
			, m_data(data.size())
		    , m_sorted(sorted)
		{
			this->m_i.copy(i);
			this->m_j.copy(j);
			this->m_data.copy(data);
		}

		/**
		* Copy constructor
		*/
		coo_matrix(my_type const& m)
			: super(m)
			, m_i(m.m_i)
			, m_j(m.m_j)
			, m_data(m.m_data)
		    , m_sorted(m.m_sorted)
		{ }

		/**
		* Copy constructor from other T
		*/
		template<typename OtherT, typename OtherMemorySpace>
		__host__ __device__
		coo_matrix(const coo_matrix<OtherT, OtherMemorySpace> &m)
		  : super(m)
		  , m_i(m.i())
		  , m_j(m.j())
		  , m_data(m.data())
		  , m_sorted(m.sorted())
		{ }

		/**
		* Deconstructor
		*/
		~coo_matrix()
		{ };

		/**
		* Return the data containing <nonzero> values
		*/
		vector_data_type const& data() const
		{
			return m_data;
		}

		/**
		* Return the row coordinates
		*/
		vector_index_type const& i() const
		{
			return m_i;
		}

		/**
		* Return the column coordinates
		*/
		vector_index_type const& j() const
		{
			return m_j;
		}

		int sorted() const
		{
			return m_sorted;
		}

		template <typename ArrayIndex, typename ArrayValue>
		void set(size_type I, size_type J, ArrayIndex const& i, ArrayIndex const& j, ArrayValue const& data)
		{
			this->m_I = I;
			this->m_J = J;
			this->m_nonzeros = data.size();
			this->m_i.copy(i);
			this->m_j.copy(j);
			this->m_data.copy(data);
		}

		void mult(vector_data_type const& x, vector_data_type& b) const
		{
			gpulab::dispatch::coo_matrix::mult(*this, x, b, MemorySpace());
		}

		void multT(vector_data_type const& x, vector_data_type& b) const
		{
			gpulab::dispatch::coo_matrix::multT(*this, x, b, MemorySpace());
		}

		void transpose()
		{
			// Swap vectors
			this->m_i.swap(this->m_j);

			// Sorting
			if (m_sorted == 1)
				m_sorted = 2;
			else if (m_sorted == 2)
				m_sorted = 1;
		}

		void sort(int sorted) {
			if (sorted == 1) {
				// Create sequence for map
				vector_index_type permutation(this->nonzeros());
				thrust::sequence(permutation.begin(), permutation.end());

				// Sort by rows
				thrust::stable_sort_by_key(m_i.begin(), m_i.end(), permutation.begin());
				vector_index_type tmpindex(m_j);
				thrust::gather(permutation.begin(), permutation.end(), tmpindex.begin(), m_j.begin());
				vector_data_type tmpdata(m_data);
				thrust::gather(permutation.begin(), permutation.end(), tmpdata.begin(), m_data.begin());

				// Set flag
				m_sorted = 1;
			} else if (sorted == 2)	{
				// Create sequence for map
				vector_index_type permutation(this->nonzeros());
				thrust::sequence(permutation.begin(), permutation.end());

				// Sort by cols
				thrust::stable_sort_by_key(m_j.begin(), m_j.end(), permutation.begin());
				vector_index_type tmpindex(m_i);
				thrust::gather(permutation.begin(), permutation.end(), tmpindex.begin(), m_i.begin());
				vector_data_type tmpdata(m_data);
				thrust::gather(permutation.begin(), permutation.end(), tmpdata.begin(), m_data.begin());

				// Set flag
				m_sorted = 2;
			} else
				m_sorted = 0;
		}

		/**
		* Private helpers
		*/
	private:

	};
} // namespace gpulab


#endif // GPULAB_COO_MATRIX_H
