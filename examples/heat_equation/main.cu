////////////////////////////////////////////////////////////////////////////////
// HEAT EQUATION TEST
////////////////////////////////////////////////////////////////////////////////

#include <gpulab.h>
#include <gpulab/grid.h>
#include <gpulab/FD/stencil.h>
#include <gpulab/integration/ode_solver.h>
#include <gpulab/integration/forward_euler.h>
#include <gpulab/io/print.h>

#include "laplacian.cu"

#define PI 3.141592653589793

// Define and assmble heat solver components here
typedef float                                           value_type;
typedef laplacian<value_type>                           rhs_type;
typedef gpulab::grid<value_type,gpulab::device_memory>  vector_type;
typedef gpulab::grid<value_type,gpulab::host_memory>    host_vector_type;
typedef vector_type::property_type                      property_type;
typedef gpulab::integration::forward_euler              time_integrator_type;

int main(int argc, char *argv[])
{
  // Initialize gpulab
  if(gpulab::init(argc,argv))
  {
    GPULAB_LOG_INF("=== BEGIN HEAT EQUATION SOLVER ===\n");

    // Get grid dimension from user unput
    int alpha       = (argc > 1) ? atoi(argv[1]) : 1;
    int N           = (argc > 2) ? atoi(argv[2]) : 16;
    value_type tend = (argc > 3) ? atof(argv[3]) : 1.;

    // Setup grids
    gpulab::grid_dim<int>         dim(N,N,1);       // Discrete dimension
    gpulab::grid_dim<value_type>  p0(0,0);          // Physical dimension start values
    gpulab::grid_dim<value_type>  p1(1,1);          // Physical dimension end values
    property_type                 props(dim,p0,p1); // Gather in property class
    host_vector_type              u0(props);        // Initialize host vector u0 for initial values
    host_vector_type              ut(props);        // Initialize host vector ut for exact solution

    value_type dx = min(u0.properties().delta.x,u0.properties().delta.y);
    value_type dt = 1./(alpha*alpha)*dx*dx;         // Time step. For explicit Euler and alpha=1, dt <= 0.5*dx^2
    GPULAB_LOG_INF("Using dt=%e\n",dt);

    // Initialize u_h
    for(int j=0; j<u0.Ny(); ++j)
    {
      for(int i=0; i<u0.Nx(); ++i)
      {
        value_type x = i/(value_type)(u0.Nx()-1);
        value_type y = j/(value_type)(u0.Ny()-1);
        u0[j*u0.Nx()+i] = sin(PI*x)*sin(PI*y);
        ut[j*ut.Nx()+i] = exp(-2.0*PI*PI*tend)*sin(PI*x)*sin(PI*y);
      }
    }

    vector_type u(u0);              // Initialize device vector u from host vector u0
    time_integrator_type solver;    // Create time integrator
    rhs_type rhs(alpha);            // Create right hand side operator
    solver(&rhs,u,0.0f,tend,dt);    // Integrate t=0 to tend using dt

    gpulab::io::print(ut,gpulab::io::TO_BINARY_FILE,1,"ut.bin");  // Print true solution

    // Compute and print error
    ut.axpy(-1.0,u);
    value_type enrm = ut.nrmi();
    GPULAB_LOG_INF("||e||_inf = %e\n", enrm);
    gpulab::io::print(ut,gpulab::io::TO_BINARY_FILE,1,"e.bin");	// Print error
    gpulab::io::print(u0,gpulab::io::TO_BINARY_FILE,1,"u0.bin");// Print initial state
    gpulab::io::print(u, gpulab::io::TO_BINARY_FILE,1,"u.bin");	// Print final state
  }
  gpulab::finalize();
  return 0;
}
