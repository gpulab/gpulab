template <typename T>
__global__ void set_dirichlet_bc(T* u, int Nx)
{
	int i = blockDim.x*blockIdx.x+threadIdx.x;
	if(i<Nx)
	{
		u[i]           = 0.0;
		u[(Nx-1)*Nx+i] = 0.0;
		u[i*Nx]        = 0.0;
		u[i*Nx+Nx-1]   = 0.0;
	}
};

template <typename T>
struct laplacian
{
	gpulab::FD::stencil_2d<T>	m_stencil;

	laplacian(int alpha) : m_stencil(2,alpha)
	{}

	template <typename V>
	void operator()(T t, V const& u, V & rhs, T dt) const
	{
		m_stencil.mult(u,rhs); // rhs = du/dxx + du/dyy

		// Make sure bc is set
		dim3 block = BLOCK1D(rhs.Nx());
		dim3 grid  = GRID1D(rhs.Nx());
		set_dirichlet_bc<<<grid,block>>>(RAW_PTR(rhs),rhs.Nx());
	}
};
