function [u u0 u_true] = heat_equation()
%% Heat Equation example for GPU book
%% Clean up
%clear all;
%close all;
%clc;

%% Init data
global Nx; 
global Ny; 
tend = 0.10;
print = 1;

Nx = 20;
Ny = 20;
Lx = 1;
Ly = 1;
h = [];
e = [];

for i=1:1
    %% Setup init data
    x = linspace(0,Lx,Nx);
    y = linspace(0,Ly,Ny);
    [X Y] = meshgrid(x,y);

    dx = 1/(Nx-1);
    dt = 0.2*dx^2;
    
    u0 = sin(pi.*X).*sin(pi.*Y);
    %[T U] = ode45(@rhs,[0 tend],u0(:));
    [T U] = forward_euler(@rhs,0:dt:tend,u0(:));

    if(i==1)
        for k=1:length(T)
            u = reshape(U(k,:),Ny,Nx);
            surf(x,y,u);
            xlabel('x'); ylabel('y'); zlabel('u');
            colormap hot;
            axis([x(1) x(end) y(1) y(end) 0 max(u0(:))]);
            drawnow;
            %pause(0.1);
            if (print && (k==1 || k==floor(length(T)/2) || k==length(T)))
                print2tikzpdf(['../../../figures/HeatSolution' num2str(T(k))]); 
            end
        end
    end
    u = reshape(U(end,:),Ny,Nx);
    surf(x,y,u);
    colormap hot;
    axis([x(1) x(end) y(1) y(end) 0 max(u0(:))]);
    drawnow;
    
    u_true = exp(-2*pi^2*T(end)).*sin(pi.*X).*sin(pi.*Y);

%     % 1d
%     [T U] = ode45(@rhs1d,[0 tend],sin(pi.*x));
%     u = U(end,:);
%     u_true = exp(-pi^2*tend).*sin(pi.*x);

    diff = u_true-u;
    e = [e norm(diff(:),inf)];
    h = [h dx];

    disp(['Done ' num2str(Nx) 'x' num2str(Ny) '. Err = ' num2str(e(end))]);
    Nx = Nx*2;
    Ny = Ny*2;
    %dt = dt*0.1;
end

figure;
loglog(h,e,'o-',h,h.^1,'--',h,h.^2,'--');

end

function uxxyy = rhs(t,u)
global Nx;
global Ny;
dx = 1/(Nx-1);
dy = 1/(Ny-1);
u = reshape(u,Ny,Nx);
d = ones(Nx*Ny,1);
A1 = 1/dx^2*spdiags([d -2*d d],[-Ny,0,Ny],Nx*Ny,Nx*Ny);
A2 = 1/dy^2*spdiags([d -2*d d],[ -1,0, 1],Nx*Ny,Nx*Ny);
B = A1+A2;
ibc = [1:Ny 1:Ny:Ny*Nx Ny:Ny:Ny*Nx Nx*Ny-Ny:Nx*Ny];
I = speye(Nx*Ny);
B(ibc,:) = I(ibc,:);
uxxyy = B*u(:);
eigs(B,1);

% Alternative to B
%i=2:Ny-1;
%j=2:Nx-1;
%uxxyy = u*0;
%uxxyy(i,j) = (u(i-1,j)-2*u(i,j)+u(i+1,j))/dy^2 + (u(i,j-1)-2*u(i,j)+u(i,j+1))/dx^2;
%uxxyy = uxxyy(:);
end

function uxx = rhs1d(t,u)
Nx = length(u);
i=2:Nx-1;
dx = 1/(Nx-1);
uxx = u*0;
uxx(i) = (u(i-1)-2*u(i)+u(i+1))/dx^2;
end

function [T Y] = forward_euler(fun,tspan,y0)

y0 = y0(:);
Y = zeros(length(tspan),length(y0));
Y(1,:) = y0;
for i=2:length(tspan)
    t=tspan(i);
    dt = t-tspan(i-1);
    Y(i,:) = Y(i-1,:) + dt.*feval(fun,t,Y(i-1,:))';
end
T = tspan;
end